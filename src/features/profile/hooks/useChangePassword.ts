import { changePass } from "@/shared/api/mutation/changePass";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { handlingErrors } from "@/shared/utils/handlingErrors";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";
import { useTranslation } from "next-i18next";

const useChangePassword = () => {
  const queryClient = useQueryClient();
  const {t: translate} = useTranslation();

  const mutationQuery = useMutationHook({
    api: changePass,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {
        handlingErrors({
          error: error,
          text: "Change Password",
        });
      },

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        if (data) {
          notification.success({
            message: translate("PasswordSuccess"),
          });
        }
      },
    },
  });

  const postChangePassword = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    postChangePassword,
  };
};

export default useChangePassword;
