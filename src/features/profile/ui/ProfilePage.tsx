import FormButton from "@/components/FormButton";
import FormInputFormik from "@/components/FormInputFormik";
import { DASHBOARD_UPDATE_PROFILE } from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { CopyOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Card, Col, Flex, Modal, Row, Space } from "antd";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import useChangePassword from "../hooks/useChangePassword";
import * as yup from "yup";
import YupPassword from "yup-password";
import useGetCountry from "@/features/updateprofile/hooks/useGetCountry";
import { imageLMS } from "@/shared/constants/imageUrl";
import dayjs from "dayjs";
YupPassword(yup); // extend yup with password method

const ProfilePage = () => {
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const { fetchQuery } = useGetCountry([]);
  const { data: dataCountry } = fetchQuery;
  const { profile: dataProfileInfo } = dataUser;
  const { t: translate } = useTranslation();
  const [isEditModalChangePass, setIsEditModalChangePass] =
    React.useState(false);
  const { postChangePassword, mutationQuery: mutationChangePassword } =
    useChangePassword();
  const {
    isPending: isLoadingChangePassword,
    isSuccess: isSuccessChangePassword,
  } = mutationChangePassword;
  const countryName = dataCountry?.find(
    (item: any) => item.key === dataProfileInfo?.country
  );

  React.useEffect(() => {
    if (!dataUser.isLogin) {
      navigate.back();
    }
  }, [dataUser]);

  React.useEffect(() => {
    if (isSuccessChangePassword) {
      setIsEditModalChangePass(false);
    }
  }, [isSuccessChangePassword]);
  const handleChangePassCancel = () => {
    setIsEditModalChangePass(false);
  };
  const handleChangePassSubmit = (values: {}) => {
    postChangePassword(values);
  };

  const changePassValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    old_password: yup
      .string()
      .min(
        8,
        ({ min }) =>
          `${translate("PasswordLength")} ${min} ${translate("Characters")}`
      )
      .required(translate("RequirePassword")),
    new_password: yup
      .string()
      .min(
        8,
        ({ min }) =>
          `${translate("PasswordLength")} ${min} ${translate("Characters")}`
      )
      .required(translate("RequirePassword"))
      .minLowercase(1, translate("Lower_Password"))
      .minUppercase(1, translate("Upper_Password"))
      .minNumbers(1, translate("Number_Password"))
      .minWords(1, translate("Word_Password"))
      .minSymbols(1, translate("Symbol_Password")),
    new_password_confirmation: yup
      .string()
      .oneOf([yup.ref("new_password")], translate("Match_Password"))
      .required(translate("Confirm_New_Password")),
  });

  return (
    <Space
      direction="vertical"
      style={{
        display: "flex",
        padding: "170px 72px 180px 72px",
        background: "white",
      }}
    >
      <Row>
        <Col span={21}>
          <span style={{ fontSize: 24, fontWeight: "bold" }}>
            {translate("MyPersonal")}
          </span>
        </Col>
      </Row>
      <Row gutter={[8, 8]}>
        <Col span={24}>
          <Card>
            <Row style={{ display: "flex", alignItems: "center" }}>
              <Col span={2}>
                {dataProfileInfo?.photo ? (
                  <Image
                    width={100}
                    height={100}
                    style={{ borderRadius: 100, width: 100, height: 100 }}
                    src={
                      dataProfileInfo.photo.includes("https")
                        ? dataProfileInfo.photo
                        : `${imageLMS}/${dataProfileInfo.photo}`
                    }
                    alt={"Icon Profile"}
                  />
                ) : (
                  <UserOutlined style={{ fontSize: 100, color: "#9F0E0F" }} />
                )}
              </Col>
              <Col span={16} style={{ marginLeft: 20 }}>
                <p style={{ fontSize: 24, color: "#9F0E0F" }}>
                  {dataProfileInfo?.name}
                </p>
                <p style={{ fontSize: 17 }}>{dataProfileInfo?.email}</p>
                <p style={{ fontSize: 17 }}>{dataProfileInfo?.role}</p>
              </Col>
              <Col span={2}>
                <Button
                  style={{
                    background: "#9F0E0F",
                    color: "white",
                  }}
                  onClick={() => setIsEditModalChangePass(true)}
                  icon={<CopyOutlined />}
                >
                  {translate("ChangePassword")}
                </Button>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
      <Row gutter={[8, 8]}>
        <Col span={24}>
          <Card>
            <Row gutter={[8, 8]} style={{ marginBottom: 30 }}>
              <Col span={21} style={{ fontSize: 24, fontWeight: "bold" }}>
                {translate("My_Profile")}
              </Col>
              <Col span={3}>
                <Button
                  style={{
                    marginBottom: 8,
                    background: "#9F0E0F",
                    color: "white",
                  }}
                  onClick={() => navigate.push(DASHBOARD_UPDATE_PROFILE)}
                  icon={<CopyOutlined />}
                >
                  {translate("Change")}
                </Button>
              </Col>
            </Row>
            <Row gutter={[8, 8]} style={{ marginBottom: 40 }}>
              <Col span={12}>
                <Space direction="vertical">
                  <span>{translate("LabelFullName")}</span>
                  <span>{dataProfileInfo?.name}</span>
                </Space>
              </Col>
              <Col span={12}>
                <Space direction="vertical">
                  <span>{translate("LabelAddress")}</span>
                  <span>{dataProfileInfo?.address}</span>
                </Space>
              </Col>
            </Row>
            <Row gutter={[8, 8]} style={{ marginBottom: 40 }}>
              <Col span={12}>
                <Space direction="vertical">
                  <span>{translate("LabelCountry")}</span>
                  <span>{countryName?.name}</span>
                </Space>
              </Col>
              <Col span={12}>
                <Space direction="vertical">
                  <span>{translate("LabelDateOfBirth")}</span>
                  <span>{dayjs(dataProfileInfo?.date_of_birth).format("DD MM YYYY")}</span>
                </Space>
              </Col>
            </Row>
            <Row gutter={[8, 8]} style={{ marginBottom: 40 }}>
              <Col span={12}>
                <Space direction="vertical">
                  <span>{translate("LabelGender")}</span>
                  <span>
                    {dataProfileInfo?.gender === "L"
                      ? "Laki-Laki"
                      : "Perempuan"}
                  </span>
                </Space>
              </Col>
              <Col span={12}>
                <Space direction="vertical">
                  <span>{translate("LabelPhone")}</span>
                  <span>{dataProfileInfo?.phone ?? "-"}</span>
                </Space>
              </Col>
            </Row>
            <Row gutter={[8, 8]} style={{ marginBottom: 40 }}>
              <Col span={12}>
                <Space direction="vertical">
                  <span>{translate("LabelIns")}</span>
                  <span>
                    {dataProfileInfo?.institution_id ? "Publik" : "OJK"}
                  </span>
                </Space>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>

      {/* Modal Change Password */}
      <Modal
        centered
        title={translate("FormChangePass")}
        open={isEditModalChangePass}
        width={800}
        onCancel={handleChangePassCancel}
        footer={null}
      >
        <Formik
          validationSchema={changePassValidationSchema}
          initialValues={{
            old_password: "",
            new_password: "",
            new_password_confirmation: "",
          }}
          onSubmit={(values) => {
            handleChangePassSubmit(values);
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterOldPassword")}
                title={translate("LabelOldPassword")}
                type={"password"}
                name={"old_password"}
                styleForm={{ marginTop: 20 }}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterNewPassword")}
                title={translate("LabelNewPassword")}
                type={"password"}
                name={"new_password"}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterNewsConfirmPassword")}
                title={translate("LabelNewsConfirmPassword")}
                type={"password"}
                name={"new_password_confirmation"}
              />
              <Flex justify="flex-end">
                <FormButton
                  isLoading={isLoadingChangePassword}
                  onClick={handleSubmit}
                  disabled={!isValid}
                  title={translate("Save")}
                  type={"submit"}
                  styleButton={{ width: 100 }}
                />
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default ProfilePage;
