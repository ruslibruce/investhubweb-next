import { CloseOutlined, PlusOutlined } from "@ant-design/icons";
import { Button, InputNumber, Select, Slider, Space } from "antd";
import React from "react";

type Props = {
  item: any;
  handleFocus: (value: any) => void;
  inputValueMin: number;
  onChangeInputTextMin: (value: any) => void;
  onChangeSlider: (value: number[]) => void;
  inputValueMax: number;
  onChangeInputTextMax: (value: any) => void;
  handleChange: (value: string) => void;
  handleSubmitChange: (value: any) => void;
  handleClearFilter: (value: any) => void;
};

export default function ListSelectStockScreener({
  item,
  handleFocus,
  inputValueMin,
  onChangeInputTextMin,
  onChangeSlider,
  inputValueMax,
  onChangeInputTextMax,
  handleChange,
  handleSubmitChange,
  handleClearFilter,
}: Readonly<Props>) {
  const [isWidthFocus, setIsWidthFocus] = React.useState<boolean>(false);
  const widthFocus = 500;
  const widthRegular = 200;
  return (
    <>
      <Select
        defaultValue={item.itemName}
        style={{
          width: isWidthFocus ? widthFocus : widthRegular,
          backgroundColor: item.maxModify ? "#9F0E0F" : "#ffffff",
        }}
        className={item.maxModify ? "select-stock-screen-red" : "none"}
        onFocus={() => {
          handleFocus(item);
        }}
        onDropdownVisibleChange={(event) => {
          if (!event) {
            setIsWidthFocus(false);
          } else {
            setIsWidthFocus(true);
          }
        }}
        suffixIcon={
          <div
            style={{
              borderRadius: 43,
              borderColor: "#bebfcf",
              borderWidth: 0.5,
              alignItems: "center",
              justifyContent: "center",
              padding: 1,
              display: "flex",
              backgroundColor: item.maxModify ? "#ffffff" : "none",
            }}
          >
            {item.maxModify ? (
              <CloseOutlined style={{ color: "#9F0E0F" }} />
            ) : (
              <PlusOutlined style={{ color: "#333333" }} />
            )}
          </div>
        }
        dropdownRender={() => (
          <Space style={{ padding: 10 }} direction="horizontal">
            <div>
              <p>Min</p>
              <InputNumber
                max={Number(item.max)}
                min={Number(item.min)}
                value={inputValueMin}
                onChange={onChangeInputTextMin}
                style={{ width: 100 }}
              />
            </div>
            <Slider
              range
              min={Number(item.min)}
              max={Number(item.max)}
              step={0.1}
              defaultValue={[Number(item.min), Number(item.max)]}
              onChange={onChangeSlider}
              style={{ width: 150 }}
            />
            <div>
              <p>Max</p>
              <InputNumber
                max={Number(item.max)}
                min={Number(item.min)}
                value={inputValueMax}
                onChange={onChangeInputTextMax}
                style={{ width: 100 }}
              />
            </div>
            <Space style={{ flexDirection: "column" }}>
              <Button
                onClick={() => handleSubmitChange(item)}
                style={{ width: 80 }}
              >
                Atur
              </Button>
              {item.maxModify && (
                <Button
                  onClick={() => handleClearFilter(item)}
                  style={{ width: 80 }}
                >
                  Hapus
                </Button>
              )}
            </Space>
          </Space>
        )}
        onChange={handleChange}
      />
      <style jsx>{`
        .ant-select-selection-placeholder {
          color: #f0f0f0;
        }
      `}</style>
    </>
  );
}
