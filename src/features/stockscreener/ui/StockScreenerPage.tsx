import {
  DASHBOARD_STOCK_SCREENER_COLOUMN,
  DASHBOARD_STOCK_SCREENER_DETAIL,
} from "@/shared/constants/path";
import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import {
  DeleteFilled,
  DownloadOutlined,
  SearchOutlined
} from "@ant-design/icons";
import type { TableProps } from "antd";
import {
  Breadcrumb,
  Button,
  Input,
  Select,
  Space,
  Table
} from "antd";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import useGetCategoryMinMax from "../hooks/useGetCategoryMinMax";
import useGetCategorySector from "../hooks/useGetCategorySector";
import useGetCategorySubSector from "../hooks/useGetCategorySubSector";
import useGetCategorySubSectorParams from "../hooks/useGetCategorySubSectorParams";
import useGetStockScreener from "../hooks/useGetStockScreener";
import useGetStockScreenerParams from "../hooks/useGetStockScreenerParams";
import ListSelectStockScreener from "./ListSelectStockScreener";

type DataType = {
  key: string;
  number: number;
  name: string;
  code: string;
  sektor: string;
  subsektor: string;
  industri: string;
  subindustri: string;
  index: string;
  per: string;
  pbv: string;
  roe: string;
  roa: string;
  der: string;
  mktcap: string;
};

const columns: TableProps<DataType>["columns"] = [
  {
    title: "No",
    dataIndex: "number",
    key: "number",
    width: 25,
    fixed: "left",
  },
  {
    title: "Company Name",
    dataIndex: "name",
    key: "name",
    render: (text, record) => (
      <Link
        style={{ color: "#9F0E0F" }}
        href={`${DASHBOARD_STOCK_SCREENER_DETAIL}/${record.code}`}
      >
        {text}
      </Link>
    ),
    sorter: (a, b) => a.name.length - b.name.length,
    width: 130,
    fixed: "left",
  },
  {
    title: "Kode Saham",
    dataIndex: "code",
    key: "code",
    sorter: (a, b) => a.code.length - b.code.length,
    width: 50,
  },
  {
    title: "Sektor",
    dataIndex: "sektor",
    key: "sektor",
    sorter: (a, b) => a.sektor.length - b.sektor.length,
    width: 80,
  },
  {
    title: "Subsektor",
    dataIndex: "subsektor",
    key: "subsektor",
    sorter: (a, b) => a.subsektor.length - b.subsektor.length,
    width: 80,
  },
  {
    title: "Industri",
    dataIndex: "industri",
    key: "industri",
    sorter: (a, b) => a.industri.length - b.industri.length,
    width: 80,
  },
  {
    title: "Subindustri",
    dataIndex: "subindustri",
    key: "subindustri",
    sorter: (a, b) => a.subindustri.length - b.subindustri.length,
    width: 80,
  },
  {
    title: "Index",
    dataIndex: "index",
    key: "index",
    sorter: (a, b) => a.index.length - b.index.length,
    width: 150,
  },
  {
    title: "PER",
    dataIndex: "per",
    key: "per",
    sorter: (a, b) => a.per.length - b.per.length,
    width: 50,
  },
  {
    title: "PBV",
    dataIndex: "pbv",
    key: "pbv",
    sorter: (a, b) => a.pbv.length - b.pbv.length,
    width: 50,
  },
  {
    title: "ROE %",
    dataIndex: "roe",
    key: "roe",
    sorter: (a, b) => a.roe.length - b.roe.length,
    width: 50,
  },
  {
    title: "ROA %",
    dataIndex: "roa",
    key: "roa",
    sorter: (a, b) => a.roa.length - b.roa.length,
    width: 50,
  },
  {
    title: "DER",
    dataIndex: "der",
    key: "der",
    sorter: (a, b) => a.der.length - b.der.length,
    width: 50,
  },
  {
    title: "Mkt Cap",
    dataIndex: "mktcap",
    key: "mktcap",
    sorter: (a, b) => a.mktcap.length - b.mktcap.length,
    width: 80,
  },
];

const dataButtonMaxMin = [
  {
    id: 1,
    itemName: "Total Rev (Rp.M)",
    max: "",
    min: "",
    name: "tRevenue",
    maxModify: "",
    minModify: "",
  },
  {
    id: 2,
    itemName: "Net Profit Margin (NPM) (%)",
    max: "",
    min: "",
    name: "npm",
    maxModify: "",
    minModify: "",
  },
  {
    id: 3,
    itemName: "PER (times)",
    max: "",
    min: "",
    name: "per",
    maxModify: "",
    minModify: "",
  },
  {
    id: 4,
    itemName: "PBV (times)",
    max: "",
    min: "",
    name: "pbv",
    maxModify: "",
    minModify: "",
  },
  {
    id: 5,
    itemName: "ROA %",
    max: "",
    min: "",
    name: "roa",
    maxModify: "",
    minModify: "",
  },
  {
    id: 6,
    itemName: "ROE %",
    max: "",
    min: "",
    name: "roe",
    maxModify: "",
    minModify: "",
  },
  {
    id: 7,
    itemName: "Debit Equity Ratio (DER)",
    max: "",
    min: "",
    name: "der",
    maxModify: "",
    minModify: "",
  },
  {
    id: 8,
    itemName: "4-wk %Pr. Chg.",
    max: "",
    min: "",
    name: "week4",
    maxModify: "",
    minModify: "",
  },
  {
    id: 9,
    itemName: "13-wk %Pr. Chg.",
    max: "",
    min: "",
    name: "week13",
    maxModify: "",
    minModify: "",
  },
  {
    id: 10,
    itemName: "26-wk %Pr. Chg.",
    max: "",
    min: "",
    name: "week26",
    maxModify: "",
    minModify: "",
  },
  {
    id: 11,
    itemName: "52-wk %Pr. Chg.",
    max: "",
    min: "",
    name: "week52",
    maxModify: "",
    minModify: "",
  },
  {
    id: 12,
    itemName: "Price MonthtoDate (MTD)",
    max: "",
    min: "",
    name: "mtd",
    maxModify: "",
    minModify: "",
  },
  {
    id: 13,
    itemName: "Price YeartoDate (YTD)",
    max: "",
    min: "",
    name: "ytd",
    maxModify: "",
    minModify: "",
  },
  {
    id: 14,
    itemName: "Market Cap",
    max: "",
    min: "",
    name: "mCap",
    maxModify: "",
    minModify: "",
  },
];

export default function StockScreenerPage() {
  const navigate = useRouter();
  const { fetchQuery } = useGetStockScreener();
  const { isLoading, data } = fetchQuery;
  const { fetchQuery: fetchQuerySector } = useGetCategorySector();
  const { isLoading: isLoadingSector, data: dataSector } = fetchQuerySector;
  const { fetchQuery: fetchQuerySubector } = useGetCategorySubSector();
  const { isLoading: isLoadingSubSector, data: dataSubSector } =
    fetchQuerySubector;
  const { mutationQuery, handleGetStockScreenerSubSectorWithParams } =
    useGetCategorySubSectorParams();
  const { data: dataSubSectorWithParams, isPending: isLoadingGetSubSector } =
    mutationQuery;
  const { fetchQuery: fetchQueryFilterMinMax } = useGetCategoryMinMax();
  const { isLoading: isLoadingFilterMinMax, data: dataMinMax } =
    fetchQueryFilterMinMax;
  const {
    mutationQuery: mutationQueryStock,
    handleGetStockScreenerWithParams,
  } = useGetStockScreenerParams();

  const { data: dataStockScreenerParams } = mutationQueryStock;

  const [dataItemCategorySektor, setDataItemCategorySektor] =
    React.useState<any>([]);
  const [dataItemCategorySubSektor, setDataItemCategorySubSektor] =
    React.useState<any>([]);
  const [dataStockScreener, setDataStockScreener] = React.useState(data);
  const [category, setCategory] = React.useState<any>({
    Sector: "",
    SubSector: "",
  });
  const [dataFilterMinMax, setDataFilterMinMax] =
    React.useState<any>(dataButtonMaxMin);
  const [inputValueMin, setInputValueMin] = React.useState<number>(0);
  const [inputValueMax, setInputValueMax] = React.useState<number>(0);

  React.useEffect(() => {
    if (dataMinMax) {
      let dataTemp = [] as any;
      dataFilterMinMax.map((item: any) => {
        item.max = dataMinMax[item.name + "Max"];
        item.min = dataMinMax[item.name + "Min"];
        dataTemp.push(item);
      });
      setDataFilterMinMax(dataTemp);
    }
  }, [dataMinMax]);

  React.useEffect(() => {
    let dataTemp: DataType[] = [];
    if (data) {
      data.map((item: any, index: any) => {
        let dataFilter = {
          key: (index + 1).toString(),
          number: (index + 1).toString(),
          name: item.companyName,
          code: item.stockCode,
          sektor: item.sector,
          subsektor: item.subSector,
          industri: item.industry,
          subindustri: item.subIndustry,
          index: item.indexCode,
          per: item.per,
          pbv: item.pbv,
          roe: item.roe,
          roa: item.roa,
          der: item.der,
          mktcap: item.marketCapital,
        };
        dataTemp.push(dataFilter);
      });
      setDataStockScreener(dataTemp);
    }
  }, [data]);

  React.useEffect(() => {
    let dataTemp: DataType[] = [];
    if (dataStockScreenerParams) {
      let dataParams = dataStockScreenerParams as any;
      dataParams.map((item: any, index: any) => {
        let dataFilter = {
          key: (index + 1).toString(),
          number: (index + 1).toString(),
          name: item.companyName,
          code: item.stockCode,
          sektor: item.sector,
          subsektor: item.subSector,
          industri: item.industry,
          subindustri: item.subIndustry,
          index: item.indexCode,
          per: item.per,
          pbv: item.pbv,
          roe: item.roe,
          roa: item.roa,
          der: item.der,
          mktcap: item.marketCapital,
        };
        dataTemp.push(dataFilter);
      });
      setDataStockScreener(dataTemp);
    }
  }, [dataStockScreenerParams]);

  React.useEffect(() => {
    if (dataSector) {
      let dataTempSector = [] as any;
      dataSector.map((item: any) => {
        let data = {
          value: item,
          label: item,
        };
        dataTempSector.push(data);
      });
      setDataItemCategorySektor(dataTempSector);
    }
  }, [dataSector]);

  React.useEffect(() => {
    if (dataSubSector) {
      let dataTempSubSector = [] as any;
      dataSubSector.map((item: any) => {
        let data = {
          value: item,
          label: item,
        };
        dataTempSubSector.push(data);
      });
      setDataItemCategorySubSektor(dataTempSubSector);
    }
  }, [dataSubSector]);

  React.useEffect(() => {
    if (dataSubSectorWithParams) {
      let dataTempSubSector = [] as any;
      let dataSub = dataSubSectorWithParams as any;
      dataSub.map((item: any) => {
        let data = {
          value: item,
          label: item,
        };
        dataTempSubSector.push(data);
      });
      setDataItemCategorySubSektor(dataTempSubSector);
    }
  }, [dataSubSectorWithParams]);

  const handleChange = (value: string) => {
    console.log(`selected ${value}`);
  };

  const handleChangeDropdown = (event: string, params: string) => {
    if (params === "Sector") {
      handleGetStockScreenerSubSectorWithParams(event);
      setCategory(() => ({
        ...category,
        [params]: event,
        SubSector: "",
      }));
      return;
    }
    setCategory(() => ({
      ...category,
      [params]: event,
    }));
  };

  const onChangeSlider = (value: number[]) => {
    console.log("onChange: ", value);
    setInputValueMin(value[0]);
    setInputValueMax(value[1]);
  };

  const onChangeInputTextMax = (value: any) => {
    if (isNaN(value)) {
      return;
    }
    setInputValueMax(value);
  };

  const onChangeInputTextMin = (value: any) => {
    if (isNaN(value)) {
      return;
    }
    setInputValueMin(value);
  };

  const handleFocus = (value: { max: any; min: any }) => {
    setInputValueMax(Number(value.max));
    setInputValueMin(Number(value.min));
  };

  const handleSubmitChange = (item: any) => {
    item.maxModify = inputValueMax;
    item.minModify = inputValueMin;
    let categoryTemp = {
      [item.name + "Min"]: item.minModify,
      [item.name + "Max"]: item.maxModify,
    };
    setCategory({
      ...category,
      ...categoryTemp,
    });
    setDataFilterMinMax([...dataFilterMinMax, item]);
  };

  const handleClearFilterMaxMin = (item: any) => {
    item.maxModify = "";
    item.minModify = "";
    delete category[item.name + "Min"];
    delete category[item.name + "Max"];
    setDataFilterMinMax([...dataFilterMinMax, item]);
  };

  const handleClearFilter = () => {
    let dataFilter = dataFilterMinMax.map((item: any) => {
      item.maxModify = "";
      item.minModify = "";
      return item;
    });
    setDataFilterMinMax(dataFilter);
    let temp = {
      Sector: "",
      SubSector: "",
    };
    setCategory({
      ...temp,
    });
  };

  const handleGetParamsFilter = () => {
    handleGetStockScreenerWithParams(category);
  };

  return (
    <Space
      direction="vertical"
      className="hero-container-background"
      size="middle"
      style={{ display: "flex", marginTop: 35, marginBottom: 72 }}
    >
      <div className="stock-screener-header">
        <div className="stock-screener-breadcum">
          <Breadcrumb
            separator=">"
            items={[
              {
                href: "",
                title: (
                  <>
                    <Image
                      src={iconBreadcrumb}
                      alt="Breadcrumb Icon"
                    />
                  </>
                ),
              },
              {
                title: (
                  <span className="textfs16-fw400-red">Stock Screener</span>
                ),
              },
            ]}
          />
        </div>

        <div className="stock-screener-header-1">
          <div className="stock-screener-title">
            <h2>Stock Screener</h2>
          </div>
          <div className="stock-screener-company-list">
            <div className="stock-screener-company-list-title">
              <h2>Company List</h2>
            </div>
            <div className="stock-screener-header-1-input">
              <div className="stock-screener-search">
                <Input
                  size="large"
                  prefix={<SearchOutlined style={{ marginRight: 8 }} />}
                  placeholder="Search Stock Screener"
                />
              </div>
              <div className="stock-screener-export">
                <Button icon={<DownloadOutlined />} size={"middle"}>
                  Export
                </Button>
              </div>
            </div>
          </div>
        </div>
        <div className="stock-screener-header-2">
          <Select
            value={category.Sector ? category.Sector : "Sektor"}
            size="large"
            className="container-button-stock"
            onChange={(event) => handleChangeDropdown(event, "Sector")}
            options={dataItemCategorySektor}
          />

          <Select
            value={category.SubSector ? category.SubSector : "Subsektor"}
            size="large"
            className="container-button-stock"
            onChange={(event) => handleChangeDropdown(event, "SubSector")}
            options={dataItemCategorySubSektor}
          />
          <div className="button-trash-stock-screneer">
            <Button
              onClick={handleClearFilter}
              icon={<DeleteFilled style={{ color: "#9F0E0F"}} />}
              size={"middle"}
            >
              Clear Filter
            </Button>
          </div>
        </div>
      </div>
      <div className="stock-screener-header-3">
        <Space wrap>
          {dataButtonMaxMin.map((item) => (
            <ListSelectStockScreener
              key={item.id}
              item={item}
              handleFocus={handleFocus}
              inputValueMin={inputValueMin}
              onChangeInputTextMin={onChangeInputTextMin}
              onChangeSlider={onChangeSlider}
              inputValueMax={inputValueMax}
              onChangeInputTextMax={onChangeInputTextMax}
              handleChange={handleChange}
              handleSubmitChange={handleSubmitChange}
              handleClearFilter={handleClearFilterMaxMin}
            />
          ))}
        </Space>
        <div className="button-stock-screener-header-3">
          <Button
            onClick={handleGetParamsFilter}
            type="primary"
            style={{ marginTop: 32 }}
          >
            Terapkan
          </Button>
        </div>
      </div>

      <div className="table-stock-screener">
        <Table
          loading={isLoading}
          size="small"
          columns={columns}
          dataSource={dataStockScreener}
          scroll={{ x: 1800 }}
        />
      </div>
      <div className="footer-stock-screener">
        <span className="textfs14-fw400-gray">
          An explanation of the stock screener column can be seen
        </span>
        <Space
          onClick={() => navigate.push(DASHBOARD_STOCK_SCREENER_COLOUMN)}
          style={{
            color: "#9F0E0F",
            border: "1px solid var(--Primary, #9F0E0F)",
            padding: "0px 8px",
            borderRadius: 4,
            width: "max-content",
          }}
        >
          <span className="textfs14-fw400-red">Coloumn Explanation</span>
        </Space>
      </div>
    </Space>
  );
}
