import { fetchStockScreenerParams } from "@/shared/api/fetch/stockScreener";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { paramsToString } from "@/shared/utils/string";
import { useQueryClient } from "@tanstack/react-query";

const useGetStockScreenerParams = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: fetchStockScreenerParams,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        queryClient.invalidateQueries({ queryKey: ["stock_screener"] });
      },
    },
  });

  const handleGetStockScreenerWithParams = (object: {}) => {
    mutationQuery.mutate(paramsToString(object));
  };

  return {
    mutationQuery,
    handleGetStockScreenerWithParams,
  };
};

export default useGetStockScreenerParams;
