import { fetchStockScreener } from "@/shared/api/fetch/stockScreener";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchStockScreener();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetStockScreener = (initialData?: any) => {
  const fetchDataStockScreener = fetchStockScreener();

  const fetchQuery = useFetchHook({
    keys: fetchDataStockScreener.key,
    api: fetchDataStockScreener.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetStockScreener;
