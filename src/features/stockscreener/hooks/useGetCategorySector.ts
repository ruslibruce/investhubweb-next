import { fetchStockScreenerCategorySector } from "@/shared/api/fetch/stockScreener";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchStockScreenerCategorySector();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetCategorySector = (initialData?: any) => {
  const fetchDataCategorySector = fetchStockScreenerCategorySector();

  const fetchQuery = useFetchHook({
    keys: fetchDataCategorySector.key,
    api: fetchDataCategorySector.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetCategorySector;
