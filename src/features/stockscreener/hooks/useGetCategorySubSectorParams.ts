import { fetchStockScreenerCategorySubSectorParams } from "@/shared/api/fetch/stockScreener";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { paramsToString } from "@/shared/utils/string";
import { useQueryClient } from "@tanstack/react-query";

const useGetCategorySubSectorParams = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: fetchStockScreenerCategorySubSectorParams,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        queryClient.invalidateQueries({ queryKey: ["category_sub_sector"] });
      },
    },
  });

  const handleGetStockScreenerSubSectorWithParams = (value: string) => {
    mutationQuery.mutate(
      paramsToString({
        sector: value,
      })
    );
  };

  return {
    mutationQuery,
    handleGetStockScreenerSubSectorWithParams,
  };
};

export default useGetCategorySubSectorParams;
