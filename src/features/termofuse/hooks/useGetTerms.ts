import { fetchTerms } from "@/shared/api/fetch/terms";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchTerms();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetTerms = (initialData?: any) => {
  const fetchDataPrivacy = fetchTerms();

  const fetchQuery = useFetchHook({
    keys: fetchDataPrivacy.key,
    api: fetchDataPrivacy.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetTerms;
