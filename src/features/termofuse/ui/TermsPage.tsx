import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import { Breadcrumb } from "antd";
import Image from "next/image";
import useGetTerms from "../hooks/useGetTerms";
import { useTranslation } from "next-i18next";

const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

export default function TermsPage() {
  const { t: translate } = useTranslation();
  const { fetchQuery } = useGetTerms({});
  const { isLoading, data: dataPrivacy } = fetchQuery;

  const handleChange = (value: string) => {
    console.log(`selected ${value}`);
  };

  return (
    <div className="privacy">
      <div className="privacy-breadcrumb">
        <Breadcrumb
          separator=">"
          items={[
            {
              href: "",
              title: <Image src={iconBreadcrumb} alt="Breadcrumb Icon" />,
            },
            {
              title: (
                <span className="textfs16-fw400-red">{translate("Terms")}</span>
              ),
            },
          ]}
        />
      </div>
      <div className="privacy-content">
        <h2 className="privacy-title">{dataPrivacy?.title}</h2>
        <div className="privacy-content-text">{dataPrivacy?.body}</div>
      </div>
    </div>
  );
}
