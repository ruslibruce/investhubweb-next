import { getQuizLevelup } from "@/shared/api/mutation/levelupQuiz";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";

const useGetQuizLevelup = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: getQuizLevelup,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        queryClient.invalidateQueries({ queryKey: ["level_up"] });
      },
    },
  });

  const handleGetQuiz = (id: string) => {
    mutationQuery.mutate(id);
  };

  return {
    mutationQuery,
    handleGetQuiz,
  };
};

export default useGetQuizLevelup;
