import { postRetakeQuizLevelup } from "@/shared/api/mutation/levelupQuiz";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";

const usePostRetakeQuizLevelup = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postRetakeQuizLevelup,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
      },
    },
  });

  const retakeDataQuiz = (test_attempt_id: string) => {
    mutationQuery.mutate({
      test_attempt_id,
    });
  };

  return {
    mutationQuery,
    retakeDataQuiz,
  };
};

export default usePostRetakeQuizLevelup;
