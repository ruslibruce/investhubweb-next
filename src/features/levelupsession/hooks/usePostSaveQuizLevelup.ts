import { postSaveQuizLevelup } from "@/shared/api/mutation/levelupQuiz";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";

const usePostSaveQuizLevelup = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postSaveQuizLevelup,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
      },
    },
  });

  const saveDataQuiz = (
    test_attempt_id: string,
    answers: {},
    remaining_time: number
  ) => {
    mutationQuery.mutate({
      test_attempt_id,
      answers,
      remaining_time,
    });
  };

  return {
    mutationQuery,
    saveDataQuiz,
  };
};

export default usePostSaveQuizLevelup;
