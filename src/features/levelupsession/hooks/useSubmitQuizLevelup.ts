import { postQuizLevelupSubmit } from "@/shared/api/mutation/levelupQuiz";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";

const useSubmitLevelupQuiz = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postQuizLevelupSubmit,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["notification"]] });
          queryClient.invalidateQueries({ queryKey: ["quiz"] });
        }
      },
    },
  });

  const handleSubmitQuiz = (value: { value: string }, answers: {}) => {
    mutationQuery.mutate({
      test_attempt_id: value,
      answers,
    });
  };

  return {
    mutationQuery,
    handleSubmitQuiz,
  };
};

export default useSubmitLevelupQuiz;
