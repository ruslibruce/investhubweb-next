import FormButton from "@/components/FormButton";
import iconCupCard from "@/shared/images/icon/icon_cup_card.webp";
import iconNote from "@/shared/images/icon/icon_note.webp";
import {
  arrayContainsEmptyString,
  getStatusColor,
  sleep,
} from "@/shared/utils/helper";
import MyTimer from "@/shared/widgets/MyTimer";
import type { RadioChangeEvent } from "antd";
import { Button, Card, Col, Flex, Radio, Row, Space, Spin, Steps } from "antd";
import { useTranslation } from "next-i18next";
import React from "react";
import useGetQuizLevelup from "../hooks/useGetQuizLevelup";
import useSubmitLevelupQuiz from "../hooks/useSubmitQuizLevelup";
import ScoreComponent from "./ScoreComponent";
import { useRouter } from "next/router";
import usePostRetakeQuizLevelup from "../hooks/usePostRetakeQuizLevelup";
import ModalComponent from "@/shared/components/ModalComponent";
import LoaderSpinGif from "@/components/LoaderSpinGif";
import NoData from "@/components/NoData";
import {
  ArrowRightOutlined,
  CheckCircleFilled,
  CloseCircleFilled,
} from "@ant-design/icons";
import IconCirle from "@/shared/images/svg/icon_circle.svg";
import Image from "next/image";

export default function LevelUpPage() {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const { slug } = navigate.query;
  const id = slug?.[0];
  const name = slug?.[1];

  const dataSection = [
    {
      id: 1,
      section: `${translate("Level")} 1`,
      isFinish: false,
      isStarted: true,
    },
    {
      id: 2,
      section: `${translate("Level")} 2`,
      isFinish: false,
      isStarted: false,
    },
    {
      id: 3,
      section: `${translate("Level")} 3`,
      isFinish: false,
      isStarted: false,
    },
  ];

  const { mutationQuery, handleGetQuiz } = useGetQuizLevelup();
  const {
    data: dataQuiz,
    isPending: isLoadingQuiz,
    isSuccess: isSuccessQuiz,
    mutate: mutateQuiz,
  } = mutationQuery;
  const { mutationQuery: mutationQueryRetake, retakeDataQuiz } =
    usePostRetakeQuizLevelup();
  const { isSuccess: isSuccessRetake, isPending: isLoadingRetake } =
    mutationQueryRetake;
  const [dataQuizRegenerate, setDataQuizRegenerate] = React.useState({} as any);
  const [buttonSections, setButtonSections] = React.useState(dataSection);
  const [testStarted, setTestStarted] = React.useState(false);

  const { mutationQuery: mutationQuerySubmit, handleSubmitQuiz } =
    useSubmitLevelupQuiz();
  const {
    data: dataSubmit,
    isPending: isLoadingSubmit,
    reset: resetSubmit,
  } = mutationQuerySubmit;
  const [selectedIndex, setSelectedIndex] = React.useState("");
  const [dataExercise, setDataExercise] = React.useState<any>([]);
  const [dataScore, setDataScore] = React.useState({} as any);
  const [indexSoal, setIndexSoal] = React.useState(0);
  const [isModalLevelUp, setIsModalLevelup] = React.useState(false);
  const time = new Date(); // 10 minutes timer
  const [timer, setTimer] = React.useState(0);

  React.useEffect(() => {
    if (id) {
      handleGetQuiz(id as string);
    }
  }, [id]);

  const onChangeSteps = (value: number) => {
    console.log("onChangestep:", value);
    dataExercise.map((key: any, ind: any) => {
      if (ind == value) {
        setSelectedIndex(key.answer);
      }
    });
    setIndexSoal(value);
  };

  const handleTimeCountDown = () => {
    // alert("Time is over");
    handleSubmit();
  };

  const onChangeRadio = (e: RadioChangeEvent) => {
    dataExercise[indexSoal]?.options?.map((key: any) => {
      if (e.target.value == key.option) {
        dataExercise[indexSoal].answer = key.option;
        setSelectedIndex(e.target.value);
      }
    });
  };

  const handleClick = (value: any) => {
    setSelectedIndex("");
    if (value === "previous") {
      let resulIndex = indexSoal === 0 ? indexSoal : indexSoal - 1;
      setIndexSoal(resulIndex);
      dataExercise.map((key: any, ind: any) => {
        if (ind == resulIndex) {
          setSelectedIndex(key.answer);
        }
      });
    }

    if (value === "next") {
      let resulIndex =
        indexSoal === dataExercise.length - 1 ? indexSoal : indexSoal + 1;
      setIndexSoal(resulIndex);
      dataExercise.map((key: any, ind: any) => {
        if (ind == resulIndex) {
          setSelectedIndex(key.answer);
        }
      });
    }
  };

  const handleSubmit = () => {
    let answer = {} as any;
    dataExercise.map((item: any) => {
      answer[item.id] = item.answer;
    });
    handleSubmitQuiz(dataQuizRegenerate.id, answer);
    handleFinishSection();
  };

  const handleFinishSection = () => {
    let result = buttonSections.map((section) => {
      if (section.isStarted === true) {
        section.isFinish = true;
      }
      return section;
    });
    setButtonSections(result);
  };

  React.useEffect(() => {
    if (dataSubmit) {
      setDataScore(dataSubmit);
      sleep(1000).then(() => {
        setIsModalLevelup(!isModalLevelUp);
        resetSubmit();
        mutateQuiz(id as string);
        setIndexSoal(0);
      });
      setTestStarted(false);
    }

    if (isSuccessRetake) {
      navigate.back();
      return;
    }
  }, [dataSubmit, isSuccessRetake]);

  React.useEffect(() => {
    if (dataQuiz) {
      setDataQuizRegenerate(dataQuiz);
      let dataTemp = dataQuiz as any;
      if (dataTemp.is_completed) {
        let dataAnswer = JSON.parse(dataTemp.answers);
        let dataReview = JSON.parse(dataTemp.review);
        dataTemp.questions.map((item: any) => {
          let dataReviewResult = dataReview.find(
            (key: any) => key.question_id === item.id
          );
          item.answer = dataAnswer[item.id];
          if (dataReviewResult) {
            item.review = dataReviewResult;
          }
          return item;
        });
        setDataExercise(dataTemp.questions);
        setSelectedIndex(dataAnswer[dataTemp.questions[indexSoal].id]);
        setTimer(0);
      } else {
        let dataAnswer = JSON.parse(dataTemp.answers);
        dataTemp.questions.map((item: any) => {
          item.answer = dataAnswer ? dataAnswer[item.id] : "";
          return item;
        });
        setDataExercise(dataTemp.questions);
        setTimer(
          time.setSeconds(
            time.getSeconds() + dataTemp.remaining_time &&
              dataTemp.remaining_time > 0
              ? dataTemp.remaining_time
              : dataTemp.duration * 60
          )
        );
        setSelectedIndex("");
      }
      setTestStarted(true);
    }
  }, [dataQuiz]);

  return (
    <Space
      direction="vertical"
      style={{ paddingTop: 20, width: "100%", backgroundColor: "#FFF" }}
      className="hero-container-background"
    >
      <Space
        direction="vertical"
        className="container-breadcumb-title-investment"
      >
        <Space>
          <div className="textfs26-fw700-black">
            {translate("Level_Up_Capital")}
          </div>
        </Space>
      </Space>
      <Row className="container-card-detailClassSchedule">
        <Col span={24}>
          <Card bordered={false} className="card-right-levelUp">
            {testStarted ? (
              <>
                <div className="container-cardRight-levelUp">
                  <span className="textfs30-fw600-black">{`${translate(
                    "Level_Up_Capital"
                  )} :`}</span>
                  <span className="textfs30-fw600-red">{name}</span>
                </div>
                <div style={{ height: 40 }} />
                {dataQuizRegenerate?.is_completed && (
                  <Row
                    justify={"space-between"}
                    style={{
                      width: "100%",
                      alignItems: "center",
                      marginBottom: 20,
                    }}
                  >
                    <Col style={{ display: "flex" }}>
                      <Col style={{ width: 50 }}>
                        <p>{translate("Score")}</p>
                      </Col>
                      <Col style={{ marginRight: 15 }}>
                        <p>{" : "}</p>
                      </Col>
                      <Col>
                        <p>{dataQuizRegenerate?.score}</p>
                      </Col>
                    </Col>
                    <Col>
                      <Button
                        loading={isLoadingRetake}
                        onClick={() => retakeDataQuiz(dataQuizRegenerate?.id)}
                        type="primary"
                        style={{
                          backgroundColor: getStatusColor("pending"),
                          color: "white",
                          marginRight: 10,
                        }}
                      >
                        {translate("RetakeQuiz")}
                      </Button>
                      <Button
                        type="primary"
                        disabled
                        style={{
                          backgroundColor: dataQuizRegenerate?.is_passed
                            ? "#00C82C"
                            : "#F22350",
                          color: "white",
                        }}
                      >
                        {dataQuizRegenerate?.is_passed
                          ? translate("Passed")
                          : translate("Failed")}
                      </Button>
                    </Col>
                  </Row>
                )}
                <Steps
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    padding: "0px 32px",
                    paddingTop: 16,
                    paddingBottom: 5,
                    borderRadius: 16,
                    border: "1px solid var(--Stroke-3, #E8E8E8)",
                  }}
                  current={indexSoal}
                  onChange={onChangeSteps}
                  status="error"
                  items={dataExercise.map((key: any) => {
                    if (key.answer) {
                      return {
                        icon: (
                          <CheckCircleFilled
                            style={{ color: "#00C82C", fontSize: 35 }}
                          />
                        ),
                      };
                    } else {
                      return {
                        icon: (
                          <Image
                            alt="icon-circle"
                            src={IconCirle}
                            width={30}
                            height={30}
                          />
                        ),
                      };
                    }
                  })}
                />
                <div style={{ height: 40 }} />
                <Space
                  direction="horizontal"
                  className="container-buttonCard-forum"
                >
                  <div
                    style={{ cursor: "pointer" }}
                    onClick={() => handleClick("previous")}
                    className="button-card-levelUp"
                  >
                    <ArrowRightOutlined
                      style={{ transform: "rotate(180deg)", color: "#9F0E0F" }}
                    />
                    <span className="textfs16-fw400-red">
                      {translate("Previous")}
                    </span>
                  </div>
                  {dataQuizRegenerate.duration !== 0 && (
                    <div className="button-center-card-levelUp">
                      {timer > 0 ? (
                        <>
                          <span className="textfs20-fw400-red">
                            {translate("TimeLeft")}
                          </span>
                          <MyTimer
                            expiryTimestamp={timer}
                            handleTimeCountDown={handleTimeCountDown}
                            className={"textfs32-fw400-red"}
                            dataExercise={dataExercise}
                            id={dataQuizRegenerate.id}
                          />
                        </>
                      ) : null}
                    </div>
                  )}
                  {indexSoal === dataExercise.length - 1 ? (
                    <>
                      {arrayContainsEmptyString(dataExercise) ? (
                        <div
                          style={{
                            cursor: "not-allowed",
                            background: "white",
                            color: "gray",
                          }}
                          className="button-card-levelUp"
                        >
                          <span className="textfs16-fw400-red">
                            {translate("Submit")}
                          </span>
                        </div>
                      ) : (
                        <>
                          {dataQuizRegenerate.is_completed ? null : (
                            <div
                              style={{ cursor: "pointer" }}
                              onClick={handleSubmit}
                              className="button-card-levelUp"
                            >
                              {isLoadingSubmit && (
                                <LoaderSpinGif size="small" />
                              )}
                              <span className="textfs16-fw400-red">
                                {translate("Submit")}
                              </span>
                            </div>
                          )}
                        </>
                      )}
                    </>
                  ) : (
                    <div
                      style={{ cursor: "pointer" }}
                      onClick={() => handleClick("next")}
                      className="button-card-levelUp"
                    >
                      {isLoadingSubmit && <LoaderSpinGif size="small" />}
                      <span className="textfs16-fw400-red">
                        {translate("Next_Capital")}
                      </span>
                      <ArrowRightOutlined style={{ color: "#9F0E0F" }} />
                    </div>
                  )}
                </Space>
                {dataExercise.length > 0 ? (
                  <React.Fragment>
                    <div style={{ height: 49 }} />
                    {!dataQuiz && dataExercise.length === 0 && (
                      <LoaderSpinGif size="large" />
                    )}
                    {dataQuiz && dataExercise.length === 0 ? <NoData /> : null}
                    {dataExercise.length > 0 && (
                      <span className="textfs18-fw400-gray">{`${translate(
                        "Question"
                      )} ${indexSoal + 1} ${translate("Of")} ${
                        dataExercise.length
                      }`}</span>
                    )}
                    <div style={{ height: 8 }} />
                    <span
                      style={{ display: "flex" }}
                      className="textfs20-fw600-black"
                      dangerouslySetInnerHTML={{
                        __html: `${dataExercise[indexSoal]?.question}`,
                      }}
                    />
                    <div style={{ height: 32 }} />
                    <Radio.Group
                      disabled={dataQuizRegenerate.is_completed ? true : false}
                      onChange={onChangeRadio}
                      value={selectedIndex}
                    >
                      <Row>
                        {dataExercise[indexSoal]?.options.map(
                          (key: any, value: any) => {
                            return (
                              <Col
                                span={24}
                                style={{
                                  justifyContent: "space-between",
                                  display: "flex",
                                }}
                              >
                                <Radio
                                  className="textfs18-fw400-black"
                                  value={key.option}
                                >
                                  <Col>
                                    <div
                                      style={{
                                        paddingTop: 15,
                                      }}
                                      dangerouslySetInnerHTML={{
                                        __html: `${key.value}`,
                                      }}
                                    />
                                  </Col>
                                </Radio>
                                <Col>
                                  {dataExercise[indexSoal].review?.answer ==
                                    key.option && (
                                    <>
                                      {dataExercise[indexSoal].review
                                        ?.correct ? (
                                        <CheckCircleFilled
                                          style={{
                                            color: "#00C82C",
                                            fontSize: 30,
                                            marginTop: 15,
                                            marginLeft: 10,
                                          }}
                                        />
                                      ) : (
                                        <CloseCircleFilled
                                          style={{
                                            color: "#F22350",
                                            fontSize: 30,
                                            marginTop: 15,
                                            marginLeft: 10,
                                          }}
                                        />
                                      )}
                                    </>
                                  )}
                                </Col>
                              </Col>
                            );
                          }
                        )}
                      </Row>
                    </Radio.Group>
                  </React.Fragment>
                ) : (
                  <LoaderSpinGif size="large" />
                )}
              </>
            ) : (
              <LoaderSpinGif size="large" />
            )}
          </Card>
        </Col>
      </Row>

      {/* Modal Score */}
      <ModalComponent
        centered
        title={
          dataScore.is_passed
            ? translate("LabelResultTest")
            : translate("LabelSorry")
        }
        open={isModalLevelUp}
        width={800}
        onCancel={() => setIsModalLevelup(false)}
        footer={[
          <Flex justify="center">
            <Button type="primary" onClick={() => setIsModalLevelup(false)}>
              {translate("Save")}
            </Button>
          </Flex>,
        ]}
      >
        <ScoreComponent
          desc={
            dataScore.is_passed
              ? `${translate("DescResultTest")} ${
                  dataQuizRegenerate!.duration * 60
                } second`
              : `${translate("DescFailResultTest")} ${
                  dataQuizRegenerate!.duration * 60
                } second`
          }
          desc2={`${translate("Score")} : ${dataScore.score}`}
        />
      </ModalComponent>
    </Space>
  );
}
