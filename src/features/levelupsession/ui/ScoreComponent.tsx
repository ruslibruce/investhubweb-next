import IconSuccess from "@/shared/images/icon/ic_round_check_circle_green_large.webp";
import { Flex, Space } from "antd";
import Image from "next/image";

type PropsModal = {
  desc: string;
  desc2?: string;
};

function ScoreComponent({
  desc,
  desc2,
}: PropsModal) {
  return (
    <Flex justify="center" style={{ flexDirection: "column" }}>
      <div style={{marginLeft: "auto", marginRight: "auto"}} className="containerModalImgGreen">
        <Image src={IconSuccess} alt="modal-img" className="modalImage" />
      </div>
      <div className="modalContent">{desc}</div>
      <div className="modalContentRed">{desc2}</div>
    </Flex>
  );
}

export default ScoreComponent;
