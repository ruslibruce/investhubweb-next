import Calendar from "@/components/Calendar";
import {
  DASHBOARD_DETAIL_MY_LEARNING,
  DASHBOARD_EVENT,
  DASHBOARD_EVENT_DETAIL,
  DASHBOARD_NEWS,
  DASHBOARD_NEWS_DETAIL,
} from "@/shared/constants/path";
import Image from "next/image";
import React from "react";
import events1 from "../../shared/images/events/events1.jpg";

// react icon
import LearningJourneyCarousel from "@/components/LearningJourneyCarousel";
import LearningProgressCarousel from "@/components/LearningProgressCarousel";
import NewsCarousel from "@/components/NewsCarousel";
import { Box, CircularProgress, CircularProgressLabel } from "@chakra-ui/react";
import IconDot from "@/shared/images/svg/icon_dot.svg";

import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { Col, Flex, Progress, Row, Space } from "antd";
import { useRouter } from "next/router";

// API
import LoaderSpinGif from "@/components/LoaderSpinGif";
import NoData from "@/components/NoData";
import TextResponsive from "@/components/TextResponsive";
import useGetEvent from "@/features/event/hooks/useGetEvent";
import useGetNews from "@/features/news/hooks/useGetNews";
import { USER } from "@/shared/constants/storageStatis";
import { paramsToString } from "@/shared/utils/string";
import moment from "moment";
import { useTranslation } from "next-i18next";
import Link from "next/link";
import useGetLearningJourney from "./hooks/useGetLearningJourney";
import BackgroundHero from "@/components/BackgroundHero";
import { imageLMS } from "@/shared/constants/imageUrl";
import {
  ArrowRightOutlined,
  ClockCircleOutlined,
  RightOutlined,
  SketchOutlined,
} from "@ant-design/icons";
import EventCarousel from "@/components/EventCarousel";

export default function HomePage() {
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const [limit, setLimit] = React.useState<number>(5);
  const { fetchQuery: fetchNews } = useGetNews(
    {},
    paramsToString({ page: 1, limit: limit })
  );
  const { t: translate } = useTranslation();
  const { fetchQuery: fetchEvent } = useGetEvent({});

  const { data: dataResultNews, isLoading: isLoadingNews } = fetchNews;
  const { data: dataNews } = dataResultNews;

  const { isLoading: isLoadingEvent, data: dataResultEvent } = fetchEvent;
  const { data: dataEvent } = dataResultEvent;
  const { fetchGetLearnJourney } = useGetLearningJourney({});
  const { data: dataResultLearnJourney, isLoading: isLoadingJourney } =
    fetchGetLearnJourney;
  const { data: dataLearnJourney } = dataResultLearnJourney;
  // console.log("dataLearnJourney", JSON.stringify(dataLearnJourney));
  console.log("CEK ENV ===> ", process.env.NEXT_PUBLIC_CALLBACK_URL);

  const handleNews = (event: React.MouseEvent<HTMLElement>, value: any) => {
    navigate.push(`${DASHBOARD_NEWS_DETAIL}/${value.id}`);
  };

  const handleEvent = (event: React.MouseEvent<HTMLElement>, value: any) => {
    navigate.push(`${DASHBOARD_EVENT_DETAIL}/${value.id}`);
  };

  const renderCards = () => {
    if (!dataNews) return <LoaderSpinGif size="large" />;
    if (dataNews?.length === 0) return null;
    return dataNews?.map((newsItem: any, index: any) => (
      <div key={newsItem.id} className="news-container-content">
        {/* <div className="news-head-content">
          <Image
            src={profile2}
            alt="the batman"
            width={30}
            height={30}
            style={{
              objectFit: "cover",
              borderRadius: "100px",
            }}
          />
          <Image src={IconDot} alt="dot" />
          <h4>{newsItem.publish_time}</h4>
        </div> */}
        <div className="news-main-content">
          <div
            style={{ display: "flex", justifyContent: "space-between" }}
            className="news-main-title"
          >
            <div style={{ display: "flex" }}>
              {newsItem.source_id ? (
                <img
                  src={newsItem.cover_image}
                  alt="image-news"
                  width={80}
                  height={80}
                  style={{
                    objectFit: "cover",
                    borderRadius: "10px",
                    height: "80px",
                  }}
                />
              ) : (
                <Image
                  src={newsItem.cover_image}
                  alt="image-news"
                  width={80}
                  height={80}
                  style={{
                    objectFit: "cover",
                    borderRadius: "10px",
                    height: "80px",
                  }}
                />
              )}
              <span
                className="textfs20-fw700-black"
                style={{
                  display: "-webkit-box",
                  WebkitLineClamp: 2,
                  WebkitBoxOrient: "vertical",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  marginLeft: 10,
                  width: "80%",
                  paddingTop: 10,
                }}
                dangerouslySetInnerHTML={{ __html: newsItem?.subject }}
              />
            </div>
            <span className="textfs14-fw400-black">
              {moment(newsItem.publish_time).format("DD MMM YYYY")}
            </span>
          </div>
          <span
            className="textfs16-fw400-black"
            style={{
              display: "-webkit-box",
              WebkitLineClamp: 2,
              WebkitBoxOrient: "vertical",
              overflow: "hidden",
              textOverflow: "ellipsis",
            }}
            dangerouslySetInnerHTML={{ __html: newsItem?.body }}
          />
        </div>
        <div className="news-footer-content">
          {/* <div className="news-footer-interactions">
            <FaRegHeart />
            <h4>12K likes</h4>
            <Image src={IconDot} alt="dot" />
            <FaRegComment />
            <h4>823 comments</h4>
          </div> */}
          <div />
          <div
            className="news-see-detail"
            onClick={(e) => handleNews(e, newsItem)}
            style={{ cursor: "pointer" }}
          >
            <Space className="textfs14-fw400-red">
              {`${translate("SeeDetail")}`}
              <RightOutlined style={{ color: "#9F0E0F" }} />
            </Space>
          </div>
        </div>
      </div>
    ));
  };

  const renderEventCards = () => {
    if (!dataEvent) return <LoaderSpinGif size="large" />;
    if (dataEvent?.length === 0) return null;
    const reverseDataEvent = dataEvent && [...dataEvent].reverse().slice(0, 5);
    return (
      reverseDataEvent?.length > 0 &&
      reverseDataEvent?.map((eventItem: any, index: any) => (
        <div
          key={eventItem.id}
          className="events-content-card"
          onClick={(e) => handleEvent(e, eventItem)}
          style={{ cursor: "pointer" }}
        >
          <div className="events-content-card-header">
            <span>{eventItem.location}</span>
            <Image src={IconDot} alt="dot" />
            {eventItem.start_time}
          </div>
          <div className="events-content-card-main">
            <Image
              src={eventItem.cover_image}
              alt="image-news"
              width={60}
              height={60}
              style={{
                objectFit: "cover",
                borderRadius: "10px",
              }}
            />
            <span
              className="textfs14-fw700-black"
              style={{
                display: "-webkit-box",
                WebkitLineClamp: 2,
                WebkitBoxOrient: "vertical",
                overflow: "hidden",
                textOverflow: "ellipsis",
                marginLeft: 10,
                height: 50,
                paddingTop: 10,
              }}
              dangerouslySetInnerHTML={{ __html: eventItem.title }}
            />
          </div>
        </div>
      ))
    );
  };

  const handleMyLearningJourney = (item: any) => {
    navigate.push(`${DASHBOARD_DETAIL_MY_LEARNING}/${item.id}/${item.name}`);
  };

  return (
    <div className="hero-container-backgroundImg">
      <Row
        style={{
          borderBottomLeftRadius: 40,
          borderBottomRightRadius: 40,
          paddingTop: 20,
          paddingBottom: 30,
          paddingLeft: 70,
          paddingRight: 70,
          position: "relative",
          overflow: "hidden",
        }}
        gutter={16}
        align={"middle"}
        className="hero-container-antd"
      >
        <BackgroundHero />
        <Col span={12}>
          <TextResponsive
            bigScreen={60}
            mobile={24}
            color="white"
            fontWeight={700}
            isSpan
            desktop={45}
          >
            {translate("Title_Hero_Home")}
          </TextResponsive>
        </Col>
        <Col span={12}>
          <TextResponsive
            bigScreen={22}
            mobile={12}
            color="white"
            fontWeight={400}
            isSpan
            desktop={18}
          >
            {translate("Desc_Hero_Home")}
          </TextResponsive>
        </Col>
      </Row>

      {dataUser?.isLogin && (
        <>
          <LearningProgressCarousel />
          {!dataLearnJourney && dataUser.isLogin ? (
            <div className="learning-journey">
              <LoaderSpinGif size="large" />
            </div>
          ) : (
            <div className="learning-journey">
              <div className="learning-journey-title">
                <h2>{translate("MyLearningJourney")}</h2>
                {/* <h3>{translate("ViewAll")}</h3> */}
              </div>
              <div className="learning-journey-carousel">
                <LearningJourneyCarousel />
              </div>
              <div
                style={{
                  overflowY: "scroll",
                  WebkitScrollSnapType: "none",
                  paddingBottom: 30,
                }}
                className="learning-journey-content"
              >
                {dataLearnJourney?.length === 0 && <NoData />}
                {dataLearnJourney?.length > 0 &&
                  dataLearnJourney?.map((item: any, index: any) => {
                    const testCompleted = item?.is_test_passed ? 1 : 0;
                    const completedCount =
                      item.requirements.length > 0
                        ? item.requirements.filter(
                            (val: any) =>
                              val.progress && val.progress.is_completed
                          ).length + testCompleted
                        : 0;
                    const coursePlusTest = item.requirements?.length + 1;
                    const valueCircular =
                      completedCount === 0
                        ? 0
                        : (completedCount / coursePlusTest) * 100;
                    return (
                      <Flex
                        style={{
                          cursor: "pointer",
                        }}
                        key={index}
                        onClick={() => handleMyLearningJourney(item)}
                        className="learning-journey-card"
                      >
                        <CircularProgress
                          value={valueCircular}
                          color="green.400"
                        >
                          <CircularProgressLabel>
                            <Box
                              display="flex"
                              alignItems="center"
                              justifyContent="center"
                            >
                              {item.badge_icon ? (
                                <Image
                                  alt="image-badge"
                                  src={`${imageLMS}/${item.badge_icon}`}
                                  width={100}
                                  height={100}
                                  style={{ width: 25, height: 25 }}
                                />
                              ) : (
                                <SketchOutlined
                                  style={{
                                    color: item.achieved_at
                                      ? "green"
                                      : item.requirements.length > 0
                                      ? "red"
                                      : "gray",
                                    fontSize: 25,
                                  }}
                                />
                              )}
                            </Box>
                          </CircularProgressLabel>
                        </CircularProgress>
                        <h3
                          style={{
                            display: "-webkit-box",
                            WebkitLineClamp: 2,
                            WebkitBoxOrient: "vertical",
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            height: 40,
                            textAlign: "center",
                            width: 100,
                          }}
                          className="textfs12-fw400-black"
                        >
                          {item.name}
                        </h3>
                        <Progress
                          status="success"
                          percent={
                            item.requirements.length > 0
                              ? (completedCount / coursePlusTest) * 100
                              : 0
                          }
                          showInfo={false}
                        />
                        <span className="textfs10-fw400-black">
                          {item.achieved_at
                            ? translate("Completed")
                            : `${completedCount} of ${coursePlusTest} ${translate(
                                "Completed"
                              )}`}
                        </span>
                      </Flex>
                    );
                  })}
              </div>
            </div>
          )}
        </>
      )}

      {/* {!dataUser.isLogin && (
        <div className="promo">
          <div className="promo-title">
            <h2>Promo</h2>
            <h3>{translate("ViewAll")}</h3>
          </div>
          <div className="promo-content">
            <SwiperComponent />
          </div>
        </div>
      )} */}

      <div className="news-events-container">
        {!dataNews ? (
          <div className="news-container">
            <LoaderSpinGif size="large" />
          </div>
        ) : (
          <div className="news-container">
            <h2 className="news-title">
              {translate("LatestNews")}
              <Link href={DASHBOARD_NEWS}>
                <ArrowRightOutlined />
              </Link>
            </h2>
            <div className="news-carousel">
              <NewsCarousel dataNews={dataNews} isLoading={isLoadingNews} />
            </div>
            {renderCards()}
            {dataResultNews.npage > 1 && (
              <div className="btn-container">
                <div className="btn-load-more">
                  <div
                    onClick={() => setLimit(limit + 5)}
                    style={{ cursor: "pointer" }}
                  >
                    {translate("LoadMore")}
                  </div>
                </div>
              </div>
            )}
          </div>
        )}
        <div className="events-container">
          {/* <div className="events-ebook">
            <h2 className="events-ebook-title">
              {translate("Ebook_Capital")} <ArrowRightOutlined />
            </h2>
            <div className="events-ebook-content">
              <div className="ebook-carousel">
                <EbookCarousel />
              </div>
              <Image
                src={ebook1}
                alt="buku trader"
                style={{
                  objectFit: "cover",
                  borderRadius: "0 8px 0 8px",
                  margin: "0 1rem 1rem 0",
                  width: "150px",
                  height: "180px",
                }}
              />
              <Image
                src={ebook2}
                alt="buku start-up"
                style={{
                  objectFit: "cover",
                  borderRadius: "0 8px 0 8px",
                  width: "120px",
                  height: "150px",
                }}
              />
            </div>
            <p className="events-ebook-content-text">
              {translate("Ebook_Collaborate")}
            </p>
          </div> */}
          <div className="events-calendar">
            <Calendar />
          </div>
          <div className="events">
            <h2 className="events-title">
              {translate("LatestEvent")}
              <Link href={DASHBOARD_EVENT}>
                <ArrowRightOutlined />
              </Link>
            </h2>
            <div className="events-content">
              <div className="events-content-card-img">
                <Image
                  src={events1}
                  alt="city-night"
                  width={310}
                  height={150}
                  style={{
                    objectFit: "cover",
                    borderRadius: "10px",
                    filter: "blur(0.5px)",
                    width: 310,
                    height: 150,
                  }}
                />
                <p className="events-content-card-img-title">
                  10 Habits That Will Change Your Live for the Better If envy
                  and jealousy are impacting your friendships
                </p>
                <p className="events-content-card-img-date">
                  Jakarta - <ClockCircleOutlined /> 25 Feb 2024 10:00
                </p>
              </div>
              {renderEventCards()}
            </div>
          </div>
        </div>
      </div>

      <div className="event-carousel">
        {!dataEvent ? (
          <div className="news-container">
            <LoaderSpinGif size="large" />
          </div>
        ) : (
          <div className="news-container">
            <h2 className="news-title">
              {translate("LatestEvent")}
              <Link href={DASHBOARD_EVENT}>
                <ArrowRightOutlined />
              </Link>
            </h2>
            <EventCarousel dataEvent={dataEvent} isLoading={isLoadingEvent} />
            {renderCards()}
            {dataResultNews.npage > 1 && (
              <div className="btn-container">
                <div className="btn-load-more">
                  <div
                    onClick={() => setLimit(limit + 5)}
                    style={{ cursor: "pointer" }}
                  >
                    {translate("LoadMore")}
                  </div>
                </div>
              </div>
            )}
          </div>
        )}
        <div className="events-container">
          {/* <div className="events-ebook">
            <h2 className="events-ebook-title">
              {translate("Ebook_Capital")} <ArrowRightOutlined />
            </h2>
            <div className="events-ebook-content">
              <div className="ebook-carousel">
                <EbookCarousel />
              </div>
              <Image
                src={ebook1}
                alt="buku trader"
                style={{
                  objectFit: "cover",
                  borderRadius: "0 8px 0 8px",
                  margin: "0 1rem 1rem 0",
                  width: "150px",
                  height: "180px",
                }}
              />
              <Image
                src={ebook2}
                alt="buku start-up"
                style={{
                  objectFit: "cover",
                  borderRadius: "0 8px 0 8px",
                  width: "120px",
                  height: "150px",
                }}
              />
            </div>
            <p className="events-ebook-content-text">
              {translate("Ebook_Collaborate")}
            </p>
          </div> */}
          <div className="events-calendar">
            <Calendar />
          </div>
          <div className="events">
            <h2 className="events-title">
              {translate("LatestEvent")}
              <Link href={DASHBOARD_EVENT}>
                <ArrowRightOutlined />
              </Link>
            </h2>
            <div className="events-content">
              <div className="events-content-card-img">
                <Image
                  src={events1}
                  alt="city-night"
                  width={310}
                  height={150}
                  style={{
                    objectFit: "cover",
                    borderRadius: "10px",
                    filter: "blur(0.5px)",
                    width: 310,
                    height: 150,
                  }}
                />
                <p className="events-content-card-img-title">
                  10 Habits That Will Change Your Live for the Better If envy
                  and jealousy are impacting your friendships
                </p>
                <p className="events-content-card-img-date">
                  Jakarta - <ClockCircleOutlined /> 25 Feb 2024 10:00
                </p>
              </div>
              {renderEventCards()}
            </div>
          </div>
        </div>
      </div>

      {/* {!dataUser?.isLogin && (
        <div className="forum-container">
          <ForumCarousel />
        </div>
      )} */}
    </div>
  );
}
