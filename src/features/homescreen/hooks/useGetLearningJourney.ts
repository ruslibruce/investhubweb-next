import { fetchGetLearningJourney } from "@/shared/api/fetch/learningJourney";
import { USER } from "@/shared/constants/storageStatis";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchGetLearningJourney();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetLearningJourney = (initialData?: any) => {
  const dataUser = storageCheck(USER);
  const fetchDataInvestment = dataUser.isLogin
    ? fetchGetLearningJourney()
    : { key: [], api: () => Promise.resolve({}) };

  const fetchGetLearnJourney = useFetchHook({
    keys: fetchDataInvestment.key,
    api: fetchDataInvestment.api,
    initialData,
    options: {},
  });

  return {
    fetchGetLearnJourney,
  };
};

export default useGetLearningJourney;
