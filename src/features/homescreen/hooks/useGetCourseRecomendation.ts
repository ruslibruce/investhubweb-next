import { getCourseRecomendations } from "@/shared/api/fetch/course";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = getCourseRecomendations();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetCourseRecomendation = (initialData?: any, params?: string) => {
  const fetchDataColors = getCourseRecomendations(params);

  const fetchGetCourseRecomendations = useFetchHook({
    keys: [fetchDataColors.key, params],
    api: fetchDataColors.api,
    initialData,
    options: {},
  });

  return {
    fetchGetCourseRecomendations,
  };
};

export default useGetCourseRecomendation;
