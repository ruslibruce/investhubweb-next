import { fetchDisclaimer } from "@/shared/api/fetch/disclaimer";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchDisclaimer();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetDisclaimer = (initialData?: any) => {
  const fetchDataDisclaimer = fetchDisclaimer();

  const fetchQuery = useFetchHook({
    keys: fetchDataDisclaimer.key,
    api: fetchDataDisclaimer.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetDisclaimer;
