import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import iconLogo from "@/shared/images/logo/logo_investhub.webp";
import { Breadcrumb, Col, Row, Space } from "antd";
import Image from "next/image";
import React from "react";
import useGetDisclaimer from "../hooks/useGetDisclaimer";

export default function DisclaimerPage() {
  const { fetchQuery } = useGetDisclaimer({});
  const { isLoading, data: dataDisclaimer } = fetchQuery;

  const handleChange = (value: string) => {
    console.log(`selected ${value}`);
  };

  return (
    <div className="disclaimer">
      <div className="disclaimer-breadcrumb">
        <Breadcrumb
          separator=">"
          items={[
            {
              href: "",
              title: (
                <Image
                  src={iconBreadcrumb}
                  alt="Breadcrumb Icon"
                />
              ),
            },
            {
              title: <span className="textfs16-fw400-red">{"Disclaimer"}</span>,
            },
          ]}
        />
      </div>
      <div className="disclaimer-content">
        <h2 className="disclaimer-title">{dataDisclaimer?.title}</h2>
        <div className="disclaimer-content-text" dangerouslySetInnerHTML={{__html: dataDisclaimer?.body}} />
      </div>
    </div>
  );
}
