import { changeMail } from "@/shared/api/mutation/changeMail";
import { DASHBOARD_EMAIL_CONFIRM } from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { storageCheck, storageSet } from "@/shared/utils/clientStorageUtils";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";

const usePostChangeEmail = () => {
  const queryClient = useQueryClient();
  const { t: translate } = useTranslation();
  const navigate = useRouter();

  const mutationQuery = useMutationHook({
    api: changeMail,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables: any, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        if (data) {
          notification.success({
            message: translate("Email_Success"),
          });
          const dataUser = storageCheck(USER);
          let temp = {
            ...dataUser,
            user: {
              ...dataUser.user,
              email: variables.email,
            },
          };
          storageSet(USER, temp);
          navigate.replace(DASHBOARD_EMAIL_CONFIRM);
        }
      },
    },
  });

  const handleChangeEmail = (data: {}) => {
    mutationQuery.mutate(data);
  };

  return {
    mutationQuery,
    handleChangeEmail,
  };
};

export default usePostChangeEmail;
