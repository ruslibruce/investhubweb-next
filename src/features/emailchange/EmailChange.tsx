import FooterAuth from "@/components/FooterAuth";
import FormButton from "@/components/FormButton";
import FormInputFormik from "@/components/FormInputFormik";
import ButtonFlag from "@/shared/components/layout/auth-layout/sider/ButtonFlag";
import IconBack from "@/shared/images/icon/arrow-left.webp";
import IconLogo from "@/shared/images/logo/logo_investhub.webp";
import { Space } from "antd";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import * as yup from "yup";
import usePostChangeEmail from "./hooks/usePostChangeEmail";
import { regexEmail } from "@/shared/utils/helper";

function EmailChange() {
  const navigate = useRouter();
  const { t: translate } = useTranslation();
  const { handleChangeEmail, mutationQuery } = usePostChangeEmail();
  const { isPending } = mutationQuery;

  const handleChange = (values: any) => {
    handleChangeEmail(values);
  };

  const emailValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    email: yup
      .string()
      .matches(regexEmail(), translate("ValidEmail"))
      .max(
        30,
        ({ max }) =>
          `${translate("EmailLength")} ${max} ${translate("Characters")}`
      )
      .required(translate("RequireEmail")),
  });
  return (
    <Space direction="vertical" className="change-email-container">
      <div
        style={{ marginTop: "20px" }}
        className="language-switch"
      >
        <ButtonFlag mobileVersion={true} />
      </div>
      <div className="container-header-register">
        <Image
          width={200}
          src={IconLogo}
          alt="icon-logo"
          className="icon-logo"
        />
      </div>
      <div className="container-title">
        <Image
          onClick={() => navigate.back()}
          src={IconBack}
          alt="icon-back"
          className="icon-back"
        />
        <div className="text-title">{translate("Title_EmailChange")}</div>
        <div className="text-title-info">{translate("Desc_EmailChange")}</div>
        <Formik
          validationSchema={emailValidationSchema}
          initialValues={{ email: "" }}
          onSubmit={(values) => {
            handleChange(values);
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterEmail")}
                title={translate("LabelEmail")}
                type={"email"}
                name={"email"}
                styleForm={{marginTop: 20}}
              />
              <FormButton
                styleButton={{ marginTop: 32 }}
                onClick={handleSubmit}
                disabled={!isValid}
                title={translate("Send")}
                type={"submit"}
                isLoading={isPending}
              />
            </>
          )}
        </Formik>
      </div>
      <FooterAuth hiddenLineOr={false} hiddenButton={true} backToLogin={true} />
    </Space>
  );
}

export default EmailChange;
