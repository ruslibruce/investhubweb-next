import { fetchDetailNews } from "@/shared/api/fetch/news";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchDetailNews();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetDetailNews = (id: string, initialData?: any) => {
  const fetchDataNews = id
    ? fetchDetailNews(id)
    : { key: [], api: () => Promise.resolve(initialData || {}) };

  const fetchGetDetailNews = useFetchHook({
    keys: fetchDataNews.key,
    api: fetchDataNews.api,
    initialData,
    options: {},
  });

  return {
    fetchGetDetailNews,
  };
};

export default useGetDetailNews;
