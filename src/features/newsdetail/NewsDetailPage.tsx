import iconCalendarCard from "@/shared/images/icon/icon_calendar_card.webp";
import iconBreadcrumb from "@/shared/images/news/News.webp";
import imageDummyCard from "@/shared/images/news/dummy4.webp";
import {
  FacebookOutlined,
  InstagramOutlined,
  SearchOutlined,
  XOutlined,
} from "@ant-design/icons";
import { Breadcrumb, Button, Card, Col, Input, Row, Space, Spin } from "antd";
import { useRouter } from "next/router";
import React, { useState } from "react";
import PaginationComponent from "@/shared/components/PaginationComponent";
import moment from "moment";
import Image from "next/image";
import NoData from "@/components/NoData";
import useGetDetailNews from "./hooks/useGetDetailNews";
import { DASHBOARD_NEWS } from "@/shared/constants/path";
import LoaderSpinGif from "@/components/LoaderSpinGif";
import { useTranslation } from "next-i18next";

export default function NewsPage() {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const { id } = navigate.query;
  const { fetchGetDetailNews } = useGetDetailNews(id as string);
  const { data: dataNews, isLoading } = fetchGetDetailNews;
  // console.log("dataNews Detail:", dataNews);

  const renderCardDetail = () => {
    if (isLoading) {
      // Tampilkan pesan loading atau spinner
      return <LoaderSpinGif size="large" />;
    }

    if (`${dataNews.id}` !== id) {
      // Tampilkan pesan loading atau spinner
      return <LoaderSpinGif size="large" />;
    }

    if (!dataNews) {
      // Tampilkan pesan jika dataNews tidak tersedia
      return <NoData />;
    }

    return (
      <div className="top-detail-event-main">
        <Card
          bordered={true}
          style={{
            marginBottom: 45,
            marginTop: 20,
          }}
          className="card-course"
        >
          <div className="">
            <p
              className="title"
              dangerouslySetInnerHTML={{ __html: dataNews?.subject }}
            />
            <p style={{ color: "gray", fontWeight: "bold", fontSize: 20 }}>
              {dataNews?.category ? dataNews?.category : "RSS NEWS"} •
              <span
                style={{ marginLeft: "8px", marginTop: "23px" }}
                className=""
              >
                {moment(dataNews?.publish_time).format("MMM DD, YYYY")}
              </span>
            </p>

            <Image
              width={100}
              height={100}
              src={dataNews?.cover_image}
              alt="img cover"
              style={{ width: "100%", height: 400 }}
            />
          </div>
          <Space direction="vertical">
            <p
              className="news-detail-content-main"
              style={{ marginTop: "24px", color: "gray", textAlign: "justify" }}
              dangerouslySetInnerHTML={{ __html: dataNews?.body }}
            />
            {dataNews?.link && (
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-start",
                  height: 40,
                }}
              >
                <div style={{ display: "flex" }}>
                  <span className="textfs14-fw600-black">{"Source"}</span>
                  <span
                    style={{ marginLeft: 10, marginRight: 10 }}
                    className="textfs14-fw600-black"
                  >
                    {":"}
                  </span>
                </div>
                <button
                  style={{
                    cursor: "pointer",
                    paddingTop: 2,
                  }}
                  onClick={() => window.open(dataNews?.link, "_blank")}
                  className="textfs12-fw600-gray"
                >
                  {dataNews?.link}
                </button>
              </div>
            )}
            {/* <div className="news-detail-content-main-interaction">
              <Image
                style={{
                  marginTop: "20px",
                  width: "25px",
                  height: "25px",
                }}
                src={likeDefault}
                alt="like-card"
              />
              <span style={{ marginTop: "20px" }}>Like</span>
              <Image
                style={{
                  width: "24px",
                  height: "24px",
                  marginLeft: "25px",
                  marginTop: "20px",
                }}
                src={iconCommentCard}
                alt="comment-card"
              />
              <span
                style={{
                  marginTop: "20px",
                  marginLeft: "4px",
                  fontSize: "14px",
                }}
              >
                Comment
              </span>
              <Image
                style={{
                  width: "24px",
                  height: "24px",
                  marginLeft: "25px",
                  marginTop: "20px",
                }}
                src={iconShareCard}
                alt="comment-card"
              />
              <span
                style={{
                  marginTop: "20px",
                  marginLeft: "4px",
                  fontSize: "14px",
                }}
              >
                Share
              </span>
              <Image
                style={{
                  width: "24px",
                  height: "24px",
                  marginLeft: "25px",
                  marginTop: "20px",
                }}
                src={iconBookmarkCard}
                alt="comment-card"
              />
              <span
                style={{
                  marginTop: "20px",
                  marginLeft: "4px",
                  fontSize: "14px",
                }}
              >
                Bookmark
              </span>
            </div> */}
          </Space>
        </Card>
      </div>
    );
  };

  return (
    <div className="hero-container-background">
      <Breadcrumb
        separator=">"
        className="breadcrumb-event-news"
        items={[
          {
            href: "/news",
            title: <Image src={iconBreadcrumb} alt="Breadcrumb Icon" />,
          },
          {
            title: <span style={{ fontSize: 14 }}>{translate("News")}</span>,
            href: DASHBOARD_NEWS,
          },
          {
            title: (
              <p
                style={{ fontSize: 14, color: "#9F0E0F", paddingTop: 2 }}
                dangerouslySetInnerHTML={{ __html: dataNews?.subject }}
              />
            ),
          },
        ]}
      />

      <>
        <Row>
          <div className="top-detail-event-container padding-left-news-event">
            {renderCardDetail()}
            {/* Sisi kanan */}
            {/* Rekomen */}
            {/* <Col className="top-detail-event-aside">
              <Card
                hoverable
                bordered={false}
                style={{ marginTop: "105px", marginRight: "40px" }}
              >
                <span
                  style={{
                    marginTop: "25px",
                    fontSize: "24px",
                    fontWeight: "bold",
                  }}
                >
                  Recommended
                </span>
                <br />
                {[1, 2, 3, 4, 5].map((item) => (
                  <div style={{ marginTop: "30px" }}>
                    <span
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        marginRight: 18,
                      }}
                    >
                      Bussiness
                    </span>
                    <span
                      style={{ marginRight: 18 }}
                      className="textfs14-fw400-gray"
                    >
                      •
                    </span>
                    <span className="textfs14-fw400-gray">
                      22 Oct 2024 09:12
                    </span>

                    <Row style={{ marginTop: "15px" }}>
                      <Col span={4}>
                        <Image
                          style={{
                            width: "45px",
                            height: "45px",
                            borderRadius: "15px",
                          }}
                          src={imageDummyCard}
                          alt="comment-card"
                        />
                      </Col>
                      <Col span={20}>
                        <span
                          style={{
                            fontWeight: "bold",
                            fontSize: "18px",
                          }}
                        >
                          Class adds $30 millions to its balance sheet
                        </span>
                      </Col>
                    </Row>
                  </div>
                ))}
              </Card>
            </Col> */}
          </div>
        </Row>
        {/* <div className="popular-news-newsPage">
          <Col>
            <span
              style={{
                marginLeft: "40px",
                fontWeight: "bold",
                fontSize: "28px",
              }}
            >
              Popular News
            </span>
          </Col>
          <Col className="input">
            <Input
              prefix={<SearchOutlined />}
              style={{
                marginLeft: "50px",
                marginBottom: "20px",
                width: "200px",
                height: "50px",
              }}
              placeholder="Search Here"
            />
          </Col>
        </div>
        <div className="popular-news-content">{renderCards()}</div>
        <PaginationComponent
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          total={50}
        /> */}
      </>
    </div>
  );
}
