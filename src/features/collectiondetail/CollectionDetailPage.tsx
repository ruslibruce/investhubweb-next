import CardCourse from "@/components/CardCourse";
import NoData from "@/components/NoData";
import PaginationComponent from "@/shared/components/PaginationComponent";
import {
  DASHBOARD_MY_COLLECTION
} from "@/shared/constants/path";
import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import { Breadcrumb, Card, Row, Space, Spin } from "antd";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import IconBookmark from '@/shared/images/svg/icon_bookmark.svg';
import useGetCollectionContent from "./hooks/useGetCollectionContent";
import LoaderSpinGif from "@/components/LoaderSpinGif";

export default function CollectionDetailPage() {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const { slug } = navigate.query;
  const id = slug?.[0];
  const name = slug?.[1];

  const { fetchQuery } = useGetCollectionContent(null, id);
  const { isLoading, data: dataContent } = fetchQuery;
  const [currentPage, setCurrentPage] = React.useState<number>(1);

  if (!dataContent) {
    return <LoaderSpinGif size="large" isFullScreen={true} />;
  }

  return (
    <div className="hero-container-background">
      <Breadcrumb
        separator=">"
        style={{ paddingLeft: 80, paddingTop: 170 }}
        items={[
          {
            href: "",
            title: (
              <Image
                src={iconBreadcrumb}
                alt="Breadcrumb Icon"
                style={{ paddingBottom: 5 }}
              />
            ),
          },
          {
            title: (
              <Link href={DASHBOARD_MY_COLLECTION}>
                <span className="textfs16-fw400-red">
                  {translate("MyCollection")}
                </span>
              </Link>
            ),
          },
          {
            title: (
              <span className="textfs16-fw400-red">
                {translate("ListCollection")}
              </span>
            ),
          },
        ]}
      />
      <Card className="card-detail-collection">
        {dataContent?.length > 0 && (
          <Space style={{ gap: 10 }}>
            <div className="detail-collection-icon-1">
              <Image src={IconBookmark} width={200} height={200} alt="Bookmark Icon" />
            </div>
            <div className="detail-collection-icon-2">
            <Image src={IconBookmark} alt="Bookmark Icon" />
            <Image src={IconBookmark} width={200} height={200} alt="Bookmark Icon" />
            </div>
            <div className="title-detail-collection">
              <span className="textfs30-fw600-black">{name}</span>
              <span className="textfs24-fw600-red">{`${dataContent?.length} ${translate("Course")}`}</span>
            </div>
          </Space>
        )}
        {dataContent?.length === 0 && <NoData />}
        {dataContent?.length > 0 && (
          <Row gutter={10} style={{ marginTop: 40 }}>
            {dataContent?.map((item: any) => {
              return <CardCourse item={item} key={item.id} />;
            })}
          </Row>
        )}
      </Card>
      <PaginationComponent
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        total={50}
      />
    </div>
  );
}
