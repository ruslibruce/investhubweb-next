import { fetchPlacement } from "@/shared/api/fetch/placement";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchPlacement();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetQuizPreliminary = (initialData?: any) => {
  const fetchDataPlacement = fetchPlacement();

  const fetchQuiz = useFetchHook({
    keys: fetchDataPlacement.key,
    api: fetchDataPlacement.api,
    initialData,
    options: {},
  });

  return {
    fetchQuiz,
  };
};

export default useGetQuizPreliminary;
