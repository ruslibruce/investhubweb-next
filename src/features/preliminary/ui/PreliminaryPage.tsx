import useSubmitLevelupQuiz from "@/features/levelupsession/hooks/useSubmitQuizLevelup";
import ScoreComponent from "@/features/levelupsession/ui/ScoreComponent";
import { arrayContainsEmptyString, sleep } from "@/shared/utils/helper";
import { useSpring } from "@react-spring/web";
import { Button, Col, Flex, Radio, RadioChangeEvent, Row, Space } from "antd";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import React from "react";
import useGetQuizPreliminary from "../hooks/useGetQuizPreliminary";
import ModalComponent from "@/shared/components/ModalComponent";
import LoaderSpinGif from "@/components/LoaderSpinGif";
import NoData from "@/components/NoData";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";

const PreliminaryPage = () => {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const [indexSoal, setIndexSoal] = React.useState(0);
  const [springs, api] = useSpring(() => ({
    from: { x: 0 },
  }));
  const [dataExercise, setDataExercise] = React.useState<any>([]);
  const [selectedIndex, setSelectedIndex] = React.useState("");
  const { fetchQuiz } = useGetQuizPreliminary();
  const { data: dataQuiz } = fetchQuiz;
  // console.log("dataQuiz", dataQuiz);

  const [dataQuizRegenerate, setDataQuizRegenerate] = React.useState({} as any);

  const { mutationQuery: mutationQuerySubmit, handleSubmitQuiz } =
    useSubmitLevelupQuiz();
  const { data: dataSubmit, isPending: isLoadingSubmit } = mutationQuerySubmit;
  const [isModalLevelUp, setIsModalLevelup] = React.useState<boolean>(false);

  const onChangeRadio = (e: RadioChangeEvent) => {
    dataExercise[indexSoal]?.options?.map((key: any) => {
      if (e.target.value == key.option) {
        dataExercise[indexSoal].answer = key.option;
        setSelectedIndex(e.target.value);
      }
    });
  };

  const handleClick = (value: any) => {
    setSelectedIndex("");
    if (value === "previous") {
      let resulIndex = indexSoal === 0 ? indexSoal : indexSoal - 1;
      setIndexSoal(resulIndex);
      dataExercise.map((key: any, ind: any) => {
        if (ind == resulIndex) {
          setSelectedIndex(key.answer);
        }
      });
      if (indexSoal === 0) {
        return;
      }
      if (indexSoal === dataExercise.length - 1) {
        animatedToLeft(50);
      }
      if (indexSoal === 1) {
        animatedToLeft(50);
      }
    }

    if (value === "next") {
      let resulIndex =
        indexSoal === dataExercise.length - 1 ? indexSoal : indexSoal + 1;
      setIndexSoal(resulIndex);
      dataExercise.map((key: any, ind: any) => {
        if (ind == resulIndex) {
          setSelectedIndex(key.answer);
        }
      });
      if (indexSoal === dataExercise.length - 1) {
        return;
      }
      if (indexSoal === 0) {
        animatedToRight(5);
      }
      if (indexSoal === dataExercise.length - 2) {
        animatedToRight(5);
      }
    }
  };

  const handleSubmit = () => {
    let answer = {} as any;
    dataExercise.map((item: any) => {
      answer[item.id] = item.answer;
    });
    handleSubmitQuiz(dataQuizRegenerate.id, answer);
  };

  React.useEffect(() => {
    if (dataSubmit) {
      sleep(1000).then(() => setIsModalLevelup(!isModalLevelUp));
    }
  }, [dataSubmit]);

  React.useEffect(() => {
    if (dataQuiz && Object.keys(dataQuiz).length > 0) {
      setDataQuizRegenerate(dataQuiz);
      let dataTemp = dataQuiz as any;
      dataTemp.questions.map((item: any) => {
        item.answer = "";
        return item;
      });
      setDataExercise(dataTemp.questions);
    }
  }, [dataQuiz]);

  const animatedToRight = (value: any) => {
    api.start({
      from: {
        x: 0,
      },
      to: {
        x: value,
      },
    });
  };

  const animatedToLeft = (value: any) => {
    api.start({
      from: {
        x: value,
      },
      to: {
        x: 0,
      },
    });
  };

  return (
    <Space direction="vertical" style={{ display: "flex", marginTop: "20px" }}>
      <div
        className="container-updateProfile"
        style={{
          // backgroundImage: `url(${bgHero})`,
          backgroundSize: "cover",
          display: "flex",
          flexDirection: "column",
          flex: 1,
          height: "100%",
          width: "100%",
          alignItems: "flex-start",
          justifyContent: "center",
          backgroundColor: "#ffffff",
        }}
      >
        <div className="container-preliminary">
          <div className="container-top-preliminary">
            <div className="group-preliminary-text">
              <div className="text-title">{translate("PlacementTest")}</div>
              <div className="text-title-info">
                {translate("Desc_PlacementTest")}
              </div>
            </div>
            <div className="countDown-time"></div>
          </div>
          <div className="container-bottom-preliminary">
            {!dataQuiz && dataExercise.length === 0 && (
              <LoaderSpinGif size="large" />
            )}
            {dataQuiz && dataExercise.length === 0 && <NoData />}
            {dataExercise.length > 0 && (
              <div
                style={{ display: "flex" }}
                className="text-preliminary-exercise"
                dangerouslySetInnerHTML={{
                  __html: `${indexSoal + 1}.&nbsp;${
                    dataExercise[indexSoal]?.question
                  }`,
                }}
              />
            )}
            {dataExercise[indexSoal]?.options && (
              <Radio.Group onChange={onChangeRadio} value={selectedIndex}>
                <Space direction="vertical">
                  {dataExercise[indexSoal]?.options.map(
                    (key: any, value: any) => {
                      return (
                        <Radio
                          className="textfs18-fw400-black"
                          value={key.option}
                        >
                          <div
                            style={{ display: "flex", paddingTop: 15 }}
                            dangerouslySetInnerHTML={{
                              __html: `${key.value}`,
                            }}
                          />
                        </Radio>
                      );
                    }
                  )}
                </Space>
              </Radio.Group>
            )}
            <Row
              wrap
              style={{
                marginTop: 20,
                width: "80vw",
              }}
              justify={"center"}
            >
              {indexSoal > 0 && (
                <Col
                  onClick={() => handleClick("previous")}
                  className="btnNext-preliminary"
                >
                  <LeftOutlined style={{ color: "#9f0e0f", fontSize: 20 }} />
                </Col>
              )}
              <Col className="btnDropdown-scroll">{indexSoal + 1}</Col>
              {indexSoal !== dataExercise.length - 1 && (
                <Col
                  onClick={() => handleClick("next")}
                  className="btnNext-preliminary"
                >
                  <RightOutlined style={{ color: "#9f0e0f", fontSize: 20 }} />
                </Col>
              )}
              {indexSoal === dataExercise.length - 1 && (
                <Col>
                  {arrayContainsEmptyString(dataExercise) ? (
                    <Button
                      type="primary"
                      style={{
                        backgroundColor: "gray",
                        color: "white",
                        height: 55,
                      }}
                      block
                      spellCheck={true}
                      loading={isLoadingSubmit}
                      htmlType="submit"
                      disabled
                    >
                      {translate("Submit")}
                    </Button>
                  ) : (
                    <Button
                      type="primary"
                      style={{ backgroundColor: "#CB0B0C" }}
                      block
                      spellCheck={true}
                      loading={isLoadingSubmit}
                      htmlType="submit"
                      onClick={handleSubmit}
                    >
                      {translate("Submit")}
                    </Button>
                  )}
                </Col>
              )}
            </Row>
          </div>
        </div>
      </div>

      {/* Modal Score */}
      <ModalComponent
        centered
        title={translate("LabelResultTest")}
        open={isModalLevelUp}
        width={800}
        onCancel={() => {
          setIsModalLevelup(false);
          navigate.push("/");
        }}
        footer={[
          <Flex justify="center">
            <Button
              type="primary"
              onClick={() => {
                setIsModalLevelup(false);
                navigate.push("/");
              }}
            >
              {translate("Save")}
            </Button>
          </Flex>,
        ]}
      >
        <ScoreComponent
          desc={translate("DescResultPlacement")}
          // desc2={`${translate("Score")} : ${dataScore.score}`}
        />
      </ModalComponent>
    </Space>
  );
};

export default PreliminaryPage;
