import BackgroundHero from "@/components/BackgroundHero";
import FormButton from "@/components/FormButton";
import TextResponsive from "@/components/TextResponsive";
import { USER } from "@/shared/constants/storageStatis";
import bgHero from "@/shared/images/icon/Hero_Section.webp";
import imageDummyCard from "@/shared/images/icon/image_dummy.webp";
import iconCalendar from "@/shared/images/svg/ic_calendar_month.svg";
import iconCalendarRed from "@/shared/images/svg/ic_red_calendar_month.svg";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { Card, Col, Flex, Row, Space, Spin, Tabs, TabsProps } from "antd";
import moment from "moment";
import Image from "next/image";
import { useRouter } from "next/router";
import { MenuInfo } from "rc-menu/lib/interface";
import React from "react";

type dateArray = {
  date: string;
}[];

const customDate = [
  {
    id: 0,
    day: "Sun",
  },
  {
    id: 1,
    day: "Mon",
  },
  {
    id: 2,
    day: "Tue",
  },
  {
    id: 3,
    day: "Wed",
  },
  {
    id: 4,
    day: "Thu",
  },
  {
    id: 5,
    day: "Fri",
  },
  {
    id: 6,
    day: "Sat",
  },
];

export default function ClassScheduleDetailPage() {
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const { id } = navigate.query;

  const [formData, setFormData] = React.useState<any>({
    materi: "",
    level: "",
    topik: "",
  });
  const [introduction, setIntroduction] = React.useState<boolean>(false);
  const [understanding, setUnderstanding] = React.useState<boolean>(false);
  const [mainTopic, setMainTopic] = React.useState<boolean>(false);
  const [quiz, setQuiz] = React.useState<boolean>(false);
  const [dateWeeks, setDateWeeks] = React.useState<dateArray>([]);

  React.useEffect(() => {
    let tempArray: {
      date: string;
      day: string;
    }[] = [];
    customDate.map((item) => {
      tempArray.push({
        date: moment().day(item.id).format("DD"),
        day: item.day,
      });
    });

    setDateWeeks(tempArray);
  }, []);

  const video = {
    url: "https://www.youtube.com/watch?v=ZOVtvj-CFeM",
    width: "400px",
    height: "200px",
  };

  const dataDummy2 = {
    id: 1,
    name: "Rusli Wanasuria",
    section: "3",
    totalCourse: "28",
    seeing: 5,
    totalHours: "3 hours, 20 min",
    date: "22 Oct 2023 09:12",
    title: "Introduction Stock Part 1",
    description:
      "Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...",
    image: imageDummyCard,
    level: "Beginner",
    owner: "IDX Stock",
    likes: "12",
    student: "1500",
    comments: "10",
    shared: "5",
    updateAt: "2024-01-26",
    lesson: "10",
    certificate: "Digital Certificate",
    tag: [
      {
        id: 1,
        name: "Management",
      },
      {
        id: 2,
        name: "Stock",
      },
      {
        id: 3,
        name: "Business",
      },
    ],
  };

  const handleChangeDropdown = (event: MenuInfo, target: string) => {
    setFormData(() => ({
      ...formData,
      [target]: event.key,
    }));
  };

  const handleCourse = () => {
    console.log("clicked");
  };

  const onChange = (key: string) => {
    console.log(key);
  };

  const items: TabsProps["items"] = [
    {
      key: "1",
      label: "Description",
      children: "",
    },
    {
      key: "2",
      label: "Reviews",
      children: "",
    },
  ];

  const handleToggleCollapse = (
    e: React.MouseEvent<HTMLDivElement>,
    value: string
  ) => {
    console.log("clicked", value);
    switch (value) {
      case "A":
        setIntroduction(!introduction);
        break;
      case "B":
        setUnderstanding(!understanding);
        break;
      case "C":
        setMainTopic(!mainTopic);
        break;
      case "D":
        setQuiz(!quiz);
        break;

      default:
        return;
    }
  };

  return (
    <Space direction="vertical" className="hero-container-backgroundImg">
      <Row
        style={{
          borderBottomLeftRadius: 40,
          borderBottomRightRadius: 40,
          paddingTop: 20,
          paddingBottom: 30,
          paddingLeft: 70,
          paddingRight: 70,
          position: "relative",
          overflow: "hidden",
        }}
        gutter={[16, 16]}
        align={"middle"}
        className="hero-container-antd"
      >
        <BackgroundHero />
        <Col span={12}>
          <TextResponsive
            bigScreen={60}
            mobile={24}
            color="white"
            fontWeight={700}
            isSpan
            desktop={45}
          >
            "PT XXXX XXX XX"
          </TextResponsive>
        </Col>
        <Col span={12}>
          <TextResponsive
            mobile={14}
            color="white"
            fontWeight={400}
            isSpan
            desktop={16}
          >
            Lorem ipsum dolor sit amet consectetur. Auctor imperdiet tincidunt ipsum tellus dignissim tristique eget blandit. Commodo aenean risus nulla est.
          </TextResponsive>
        </Col>
      </Row>
      <Tabs
        className="class-schedule-detail-tabs"
        style={{ margin: "0px 89px" }}
        defaultActiveKey="1"
        items={items}
        onChange={onChange}
        indicator={{ size: (origin) => origin + 30, align: "center" }}
      />
      <Space
        direction="horizontal"
        className="container-card-detailClassSchedule"
      >
        <Card bordered={false} className="card-course-detail">
          <span className="text-top-card-detail">
            {"Pelajari Profil dan Keunggulan PT. XXXX XXX XX"}
          </span>
          <div className="image-card-courseDetail">
            <Image src={imageDummyCard} alt="image-dummy-card" />
          </div>
          <div style={{ height: "40px" }} />
          <span className="textfs24-fw700-black">{"Company Profile"}</span>
          <div style={{ height: "16px" }} />
          <span className="text-title-card">{"Vision"}</span>
          <div style={{ height: "16px" }} />
          <div className="text-description-courseDetail">
            {
              "Lorem ipsum dolor sit amet consectetur. Lacus et gravida sem lectus ipsum vitae porttitor. Dui quis leo nulla massa felis habitant interdum vitae. Gravida consectetur vulputate sagittis rhoncus. Nisi amet tellus semper elit arcu. Ac lacus quis morbi amet facilisis in quam urna. Donec vitae turpis lorem et egestas. Sed nibh nulla cursus sit eget gravida. Sed tortor pretium maecenas proin curabitur. Pellentesque suspendisse nibh ornare metus ultrices eget purus. Lectus blandit molestie eget tristique diam nec dui quis. Mauris diam sem consequat vitae. Elit sapien leo vitae enim pellentesque et aliquam. Velit auctor lacus viverra imperdiet. Blandit est sed in interdum. Semper nibh purus id netus."
            }
          </div>
          <div style={{ height: "16px" }} />
          <span className="text-title-card">{"Mission"}</span>
          <div style={{ height: "16px" }} />
          <div className="text-description-courseDetail">
            {
              "Lorem ipsum dolor sit amet consectetur. Lacus et gravida sem lectus ipsum vitae porttitor. Dui quis leo nulla massa felis habitant interdum vitae. Gravida consectetur vulputate sagittis rhoncus. Nisi amet tellus semper elit arcu. Ac lacus quis morbi amet facilisis in quam urna. Donec vitae turpis lorem et egestas. Sed nibh nulla cursus sit eget gravida. Sed tortor pretium maecenas proin curabitur. Pellentesque suspendisse nibh ornare metus ultrices eget purus. Lectus blandit molestie eget tristique diam nec dui quis. Mauris diam sem consequat vitae. Elit sapien leo vitae enim pellentesque et aliquam. Velit auctor lacus viverra imperdiet. Blandit est sed in interdum. Semper nibh purus id netus."
            }
          </div>
          <div style={{ height: "40px" }} />
          <span className="text-title-card">
            {"Unique Selling Proposition"}
          </span>
          <div style={{ height: "16px" }} />
          <div className="text-description-courseDetail">
            {
              "Lorem ipsum dolor sit amet consectetur. Lacus et gravida sem lectus ipsum vitae porttitor. Dui quis leo nulla massa felis habitant interdum vitae. Gravida consectetur vulputate sagittis rhoncus. Nisi amet tellus semper elit arcu. Ac lacus quis morbi amet facilisis in quam urna. Donec vitae turpis lorem et egestas. Sed nibh nulla cursus sit eget gravida. Sed tortor pretium maecenas proin curabitur. Pellentesque suspendisse nibh ornare metus ultrices eget purus. Lectus blandit molestie eget tristique diam nec dui quis. Mauris diam sem consequat vitae. Elit sapien leo vitae enim pellentesque et aliquam. Velit auctor lacus viverra imperdiet. Blandit est sed in interdum. Semper nibh purus id netus."
            }
          </div>
          <div style={{ height: "40px" }} />
          <Flex justify="end">
            <FormButton
              styleButton={{ width: 202, marginTop: 44 }}
              onClick={handleCourse}
              title={"Start Course"}
              type={"submit"}
              classButton={"button-course-card"}
            />
          </Flex>
        </Card>
        <Space direction="vertical" className="space-card-courseDetail">
          <Card bordered={false} className="container-card-right">
            <Space direction="vertical">
              <Space
                direction="horizontal"
                className="container-card-right-detailClassSchedule"
              >
                <span className="textfs20-fw700-black">{"Schedule"}</span>
                <Image src={iconCalendar} alt="calendar-card" />
              </Space>
              <div style={{ height: "16px" }} />
              <Space
                direction="horizontal"
                className="container-card-rightSub-detailClassSchedule"
              >
                {dateWeeks.map((date: any, i) => (
                  <Space direction="vertical">
                    <span className="text-left-card-detailCourse">
                      {date.day}
                    </span>
                    <span
                      className={
                        moment().format("DD") === date.date
                          ? "text-leftRed-card-detailCourse textfs14-fw400-white"
                          : "text-left-card-detailCourse"
                      }
                    >
                      {date.date}
                    </span>
                  </Space>
                ))}
              </Space>
            </Space>
          </Card>
          <Card bordered={false} className="container-card-right">
            <Space className="card-courseDetail-collapse-header">
              <span className="textfs20-fw700-gray">{"Upcoming Class"}</span>
              <span className="textfs14-fw400-gray">{"View All"}</span>
            </Space>
            {[1, 2, 3, 4, 5, 6].map((val) => (
              <>
                <hr className="my-hr-line" />
                <Space direction="horizontal">
                  <Image
                    src={imageDummyCard}
                    alt="icon-calendar-course"
                    className="width-image-detailClassSchedule"
                  />
                  <Space direction="vertical">
                    <span className="textfs16-fw700-gray">
                      {"Lorem ipsum dolor sit amet"}
                    </span>
                    <Space direction="horizontal">
                      <Image src={iconCalendarRed} alt="icon-calendar-course" />
                      <span className="textfs14-fw400-gray">
                        {"Friday, 02 February  2024"}
                      </span>
                    </Space>
                  </Space>
                </Space>
              </>
            ))}
          </Card>
        </Space>
      </Space>
    </Space>
  );
}
