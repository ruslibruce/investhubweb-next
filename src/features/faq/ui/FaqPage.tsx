import LoaderSpinGif from "@/components/LoaderSpinGif";
import NoData from "@/components/NoData";
import PaginationComponent from "@/shared/components/PaginationComponent";
import imageFaq from "@/shared/images/faq/faq-image.png";
import { paramsToString } from "@/shared/utils/string";
import {
  CloseOutlined,
  PlusOutlined,
  SearchOutlined
} from "@ant-design/icons";
import { Collapse, Input } from "antd";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import React, { useState } from "react";
import useGetFaq from "../hooks/useGetFaq";

export default function FaqPage() {
  const { t: translate } = useTranslation();
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [search, setSearch] = useState<string>("");
  const [searchApi, setSearchApi] = useState<string>("");
  const { fetchQuery } = useGetFaq(
    {},
    paramsToString({ page: currentPage, question: searchApi })
  );
  const { data: dataResultFaq, isLoading } = fetchQuery;
  const { data: dataFaq } = dataResultFaq;

  React.useEffect(() => {
    if (search.length > 2) {
      setSearchApi(search);
    } else {
      setSearchApi("");
    }
  }, [search]);

  return (
    <div className="hero-container-background">
      <div className="faq-header">
        <div className="faq-title">
          <b
            style={{
              fontWeight: "bold",
            }}
          >
            {translate("Faq")}
          </b>
        </div>
        <div className="faq-search">
          <Input
            prefix={<SearchOutlined style={{ marginRight: 8 }} />}
            value={search}
            suffix={
              search.length > 0 && (
                <CloseOutlined onClick={() => setSearch("")} />
              )
            }
            placeholder={`${translate("FindFAQ")}`}
            onChange={(e) => setSearch(e.target.value)}
          />
        </div>
      </div>
      {!dataFaq && (
        <div className="faq-content">
          <div className="faq-questions">
            <LoaderSpinGif size="large" />
          </div>
        </div>
      )}
      {dataFaq && dataFaq.length === 0 && <NoData />}
      {dataFaq && dataFaq.length > 0 && (
        <>
          <div className="faq-content">
            <div className="faq-questions">
              {dataFaq.map((faqItem: any, index: any) => (
                <Collapse
                  key={index}
                  style={{ marginTop: 28 }}
                  expandIconPosition="end"
                  expandIcon={({ isActive }) => (
                    <PlusOutlined rotate={isActive ? 90 : 0} />
                  )}
                  items={[
                    {
                      key: faqItem.id,
                      label: faqItem.question,
                      children: <p>{faqItem.answer}</p>,
                    },
                  ]}
                />
              ))}
            </div>
            <div className="faq-question-image">
              <Image src={imageFaq} alt="image-faq" />
              <p style={{ fontSize: 28, fontWeight: "bold" }}>
                {translate("FrequentlyAskedQuestion")}
              </p>
              <p>{translate("WhatAsk")}</p>
            </div>
          </div>
          <PaginationComponent
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            total={dataResultFaq.records}
            limit={dataResultFaq.limit}
          />
        </>
      )}
    </div>
  );
}
