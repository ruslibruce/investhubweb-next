import { fetchFaq } from "@/shared/api/fetch/faq";
import { USER } from "@/shared/constants/storageStatis";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchFaq();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetFaq = (initialData?: any, params?: string) => {
  const dataUser = storageCheck(USER);
  const fetchDataFaq = dataUser.isLogin
    ? fetchFaq(params)
    : { key: [], api: () => Promise.resolve(initialData || {}) };

  const fetchQuery = useFetchHook({
    keys: [fetchDataFaq.key, params],
    api: fetchDataFaq.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetFaq;
