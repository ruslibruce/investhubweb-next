import LoaderSpinGif from "@/components/LoaderSpinGif";
import NoData from "@/components/NoData";
import { USER } from "@/shared/constants/storageStatis";
import IconMyCertificate from "@/shared/images/icon/icon_my_certificate.webp";
import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getStatusColor } from "@/shared/utils/helper";
import { Breadcrumb, Button, Space } from "antd";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import useGetCertificate from "./hooks/useGetCertificate";

export default function CertificatePage() {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const dataUser = storageCheck(USER);
  const { fetchCertificate } = useGetCertificate({});
  const { isLoading, data: dataResultCertificate } = fetchCertificate;
  const { data: dataCertificate } = dataResultCertificate;
  // console.log(
  //   "dataCertificate",
  //   JSON.stringify(dataResultCertificate, null, 2)
  // );

  React.useEffect(() => {
    if (!dataUser.isLogin) {
      navigate.back();
    }
  }, [dataUser]);

  return (
    <div className="hero-container-background">
      <Breadcrumb
        separator=">"
        style={{ paddingLeft: 80, paddingTop: 170 }}
        items={[
          {
            href: "",
            title: (
              <Image
                src={iconBreadcrumb}
                alt="Breadcrumb Icon"
                style={{ paddingBottom: 5 }}
              />
            ),
          },
          {
            title: (
              <span className="textfs16-fw400-red">
                {translate("MyCertificate")}
              </span>
            ),
          },
        ]}
      />
      <div className="collection-header">
        <span className="textfs36-fw700-black">
          {translate("MyCertificate")}
        </span>
      </div>
      <div className="collection-main" style={{ paddingBottom: 70 }}>
        {!dataCertificate && <LoaderSpinGif size="large" />}
        {dataCertificate?.length === 0 && <NoData />}
        {dataCertificate?.length > 0 &&
          dataCertificate?.map((item: any) => (
            <div className="collection-main-content" key={item.id}>
              <Space style={{ gap: 10 }}>
                <div className="collection-bookmark-icon">
                  <Image
                    width={40}
                    height={40}
                    src={IconMyCertificate}
                    alt="icon-my-certificate"
                  />
                </div>
                <Space direction="vertical">
                  <span className="textfs18-fw600-black">{item.provider}</span>
                  <span className="textfs16-fw400-black">{item.title}</span>
                </Space>
              </Space>
              <Space>
                <Button
                  disabled
                  style={{
                    backgroundColor: !item.is_verified
                      ? getStatusColor("pending")
                      : item.is_approved
                      ? getStatusColor("published")
                      : getStatusColor("rejected"),
                    color: "white",
                  }}
                >
                  {!item.is_verified
                    ? translate("Pending")
                    : item.is_approved
                    ? translate("Approved")
                    : translate("Rejected")}
                </Button>
              </Space>
            </div>
          ))}
      </div>
    </div>
  );
}
