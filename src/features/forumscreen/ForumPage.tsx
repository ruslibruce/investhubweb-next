import FormButton from "@/components/FormButton";
import { USER } from "@/shared/constants/storageStatis";
import iconShareCard from "@/shared/images/icon/icon_share_card.webp";
import imageDummyCard from "@/shared/images/icon/image_dummy.webp";
import iconComment from "@/shared/images/svg/ic_comment_black.svg";
import iconHomeForum from "@/shared/images/svg/ic_home_black.svg";
import iconHomeForumRed from "@/shared/images/svg/ic_home_red.svg";
import iconSearch from "@/shared/images/svg/ic_search_black.svg";
import iconSearchWhite from "@/shared/images/svg/ic_search_white.svg";
import iconSetting from "@/shared/images/svg/ic_settings_black.svg";
import iconStudent from "@/shared/images/svg/ic_student_black.svg";
import iconStudentWhite from "@/shared/images/svg/ic_student_white.svg";
import iconThreeDotHorizontal from "@/shared/images/svg/ic_three_dot_horizontal.svg";
import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { LikeOutlined, PlusOutlined, UserOutlined } from "@ant-design/icons";
import {
  Avatar,
  Breadcrumb,
  Card,
  Col,
  Divider,
  Row,
  Space,
  Tabs,
  TabsProps,
  Tooltip,
} from "antd";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import IconDot from "@/shared/images/svg/icon_dot.svg";

export default function ForumPage() {
  const { t: translate } = useTranslation();
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const [selected, setSelected] = React.useState("Home");
  const [forumDetail, setForumDetail] = React.useState(false);

  const onChange = (key: string) => {
    console.log("onChange", key);
  };

  const items: TabsProps["items"] = [
    {
      key: "1",
      label: "Discussion",
      children: "",
    },
    {
      key: "2",
      label: "User",
      children: "",
    },
    {
      key: "3",
      label: "Events",
      children: "",
    },
    {
      key: "4",
      label: "Media",
      children: "",
    },
  ];

  return (
    <div style={{ paddingTop: 20 }} className="hero-container-background">
      <Space
        direction="vertical"
        className="container-breadcumb-title-investment"
      >
        <Breadcrumb
          items={[
            {
              href: "",
              title: <Image src={iconBreadcrumb} alt="image-investment" />,
            },
            {
              href: "",
              title: translate("Forum"),
            },
          ]}
        />
        <div style={{ height: 8 }} />
        <Space>
          <div className="textfs26-fw700-black">{translate("Forum")}</div>
        </Space>
        <div style={{ height: 34 }} />
        <Space>
          <div className="textfs20-fw400-black">New Activity</div>
        </Space>
        <div style={{ height: 32 }} />
      </Space>
      <Space
        direction="horizontal"
        className="container-card-detailClassSchedule"
      >
        {selected === "Home" && (
          <Space direction="vertical">
            {[1, 2, 3].map((item) => (
              <Card bordered={false} className="card-course-detail">
                <div className="container-forum">
                  <Space
                    direction="horizontal"
                    className="container-leftCard-forum"
                  >
                    <div className="image-card-forum">
                      <Image
                        src={imageDummyCard}
                        objectFit="cover"
                        alt="image-dummy-card"
                      />
                    </div>
                    <div className="container-title-card-forum">
                      <span className="textfs18-fw600-black">Forum Name 1</span>
                      <div style={{ display: "flex" }}>
                        <span className="textfs14-fw400-gray">User Group </span>
                        <Image src={IconDot} alt="icon-dot" />
                        <span className="textfs14-fw400-gray"> 20 hour</span>
                      </div>
                    </div>
                  </Space>
                  <Space>
                    <Image
                      src={iconThreeDotHorizontal}
                      alt="image-threedot-card"
                    />
                  </Space>
                </div>
                <span className="textfs18-fw400-black">
                  {
                    "Lorem ipsum dolor sit amet consectetur. Lacus et gravida sem lectus ipsum vitae porttitor. Dui quis leo nulla massa felis habitant interdum vitae. Gravida consectetur vulputate sagittis rhoncus. Nisi amet tellus semper elit arcu."
                  }
                </span>
                <div className="image-card-courseDetail">
                  <Image src={imageDummyCard} alt="image-dummy-card" />
                </div>
                <div style={{ height: "16px" }} />
                <hr className="my-hr-line-forum" />
                <Space
                  direction="horizontal"
                  className="container-buttonCard-forum"
                >
                  <div className="button-card-forum">
                    <LikeOutlined />
                    <span className="textfs10-fw400-gray">Like</span>
                  </div>
                  <div className="button-card-forum">
                    <Image src={iconComment} alt="image-dummy-card" />
                    <span className="textfs10-fw400-gray">Comment</span>
                  </div>
                  <div className="button-card-forum">
                    <Image src={iconShareCard} alt="share-card" />
                    <span className="textfs10-fw400-gray">Share</span>
                  </div>
                </Space>
                <hr className="my-hr-line-forum" />
              </Card>
            ))}
          </Space>
        )}
        {selected === "FindForum" && !forumDetail && (
          <Row gutter={12}>
            {[1, 2, 3, 4, 5, 6].map((item: any, index: any) => {
              return (
                <Col key={item.id} xs={24} sm={24} md={12} xl={12}>
                  x{" "}
                  <Card
                    cover={<Image alt="example" src={imageDummyCard} />}
                    className="card-leftFindForum-forum"
                    bordered={false}
                  >
                    <span className="textfs16-fw600-black">Forum Name 1</span>
                    <div className="container-textIcon-FindForum">
                      <Image src={iconStudent} alt="icon-cardLeft-forum" />
                      <span className="textfs14-fw400-gray">User Group </span>
                      <Image src={IconDot} alt="icon-dot" />
                      <span className="textfs14-fw400-gray"> 20 hour</span>
                    </div>
                    <FormButton
                      styleButton={{ marginTop: 24 }}
                      onClick={() => setForumDetail(true)}
                      title={"Join Forum"}
                      type={"submit"}
                      hiddenIcon={true}
                      classButton={"button-forum-card"}
                    />
                  </Card>
                </Col>
              );
            })}
          </Row>
        )}

        {selected === "FindForum" && forumDetail && (
          <div>
            <Card
              cover={<Image alt="example" src={imageDummyCard} />}
              className="card-findforum-detail"
              bordered={false}
            >
              <div style={{ height: 40 }} />
              <span className="textfs16-fw600-black">Forum Name 1</span>
              <div className="container-textIcon-FindForum">
                <Image src={iconStudent} alt="icon-cardLeft-forum" />
                <span className="textfs14-fw400-gray">20 rb users</span>
              </div>
              <div style={{ height: 40 }} />
              <div className="container-avatar-button-findForum">
                <div>
                  <Avatar.Group
                    maxCount={3}
                    maxStyle={{ color: "#f56a00", backgroundColor: "#fde3cf" }}
                  >
                    <Avatar src="https://api.dicebear.com/7.x/miniavs/svg?seed=3" />
                    <Avatar style={{ backgroundColor: "#f56a00" }}>K</Avatar>
                    <Tooltip title="Ant User" placement="top">
                      <Avatar
                        style={{ backgroundColor: "#87d068" }}
                        icon={<UserOutlined />}
                      />
                    </Tooltip>
                    <Avatar
                      style={{ backgroundColor: "#1677ff" }}
                      icon={<UserOutlined />}
                    />
                  </Avatar.Group>
                </div>

                <div className="container-button-findForum">
                  <div className="button-invite-findForum">
                    <PlusOutlined style={{ color: "#fff" }} />
                    <span className="textfs16-fw600-white">Invite</span>
                  </div>
                  <div className="button-joined-findForum">
                    <Image src={iconStudent} alt="icon-cardLeft-forum" />
                    <span className="textfs16-fw600-black">Joined</span>
                  </div>
                </div>
              </div>
              <Divider />
              <Tabs
                defaultActiveKey="1"
                items={items}
                onChange={onChange}
                indicator={{ size: (origin) => origin + 30, align: "center" }}
              />
              {[1, 2].map((item, index) => (
                <Card bordered={false} className="card-course-findForum">
                  <div className="container-forum">
                    <Space
                      direction="horizontal"
                      className="container-leftCard-forum"
                    >
                      <div className="image-card-forum">
                        <Image
                          src={imageDummyCard}
                          objectFit="cover"
                          alt="image-dummy-card"
                        />
                      </div>
                      <div className="container-title-card-forum">
                        <span className="textfs18-fw600-black">Users 1</span>
                        <div>
                          <span className="textfs14-fw400-gray">3 hours</span>
                        </div>
                      </div>
                    </Space>
                    <Space>
                      <Image
                        src={iconThreeDotHorizontal}
                        alt="image-threedot-card"
                      />
                    </Space>
                  </div>
                  <span className="textfs18-fw400-black">
                    {
                      "Lorem ipsum dolor sit amet consectetur. Lacus et gravida sem lectus ipsum vitae porttitor. Dui quis leo nulla massa felis habitant interdum vitae. Gravida consectetur vulputate sagittis rhoncus. Nisi amet tellus semper elit arcu."
                    }
                  </span>
                  {index === 0 && (
                    <div className="image-card-courseDetail">
                      <Image src={imageDummyCard} alt="image-dummy-card" />
                    </div>
                  )}
                  <div style={{ height: "16px" }} />
                  <hr className="my-hr-line-forum" />
                  <Space
                    direction="horizontal"
                    className="container-buttonCard-forum"
                  >
                    <div className="button-card-forum">
                      <LikeOutlined />
                      <span className="textfs10-fw400-gray">Like</span>
                    </div>
                    <div className="button-card-forum">
                      <Image src={iconComment} alt="image-dummy-card" />
                      <span className="textfs10-fw400-gray">Comment</span>
                    </div>
                    <div className="button-card-forum">
                      <Image src={iconShareCard} alt="share-card" />
                      <span className="textfs10-fw400-gray">Share</span>
                    </div>
                  </Space>
                  <hr className="my-hr-line-forum" />
                </Card>
              ))}
            </Card>
          </div>
        )}
        <Space direction="vertical" className="space-card-courseDetail">
          <Card bordered={false} className="container-card-right">
            <Space className="card-courseDetail-collapse-header">
              <span
                style={{ color: "var(--Gray-1, #333)" }}
                className="textfs24-fw700-gray"
              >
                {"Forum"}
              </span>
              <div className="container-image-setting-forum">
                <Image src={iconSetting} alt="image-setting-card" />
              </div>
            </Space>
            <div style={{ height: 16 }} />
            <Space direction="vertical">
              <div
                onClick={() => setSelected("Home")}
                className={
                  selected === "Home"
                    ? "button-cardRight-forum-selected"
                    : "button-cardRight-forum"
                }
              >
                <div
                  className={
                    selected === "Home"
                      ? "container-icon-forum-selected"
                      : "container-icon-forum"
                  }
                >
                  <Image
                    src={selected === "Home" ? iconHomeForumRed : iconHomeForum}
                    alt="icon-home-forum"
                  />
                </div>
                <span className="textfs14-fw600-black">Home</span>
              </div>
              <div
                onClick={() => setSelected("FindForum")}
                className={
                  selected === "FindForum"
                    ? "button-cardRight-forum-selected"
                    : "button-cardRight-forum"
                }
              >
                <div
                  className={
                    selected === "FindForum"
                      ? "container-icon-forum-selected"
                      : "container-icon-forum"
                  }
                >
                  <Image
                    src={
                      selected === "FindForum" ? iconSearchWhite : iconSearch
                    }
                    className="icon_search_forum"
                    alt="icon-home-forum"
                  />
                </div>
                <span className="textfs14-fw600-black">Find Forum</span>
              </div>
              <div
                onClick={() => setSelected("MyForum")}
                className={
                  selected === "MyForum"
                    ? "button-cardRight-forum-selected"
                    : "button-cardRight-forum"
                }
              >
                <div
                  className={
                    selected === "MyForum"
                      ? "container-icon-forum-selected"
                      : "container-icon-forum"
                  }
                >
                  <Image
                    objectFit="cover"
                    src={
                      selected === "MyForum" ? iconStudentWhite : iconStudent
                    }
                    alt="icon-home-forum"
                  />
                </div>
                <span className="textfs14-fw600-black">My Forum</span>
              </div>
            </Space>
            <div style={{ height: 16 }} />
            <hr className="my-hr-line-forum" />
            <div style={{ height: 24 }} />
            <Space className="card-courseDetail-collapse-header">
              <span className="textfs20-fw600-gray">
                {"You joined the forum"}
              </span>
              <span className="textfs14-fw400-gray">{"View All"}</span>
            </Space>
            <div style={{ height: 24 }} />
            <hr className="my-hr-line-forum" />
            <Space direction="vertical">
              {[1, 2, 3, 4, 5, 6].map((val) => (
                <>
                  <Space direction="horizontal">
                    <Image
                      src={imageDummyCard}
                      alt="icon-calendar-course"
                      className="width-image-detailClassSchedule"
                    />
                    <div className="container-textCardRight-forum">
                      <span className="textfs16-fw700-gray">
                        {"Forum Name 1"}
                      </span>
                      <span className="textfs14-fw400-gray">
                        {"Last active 20 hours ago "}
                      </span>
                    </div>
                  </Space>
                </>
              ))}
            </Space>
          </Card>
        </Space>
      </Space>
    </div>
  );
}
