import FormButton from "@/components/FormButton";
import FormDropdown from "@/components/FormDropdown";
import NoData from "@/components/NoData";
import { USER } from "@/shared/constants/storageStatis";
import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { Breadcrumb, Card, Col, Flex, Row, Spin } from "antd";
import Image from "next/image";
import { useRouter } from "next/router";
import { MenuInfo } from "rc-menu/lib/interface";
import React from "react";
import useGetInvestment from "../hooks/useGetInvestment";
import useGetInvestmentCategory from "../hooks/useGetInvestmentCategory";
import { useTranslation } from "next-i18next";
import LoaderSpinGif from "@/components/LoaderSpinGif";

export default function InvestmentPartnerPage() {
  const { t: translate } = useTranslation();
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const [formData, setFormData] = React.useState<any>({
    materi: "",
    level: "",
    topik: "",
  });
  const [params, setParams] = React.useState("");
  const { fetchQuery } = useGetInvestment(params);
  const { fetchQuery: fetchCategory } = useGetInvestmentCategory([]);
  const { isLoading, data: dataInvestment } = fetchQuery;
  const { isLoading: isLoadingCategory, data: dataCategory } = fetchCategory;

  const handleChangeDropdown = (event: MenuInfo, target: string) => {
    dataCategory.map((item: any) => {
      if (item.key == event.key) {
        setFormData(() => ({
          ...formData,
          [target]: item.name,
        }));
      }
    });
    if (event.key === "0") {
      setParams("");
      return;
    }
    setParams(event.key);
  };

  return (
    <div className="hero-container-background">
      <Breadcrumb
        separator=">"
        style={{ paddingLeft: 80, paddingTop: 170 }}
        items={[
          {
            href: "",
            title: (
              <>
                <Image src={iconBreadcrumb} alt="Breadcrumb Icon" />
              </>
            ),
          },
          {
            title: (
              <span className="textfs16-fw400-red">Investment Partner</span>
            ),
          },
        ]}
      />
      <Flex
        style={{
          margin: window.innerWidth > 765 ? "40px 72px" : "20px 30px",
        }}
      >
        <FormDropdown
          placeholder={translate("FilterCategory")}
          value={formData.level}
          data={dataCategory}
          onClick={(e) => handleChangeDropdown(e, "level")}
          isRequired={false}
          styleForm={{ width: window.innerWidth > 765 ? 350 : "100vw" }}
        />
      </Flex>
      {!dataInvestment && (
        <Row
          gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}
          className="container-card-course"
        >
          <LoaderSpinGif size="large" />
        </Row>
      )}
      {dataInvestment?.length === 0 && <NoData />}
      {dataInvestment?.length > 0 && (
        <Row
          gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}
          className="container-card-course"
        >
          {dataInvestment?.map((item: any) => {
            return (
              <Col key={item.id} xs={24} sm={12} md={8} xl={6}>
                <Card
                  key={item.id}
                  cover={
                    <Image
                      alt="card-investment"
                      src={item.cover_image}
                      width={100}
                      height={100}
                      style={{ height: 200, padding: 20 }}
                    />
                  }
                  bordered={false}
                  className="card-course-investment"
                >
                  <div style={{ height: "16px" }} />
                  <span className="textfs14-fw600-black">{item.name}</span>
                  {/* <div className="container-description-investment">
                                            <span className="textfs10-fw400-gray">
                                                {item.description}
                                            </span>
                                            <LuDot />
                                            <span className="textfs10-fw400-gray">
                                                {item.description2}
                                            </span>
                                        </div> */}
                  <FormButton
                    onClick={() => window.open(item.url, "_blank")}
                    title={"Open Account"}
                    type={"submit"}
                    classButton={"button-investment"}
                    hiddenIcon={true}
                    styleButton={{ marginTop: 10 }}
                  />
                </Card>
              </Col>
            );
          })}
        </Row>
      )}
    </div>
  );
}
