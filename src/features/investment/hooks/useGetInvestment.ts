import { fetchInvestment } from "@/shared/api/fetch/investment";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { paramsToString } from "@/shared/utils/string";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchInvestment();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetInvestment = (value = "", initialData?: any) => {
  const fetchDataInvestment = fetchInvestment(
    paramsToString({
      category: value,
    })
  );

  const fetchQuery = useFetchHook({
    keys: [fetchDataInvestment.key, value],
    api: fetchDataInvestment.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetInvestment;
