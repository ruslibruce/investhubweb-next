import { resendMail } from "@/shared/api/mutation/resendMail";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";
import { useTranslation } from "next-i18next";

const usePostResendEmail = () => {
  const queryClient = useQueryClient();
  const { t: translate } = useTranslation();

  const mutationQuery = useMutationHook({
    api: resendMail,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        if (data) {
          notification.success({
            message: translate("EmailResent"),
          });
        }
      },
    },
  });

  const handleResendEmail = (data?: {}) => {
    mutationQuery.mutate(data);
  };

  return {
    mutationQuery,
    handleResendEmail,
  };
};

export default usePostResendEmail;
