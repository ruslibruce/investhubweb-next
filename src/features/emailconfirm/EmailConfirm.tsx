import FooterAuth from "@/components/FooterAuth";
import FormButton from "@/components/FormButton";
import Modal from "@/components/Modal";
import ButtonFlag from "@/shared/components/layout/auth-layout/sider/ButtonFlag";
import { DASHBOARD_EMAIL_CHANGE } from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import IconEmailConfirm from "@/shared/images/icon/icon_emailConfirm.webp";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { notification, Space } from "antd";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import usePostResendEmail from "./hooks/usePostResendEmail";
import ReCAPTCHA from "react-google-recaptcha";

function EmailConfirm() {
  const navigate = useRouter();
  const { t: translate } = useTranslation();
  const [modalVisible, setModalVisible] = React.useState<boolean>(false);
  const dataUser = storageCheck(USER);
  const { handleResendEmail, mutationQuery } = usePostResendEmail();
  // console.log("dataUser email confirm", dataUser);
  const { isSuccess } = mutationQuery;
  const [timeLeft, setTimeLeft] = React.useState<number>(300);
  const [isTimerRun, setIsTimerRun] = React.useState<boolean>(false);
  const [isSuccessPopUp, setIsSuccessPopUp] = React.useState<boolean>(false);
  const [token, setToken] = React.useState<any>("");
  const captchaRef = React.useRef<any>(null);

  // Convert timeLeft into minutes and seconds
  const minutes = Math.floor(timeLeft / 60);
  const seconds = timeLeft % 60;

  React.useEffect(() => {
    if (isSuccess) {
      setIsSuccessPopUp(true);
    }
  }, [isSuccess]);

  React.useEffect(() => {
    setTimeout(() => {
      setIsSuccessPopUp(false);
    }, 3000);
  }, [isSuccessPopUp]);

  const handleHiddenModal = () => {
    setModalVisible(!modalVisible);
  };

  React.useEffect(() => {
    if (!isTimerRun) {
      return;
    }
    // Exit early when we reach 0
    if (timeLeft === 0) {
      setIsTimerRun(false);
      setTimeLeft(300);
      return;
    }

    // Save intervalId to clear the interval when the component re-renders
    const intervalId = setInterval(() => {
      // Decrease time left by one second
      setTimeLeft(timeLeft - 1);
    }, 1000);

    // Clear interval on re-render to avoid memory leaks
    return () => clearInterval(intervalId);
    // Add timeLeft as a dependency to re-run the effect when we update it
  }, [timeLeft, isTimerRun]);

  const onResendEmail = () => {
    if (!token) {
      notification.error({
        message: "reCAPTCHA",
        description: translate("Recaptcha"),
      });
      return;
    }

    setToken("");
    setIsTimerRun(true);
    handleResendEmail({
      "g-recaptcha-response": captchaRef.current.getValue(),
    });
    captchaRef.current.reset();
  };

  const onVerify = () => {
    const tokenCaptcha = captchaRef.current.getValue();
    // alert(tokenCaptcha);
    setToken(tokenCaptcha);
  };

  const onExpire = () => {
    notification.info({
      message: "reCAPTCHA",
      description: translate("Recaptcha_Expired"),
    });
  };

  return (
    <Space direction="vertical" className="email-confirm-container">
      <div style={{ marginTop: "20px" }} className="language-switch">
        <ButtonFlag mobileVersion={true} />
      </div>
      <div className="container-title-emailConfirm">
        <Image src={IconEmailConfirm} alt="icon-back" className="icon-back" />
        <div className="text-title">{translate("Title_EmailConfirm")}</div>
        <div className="text-title-info text-title-infoConfirmEmail">
          {translate("Desc_EmailConfirm")}
          <span
            style={{ marginLeft: 10 }}
            className="text-redEmail"
          >{`${dataUser?.user?.email}`}</span>
        </div>
        <div className={"position-recaptcha"}>
          <ReCAPTCHA
            sitekey={process.env.NEXT_PUBLIC_SITE_KEY as string}
            onChange={onVerify}
            onExpired={onExpire}
            ref={captchaRef}
          />
        </div>
        <div style={{ marginTop: 32 }} className="group-button-emailConfirm">
          <Space direction="vertical">
            <FormButton
              title={
                isTimerRun
                  ? `${minutes}:${seconds < 10 ? `0${seconds}` : seconds}`
                  : translate("ResendEmail")
              }
              type={"submit"}
              hiddenIcon={true}
              disabled={isTimerRun}
              onClick={onResendEmail}
            />
            {isSuccessPopUp && (
              <div
                style={{
                  color: "white",
                  textAlign: "center",
                  marginTop: 10,
                  borderRadius: 5,
                  border: "1px solid green",
                  padding: 5,
                  backgroundColor: "green",
                }}
              >
                {translate("EmailSentSuccess")}
              </div>
            )}
          </Space>
          <FormButton
            onClick={() => navigate.push(DASHBOARD_EMAIL_CHANGE)}
            title={translate("Title_EmailChange")}
            type={"submit"}
            hiddenIcon={true}
          />
        </div>
      </div>
      <FooterAuth hiddenLineOr={false} hiddenButton={true} backToLogin={true} />
      {modalVisible && (
        <Modal
          emailSent={true}
          handleHiddenModal={handleHiddenModal}
          title={translate("Title_Modal_EmailConfirm")}
          desc={translate("Desc_Modal_EmailConfirm")}
          btn1={translate("Close")}
          btn2={""}
        />
      )}
    </Space>
  );
}

export default EmailConfirm;
