import LoaderSpinGif from "@/components/LoaderSpinGif";
import NoData from "@/components/NoData";
import RadioButton from "@/components/RadioLanguage";
import TextResponsive from "@/components/TextResponsive";
import useGetDetailCourseProgress from "@/features/coursedetail/hooks/useGetDetailCourseProgress";
import CommentComponent from "@/features/coursedetail/ui/CommentComponent";
import ListCollapseSection from "@/features/coursedetail/ui/ListCollapseSection";
import usePostRetakeQuizLevelup from "@/features/levelupsession/hooks/usePostRetakeQuizLevelup";
import ButtonFlag from "@/shared/components/layout/auth-layout/sider/ButtonFlag";
import ModalComponent from "@/shared/components/ModalComponent";
import { USER } from "@/shared/constants/storageStatis";
import IconBack from "@/shared/images/icon/arrow-left.webp";
import bgImage from "@/shared/images/icon/bg_Default.webp";
import iconDoubleArrow from "@/shared/images/icon/icon_double_arrow.webp";
import iconStudent from "@/shared/images/icon/icon_student_white.webp";
import iconInvesthub from "@/shared/images/logo/logo_investhub.webp";
import iconInvesthubMini from "@/shared/images/logo/logo_investhub_mini.webp";
import { COLORS } from "@/shared/styles/color";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getStatusColor, sleep } from "@/shared/utils/helper";
import MyTimer from "@/shared/widgets/MyTimer";
import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  CheckCircleFilled,
  CloseCircleFilled,
  MenuOutlined,
  UserOutlined,
} from "@ant-design/icons";
import type { RadioChangeEvent } from "antd";
import {
  Avatar,
  Button,
  Card,
  Col,
  Drawer,
  Flex,
  Layout,
  message,
  Progress,
  Radio,
  Row,
  Space,
  Steps,
  theme,
  Typography,
} from "antd";
import moment from "moment";
import "moment/locale/id"; // Example for Indonesia
import { useTranslation } from "next-i18next";
import dynamic from "next/dynamic";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import useGetDetailCourse from "../../coursedetail/hooks/useGetDetailCourse";
import useGetQuizLevelup from "../../levelupsession/hooks/useGetQuizLevelup";
import useSubmitLevelupQuiz from "../../levelupsession/hooks/useSubmitQuizLevelup";
import ScoreComponent from "../../levelupsession/ui/ScoreComponent";
import CourseDetailProgressSectionComponent from "./CourseDetailProgressSectionComponent";
import IconCirle from "@/shared/images/svg/icon_circle.svg";
const { Header, Sider, Content, Footer } = Layout;
const DynamicHeaderPDF = dynamic(() => import("@/components/PDFViewer"), {
  loading: () => <LoaderSpinGif size="large" />,
});
const DynamicHeaderVideo = dynamic(() => import("@/components/Player"), {
  loading: () => <LoaderSpinGif size="large" />,
});

export default function CourseDetailProgressPage() {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const dataUser = storageCheck(USER);
  const { isReady, query, locale } = navigate;
  const { id } = query;
  // Set Moment.js to use the detected locale
  moment.locale(locale);
  const { fetchDetailCourse } = dataUser.isLogin
    ? useGetDetailCourseProgress(id as string)
    : useGetDetailCourse(id as string);
  const {
    isLoading,
    data: dataCourseDetail,
    refetch: refetchDetailCourse,
  } = fetchDetailCourse;
  console.log("dataCourseDetail", dataCourseDetail);

  const { mutationQuery, handleGetQuiz } = useGetQuizLevelup();
  const {
    data: dataQuiz,
    isPending: isLoadingQuiz,
    mutate: mutateQuiz,
  } = mutationQuery;
  const { mutationQuery: mutationQueryRetake, retakeDataQuiz } =
    usePostRetakeQuizLevelup();
  const { isSuccess: isSuccessRetake, isPending: isLoadingRetake } =
    mutationQueryRetake;
  const [dataQuizRegenerate, setDataQuizRegenerate] = React.useState({} as any);

  const { mutationQuery: mutationQuerySubmit, handleSubmitQuiz } =
    useSubmitLevelupQuiz();
  const {
    data: dataSubmit,
    isPending: isLoadingSubmit,
    reset: resetSubmit,
  } = mutationQuerySubmit;
  const [collapsed, setCollapsed] = React.useState(false);
  const [isQuiz, setIsQuiz] = React.useState<boolean>(false);
  const [selectedIndex, setSelectedIndex] = React.useState("");
  const [dataExercise, setDataExercise] = React.useState<any>([]);
  const [dataScore, setDataScore] = React.useState<any>({});
  const [indexSoal, setIndexSoal] = React.useState(0);
  const [isModalLevelUp, setIsModalLevelup] = React.useState(false);
  const [contentVideo, setContentVideo] = React.useState<any>();
  const [contentPDF, setContentPDF] = React.useState<any>();
  const [widthScreen, setWidthScreen] = React.useState(1000);
  const [collapsedWidth, setCollapsedWidth] = React.useState(1300);
  const [heightScreen, setHeightScreen] = React.useState(350);
  const time = new Date(); // 10 minutes timer
  const [timer, setTimer] = React.useState(0);
  const [openDrawer, setOpenDrawer] = React.useState(false);
  const [isModalVisibleComment, setIsModalVisibleComment] =
    React.useState(false);

  const showDrawer = () => {
    setOpenDrawer(true);
  };

  const onClose = () => {
    setOpenDrawer(false);
  };

  React.useEffect(() => {
    // do not run effect until query is ready
    if (dataCourseDetail && Object.keys(dataCourseDetail).length > 0) {
      setContentVideo({
        content_file: "https://www.youtube.com/watch?v=ZOVtvj-CFeM",
      });
    }
  }, [dataCourseDetail]);

  React.useEffect(() => {
    if (!dataUser.isLogin) {
      setIsQuiz(false);
      return;
    }
  }, [dataUser]);

  React.useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth <= 400) {
        if (collapsed) {
          setCollapsedWidth(300);
          setHeightScreen(150);
        }
        setWidthScreen(250);
        setHeightScreen(100);
      } else if (window.innerWidth <= 500) {
        if (collapsed) {
          setCollapsedWidth(300);
          setHeightScreen(200);
        }
        setWidthScreen(300);
        setHeightScreen(150);
      } else if (window.innerWidth <= 800) {
        if (collapsed) {
          setCollapsedWidth(500);
          setHeightScreen(300);
        }
        setWidthScreen(400);
        setHeightScreen(300);
      } else if (window.innerWidth <= 1200) {
        if (collapsed) {
          setCollapsedWidth(800);
          setHeightScreen(300);
        }
        setWidthScreen(700);
        setHeightScreen(300);
      } else {
        if (collapsed) {
          setCollapsedWidth(1200);
          setHeightScreen(350);
        }
        setWidthScreen(1000);
        setHeightScreen(350);
      }

      if (window.innerWidth >= 700) {
        setOpenDrawer(false);
      }
    };

    // Panggil handleResize pertama kali untuk mengatur isSmallScreen
    handleResize();

    // Tambahkan event listener untuk memantau perubahan ukuran layar
    window.addEventListener("resize", handleResize);

    // Clean up event listener saat komponen dibongkar
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [collapsed]); // Dependensi kosong agar efek hanya berjalan sekali saat komponen dipasang

  const handleClickCollapse = () => {
    setCollapsed(!collapsed);
  };
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  const onChangeSteps = (value: number) => {
    console.log("onChangestep:", value);
    dataExercise.map((key: any, ind: any) => {
      if (ind == value) {
        setSelectedIndex(key.answer);
      }
    });
    setIndexSoal(value);
  };

  const handleTimeCountDown = () => {
    // alert("Time is over");
    handleSubmit();
  };

  const onChangeRadio = (e: RadioChangeEvent) => {
    dataExercise[indexSoal]?.options?.map((key: any) => {
      if (e.target.value == key.option) {
        dataExercise[indexSoal].answer = key.option;
        setSelectedIndex(e.target.value);
      }
    });
  };

  const handleClick = (value: any) => {
    setSelectedIndex("");
    if (value === "previous") {
      let resulIndex = indexSoal === 0 ? indexSoal : indexSoal - 1;
      setIndexSoal(resulIndex);
      dataExercise.map((key: any, ind: any) => {
        if (ind == resulIndex) {
          setSelectedIndex(key.answer);
        }
      });
    }

    if (value === "next") {
      let resulIndex =
        indexSoal === dataExercise.length - 1 ? indexSoal : indexSoal + 1;
      setIndexSoal(resulIndex);
      dataExercise.map((key: any, ind: any) => {
        if (ind == resulIndex) {
          setSelectedIndex(key.answer);
        }
      });
    }
  };

  const handleSubmit = () => {
    let answer = {} as any;
    dataExercise.map((item: any) => {
      answer[item.id] = item.answer;
    });
    handleSubmitQuiz(dataQuizRegenerate.id, answer);
  };

  React.useEffect(() => {
    if (dataSubmit) {
      setDataScore(dataSubmit);
      sleep(1000).then(() => {
        setIsModalLevelup(!isModalLevelUp);
        resetSubmit();
        refetchDetailCourse();
        mutateQuiz(dataQuizRegenerate.id);
        setIndexSoal(0);
      });
      return;
    }

    if (isSuccessRetake) {
      resetSubmit();
      refetchDetailCourse();
      setDataExercise([]);
      setIsQuiz(false);
      setIndexSoal(0);
      return;
    }
  }, [dataSubmit, isSuccessRetake]);

  React.useEffect(() => {
    setTimer(0);
    if (dataQuiz) {
      setDataQuizRegenerate(dataQuiz);
      let dataTemp = dataQuiz as any;
      if (dataTemp.is_completed) {
        let dataAnswer = JSON.parse(dataTemp.answers);
        let dataReview = JSON.parse(dataTemp.review);
        dataTemp.questions.map((item: any) => {
          let dataReviewResult = dataReview.find(
            (key: any) => key.question_id === item.id
          );
          item.answer = dataAnswer[item.id];
          if (dataReviewResult) {
            item.review = dataReviewResult;
          }
          return item;
        });
        setDataExercise(dataTemp.questions);
        setSelectedIndex(dataAnswer[dataTemp.questions[indexSoal].id]);
        setTimer(0);
      } else {
        let dataAnswer = JSON.parse(dataTemp.answers);
        dataTemp.questions.map((item: any) => {
          item.answer = dataAnswer ? dataAnswer[item.id] : "";
          return item;
        });
        setDataExercise(dataTemp.questions);
        console.log("dataQuiz regenerate", dataTemp);
        setTimer(
          time.setSeconds(
            time.getSeconds() + dataTemp.remaining_time &&
              dataTemp.remaining_time > 0
              ? dataTemp.remaining_time
              : dataTemp.duration * 60
          )
        );
        if (dataAnswer) {
          setSelectedIndex(dataAnswer[dataTemp.questions[indexSoal].id]);
          return;
        }
        setSelectedIndex("");
      }
      console.log("dataQuiz regenerate", dataTemp);
      setIsQuiz(true);
    }
  }, [dataQuiz]);

  const onClickContent = (content: any) => {
    console.log("content", content);
    if (content.test_type) {
      if (!dataUser.isLogin) {
        message.info(translate("PleaseLoginFirst"));
        return;
      }
      if (!content.attempt?.id) {
        message.info(translate("QuizIDNotFound"));
        return;
      }
      handleGetQuiz(content.attempt?.id);
      setContentVideo("");
      setContentPDF("");
      setIsQuiz(true);
      return;
    }
    if (content.content_type.toLowerCase() === "articles") {
      setContentVideo("");
      setContentPDF(content);
      return;
    }
    setIsQuiz(false);
    setContentPDF("");
    setContentVideo(content);
  };

  const handleCancelComment = () => {
    setIsModalVisibleComment(false);
  };

  if (isLoading) {
    return <LoaderSpinGif size="large" isFullScreen />;
  }

  if (dataCourseDetail && `${dataCourseDetail.id}` !== id) {
    return <LoaderSpinGif size="large" isFullScreen />;
  }

  if (dataCourseDetail && Object.keys(dataCourseDetail).length === 0) {
    return <NoData />;
  }

  return (
    <Layout hasSider>
      {widthScreen >= 400 && (
        <Sider
          width={collapsed ? 100 : 410}
          style={{
            background: colorBgContainer,
            overflowY: "auto",
            overflowX: "hidden",
            position: "fixed",
            paddingLeft: collapsed ? 0 : 20,
            left: 0,
            top: 0,
            bottom: 0,
            zIndex: 10,
            userSelect: "none",
            justifyContent: "center",
            borderRight: "5px solid var(--idx-co-id-mercury, #E6E6E6)",
            minWidth: 100,
            display: widthScreen <= 400 ? "none" : "block",
            paddingRight: collapsed ? 0 : 20,
          }}
          onCollapse={() =>
            widthScreen <= 1100 ? handleClickCollapse() : null
          }
          trigger={null}
          collapsible
          collapsed={collapsed}
        >
          {!collapsed && (
            <>
              <div className="logo-idx-courseDetailProgress">
                <Image
                  src={iconInvesthub}
                  alt="icon-logo"
                  className="icon-logo-courseDetailProgress"
                  style={{
                    width: "80%",
                  }}
                />
                <Image
                  onClick={handleClickCollapse}
                  src={iconDoubleArrow}
                  alt="iconDoubleArrow"
                  className="icon-menu-courseDetailProgress"
                />
              </div>
              <div style={{ height: "20px" }} />
              <div className="bg-image-linear">
                <div className="bg-image-courseProgress" />
                <Image
                  className="width-image"
                  alt="Background"
                  objectFit="cover"
                  src={bgImage}
                />
                <div className="container-bg-image-text-course">
                  <span className="text-image-courseDetailProgress">
                    {dataCourseDetail?.title}
                  </span>
                  <Space
                    className="group-image-courseDetailProgress"
                    direction="horizontal"
                  >
                    <Avatar
                      style={{ backgroundColor: "#9F0E0F" }}
                      icon={<UserOutlined />}
                    />
                    <span className="text-courseDetailProgress-title">
                      {dataCourseDetail?.conten_provider}
                    </span>
                    <Image src={iconStudent} alt="icon-student" />
                    <span className="textfs10-fw400-white">
                      {dataCourseDetail?.number_of_participants}
                    </span>
                  </Space>
                </div>
              </div>
              <div style={{ height: "20px" }} />
              {dataUser.isLogin && (
                <Col>
                  <Progress
                    status="success"
                    percent={Number(
                      dataCourseDetail?.participant_progress?.progress
                    )}
                    percentPosition={{ align: "center", type: "inner" }}
                    size={["100%", 15]}
                  />
                </Col>
              )}
              {dataCourseDetail?.sections?.length === 0 && (
                <Space style={{ marginTop: 20 }}>
                  <NoData />
                </Space>
              )}
              {dataCourseDetail?.sections?.map((section: any) => (
                <ListCollapseSection
                  onClickContent={onClickContent}
                  item={section}
                  key={section.id}
                />
              ))}
            </>
          )}
          {collapsed && (
            <>
              <div className="logo-idx-courseDetailProgress-noncollapse">
                <Image
                  src={iconInvesthubMini}
                  alt="icon-logo"
                  className="icon-logo-courseDetailProgress-noncollapse"
                />
              </div>
              <hr className="my-hr-line" />
              <Space
                direction="vertical"
                align="center"
                style={{ display: "flex" }}
              >
                <MenuOutlined
                  style={{ fontSize: 30 }}
                  onClick={handleClickCollapse}
                  className="icon-menu-courseDetailProgress-noncollapse"
                />
                {dataCourseDetail?.sections?.map((section: any) => (
                  <CourseDetailProgressSectionComponent
                    section={section}
                    key={section.id}
                  />
                ))}
              </Space>
            </>
          )}
        </Sider>
      )}
      <Drawer
        // title="Basic Drawer"
        placement={"left"}
        closable={false}
        onClose={onClose}
        open={openDrawer}
        key={"left"}
        width={400}
      >
        <>
          <Space>
            <Image
              src={iconInvesthub}
              alt="icon-logo"
              className="icon-logo-courseDetailProgress"
              style={{
                width: "80%",
              }}
            />
            <Image
              onClick={onClose}
              src={iconDoubleArrow}
              alt="iconDoubleArrow"
              className="icon-menu-courseDetailProgress"
            />
          </Space>
          <div style={{ height: "20px" }} />
          {dataUser.isLogin && (
            <>
              <Progress
                status="success"
                percent={Number(
                  dataCourseDetail?.participant_progress?.progress
                )}
                percentPosition={{ align: "center", type: "inner" }}
                size={["100%", 15]}
              />
            </>
          )}
          {dataCourseDetail?.sections?.length === 0 && (
            <Space style={{ marginTop: 20 }}>
              <NoData />
            </Space>
          )}
          {dataCourseDetail?.sections?.map((section: any) => (
            <ListCollapseSection
              onClickContent={(e) => {
                onClickContent(e);
                onClose();
              }}
              item={section}
              key={section.id}
            />
          ))}
        </>
      </Drawer>
      <Layout>
        <Header
          style={{
            paddingLeft: 5,
            marginLeft: widthScreen <= 400 ? 0 : collapsed ? 80 : 410,
            background: "#fff",
            position: "fixed",
            top: 0,
            zIndex: 10,
            width: "100%",
            paddingRight: widthScreen <= 400 ? 10 : 0,
          }}
        >
          <Row
            wrap
            justify={"space-between"}
            style={
              collapsed
                ? {
                    // width: "95vw",
                    width: "100%",
                    paddingRight: widthScreen <= 400 ? 0 : 80,
                  }
                : {
                    paddingBottom: 5,
                    borderBottom: "2px solid var(--idx-co-id-mercury, #E6E6E6)",
                    // width: window.innerWidth < 800 ? "95vw" : window.innerWidth > 800 && window.innerWidth < 1300 ? "65vw" : "70vw",
                    width: "100%",
                    paddingRight: widthScreen <= 400 ? 0 : 420,
                  }
            }
            className="container-navbar-courseDetailProgress"
          >
            <Col>
              <Row style={{ display: "flex" }}>
                <Image
                  onClick={() => navigate.back()}
                  src={IconBack}
                  alt={"icon-back"}
                  style={{ cursor: "pointer" }}
                />
                {isQuiz ? (
                  <span className="textfs20-fw600-black">Quiz</span>
                ) : (
                  <span className="textfs20-fw600-black">
                    {collapsed ? dataCourseDetail?.title : null}
                  </span>
                )}
              </Row>
            </Col>
            <Col>
              <Flex justify="space-between">
                {/* <Input
                  placeholder="Search keywords..."
                  style={{
                    width: 300,
                    height: 42,
                    borderRadius: 8,
                    fontSize: 12,
                  }}
                  size="small"
                  prefix={<IoSearch size={20} color="#858D9D" />}
                  suffix={
                    <Space>
                      <div className="tag-search-courseDetailProgress">
                        CTRL
                      </div>
                      <div className="tag-search-courseDetailProgress">/</div>
                    </Space>
                  }
                /> */}
                {widthScreen < 500 ? (
                  <Flex>
                    <MenuOutlined
                      style={{ fontSize: 30 }}
                      onClick={showDrawer}
                      className="icon-menu-courseDetailProgress-noncollapse"
                    />
                    <ButtonFlag course={true} />
                  </Flex>
                ) : null}
                {widthScreen > 400 ? (
                  <RadioButton classNameView="radio-courseDetailProgress" />
                ) : null}
                {/* <TfiSettings size={24} color="#656565" /> */}
              </Flex>
            </Col>
            {collapsed && dataUser.isLogin && (
              <Col style={{ position: "absolute", top: 50, width: "100%" }}>
                <Progress
                  status="success"
                  percent={Number(
                    dataCourseDetail?.participant_progress?.progress
                  )}
                  percentPosition={{ align: "center", type: "inner" }}
                  size={["100%", 15]}
                />
              </Col>
            )}
          </Row>
        </Header>
        <Content
          style={{
            padding: 24,
            height: "100%",
            width: "auto",
            maxHeight: "1000vh",
            background: "#fff",
            marginLeft: widthScreen <= 400 ? 0 : collapsed ? 80 : 400,
            display: "flex",
            paddingTop: widthScreen <= 800 ? "6vh" : "15vh",
            paddingBottom: widthScreen <= 800 ? 0 : "15vh",
          }}
        >
          {!isQuiz ? (
            <Row wrap>
              <Col span={24}>
                {contentVideo ? (
                  <DynamicHeaderVideo
                    url={
                      contentVideo.content_file ||
                      "https://www.youtube.com/watch?v=ZOVtvj-CFeM"
                    }
                    width={
                      widthScreen <= 800
                        ? "100vw"
                        : collapsed
                        ? collapsedWidth
                        : widthScreen
                    }
                    height={heightScreen}
                    content={contentVideo}
                    idProgress={dataCourseDetail?.participant_progress?.id}
                  />
                ) : contentPDF ? (
                  <DynamicHeaderPDF
                    url={contentPDF.content_file}
                    content={contentPDF}
                    idProgress={dataCourseDetail?.participant_progress?.id}
                  />
                ) : (
                  <Card
                    style={{
                      width: "100%",
                      alignItems: "center",
                      justifyContent: "center",
                      display: "flex",
                    }}
                  >
                    <Typography style={{ textAlign: "center" }}>
                      <Typography.Title>
                        {translate("PlsSelectContent")}
                      </Typography.Title>
                    </Typography>
                  </Card>
                )}
              </Col>
              <Col span={24} style={{ marginTop: 20 }}>
                <Row justify={"space-between"} className="textfs24-fw700-black">
                  <TextResponsive
                    mobile={22}
                    color={COLORS.BLACK}
                    fontWeight={700}
                    desktop={24}
                    isSpan
                  >
                    {contentVideo?.title || contentPDF?.title}
                  </TextResponsive>
                  {dataUser.isLogin && (
                    <>
                      {contentPDF && (
                        <Button
                          type="primary"
                          style={{
                            backgroundColor: COLORS.PRIMARY,
                            color: "white",
                          }}
                          onClick={() => setIsModalVisibleComment(true)}
                        >
                          {translate("CommentButton")}
                        </Button>
                      )}
                      {contentVideo && (
                        <Button
                          type="primary"
                          style={{
                            backgroundColor: COLORS.PRIMARY,
                            color: "white",
                          }}
                          onClick={() => setIsModalVisibleComment(true)}
                        >
                          {translate("CommentButton")}
                        </Button>
                      )}
                    </>
                  )}
                </Row>
                <Row
                  justify={"space-between"}
                  gutter={5}
                  style={{ marginTop: 10 }}
                  wrap
                >
                  <Col xs={24} sm={14} md={collapsed ? 16 : 24} lg={collapsed ? 17 : 14} xl={collapsed ? 18 : 16}>
                    <Card className="card-left-courseDetailProgress">
                      <TextResponsive
                        mobile={14}
                        color={COLORS.GRAY}
                        fontWeight={400}
                        desktop={16}
                        isSpan
                      >
                        {dataCourseDetail?.description}
                      </TextResponsive>
                    </Card>
                  </Col>
                  <Col>
                    <Card className="card-right-courseDetailProgress">
                      {/* <div className="textfs20-fw600-black">
                        Due Date Course
                      </div>
                      <div className="textfs16-fw400-gray">
                        {moment(dataCourseDetail?.publish_date).format(
                          "dddd, MMM D, YYYY"
                        )}
                      </div> */}
                      <TextResponsive
                        mobile={18}
                        color={COLORS.BLACK}
                        fontWeight={600}
                        desktop={20}
                      >
                        {translate("AboutCourse")}
                      </TextResponsive>
                      <Flex gap={10} align="center">
                        <TextResponsive
                          mobile={18}
                          color={COLORS.PRIMARY}
                          fontWeight={600}
                          desktop={20}
                          isSpan
                        >
                          {dataCourseDetail?.sections?.length}
                          <TextResponsive
                            mobile={14}
                            color={COLORS.GRAY}
                            fontWeight={400}
                            desktop={16}
                          >
                            {translate("Section")}
                          </TextResponsive>
                        </TextResponsive>
                      </Flex>
                      <TextResponsive
                        mobile={18}
                        color={COLORS.BLACK}
                        fontWeight={600}
                        desktop={20}
                        isSpan
                      >
                        {translate("StartLearnAt")}
                      </TextResponsive>
                      <TextResponsive
                        mobile={14}
                        color={COLORS.GRAY}
                        fontWeight={600}
                        desktop={16}
                        isSpan
                      >
                        {moment(
                          dataCourseDetail?.participant_progress?.start_at
                        ).format("dddd, MMM D, YYYY")}
                      </TextResponsive>
                      {dataCourseDetail?.participant_progress?.completed_at && (
                        <>
                          <TextResponsive
                            mobile={18}
                            color={COLORS.BLACK}
                            fontWeight={600}
                            desktop={20}
                            isSpan
                          >
                            {translate("CompleteAt")}
                          </TextResponsive>
                          <TextResponsive
                            mobile={14}
                            color={COLORS.GRAY}
                            fontWeight={400}
                            desktop={16}
                            isSpan
                          >
                            {moment(
                              dataCourseDetail?.participant_progress
                                ?.completed_at
                            ).format("dddd, MMM D, YYYY")}
                          </TextResponsive>
                        </>
                      )}
                      {dataUser.isLogin && contentVideo && (
                        <Progress
                          status="success"
                          percent={Number(contentVideo?.progress?.progress)}
                          percentPosition={{
                            align: "center",
                            type: "inner",
                          }}
                          size={["100%", 15]}
                          style={{ marginTop: 10 }}
                        />
                      )}
                    </Card>
                  </Col>
                </Row>
              </Col>
            </Row>
          ) : (
            <Col span={24}>
              <Card>
                {dataQuizRegenerate.duration !== 0 && (
                  <Row className="button-center-card-levelUp">
                    {timer > 0 ? (
                      <Col
                        span={24}
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          alignItems: "center",
                          flexWrap: "wrap",
                        }}
                      >
                        <Space className="textfs20-fw400-red">
                          {translate("TimeLeft")}
                        </Space>
                        <MyTimer
                          expiryTimestamp={timer}
                          handleTimeCountDown={handleTimeCountDown}
                          className={"textfs32-fw400-red"}
                          dataExercise={dataExercise}
                          id={dataQuizRegenerate.id}
                        />
                      </Col>
                    ) : null}
                    {dataQuizRegenerate?.is_completed && (
                      <Row
                        justify={"space-between"}
                        style={{
                          width: widthScreen,
                          alignItems: "center",
                          marginBottom: 20,
                        }}
                      >
                        <Col style={{ display: "flex" }}>
                          <Col style={{ width: 50 }}>
                            <p>{translate("Score")}</p>
                          </Col>
                          <Col style={{ marginRight: 15 }}>
                            <p>{" : "}</p>
                          </Col>
                          <Col>
                            <p>{dataQuizRegenerate?.score}</p>
                          </Col>
                        </Col>
                        <Col>
                          <Button
                            loading={isLoadingRetake}
                            onClick={() =>
                              retakeDataQuiz(dataQuizRegenerate?.id)
                            }
                            type="primary"
                            style={{
                              backgroundColor: getStatusColor("pending"),
                              color: "white",
                              marginRight: 10,
                            }}
                          >
                            {translate("RetakeQuiz")}
                          </Button>
                          <Button
                            type="primary"
                            disabled
                            style={{
                              backgroundColor: dataQuizRegenerate?.is_passed
                                ? "#00C82C"
                                : "#F22350",
                              color: "white",
                            }}
                          >
                            {dataQuizRegenerate?.is_passed
                              ? translate("Passed")
                              : translate("Failed")}
                          </Button>
                        </Col>
                      </Row>
                    )}
                  </Row>
                )}
                <Steps
                  responsive
                  size="small"
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    padding: "16px 32px",
                    borderRadius: 16,
                    border: "1px solid var(--Stroke-3, #E8E8E8)",
                    overflowX: "scroll",
                    display: widthScreen <= 400 ? "none" : "flex",
                  }}
                  current={indexSoal}
                  onChange={onChangeSteps}
                  status="error"
                  items={dataExercise.map((key: any) => {
                    if (key.answer) {
                      return {
                        icon: (
                          <CheckCircleFilled
                            style={{ color: "#00C82C", fontSize: 30 }}
                          />
                        ),
                      };
                    } else {
                      return {
                        icon: (
                          <Image
                            alt="icon-circle"
                            src={IconCirle}
                            width={30}
                            height={30}
                          />
                        ),
                      };
                    }
                  })}
                />
                <div style={{ height: 40 }} />
                <Row
                  justify={indexSoal > 0 ? "space-between" : "end"}
                  style={{
                    alignItems: "flex-start",
                  }}
                >
                  {indexSoal > 0 && (
                    <Col>
                      <div
                        style={{ cursor: "pointer" }}
                        onClick={() => handleClick("previous")}
                        className="button-card-levelUp"
                      >
                        {isLoadingSubmit && <LoaderSpinGif size="small" />}
                        <ArrowLeftOutlined style={{ color: "#9F0E0F" }} />
                        <span className="textfs16-fw400-red">
                          {translate("Previous")}
                        </span>
                      </div>
                    </Col>
                  )}
                  {indexSoal === dataExercise.length - 1 ? (
                    <Col>
                      {dataQuizRegenerate.is_completed ? null : (
                        <div
                          style={{ cursor: "pointer" }}
                          onClick={handleSubmit}
                          className="button-card-levelUp"
                        >
                          {isLoadingSubmit && <LoaderSpinGif size="small" />}
                          <span className="textfs16-fw400-red">
                            {translate("Submit")}
                          </span>
                        </div>
                      )}
                    </Col>
                  ) : (
                    <Col>
                      <div
                        style={{ cursor: "pointer" }}
                        onClick={() => handleClick("next")}
                        className="button-card-levelUp"
                      >
                        {isLoadingSubmit && <LoaderSpinGif size="small" />}
                        <span className="textfs16-fw400-red">
                          {translate("Next_Capital")}
                        </span>
                        <ArrowRightOutlined style={{ color: "#9F0E0F" }} />
                      </div>
                    </Col>
                  )}
                </Row>
                <div style={{ height: 30 }} />
                <Row
                  justify={"space-between"}
                  style={{
                    alignItems: "flex-start",
                  }}
                >
                  {!dataQuiz && dataExercise.length === 0 && (
                    <LoaderSpinGif size="large" />
                  )}
                  {dataExercise.length > 0 ? (
                    <Col span={24}>
                      <span className="textfs18-fw400-gray">{`Question ${
                        indexSoal + 1
                      } of ${dataExercise.length}`}</span>
                      <div style={{ height: 8 }} />
                      <span
                        style={{ display: "flex" }}
                        className="textfs20-fw600-black"
                        dangerouslySetInnerHTML={{
                          __html: `${dataExercise[indexSoal]?.question}`,
                        }}
                      />
                      <div style={{ height: 32 }} />
                      <Radio.Group
                        disabled={
                          dataQuizRegenerate.is_completed ? true : false
                        }
                        onChange={onChangeRadio}
                        value={selectedIndex}
                      >
                        <Space direction="vertical">
                          <Row>
                            {dataExercise[indexSoal]?.options.map(
                              (key: any, value: any) => {
                                return (
                                  <Col
                                    span={24}
                                    style={{
                                      justifyContent: "space-between",
                                      display: "flex",
                                    }}
                                  >
                                    <Radio
                                      className="textfs18-fw400-black"
                                      value={key.option}
                                    >
                                      <Col>
                                        <div
                                          style={{
                                            paddingTop: 15,
                                          }}
                                          dangerouslySetInnerHTML={{
                                            __html: `${key.value}`,
                                          }}
                                        />
                                      </Col>
                                    </Radio>
                                    <Col>
                                      {dataExercise[indexSoal].review?.answer ==
                                        key.option && (
                                        <>
                                          {dataExercise[indexSoal].review
                                            ?.correct ? (
                                            <CheckCircleFilled
                                              style={{
                                                color: "#00C82C",
                                                fontSize: 30,
                                                marginTop: 15,
                                                marginLeft: 10,
                                              }}
                                            />
                                          ) : (
                                            <CloseCircleFilled
                                              style={{
                                                color: "#F22350",
                                                fontSize: 30,
                                                marginTop: 15,
                                                marginLeft: 10,
                                              }}
                                            />
                                          )}
                                        </>
                                      )}
                                    </Col>
                                  </Col>
                                );
                              }
                            )}
                          </Row>
                        </Space>
                      </Radio.Group>
                    </Col>
                  ) : (
                    <LoaderSpinGif size="large" />
                  )}
                </Row>
              </Card>
            </Col>
          )}
        </Content>
      </Layout>

      {/* Modal Score */}
      <ModalComponent
        centered
        title={
          dataScore.is_passed
            ? translate("LabelResultTest")
            : translate("LabelSorry")
        }
        open={isModalLevelUp}
        width={800}
        onCancel={() => setIsModalLevelup(false)}
        footer={[
          <Flex justify="center">
            <Button type="primary" onClick={() => setIsModalLevelup(false)}>
              {translate("Save")}
            </Button>
          </Flex>,
        ]}
      >
        <ScoreComponent
          desc={
            dataScore.is_passed
              ? `${translate("DescResultTest")} ${
                  dataQuizRegenerate!.duration * 60
                } second`
              : `${translate("DescFailResultTest")} ${
                  dataQuizRegenerate!.duration * 60
                } second`
          }
          desc2={`${translate("Score")} : ${dataScore.score}`}
        />
      </ModalComponent>

      {/* Modal Comment */}
      <ModalComponent
        centered
        title={translate("CommentCourse")}
        open={isModalVisibleComment}
        width={1100}
        onCancel={handleCancelComment}
        footer={null}
      >
        <CommentComponent
          dataCourseDetail={contentVideo ? contentVideo : contentPDF}
          isModalVisibleComment={isModalVisibleComment}
          type="content"
        />
      </ModalComponent>
    </Layout>
  );
}
