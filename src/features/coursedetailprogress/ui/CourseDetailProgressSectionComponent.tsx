import IconCirleGray from "@/shared/images/svg/icon_circle_gray.svg";
import { CheckCircleFilled, UpOutlined } from "@ant-design/icons";
import { Space } from "antd";
import Image from "next/image";
import React from "react";

type Props = {
  section: any;
  key: any;
};

export default function CourseDetailProgressSectionComponent({
  section,
  key,
}: Props) {
  const [isCollapse, setIsCollapse] = React.useState(section.is_collapse);
  const handleToggleCollapse = () => {
    setIsCollapse(!isCollapse);
  };
  return (
    <Space direction="vertical">
      <hr className="my-hr-line" />
      <UpOutlined
        onClick={(e) => handleToggleCollapse()}
        style={
          isCollapse
            ? {
                transform: "rotate(180deg)",
                marginTop: 5,
                fontSize: 15,
                color: "#9f0e0f",
              }
            : {
                marginTop: 5,
                fontSize: 15,
                color: "#9f0e0f",
              }
        }
      />
      {!isCollapse &&
        section.contents?.length > 0 &&
        section.contents.map((val: any) => (
          <Space key={val.id} direction="vertical">
            <hr className="my-hr-line" />
            {val.progress?.is_completed ? (
              <CheckCircleFilled
                style={{ color: "green", fontSize: 20 }}
                className="icon-check-courseDetail-collapse"
              />
            ) : (
              <Image
                alt="icon-circle"
                src={IconCirleGray}
                width={20}
                height={20}
              />
            )}
          </Space>
        ))}
      {!isCollapse &&
        section.post_test?.length > 0 &&
        section.post_test.map((post: any) => (
          <Space key={post.id} direction="vertical">
            <hr className="my-hr-line" />
            <Image
              alt="icon-circle"
              src={IconCirleGray}
              width={20}
              height={20}
            />
          </Space>
        ))}
      {!isCollapse &&
        section.sections.length > 0 &&
        section.sections.map((val2: any) => (
          <CourseDetailProgressSectionComponent key={val2.id} section={val2} />
        ))}
    </Space>
  );
}
