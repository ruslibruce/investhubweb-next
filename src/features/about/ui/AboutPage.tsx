import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import iconLogo from "@/shared/images/logo/logo_investhub.webp";
import { Breadcrumb, Col, Row, Space } from "antd";
import Image from "next/image";
import React from "react";
import useGetAbout from "../hooks/useGetAbout";
import originator from "@/shared/images/about/about_originator.jpeg";
import { useTranslation } from "next-i18next";

export default function AboutPage() {
  const {t: translate} = useTranslation();
  const { fetchQuery } = useGetAbout({});
  const { isLoading, data: dataAbout } = fetchQuery;

  return (
    <div className="about">
      <div className="about-breadcrumb">
        <Breadcrumb
          separator=">"
          items={[
            {
              href: "",
              title: (
                <Image
                  src={iconBreadcrumb}
                  alt="Breadcrumb Icon"
                  style={{ paddingBottom: 5 }}
                />
              ),
            },
            {
              title: <span className="textfs16-fw400-red">{translate("About_Capital")}</span>,
            },
          ]}
        />
      </div>
      <div className="about-content">
        <h2 className="about-title">{dataAbout?.title}</h2>
        <div className="about-aside-content">
          <div className="about-definition">
            <div className="about-definition-content">
              <p dangerouslySetInnerHTML={{__html: dataAbout?.body}}/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
