import { fetchAbout } from "@/shared/api/fetch/about";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchAbout();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetAbout = (initialData?: any) => {
  const fetchDataAbout = fetchAbout();

  const fetchQuery = useFetchHook({
    keys: fetchDataAbout.key,
    api: fetchDataAbout.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetAbout;
