import { fetchFaq } from "@/shared/api/fetch/faq";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchFaq();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetStockScreenerColoumn = (initialData?: any) => {
  const fetchDataStockScreener = fetchFaq();

  const fetchQuery = useFetchHook({
    keys: fetchDataStockScreener.key,
    api: fetchDataStockScreener.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetStockScreenerColoumn;
