import { DASHBOARD_STOCK_SCREENER } from "@/shared/constants/path";
import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import type { TableProps } from "antd";
import { Breadcrumb, Space, Table } from "antd";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import useGetStockScreenerColoumn from "../hooks/useGetStockScreenerColoumn";

type DataType = {
  key: string;
  number: number;
  name: string;
  satuan: string;
  kepanjangan: string;
  definisi: string;
}

const columns: TableProps<DataType>["columns"] = [
  {
    title: "No",
    dataIndex: "number",
    key: "number",
    sorter: (a, b) => a.number - b.number,
  },
  {
    title: "Singkatan",
    dataIndex: "name",
    key: "name",
    render: (text, record: { key: React.Key }) => <Link href={"/"}>{text}</Link>,
    sorter: (a, b) => a.name.length - b.name.length,
  },
  {
    title: "Satuan",
    dataIndex: "satuan",
    key: "satuan",
    sorter: (a, b) => a.satuan.length - b.satuan.length,
  },
  {
    title: "Kepanjangan",
    dataIndex: "kepanjangan",
    key: "kepanjangan",
    sorter: (a, b) => a.kepanjangan.length - b.kepanjangan.length,
  },
  {
    title: "Definisi",
    dataIndex: "definisi",
    key: "definisi",
    sorter: (a, b) => a.definisi.length - b.definisi.length,
  },
];

const dataTable: DataType[] = [
  {
    key: "1",
    number: 1,
    name: "PER",
    satuan: "X (kali)",
    kepanjangan: "Price to Earnings Ratio",
    definisi: "Harga saham dibagi dengan Laba per Saham (EPS). EPS diperoleh dengan membagi trailing 12 bulan atas laba periode berjalan yang didistribusikan kepada entitas pemilik dengan jumlah saham tercatat.",
  },
  {
    key: "2",
    number: 2,
    name: "PBV",
    satuan: "X (kali)",
    kepanjangan: "Price to Book Value Ratio",
    definisi: "Harga saham dibagi dengan nilai buku (BV). BV diperoleh dengan membagi total ekuitas dengan jumlah saham tercatat.",
  },
  {
    key: "3",
    number: 3,
    name: "ROE",
    satuan: "%",
    kepanjangan: "Return on Equity Ratio",
    definisi: "Trailing 12 bulan atas laba periode berjalan yang didistribusikan kepada entitas pemilik dibagi dengan total ekuitas.",
  },
  {
    key: "4",
    number: 4,
    name: "ROA",
    satuan: "%",
    kepanjangan: "Return on Asset Ratio",
    definisi: "Trailing 12 bulan atas laba periode berjalan yang didistribusikan kepada entitas pemilik dibagi dengan total aset.",
  },
  {
    key: "5",
    number: 5,
    name: "DER",
    satuan: "X",
    kepanjangan: "Consumer Cylicals",
    definisi: "Total liabilitas dibagi dengan total ekuitas.",
  },
  {
    key: "6",
    number: 6,
    name: "NPM",
    satuan: "%",
    kepanjangan: "Net Profit Margin Ratio",
    definisi: "Trailing 12 bulan atas laba periode berjalan yang didistribusikan kepada entitas pemilik dibagi dengan total pendapatan.",
  },
  {
    key: "7",
    number: 7,
    name: "Total Rev",
    satuan: "Rp",
    kepanjangan: "Total Revenue",
    definisi: "Total pendapatan dari perusahaan.",
  },
  {
    key: "8",
    number: 8,
    name: "Mkt Cap",
    satuan: "Rp",
    kepanjangan: "Market Capitalization",
    definisi: "Harga saham dikali dengan jumlah saham tercatat.",
  },
  {
    key: "9",
    number: 9,
    name: "4wk. %Pr. Change",
    satuan: "%",
    kepanjangan: "4 week Percentage Price Change",
    definisi: "Persentase perubahan harga dalam waktu 4 minggu.",
  },
  {
    key: "10",
    number: 10,
    name: "13wk. %Pr. Change",
    satuan: "%",
    kepanjangan: "13 week Percentage Price Change",
    definisi: "Persentase perubahan harga dalam waktu 13 minggu.",
  },
  {
    key: "11",
    number: 11,
    name: "26wk. %Pr. Change",
    satuan: "%",
    kepanjangan: "26 week Percentage Price Change",
    definisi: "Persentase perubahan harga dalam waktu 26 minggu.",
  },
  {
    key: "12",
    number: 12,
    name: "52wk. %Pr. Change",
    satuan: "%",
    kepanjangan: "52 week Percentage Price Change",
    definisi: "Persentase perubahan harga dalam waktu 52 minggu.",
  },
  {
    key: "13",
    number: 13,
    name: "MTD",
    satuan: "%",
    kepanjangan: "Month to Date",
    definisi: "Persentase perubahan harga sejak hari pertama bulan berjalan dengan tanggal hari bursa terakhir.",
  },
  {
    key: "14",
    number: 14,
    name: "YTD",
    satuan: "%",
    kepanjangan: "Year to Date",
    definisi: "Persentase perubahan harga sejak hari pertama tahun kalender sampai dengan tanggal hari bursa terakhir.",
  },
];

export default function StockScreenerColoumnPage() {
  const { fetchQuery } = useGetStockScreenerColoumn([]);
  const { isLoading, data } = fetchQuery;

  return (
    <>
      <Space direction="vertical" className="hero-container-background" size="middle" style={{ display: "flex", marginTop: 35, marginBottom: 72 }}>
        <div className="column-explanation">
          <div className="column-explanation-breadcrumb">
            <Breadcrumb
              separator=">"
              items={[
                {
                  href: "",
                  title: (
                    <>
                      <Image src={iconBreadcrumb} alt="Breadcrumb Icon" />
                    </>
                  ),
                },
                {
                  href: DASHBOARD_STOCK_SCREENER,
                  title: <span className="textfs16-fw400-gray">Stock Screener</span>,
                },
                {
                  title: <span className="textfs16-fw400-red">Coloumn Explanation</span>,
                },
              ]}
            />
          </div>
          <h2 className="coloum-explanation-title">Coloum Explanation</h2>
          <div style={{ height: 32 }} />
          <Table size="small" columns={columns} dataSource={dataTable} pagination={false} scroll={{ x: 1000 }} />
        </div>
      </Space>
    </>
  );
}
