import BackgroundHero from "@/components/BackgroundHero";
import FormButton from "@/components/FormButton";
import FormDropdown from "@/components/FormDropdown";
import TextResponsive from "@/components/TextResponsive";
import PaginationComponent from "@/shared/components/PaginationComponent";
import { DASHBOARD_CLASS_DETAIL } from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import bgHero from "@/shared/images/icon/Hero_Section.webp";
import iconAppleCard from "@/shared/images/icon/icon_apple_topcard.webp";
import imageDummyCard from "@/shared/images/icon/image_dummy.webp";
import iconCalendarCard from "@/shared/images/svg/ic_red_calendar_month.svg";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { Card, Col, Divider, Row, Space } from "antd";
import moment from "moment";
import Image from "next/image";
import { useRouter } from "next/router";
import { MenuInfo } from "rc-menu/lib/interface";
import React from "react";
import { useTranslation } from "next-i18next";

const itemsType = [
  {
    key: "1",
    label: "Introdution Stock Part 1",
  },
  {
    key: "2",
    label: "Introdution Stock Part 2",
  },
  {
    key: "3",
    label: "Advanced Stock Part 1",
  },
  {
    key: "4",
    label: "Advanced Stock Part 2",
  },
  {
    key: "5",
    label: "Advanced Stock Part 3",
  },
];

const itemsLocation = [
  {
    key: "1",
    label: "Beginner",
  },
  {
    key: "2",
    label: "Intermediate",
  },
  {
    key: "3",
    label: "Advanced",
  },
  {
    key: "4",
    label: "Expert",
  },
];

const itemsSchedule = [
  {
    key: "1",
    label: "Management",
  },
  {
    key: "2",
    label: "Stock",
  },
  {
    key: "3",
    label: "Bussiness",
  },
  {
    key: "4",
    label: "Language",
  },
];

const dataDummy = [
  {
    id: 1,
    name: "Rusli Wanasuria",
    section: "3",
    totalCourse: "28",
    seeing: 5,
    totalHours: "3 hours, 20 min",
    date: "22 Oct 2023 09:12",
    title: "Introduction Stock Part 1",
    description:
      "Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...",
    image: imageDummyCard,
    level: "Beginner",
    owner: "IDX Stock",
    likes: "12",
    student: "1500",
    comments: "10",
    shared: "5",
    tag: [
      {
        id: 1,
        name: "Management",
      },
      {
        id: 2,
        name: "Stock",
      },
      {
        id: 3,
        name: "Business",
      },
    ],
  },
  {
    id: 2,
    name: "Albert Flores",
    section: "3",
    totalCourse: "28",
    seeing: 5,
    totalHours: "3 hours, 20 min",
    date: "22 Oct 2023 09:12",
    title: "Introduction Stock Part 2",
    description:
      "Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...",
    image: imageDummyCard,
    level: "Beginner",
    owner: "IDX Stock",
    likes: "12",
    student: "1500",
    comments: "10",
    shared: "5",
    tag: [
      {
        id: 1,
        name: "Management",
      },
      {
        id: 2,
        name: "Stock",
      },
      {
        id: 3,
        name: "Business",
      },
    ],
  },
  {
    id: 3,
    name: "Albert Flores",
    section: "3",
    totalCourse: "28",
    seeing: 5,
    totalHours: "3 hours, 20 min",
    date: "22 Oct 2023 09:12",
    title: "Introduction Stock Part 3",
    description:
      "Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...",
    image: imageDummyCard,
    level: "Beginner",
    owner: "IDX Stock",
    likes: "12",
    student: "1500",
    comments: "10",
    shared: "5",
    tag: [
      {
        id: 1,
        name: "Management",
      },
      {
        id: 2,
        name: "Investing",
      },
    ],
  },
  {
    id: 4,
    name: "Albert Flores",
    section: "3",
    totalCourse: "28",
    seeing: 5,
    totalHours: "3 hours, 20 min",
    date: "22 Oct 2023 09:12",
    title: "Introduction Stock Part 4",
    description:
      "Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...",
    image: imageDummyCard,
    level: "Beginner",
    owner: "IDX Stock",
    likes: "12",
    student: "1500",
    comments: "10",
    shared: "5",
    tag: [
      {
        id: 1,
        name: "Management",
      },
      {
        id: 2,
        name: "Investing",
      },
    ],
  },
  {
    id: 5,
    name: "Albert Flores",
    section: "3",
    totalCourse: "28",
    seeing: 5,
    totalHours: "3 hours, 20 min",
    date: "22 Oct 2023 09:12",
    title: "Introduction Stock Part 5",
    description:
      "Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...",
    image: imageDummyCard,
    level: "Beginner",
    owner: "IDX Stock",
    likes: "12",
    student: "1500",
    comments: "10",
    shared: "5",
    tag: [
      {
        id: 1,
        name: "Management",
      },
      {
        id: 2,
        name: "Investing",
      },
    ],
  },
  {
    id: 6,
    name: "Albert Flores",
    section: "3",
    totalCourse: "28",
    seeing: 5,
    totalHours: "3 hours, 20 min",
    date: "22 Oct 2023 09:12",
    title: "Introduction Stock Part 2",
    description:
      "Lorem ipsum dolor sit amet consectetur. Felis in nunc elit sed sit urna. Tincidunt ultrices dignissim tincidunt urna...",
    image: imageDummyCard,
    level: "Beginner",
    owner: "IDX Stock",
    likes: "12",
    student: "1500",
    comments: "10",
    shared: "5",
    tag: [
      {
        id: 1,
        name: "Finance",
      },
      {
        id: 2,
        name: "Finance",
      },
    ],
  },
];

export default function ClassSchedulePage() {
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const [formData, setFormData] = React.useState<any>({
    type: "",
    location: "",
    schedule: "",
  });
  const [currentPage, setCurrentPage] = React.useState<number>(1);
  const { t: translate } = useTranslation();

  const handleChangeDropdown = (event: MenuInfo, target: string) => {
    setFormData(() => ({
      ...formData,
      [target]: event.key,
    }));
  };

  const handleCourse = (value: any) => {
    navigate.push(`${DASHBOARD_CLASS_DETAIL}/${value}`);
  };

  return (
    <div className="hero-container-backgroundImg">
      <Row
        style={{
          borderBottomLeftRadius: 40,
          borderBottomRightRadius: 40,
          paddingTop: 20,
          paddingBottom: 30,
          paddingLeft: 70,
          paddingRight: 70,
          position: "relative",
          overflow: "hidden",
        }}
        gutter={[16, 16]}
        align={"middle"}
        className="hero-container-antd"
      >
        <BackgroundHero />
        <Col span={12}>
          <TextResponsive
            bigScreen={60}
            mobile={24}
            color="white"
            fontWeight={700}
            isSpan
            desktop={45}
          >
            {translate("TitleHeroClass")}
          </TextResponsive>
        </Col>
        <Col span={12}>
          <TextResponsive
            mobile={14}
            color="white"
            fontWeight={400}
            isSpan
            desktop={16}
          >
            {translate("DescHeroGuest")}
          </TextResponsive>
        </Col>
      </Row>
      <div className="container-filter-course">
        <FormDropdown
          placeholder={"Filter Class Type"}
          value={formData.type}
          data={itemsType}
          onClick={(e) => handleChangeDropdown(e, "materi")}
        />
        <FormDropdown
          placeholder={"Filter Location"}
          value={formData.location}
          data={itemsLocation}
          onClick={(e) => handleChangeDropdown(e, "level")}
        />
        <FormDropdown
          placeholder={"Filter Class Schedule"}
          value={formData.schedule}
          data={itemsSchedule}
          onClick={(e) => handleChangeDropdown(e, "topik")}
        />
      </div>
      <Row className="container-card-course">
        {dataDummy.map((item, index) => {
          return (
            <Col key={item.id} span={32}>
              <Card bordered={false} className="card-course">
                <div className="top-card-position-courseSchedule">
                  <Image src={iconAppleCard} alt="menu-card" />
                  <span className="text-top-card">
                    Pelajari Profil dan Keunggulan PT. XXXX XXX XX
                  </span>
                  {/* <div className="top-left-position-card">
                                        </div> */}
                </div>
                <div className="image-card-courseSchedule">
                  <Image src={imageDummyCard} alt="image-dummy-card" />
                </div>
                <div style={{ height: "16px" }} />
                <div className="class-schedule-content">
                  <span className="text-title-card">{item.title}</span>
                  <span className="text-desc-card-courseSchedule">
                    Lorem ipsum dolor sit amet consectetur. Felis in nunc elit
                    sed sit urna. Tincidunt ultrices dignissim tincidunt urna
                    turpis, Lorem ipsum dolor sit amet consectetur. Felis in
                    nunc elit sed sit urna. Tincidunt ultrices dignissim
                    tincidunt urna turpis ...
                  </span>
                </div>
                <div style={{ height: 24 }} />
                <div className="bottom-card-position-courseSchedule">
                  <Space direction="horizontal" className="top-calendar-card">
                    <Image src={iconCalendarCard} alt="calendar-card" />
                    <span className="text-calendar-card-courseSchedule">
                      {moment(item.date).format("MMM DD, YYYY")}
                    </span>
                  </Space>
                  <FormButton
                    styleButton={{ width: 201 }}
                    onClick={() => handleCourse(item)}
                    title={"Class Detail"}
                    type={"submit"}
                    classButton={"button-course-card"}
                  />
                </div>
              </Card>
            </Col>
          );
        })}
      </Row>
      <PaginationComponent
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        total={50}
      />
    </div>
  );
}
