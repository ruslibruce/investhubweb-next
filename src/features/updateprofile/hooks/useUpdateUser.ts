import { saveUser } from "@/shared/api/mutation/user";
import { imageIAM } from "@/shared/constants/imageUrl";
import {
  DASHBOARD_HOME,
  DASHBOARD_PREFERENCES,
  DASHBOARD_PRELIMINARY,
} from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { storageCheck, storageSet } from "@/shared/utils/clientStorageUtils";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

const useUpdateUser = () => {
  const queryClient = useQueryClient();
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const {t: translate} = useTranslation();

  const mutationQuery = useMutationHook({
    api: saveUser,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables: any, context) {
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        if (data) {
          notification.success({
            message: translate("ProfileSuccess"),
          });
          let temp = {
            ...dataUser,
            user: {
              ...dataUser.user,
              is_profile_updated: true,
            },
            profile: {
              ...variables,
              photo: `${imageIAM}/${variables.photo}`,
            },
          };
          storageSet(USER, temp);
          if (
            dataUser.user?.is_preference_updated &&
            !dataUser.user?.is_placement_test_taken
          ) {
            navigate.push(DASHBOARD_PRELIMINARY);
            queryClient.invalidateQueries({ queryKey: ["users"] });
            return;
          } else if (!dataUser.user?.is_preference_updated) {
            navigate.push(DASHBOARD_PREFERENCES);
            queryClient.invalidateQueries({ queryKey: ["users"] });
            return;
          } else {
            navigate.push(DASHBOARD_HOME);
            queryClient.invalidateQueries({ queryKey: ["users"] });
          }
        }
      },
    },
  });

  const handleUpdateUser = (data: {}) => {
    mutationQuery.mutate(data);
  };

  return {
    mutationQuery,
    handleUpdateUser,
  };
};

export default useUpdateUser;
