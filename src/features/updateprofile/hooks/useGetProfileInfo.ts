import { getProfileInfo } from "@/shared/api/mutation/user";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = getProfileInfo();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetProfileInfo = (initialData?: any) => {
  const fetchDataCountry = getProfileInfo();

  const fetchProfileInfo = useFetchHook({
    keys: fetchDataCountry.key,
    api: fetchDataCountry.api,
    initialData,
    options: {},
  });

  return {
    fetchProfileInfo,
  };
};

export default useGetProfileInfo;
