import FormButton from "@/components/FormButton";
import FormDropdown from "@/components/FormDropdown";
import FormInputFormik from "@/components/FormInputFormik";
import FormRadioInput from "@/components/FormRadioInput";
import FormSelect from "@/components/FormSelect";
import { IMAGE_FORMAT, USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { DeleteOutlined, FolderOpenTwoTone } from "@ant-design/icons";
import { Col, Space, Upload, message } from "antd";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import * as yup from "yup";
import useGetCountry from "../hooks/useGetCountry";
import useUpdateUser from "../hooks/useUpdateUser";
import { handlingErrors } from "@/shared/utils/handlingErrors";
import { regexPhoneNumber } from "@/shared/utils/helper";
import LoaderSpinGif from "@/components/LoaderSpinGif";
import { imageIAM } from "@/shared/constants/imageUrl";
const { Dragger } = Upload;

const itemsInstitution = [
  {
    key: "1",
    label: "Public",
  },
  {
    key: "2",
    label: "OJK",
  },
];

const UpdateProfilePage = () => {
  const navigate = useRouter();
  const dataUser = storageCheck(USER);
  const { profile: dataProfileInfo } = dataUser;
  const { handleUpdateUser } = useUpdateUser();
  const { t: translate } = useTranslation();
  const { fetchQuery } = useGetCountry([]);
  const { data: dataCountry } = fetchQuery;
  const countryName = dataCountry?.find(
    (item: any) => item.key === dataProfileInfo?.country
  );

  // console.log("countryName", countryName);
  // console.log("dataProfileInfo", dataProfileInfo);

  React.useEffect(() => {
    if (!dataUser.isLogin) {
      navigate.back();
    }
  }, [dataUser]);

  const handleSubmitUpdate = (values: any) => {
    let idCountry = dataCountry.find(
      (item: any) => item.name === values.country
    );
    handleUpdateUser({
      ...values,
      country: idCountry?.id,
    });
  };

  const profileValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    name: yup
      .string()
      .required(translate("RequireName"))
      .max(
        50,
        ({ max }) =>
          `${translate("NameLength")} ${max} ${translate("Characters")}`
      ),
    address: yup
      .string()
      .max(
        100,
        ({ max }) =>
          `${translate("AddressLength")} ${max} ${translate("Characters")}`
      ),
    date_of_birth: yup.string(),
    phone: yup
      .string()
      .matches(regexPhoneNumber(), translate("ValidPhone"))
      .max(
        15,
        ({ max }) =>
          `${translate("PhoneNumberLength")} ${max} ${translate("Characters")}`
      ),
    country: yup.string().required(translate("RequireCountry")),
  });

  const handleUploadChange = (info: any, setFieldValue: any) => {
    const { status, response } = info;
    if (status !== "uploading") {
      console.log("info.file", info.file);
      console.log("info.fileList", info.fileList);
      const dataFile = info.file;

      if (dataFile.response?.status === "success") {
        setFieldValue("photo", dataFile.response.data.filename);
      }
    }
    if (status === "done") {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === "error") {
      console.log(
        `${info.file.name} file upload failed because of ${info.file.error}`
      );
      if (response && response?.message == "Unauthorized") {
        handlingErrors({
          error: response,
          text: "Upload Image",
        });
      }
    }
  };

  return (
    <Space direction="vertical" style={{ display: "flex", marginTop: "20px" }}>
      <div
        className="container-updateProfile"
        style={{
          // backgroundImage: `url(${bgHero})`,
          backgroundSize: "cover",
          display: "flex",
          flexDirection: "column",
          flex: 1,
          height: "100%",
          width: "100%",
          alignItems: "flex-start",
          justifyContent: "center",
          backgroundColor: "#ffffff",
        }}
      >
        <div className="text-title">{translate("UpdateProfile")}</div>
        <div className="text-title-info">{translate("Desc_UpdateProfile")}</div>
        {dataProfileInfo ? (
          <Formik
            enableReinitialize={true}
            validationSchema={profileValidationSchema}
            initialValues={{
              name: dataProfileInfo.name,
              date_of_birth: dataProfileInfo.date_of_birth,
              phone: dataProfileInfo.phone,
              gender: dataProfileInfo.gender,
              email: dataProfileInfo.email,
              address: dataProfileInfo.address,
              country: countryName
                ? countryName.value
                : dataProfileInfo.country,
              institution_id: dataProfileInfo.institution_id,
              photo: dataProfileInfo.photo.split("/").pop(),
            }}
            onSubmit={(values) => {
              console.log("values", values);
              handleSubmitUpdate(values);
            }}
          >
            {({ handleSubmit, isValid, setFieldValue, values, errors }) => (
              <>
                <Field
                  component={FormInputFormik}
                  placeholder={translate("EnterFullName")}
                  title={translate("LabelFullName")}
                  type={"text"}
                  name={"name"}
                  styleForm={{ marginTop: 20 }}
                />
                <Field
                  component={FormInputFormik}
                  placeholder={translate("EnterDateOfBirth")}
                  title={`${translate("LabelDateOfBirth")} ${translate(
                    "Optional"
                  )}`}
                  type={"date"}
                  name={"date_of_birth"}
                  isRequired={false}
                />
                <Field
                  component={FormInputFormik}
                  placeholder={translate("EnterPhone")}
                  title={`${translate("LabelPhone")} ${translate("Optional")}`}
                  type={"text"}
                  name={"phone"}
                  isRequired={false}
                />
                <FormRadioInput
                  styleForm={{ marginTop: 10 }}
                  title={translate("LabelGender")}
                  handleChangeRadio={(e) => {
                    setFieldValue("gender", e.target.value);
                  }}
                  checked={values.gender}
                />
                <Field
                  component={FormInputFormik}
                  placeholder={translate("EnterEmail")}
                  title={translate("LabelEmail")}
                  type={"email"}
                  name={"email"}
                  readOnly={true}
                />
                <Field
                  component={FormInputFormik}
                  placeholder={translate("EnterAddress")}
                  title={`${translate("LabelAddress")} ${translate(
                    "Optional"
                  )}`}
                  type={"text"}
                  name={"address"}
                  isRequired={false}
                />
                <FormSelect
                  title={translate("LabelCountry")}
                  placeholder={translate("EnterCountry")}
                  value={values.country}
                  items={dataCountry}
                  handleChange={(value: string) => {
                    setFieldValue("country", value);
                  }}
                />
                {errors.country && (
                  <div className={"error"}>{translate("RequireCountry")}</div>
                )}
                <FormDropdown
                  title={translate("LabelIns")}
                  placeholder={translate("EnterIns")}
                  value={values.institution_id == "1" ? "Public" : "OJK"}
                  data={itemsInstitution}
                  onClick={(e) => setFieldValue("institution_id", e.key)}
                />
                <Col style={{ marginBottom: 20 }}>
                  <p className="textfs14-fw400-gray">
                    {`${translate("PhotoProfile")} ${translate(
                      "Optional"
                    )} (Max. 2MB)`}
                  </p>
                  {values.photo ? (
                    <Space
                      style={{ position: "relative", marginTop: 5 }}
                      direction="horizontal"
                    >
                      <Image
                        alt="image edit"
                        width={200}
                        height={200}
                        style={{
                          width: 400,
                          height: 200,
                          borderRadius: 10,
                        }}
                        src={`${imageIAM}/${values.photo}`}
                      />
                      <DeleteOutlined
                        style={{
                          fontSize: 20,
                          color: "red",
                          position: "absolute",
                          top: 10,
                          right: 10,
                          backgroundColor: "white",
                          padding: 5,
                          borderRadius: 100,
                        }}
                        type="primary"
                        onClick={() => {
                          setFieldValue("photo", "");
                        }}
                      />
                    </Space>
                  ) : (
                    <Dragger
                      {...{
                        name: "file",
                        multiple: true,
                        maxCount: 1,
                        action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-iam/upload-file`,
                        headers: {
                          authorization: `Bearer ${dataUser?.token}`,
                        },
                        accept: IMAGE_FORMAT,
                        beforeUpload: (file) => {
                          console.log("file beforeUpload", file);
                          const isSize = file.size / 1024 / 1024 <= 2;

                          if (!isSize) {
                            message.error(
                              `${file.name} ${translate("SizeMin")} 2MB`
                            );
                          }

                          return isSize || Upload.LIST_IGNORE;
                        },
                        onChange: (info) =>
                          handleUploadChange(info, setFieldValue),
                      }}
                    >
                      <p className="ant-upload-drag-icon">
                        <FolderOpenTwoTone twoToneColor="#737373" />
                      </p>
                      <p className="ant-upload-text">
                        {translate("ClickOrDrag")}
                      </p>
                      <p className="ant-upload-hint"></p>
                    </Dragger>
                  )}
                </Col>
                <FormButton
                  styleButton={{ width: 120, float: "right", display: "flex" }}
                  onClick={handleSubmit}
                  disabled={!isValid}
                  title={
                    !dataUser.user?.is_preference_updated
                      ? translate("Next_Capital")
                      : translate("Update")
                  }
                  type={"submit"}
                />
              </>
            )}
          </Formik>
        ) : (
          <LoaderSpinGif size="large" isFullScreen={true} />
        )}
      </div>
    </Space>
  );
};

export default UpdateProfilePage;
