import { fetchFaq } from "@/shared/api/fetch/faq";
import { fetchStockScreenerProfileCompany } from "@/shared/api/fetch/stockScreener";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchFaq();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetStockScreenerDetail = (id: string, initialData?: any) => {
  const fetchDataStockScreener = id
    ? fetchStockScreenerProfileCompany(id)
    : { key: [], api: () => Promise.resolve(initialData ||[]) };

  const fetchQuery = useFetchHook({
    keys: fetchDataStockScreener.key,
    api: fetchDataStockScreener.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetStockScreenerDetail;
