import { DASHBOARD_STOCK_SCREENER } from "@/shared/constants/path";
import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import type { TableProps, TabsProps } from "antd";
import { Breadcrumb, Card, Space, Table, Tabs } from "antd";
import Image from "next/image";
import { useRouter } from "next/router";
import useGetStockScreenerDetail from "../hooks/useGetStockScreenerDetail";

type DataType = {
  name: string;
  result: string;
};

const columns: TableProps<DataType>["columns"] = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
    width: "30%",
  },
  {
    title: "Description",
    dataIndex: "result",
    key: "result",
  },
];

const dataTable: DataType[] = [
  {
    name: "Nama",
    result: "Astra Agro Lestari",
  },
  {
    name: "Kode",
    result: "AALI",
  },
  {
    name: "Alamat Kantor",
    result:
      "Jl Pulo Ayang RayaBlok OR No 1 Kawasan Industri Pulogadung Jakarta",
  },
  {
    name: "Alamat Email",
    result: "Investor@astra-agro.co.id",
  },
  {
    name: "Telepon",
    result: "461-65-55",
  },
  {
    name: "FAX",
    result: "461-6555, 461-6677, 461-6688",
  },
  {
    name: "NPWP",
    result: "01.334.427.0-054.000",
  },
  {
    name: "Situs",
    result: "http://www.astra-agro.co.id",
  },
];

export default function StockScreenerDetailPage() {
  const navigate = useRouter();

  const { id } = navigate.query;

  const { fetchQuery } = useGetStockScreenerDetail(id as string, []);
  const { isLoading, data } = fetchQuery;

  const onChange = (key: string) => {
    console.log(key);
  };

  const itemsTabs: TabsProps["items"] = [
    { key: "1", label: "Profil" },
    { key: "2", label: "Dividen" },
    { key: "3", label: "Pencatatan Saham" },
    { key: "4", label: "Obligasi & Sukuk" },
    { key: "5", label: "Kalender" },
    { key: "6", label: "Historikal" },
  ];

  return (
    <>
      <Space
        direction="vertical"
        className="hero-container-background"
        size="middle"
        style={{ display: "flex", marginTop: 35, marginBottom: 72 }}
      >
        <div className="stock-screener-detail">
          <div className="stock-screener-detail-breadcrumb">
            <Breadcrumb
              separator=">"
              items={[
                {
                  href: "",
                  title: (
                    <>
                      <Image
                        src={iconBreadcrumb}
                        alt="Breadcrumb Icon"
                      />
                    </>
                  ),
                },
                {
                  href: DASHBOARD_STOCK_SCREENER,
                  title: (
                    <span className="textfs16-fw400-gray">Stock Screener</span>
                  ),
                },
                {
                  title: (
                    <span className="textfs16-fw400-red">Profil Company</span>
                  ),
                },
              ]}
            />
          </div>

          <h2 className="stock-screener-detail-title">Profile Company</h2>
          <h2 className="company-name">Astra Agro Lestari Tbk</h2>

          <div className="stock-screener-detail-tabs">
            <Tabs
              defaultActiveKey="1"
              items={itemsTabs}
              onChange={onChange}
              indicator={{ size: (origin) => origin + 30, align: "center" }}
            />
          </div>
          <div className="stock-screener-detail-card">
            <Card
              style={{
                padding: 16,
                borderRadius: 8,
                boxShadow: "0px 1px 16px 0px rgba(0, 0, 0, 0.10)",
                maxWidth: 1362,
              }}
            >
              <Table
                className="table-card-description"
                size="small"
                columns={columns}
                dataSource={dataTable}
                pagination={false}
                showHeader={false}
                bordered={false}
              />
            </Card>
          </div>
        </div>
      </Space>
    </>
  );
}
