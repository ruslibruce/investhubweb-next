import FooterAuth from "@/components/FooterAuth";
import FormButton from "@/components/FormButton";
import FormInputFormik from "@/components/FormInputFormik";
import RadioButton from "@/components/RadioLanguage";
import useGetTerms from "@/features/termofuse/hooks/useGetTerms";
import { DASHBOARD_LOGIN } from "@/shared/constants/path";
import IconBack from "@/shared/images/icon/arrow-left.webp";
import IconLogo from "@/shared/images/logo/logo_investhub.webp";
import Checbox from "@/shared/widgets/Checbox";
import { Dropdown, Space, notification } from "antd";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import ReCAPTCHA from "react-google-recaptcha";
import * as yup from "yup";
import YupPassword from "yup-password";
YupPassword(yup); // extend yup with password method
import useRegisterForm from "../hooks/useRegisterForm";
import ButtonFlag from "@/shared/components/layout/auth-layout/sider/ButtonFlag";
import useWindowResize from "@/shared/hooks/useWindowResize";
import { regexEmail } from "@/shared/utils/helper";

type registerProps = {
  name: string;
  email: string;
  password: string;
  password_confirmation: string;
};

const FormRegister = () => {
  const navigate = useRouter();
  const { t: translate } = useTranslation();
  const { mutationQuery, handleOnSubmit } = useRegisterForm();
  const { isPending } = mutationQuery;
  const [checked, setChecked] = React.useState<boolean>(false);
  const [token, setToken] = React.useState<any>("");
  const captchaRef = React.useRef<any>(null);
  const { fetchQuery } = useGetTerms({});
  const { data: dataPrivacy } = fetchQuery;

  const handleChecked = () => {
    setChecked(!checked);
  };

  const onVerify = () => {
    const tokenCaptcha = captchaRef.current.getValue();
    setToken(tokenCaptcha);
  };

  const onExpire = () => {
    notification.info({
      message: "reCAPTCHA",
      description: translate("Recaptcha_Expired"),
    });
  };

  const handleRegister = (values: registerProps) => {
    if (!token) {
      notification.error({
        message: "reCAPTCHA",
        description: translate("Recaptcha"),
      });
      return;
    }

    if (!checked) {
      notification.error({
        message: translate("Terms"),
        description: translate("AcceptTerms"),
      });
      return;
    }

    setToken("");
    try {
      handleOnSubmit({
        ...values,
        "g-recaptcha-response": captchaRef.current.getValue(),
      });
      captchaRef.current.reset();
    } catch (error) {
      console.log(error);
    }
  };

  const registerValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    name: yup
      .string()
      .required(translate("RequireName"))
      .max(
        50,
        ({ max }) =>
          `${translate("NameLength")} ${max} ${translate("Characters")}`
      ),
    email: yup
      .string()
      .matches(regexEmail(), translate("ValidEmail"))
      .max(
        30,
        ({ max }) =>
          `${translate("EmailLength")} ${max} ${translate("Characters")}`
      )
      .required(translate("RequireEmail")),
    password: yup
      .string()
      .min(
        8,
        ({ min }) =>
          `${translate("PasswordLength")} ${min} ${translate("Characters")}`
      )
      .required(translate("RequirePassword"))
      .minLowercase(1, translate("Lower_Password"))
      .minUppercase(1, translate("Upper_Password"))
      .minNumbers(1, translate("Number_Password"))
      .minWords(1, translate("Word_Password"))
      .minSymbols(1, translate("Symbol_Password")),
    password_confirmation: yup
      .string()
      .oneOf([yup.ref("password")], translate("Match_Password"))
      .required(translate("Confirm_Password")),
  });

  const { width } = useWindowResize({ defaultWidth: 370, persentage: 0.6 });

  return (
    <Space direction="vertical" className="form-register-container">
      <div style={{ marginTop: "20px" }} className="language-switch">
        <ButtonFlag mobileVersion={true} />
      </div>
      <div className={"container-header-register"}>
        <Image
          width={200}
          src={IconLogo}
          alt="icon-logo"
          className={"icon-logo"}
        />
      </div>
      <div className={"container-title"}>
        <Image
          onClick={() => navigate.push(DASHBOARD_LOGIN)}
          src={IconBack}
          alt={"icon-back"}
          className={"icon-back"}
          style={{ cursor: "pointer" }}
        />
        <div className={"text-title"}>{translate("Account_Register")}</div>
        <div className={"text-title-info"}>{translate("Desc_Register")}</div>
        <Formik
          validationSchema={registerValidationSchema}
          initialValues={{
            name: "",
            email: "",
            password: "",
            password_confirmation: "",
          }}
          onSubmit={(values) => {
            handleRegister(values);
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterFullName")}
                title={translate("LabelFullName")}
                type={"text"}
                name={"name"}
                styleForm={{ marginTop: 20 }}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterEmail")}
                title={translate("LabelEmail")}
                type={"email"}
                name={"email"}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterPassword")}
                title={translate("LabelPasswordCreate")}
                type={"password"}
                name={"password"}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterConfirmPassword")}
                title={translate("LabelConfirmPassword")}
                type={"password"}
                name={"password_confirmation"}
              />
              <div className={"position-recaptcha"}>
                <ReCAPTCHA
                  sitekey={process.env.NEXT_PUBLIC_SITE_KEY as string}
                  onChange={onVerify}
                  onExpired={onExpire}
                  ref={captchaRef}
                />
              </div>
              <div style={{ display: "flex", alignItems: "flex-end", gap: 4 }}>
                <Checbox
                  text={translate("AgreeTerms")}
                  checked={checked}
                  handleChecked={handleChecked}
                />
                <Dropdown
                  trigger={["click"]}
                  dropdownRender={(menu) => (
                    <Space
                      style={{
                        overflowY: "scroll",
                        maxHeight: 300,
                        justifyContent: "space-between",
                        backgroundColor: "white",
                        padding: 20,
                        width: width,
                      }}
                      direction="vertical"
                    >
                      {dataPrivacy && (
                        <Space direction="vertical">
                          <span className="textfs14-fw600-black">
                            {dataPrivacy.title}
                          </span>
                          <span
                            className="textfs14-fw400-gray"
                            style={{ textAlign: "justify" }}
                            dangerouslySetInnerHTML={{
                              __html: dataPrivacy.body,
                            }}
                          />
                        </Space>
                      )}
                    </Space>
                  )}
                >
                  <div className="textfs12-fw400-red">{translate("Terms")}</div>
                </Dropdown>
              </div>
              <FormButton
                styleButton={{ marginTop: 32 }}
                isLoading={isPending}
                onClick={handleSubmit}
                disabled={!isValid}
                title={translate("Submit_Register")}
                type={"submit"}
              />
            </>
          )}
        </Formik>
      </div>
      <FooterAuth
        backToLogin={false}
        hiddenButton={false}
        hiddenLineOr={false}
      />
    </Space>
  );
};

export default FormRegister;
