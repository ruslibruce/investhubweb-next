import { register } from "@/shared/api/mutation/register";
import { DASHBOARD_EMAIL_CONFIRM } from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { storageSet } from "@/shared/utils/clientStorageUtils";
import { notification } from "antd";
import { jwtDecode } from "jwt-decode";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

const useRegisterForm = () => {
  const navigate = useRouter();
  const { t: translate } = useTranslation();

  const mutationQuery = useMutationHook({
    api: register,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        if (data) {
          notification.success({
            message: translate("RegisterSuccess"),
          });
          const decodedToken: any = jwtDecode(data.data.authorisation.token);
          let temp = {
            loading: true,
            isLogin: false,
            user: decodedToken.user,
            initialRoute: "Home",
            token: data.data.authorisation.token,
            fcmToken: null,
          };
          storageSet(USER, temp);
          navigate.replace(DASHBOARD_EMAIL_CONFIRM);
        }
      },
    },
  });

  const handleOnSubmit = (values: any) => {
    mutationQuery.mutate(values);
  };

  return {
    mutationQuery,
    handleOnSubmit,
  };
};

export default useRegisterForm;
