import { putUpdatePreferences } from "@/shared/api/fetch/preferences";
import { DASHBOARD_HOME, DASHBOARD_PRELIMINARY } from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { storageCheck, storageSet } from "@/shared/utils/clientStorageUtils";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

const usePutPreferences = () => {
  const queryClient = useQueryClient();
  const dataUser = storageCheck(USER);
  const navigation = useRouter();
  const { t: translate } = useTranslation();

  const mutationQuery = useMutationHook({
    api: putUpdatePreferences,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables: any, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        queryClient.invalidateQueries({ queryKey: ["preferences"] });
        queryClient.invalidateQueries({ queryKey: ["users"] });
        if (data) {
          notification.success({
            message: translate("PreferenceSuccess"),
          });
          let temp = {
            ...dataUser,
            user: {
              ...dataUser.user,
              is_preference_updated: true,
            },
          };
          storageSet(USER, temp);
          if (!temp.user.is_placement_test_taken) {
            navigation.push(DASHBOARD_PRELIMINARY);
          } else {
            navigation.push(DASHBOARD_HOME);
          }
        }
      },
    },
  });

  const handlePutPreferences = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handlePutPreferences,
  };
};

export default usePutPreferences;
