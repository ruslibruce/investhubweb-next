import { fetchPreferences } from "@/shared/api/fetch/preferences";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchPreferences();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetPreferences = (initialData?: any) => {
  const fetchDataPreferences = fetchPreferences();

  const getPreferences = useFetchHook({
    keys: fetchDataPreferences.key,
    api: fetchDataPreferences.api,
    initialData,
    options: {},
  });

  return {
    getPreferences,
  };
};

export default useGetPreferences;
