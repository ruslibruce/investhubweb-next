import FormButton from "@/components/FormButton";
import { DASHBOARD_PRELIMINARY } from "@/shared/constants/path";
import IconBack from "@/shared/images/icon/arrow-left.webp";
import iconDotMid from "@/shared/images/icon/icon_dot_mid.webp";
import { Space } from "antd";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import useGetPreferences from "../hooks/useGetPreferences";
import usePutPreferences from "../hooks/usePutPreferences";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { USER } from "@/shared/constants/storageStatis";
import LoaderSpinGif from "@/components/LoaderSpinGif";
import NoData from "@/components/NoData";

const PreferencesPage = () => {
  const dataUser = storageCheck(USER);
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const [listPreferences, setListPreferences] = React.useState<any>([]);
  const { getPreferences } = useGetPreferences();
  const { data: dataPreferences } = getPreferences;
  const { handlePutPreferences, mutationQuery } = usePutPreferences();
  const { isPending } = mutationQuery;

  React.useEffect(() => {
    if (!dataUser.isLogin) {
      navigate.back();
    }
  }, []);

  React.useEffect(() => {
    if (dataPreferences) {
      setListPreferences(dataPreferences);
    }
  }, [dataPreferences]);

  const handleChoosePref = (item: any) => {
    let temp = listPreferences.map((val: any) => {
      if (val === item) {
        val.choosen = !val.choosen;
      }
      return val;
    });
    setListPreferences(temp);
  };

  const handleSubmitPreferences = () => {
    let temp = listPreferences.filter((item: any) => item.choosen);
    let data = {
      preferences: temp.map((item: any) => item.id),
    };

    handlePutPreferences(data);
  };

  const checkMax3Value = React.useMemo(() => {
    let temp = listPreferences.filter((item: any) => item.choosen);
    if (temp.length >= 3) {
      return true;
    }
    return false;
  }, [listPreferences]);

  return (
    <Space direction="vertical" style={{ display: "flex", marginTop: "20px" }}>
      <div
        className="container-updateProfile"
        style={{
          // backgroundImage: `url(${bgHero})`,
          backgroundSize: "cover",
          display: "flex",
          flexDirection: "column",
          flex: 1,
          height: "100%",
          width: "100%",
          alignItems: "flex-start",
          justifyContent: "center",
          backgroundColor: "#ffffff",
        }}
      >
        <div className="text-title">{translate("Preferences")}</div>
        <div className="text-title-info">{translate("Desc_Preferences")}</div>
        <div style={{ height: "33px" }} />
        <ul>
          {!dataPreferences && <LoaderSpinGif size="large" />}
          {dataPreferences && dataPreferences.length === 0 && (
            <NoData />
          )}
          {listPreferences.map((item: any) => (
            <li
              style={
                item.choosen
                  ? {
                      color: "#ffffff",
                      backgroundColor: "#9f0e0f",
                      cursor: "pointer",
                    }
                  : { cursor: "pointer" }
              }
              key={item.id}
              onClick={() => handleChoosePref(item)}
            >
              {navigate.locale === "en" ? item.name_en : item.name}
            </li>
          ))}
        </ul>
        <div
          style={{
            display: "flex",
            width: "90%",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Image src={iconDotMid} alt="icon-dot" />
          <div style={{ width: "30%" }}>
            {checkMax3Value ? (
              <FormButton
                isLoading={isPending}
                onClick={handleSubmitPreferences}
                title={
                  !dataUser.user?.is_placement_test_taken
                    ? translate("Next_Capital")
                    : translate("Update")
                }
                type={"submit"}
              />
            ) : (
              <FormButton
                disabled
                title={translate("Next_Capital")}
                type={"submit"}
              />
            )}
          </div>
        </div>
      </div>
    </Space>
  );
};

export default PreferencesPage;
