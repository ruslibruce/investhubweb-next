import { fetchDetailCoursesProgress } from "@/shared/api/fetch/course";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchDetailCoursesProgress();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetDetailCourseProgress = (id: string, initialData?: any) => {
  const fetchDataColors = id
    ? fetchDetailCoursesProgress(id)
    : { key: [], api: () => Promise.resolve(initialData || {}) };

  const fetchDetailCourse = useFetchHook({
    keys: fetchDataColors.key,
    api: fetchDataColors.api,
    initialData,
    options: {},
  });

  return {
    fetchDetailCourse,
  };
};

export default useGetDetailCourseProgress;
