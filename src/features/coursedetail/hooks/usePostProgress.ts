import { postProgress } from "@/shared/api/fetch/course";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";

const usePostProgress = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postProgress,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["courseDetail"] });
        }
      },
    },
  });

  const handlePostProgress = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handlePostProgress,
  };
};

export default usePostProgress;
