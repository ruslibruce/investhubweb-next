import { fetchDetailCourses } from "@/shared/api/fetch/course";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchDetailCourses();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetDetailCourse = (id: string, initialData?: any) => {
  const fetchDataColors = id
    ? fetchDetailCourses(id)
    : { key: [], api: () => Promise.resolve(initialData || {}) };

  const fetchDetailCourse = useFetchHook({
    keys: fetchDataColors.key,
    api: fetchDataColors.api,
    initialData,
    options: {},
  });

  return {
    fetchDetailCourse,
  };
};

export default useGetDetailCourse;
