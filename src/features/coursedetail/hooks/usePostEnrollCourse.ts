import { postEnrollCourse } from "@/shared/api/fetch/course";
import { DASHBOARD_COURSE_PROGRESS } from "@/shared/constants/path";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";
import { useRouter } from "next/router";

const usePostEnrollCourse = () => {
  const queryClient = useQueryClient();
  const navigate = useRouter();

  const mutationQuery = useMutationHook({
    api: postEnrollCourse,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables: any, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        queryClient.invalidateQueries({ queryKey: ["courseDetail"] });
        if (data) {
          navigate.push(`${DASHBOARD_COURSE_PROGRESS}/${variables.id}`);
        }
      },
    },
  });

  const handlePostEnroll = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handlePostEnroll,
  };
};

export default usePostEnrollCourse;
