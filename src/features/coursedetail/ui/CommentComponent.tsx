import NoData from "@/components/NoData";
import useGetComment from "@/features/course/hooks/useGetComment";
import usePostComment from "@/features/course/hooks/usePostComment";
import { checkIsEmptyObject } from "@/shared/utils/helper";
import { SaveOutlined } from "@ant-design/icons";
import { Button, Card, Col, Flex, Input, Row, Spin } from "antd";
import React from "react";
import { useTranslation } from "next-i18next";
import ReplyCommentComponent from "./replyCommentComponent";
import InputEmoji from "react-input-emoji";
import LoaderSpinGif from "@/components/LoaderSpinGif";

type PropsComment = {
  dataCourseDetail: any;
  isModalVisibleComment: boolean;
  type: string;
};

export default function CommentComponent({
  dataCourseDetail,
  isModalVisibleComment,
  type,
}: PropsComment) {
  const { t: translate } = useTranslation();
  const { handleGetCommentCourse, mutationQuery: mutationGetComment } =
    useGetComment();
  const { data: dataApiComment } = mutationGetComment;
  const dataComment = dataApiComment as any;
  const { handleCommentCourse, mutationQuery: mutationPostComment } =
    usePostComment();
  const { isPending: isLoadingComment, isSuccess: isSuccessComment } =
    mutationPostComment;
  const [formComment, setFormComment] = React.useState<any>({
    comment: "",
  });

  React.useEffect(() => {
    if (isSuccessComment) {
      setFormComment((prev: any) => ({
        ...prev,
        comment: "",
      }));
      handleGetCommentCourse(dataCourseDetail?.id, type);
    }
  }, [isSuccessComment]);

  React.useEffect(() => {
    if (isModalVisibleComment) {
      handleGetCommentCourse(dataCourseDetail?.id, type);
      setFormComment({
        parent_id: dataCourseDetail?.id,
        parent_type: type,
        comment: "",
      });
    }
  }, [isModalVisibleComment]);

  const handleAddComment = () => {
    handleCommentCourse(formComment);
  };

  const onChangeComment = (value: any) => {
    setFormComment((prev: any) => ({ ...prev, comment: value }));
  };

  return (
    <Row>
      <Col span={24} style={{ marginBottom: 10 }}>
        <InputEmoji
          value={formComment.comment}
          onChange={onChangeComment}
          placeholder={translate("EnterComment")}
          shouldReturn={true}
          shouldConvertEmojiToImage={false}
        />
      </Col>
      <Flex justify="end" style={{ marginBottom: 24, marginLeft: "auto" }}>
        <Button
          icon={<SaveOutlined />}
          style={{
            backgroundColor: checkIsEmptyObject(formComment)
              ? "#a6a6a6"
              : "#CB0B0C",
            color: "white",
          }}
          disabled={checkIsEmptyObject(formComment)}
          type="primary"
          loading={isLoadingComment}
          onClick={handleAddComment}
        >
          {translate("Comment")}
        </Button>
      </Flex>
      <Col span={24} style={{ marginBottom: 10 }}>
        <p className="textfs18-fw600-black">{translate("ListComment")}</p>
      </Col>
      <Col span={24}>
        {!dataComment && <LoaderSpinGif size="large" />}
        {dataComment?.length === 0 && <NoData />}
        {dataComment?.length > 0 &&
          dataComment?.map((item: any) => (
            <ReplyCommentComponent
              key={item.id}
              item={item}
              isModalVisibleComment={isModalVisibleComment}
              handleGetCommentCourse={handleGetCommentCourse}
              dataCourseDetail={dataCourseDetail}
              type={type}
              isLoadingComment={isLoadingComment}
            />
          ))}
      </Col>
    </Row>
  );
}
