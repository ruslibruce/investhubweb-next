import usePostComment from "@/features/course/hooks/usePostComment";
import { COLORS } from "@/shared/styles/color";
import { checkIsEmptyObject } from "@/shared/utils/helper";
import { SaveOutlined } from "@ant-design/icons";
import { Button, Card, Col, Row } from "antd";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import React from "react";
import { useTranslation } from "next-i18next";
import InputEmoji from "react-input-emoji";
dayjs.extend(relativeTime);

type PropsReplyComment = {
  item: any;
  isModalVisibleComment: boolean;
  handleGetCommentCourse: any;
  dataCourseDetail: any;
  type: string;
  isLoadingComment: boolean;
};

export default function ReplyCommentComponent({
  item,
  isModalVisibleComment,
  handleGetCommentCourse,
  dataCourseDetail,
  type,
  isLoadingComment,
}: PropsReplyComment) {
  const { t: translate, i18n } = useTranslation();
  dayjs.locale(i18n.language);
  const { handleCommentCourse, mutationQuery: mutationPostComment } =
    usePostComment();
  const { isPending: isLoadingPostComment, isSuccess: isSuccessComment } =
    mutationPostComment;
  const [formCommentReply, setFormCommentReply] = React.useState<any>({
    comment: "",
  });
  const [isReplyComment, setIsReplyComment] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (isSuccessComment) {
      setFormCommentReply((prev: any) => ({
        ...prev,
        comment: "",
      }));
      setIsReplyComment(false);
      handleGetCommentCourse(dataCourseDetail?.id, type);
    }

    if (isModalVisibleComment) {
      setIsReplyComment(false);
    }
  }, [isSuccessComment, isModalVisibleComment]);

  const handleCommentReply = () => {
    handleCommentCourse(formCommentReply);
  };

  const onChangeCommentReply = (value: any) => {
    setFormCommentReply((prev: any) => ({ ...prev, comment: value }));
  };

  React.useEffect(() => {
    if (isReplyComment) {
      setFormCommentReply((prev: any) => ({
        ...prev,
        parent_id: item.id,
        parent_type: "comment",
        comment: "",
      }));
    }
  }, [isReplyComment]);

  return (
    <Col style={{ marginLeft: 20 }}>
      <Card loading={isLoadingComment}>
        <Row justify={"space-between"}>
          <p className="textfs14-fw600-black">{item.name}</p>
          <p className="textfs10-fw600-black">
            {dayjs(item.created_at).fromNow()}
          </p>
        </Row>
        <p className="textfs14-fw400-black">{item.comment}</p>
      </Card>
      <p
        style={{
          cursor: "pointer",
          marginLeft: "auto",
          color: COLORS.BLUE,
        }}
        onClick={() => {
          setFormCommentReply((prev: any) => ({
            ...prev,
            parent_id: item.id,
            parent_type: "comment",
            comment: "",
          }));
          setIsReplyComment(true);
        }}
        className="textfs14-fw400-black"
      >
        {translate("Reply")}
      </p>
      {item.replies?.length > 0 &&
        item.replies?.map((val: any) => (
          <ReplyCommentComponent
            key={val.id}
            item={val}
            isModalVisibleComment={isModalVisibleComment}
            handleGetCommentCourse={handleGetCommentCourse}
            dataCourseDetail={dataCourseDetail}
            type={type}
            isLoadingComment={isLoadingComment}
          />
        ))}
      {isReplyComment && (
        <Row>
          <InputEmoji
            value={formCommentReply.comment}
            onChange={onChangeCommentReply}
            placeholder={translate("EnterComment")}
            shouldReturn={true}
            shouldConvertEmojiToImage={false}
          />
          <Button
            icon={<SaveOutlined />}
            style={{
              backgroundColor: checkIsEmptyObject(formCommentReply)
                ? "#a6a6a6"
                : "#CB0B0C",
              color: "white",
              marginBottom: 10,
              marginTop: 10,
              marginLeft: "auto",
            }}
            disabled={checkIsEmptyObject(formCommentReply)}
            type="primary"
            loading={isLoadingPostComment}
            onClick={handleCommentReply}
          >
            {translate("Comment")}
          </Button>
        </Row>
      )}
    </Col>
  );
}
