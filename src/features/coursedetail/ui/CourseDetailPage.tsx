import BackgroundHero from "@/components/BackgroundHero";
import FormButton from "@/components/FormButton";
import LoaderSpinGif from "@/components/LoaderSpinGif";
import NoData from "@/components/NoData";
import TextResponsive from "@/components/TextResponsive";
import useGetCollection from "@/features/collection/hooks/useGetCollection";
import useAddCollectionContent from "@/features/collectiondetail/hooks/useAddCollectionContent";
import useGetLike from "@/features/course/hooks/useGetLike";
import usePostLike from "@/features/course/hooks/usePostLike";
import ModalComponent from "@/shared/components/ModalComponent";
import {
  DASHBOARD_COURSE_PROGRESS,
  DASHBOARD_MY_COLLECTION,
} from "@/shared/constants/path";
import { BUTTON_COLOR, USER } from "@/shared/constants/storageStatis";
import iconCommentCard from "@/shared/images/icon/icon_comment_card.webp";
import iconMenuCard from "@/shared/images/icon/icon_menu_card.webp";
import iconShareCard from "@/shared/images/icon/icon_share_card.webp";
import iconCalendarCard from "@/shared/images/svg/ic_red_calendar_month.svg";
import IconBookmark from "@/shared/images/svg/icon_bookmark.svg";
import { COLORS } from "@/shared/styles/color";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  LikeFilled,
  LikeOutlined,
  SaveFilled
} from "@ant-design/icons";
import {
  Avatar,
  Button,
  Card,
  Col,
  Flex,
  Modal,
  Rate,
  Row,
  Space,
  Tabs,
  TabsProps
} from "antd";
import dynamic from "next/dynamic";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import { useTranslation } from "next-i18next";
import {
  EmailIcon,
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  LineIcon,
  LineShareButton,
  LinkedinIcon,
  LinkedinShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton,
  XIcon,
} from "react-share";
import useGetDetailCourse from "../hooks/useGetDetailCourse";
import useGetDetailCourseProgress from "../hooks/useGetDetailCourseProgress";
import usePostEnrollCourse from "../hooks/usePostEnrollCourse";
import CommentComponent from "./CommentComponent";
import ListCollapseSection from "./ListCollapseSection";
const DynamicHeaderVideo = dynamic(() => import("@/components/Player"), {
  loading: () => <LoaderSpinGif size="large" />,
});

export default function CourseDetailPage() {
  const { t: translate } = useTranslation();
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const { isReady, query } = navigate;
  const { id } = query;
  console.log("id", id);

  // React.useEffect(() => {
  //   // do not run effect until query is ready
  //   if (!isReady) {
  //     return;
  //   }
  // }, [isReady]);

  const { fetchDetailCourse } = dataUser.isLogin
    ? useGetDetailCourseProgress(id as string)
    : useGetDetailCourse(id as string);
  const { fetchLikeCourse } = useGetLike({}, id as string);
  const { data: dataResultLikeCourse, isLoading: isLoadingLike } =
    fetchLikeCourse;
  const { data: dataLikeCourse = [] } = dataResultLikeCourse;
  console.log("dataResultLikeCourse", dataResultLikeCourse);
  const { isLoading, data: dataCourseDetail } = fetchDetailCourse;
  console.log("dataCourseDetail", dataCourseDetail);
  const [widthScreen, setWidthScreen] = React.useState(350);
  const [heightScreen, setHeightScreen] = React.useState(160);
  const { handlePostEnroll } = usePostEnrollCourse();
  const { handleLikeCourse } = usePostLike();
  const [isModalVisibleComment, setIsModalVisibleComment] =
    React.useState(false);
  const [isModalVisibleShare, setIsModalVisibleShare] = React.useState(false);
  const [shareUrl, setShareUrl] = React.useState("");
  const [isModalUploadAddCollection, setIsModalUploadAddCollection] =
    React.useState<boolean>(false);
  const { handleAddCollectionContent } = useAddCollectionContent();
  const { fetchQuery: fetchCollection } = useGetCollection([]);
  const { data: dataCollection, isLoading: isLoadingCollection } =
    fetchCollection;
  const [indexChoose, setIndexChoose] = React.useState(0);
  const [idCourseChoose, setIdCourseChoose] = React.useState();

  const IconShare = [
    {
      id: 1,
      icon: (
        <FacebookShareButton
          url={shareUrl}
          className="Demo__some-network__share-button"
        >
          <FacebookIcon size={52} round />
        </FacebookShareButton>
      ),
    },
    {
      id: 2,
      icon: (
        <TwitterShareButton
          url={shareUrl}
          title={dataCourseDetail?.title}
          className="Demo__some-network__share-button"
        >
          <XIcon size={52} round />
        </TwitterShareButton>
      ),
    },
    {
      id: 3,
      icon: (
        <TelegramShareButton
          url={shareUrl}
          title={dataCourseDetail?.title}
          className="Demo__some-network__share-button"
        >
          <TelegramIcon size={52} round />
        </TelegramShareButton>
      ),
    },
    {
      id: 4,
      icon: (
        <WhatsappShareButton
          url={shareUrl}
          title={dataCourseDetail?.title}
          className="Demo__some-network__share-button"
        >
          <WhatsappIcon size={52} round />
        </WhatsappShareButton>
      ),
    },
    {
      id: 5,
      icon: (
        <LinkedinShareButton
          url={shareUrl}
          className="Demo__some-network__share-button"
        >
          <LinkedinIcon size={52} round />
        </LinkedinShareButton>
      ),
    },
    {
      id: 6,
      icon: (
        <EmailShareButton
          url={shareUrl}
          subject={dataCourseDetail?.title}
          body="body"
          className="Demo__some-network__share-button"
        >
          <EmailIcon size={52} round />
        </EmailShareButton>
      ),
    },
    {
      id: 7,
      icon: (
        <LineShareButton
          url={shareUrl}
          title={dataCourseDetail?.title}
          className="Demo__some-network__share-button"
        >
          <LineIcon size={52} round />
        </LineShareButton>
      ),
    },
  ];

  React.useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth <= 950) {
        setWidthScreen(200);
        setHeightScreen(100);
      } else if (window.innerWidth <= 1100) {
        setWidthScreen(250);
        setHeightScreen(120);
      } else {
        setWidthScreen(350);
        setHeightScreen(160);
      }
    };

    // Panggil handleResize pertama kali untuk mengatur isSmallScreen
    handleResize();

    // Tambahkan event listener untuk memantau perubahan ukuran layar
    window.addEventListener("resize", handleResize);

    // Clean up event listener saat komponen dibongkar
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []); // Dependensi kosong agar efek hanya berjalan sekali saat komponen dipasang

  const handleCourse = () => {
    console.log("clicked");
    navigate.push(`${DASHBOARD_COURSE_PROGRESS}/${dataCourseDetail?.id}`);
  };

  const handleCourseEnroll = () => {
    console.log("clicked");
    handlePostEnroll({
      course_id: id,
      id: id,
    });
  };

  const onChange = (key: string) => {
    console.log("onChange", key);
  };

  const items: TabsProps["items"] = [
    {
      key: "1",
      label: "Description",
      children: "",
    },
    // {
    //   key: "2",
    //   label: "Reviews",
    //   children: "",
    // },
    // {
    //   key: "3",
    //   label: "Discussion",
    //   children: "",
    // },
    // {
    //   key: "4",
    //   label: "Resources",
    //   children: "",
    // },
    // {
    //   key: "5",
    //   label: "Instructor",
    //   children: "",
    // },
  ];

  const onClickContent = (content: any) => {
    console.log("content", content);
  };

  const handleCancelComment = () => {
    setIsModalVisibleComment(false);
  };

  const handleCancelShare = () => {
    setIsModalVisibleShare(false);
  };

  const handleAddContent = () => {
    let data = {
      collection_id: dataCollection[indexChoose].id,
      course_id: idCourseChoose,
    };
    handleAddCollectionContent(data);
  };

  if (isLoading) {
    return <LoaderSpinGif size="large" isFullScreen />;
  }

  if (dataCourseDetail && `${dataCourseDetail.id}` !== id) {
    return <LoaderSpinGif size="large" isFullScreen />;
  }

  if (dataCourseDetail && Object.keys(dataCourseDetail).length === 0) {
    return <NoData />;
  }

  return (
    <div className="hero-container-backgroundImg">
      <Row
        style={{
          borderBottomLeftRadius: 40,
          borderBottomRightRadius: 40,
          paddingTop: 20,
          paddingBottom: 30,
          paddingLeft: 70,
          paddingRight: 70,
          position: "relative",
          overflow: "hidden",
        }}
        gutter={[16, 16]}
        align={"middle"}
        className="hero-container-antd"
      >
        <BackgroundHero />
        <Col span={24}>
          <TextResponsive
            mobile={24}
            color="white"
            fontWeight={700}
            isSpan
            desktop={35}
          >
            {dataCourseDetail?.title}
          </TextResponsive>
          <TextResponsive
            mobile={14}
            color="white"
            fontWeight={400}
            isSpan
            desktop={16}
            numberOfLines={3}
          >
            {dataCourseDetail?.description}
          </TextResponsive>
          <Space direction="horizontal">
            <Rate
              style={{ paddingTop: 6 }}
              disabled
              defaultValue={dataCourseDetail?.rating}
            />
            <span className="icon-play-desc-subtitle">{`(${dataCourseDetail?.rating} ratings)`}</span>
          </Space>
        </Col>
        {/* <Col span={8}>
          <DynamicHeaderVideo
            url={"https://www.youtube.com/watch?v=ZOVtvj-CFeM"}
            width={widthScreen}
            height={heightScreen}
          />
          <Space direction="horizontal">
            <Avatar
              style={{ backgroundColor: "#9F0E0F" }}
              icon={<UserOutlined />}
            />
            <span className="icon-play-desc-title">
              Henry S. Miller{" "}
              <span className="icon-play-desc-subtitle">+2 instructor</span>
            </span>
            <div style={{ width: "10px" }} />
            <Image src={iconStudent} alt="icon-student" />
            <span className="textfs14-fw400-white">{"40 students"}</span>
          </Space>
        </Col> */}
      </Row>
      <Tabs
        className="tabs-detail-course"
        defaultActiveKey="1"
        items={items}
        onChange={onChange}
        indicator={{ size: (origin) => origin + 30, align: "center" }}
      />
      <Space direction="horizontal" className="container-card-course-detail">
        <Card bordered={false} className="card-course-detail">
          <span className="text-top-card-detail">
            {dataCourseDetail?.title}
          </span>
          <div className="top-card-position-courseDetail">
            <Space className="top-left-position-card-courseDetail">
              <Image src={iconMenuCard} alt="menu-card" />
              <span className="text-top-courseDetail">
                {dataCourseDetail?.conten_provider}
              </span>
            </Space>
            <Space className="top-calendar-card">
              <Image src={iconCalendarCard} alt="calendar-card" />
              <span className="text-top-courseDetail">
                {dataCourseDetail?.publish_date}
              </span>
            </Space>
          </div>
          <div className="image-card-courseDetail">
            {dataCourseDetail?.cover_image_file && (
              <Image
                src={dataCourseDetail?.cover_image_file}
                width={300}
                height={300}
                alt="image-dummy-card"
              />
            )}
          </div>
          <div style={{ height: "40px" }} />
          <span className="text-title-card">{"About Course"}</span>
          <div style={{ height: "16px" }} />
          <div className="text-description-courseDetail">
            {dataCourseDetail?.description}
          </div>
          <div style={{ height: "40px" }} />
          <span className="text-title-card">{"Course Category"}</span>
          <div style={{ marginTop: 5 }} className="selection-button">
            <Button
              style={{
                cursor: "default",
                background: BUTTON_COLOR[0].color,
                color: "white",
              }}
              type="primary"
              disabled
            >
              {dataCourseDetail?.category}
            </Button>
          </div>
          <div style={{ height: "40px" }} />
          <Space className="container-courseDetail-learn" direction="vertical">
            <span className="text-title-card">
              {"What will you learn in this online course?"}
            </span>
            <div style={{ height: "16px" }} />
            <div>{dataCourseDetail?.description}</div>
          </Space>
          {dataUser.isLogin && (
            <Row style={{ alignItems: "center", marginTop: 10 }}>
              <Avatar.Group
                max={{
                  count: 3,
                  style: {
                    color: COLORS.WHITE,
                    backgroundColor: COLORS.PRIMARY,
                  },
                }}
              >
                {dataLikeCourse?.map((item: any) => (
                  <Avatar
                    key={item.id}
                    style={{ backgroundColor: COLORS.PRIMARY }}
                  >
                    {item.name.slice(0, 1).toLowerCase()}
                  </Avatar>
                ))}
              </Avatar.Group>
            </Row>
          )}
          {dataUser.isLogin && (
            <Row style={{ justifyContent: "space-between", marginTop: 10 }}>
              <Col
                style={{
                  cursor: "pointer",
                  display: "flex",
                  alignItems: "center",
                  gap: 8,
                }}
                onClick={() =>
                  handleLikeCourse({ course_id: dataCourseDetail?.id })
                }
                className="like-card"
              >
                {dataResultLikeCourse.liked ? (
                  <LikeFilled style={{ color: COLORS.PRIMARY }} />
                ) : (
                  <LikeOutlined style={{ color: COLORS.PRIMARY }} />
                )}
                <span className="text-bottom-courseDetail">{"Like"}</span>
              </Col>
              <Col
                style={{
                  cursor: "pointer",
                  display: "flex",
                  alignItems: "center",
                  gap: 8,
                }}
                onClick={() => {
                  setIsModalVisibleComment(true);
                }}
                className="comment-card"
              >
                <Image src={iconCommentCard} alt="coment-card" />
                <span className="text-bottom-courseDetail">{"Comment"}</span>
              </Col>
              <Col
                style={{
                  cursor: "pointer",
                  display: "flex",
                  alignItems: "center",
                  gap: 8,
                }}
                onClick={() => {
                  setIsModalUploadAddCollection(true);
                  setIdCourseChoose(dataCourseDetail?.id);
                }}
                className="comment-card"
              >
                <Image
                  src={IconBookmark}
                  width={20}
                  height={20}
                  alt="coment-card"
                />
                <span className="text-bottom-courseDetail">
                  {"Add Bookmark"}
                </span>
              </Col>
              <Col
                style={{
                  cursor: "pointer",
                  display: "flex",
                  alignItems: "center",
                  gap: 8,
                }}
                onClick={() => {
                  setShareUrl(
                    `${window.location.origin}/courses/detail/${dataCourseDetail?.id}`
                  );
                  setIsModalVisibleShare(true);
                }}
                className="share-card"
              >
                <Image src={iconShareCard} alt="share-card" />
                <span className="text-bottom-courseDetail">{"Share"}</span>
              </Col>
            </Row>
          )}
          <Flex justify="end">
            {dataUser.isLogin ? (
              <FormButton
                styleButton={{ width: 202, marginTop: 44 }}
                onClick={
                  dataCourseDetail?.participant_progress
                    ? handleCourse
                    : handleCourseEnroll
                }
                title={
                  dataCourseDetail?.participant_progress
                    ? "Continue Course"
                    : "Start Course"
                }
                type={"submit"}
                classButton={"button-course-card"}
              />
            ) : (
              <FormButton
                styleButton={{ width: 202, marginTop: 44 }}
                onClick={handleCourse}
                title={"View Course"}
                type={"submit"}
                classButton={"button-course-card"}
              />
            )}
          </Flex>
        </Card>
        <Space direction="vertical" className="space-card-courseDetail">
          <Card bordered={false} className="container-card-right">
            {/* <Space className="position-card-right">
              <Space className="content-left-card-right">
                <div className="bullet-detail-course" />
                <span className="text-left-card-detailCourse">
                  {"Skill Level"}
                </span>
              </Space>
              <Space className="image-card-detail">
                <Image src={iconCupCard} alt="calendar-card" />
                <span className="text-right-card-detailCourse-red">
                  {dataCourseDetail?.course_level}
                </span>
              </Space>
            </Space> */}
            <div style={{ height: "16px" }} />
            {/* <Space className="position-card-right">
              <Space className="content-left-card-right">
                <div className="bullet-detail-course" />
                <span className="text-left-card-detailCourse">
                  {"Duration"}
                </span>
              </Space>
              <span className="text-right-card-detailCourse-red">
                {dataDummy2.totalHours}
              </span>
            </Space>
            <div style={{ height: "16px" }} /> */}
            <Space className="position-card-right">
              <Space className="content-left-card-right">
                <div className="bullet-detail-course" />
                <span className="text-left-card-detailCourse">
                  {"Last Updated"}
                </span>
              </Space>
              <span className="text-right-card-detailCourse">
                {dataCourseDetail?.publish_date}
              </span>
            </Space>
            <div style={{ height: "16px" }} />
            {/* <Space className="position-card-right">
              <Space className="content-left-card-right">
                <div className="bullet-detail-course" />
                <span className="text-left-card-detailCourse">
                  {"Certificate"}
                </span>
              </Space>
              <span className="text-right-card-detailCourse">
                {dataDummy2.certificate}
              </span>
            </Space>
            <div style={{ height: "16px" }} /> */}
            <Space className="position-card-right">
              <Space className="content-left-card-right">
                <div className="bullet-detail-course" />
                <span className="text-left-card-detailCourse">
                  {"Students"}
                </span>
              </Space>
              <span className="text-right-card-detailCourse">
                {dataCourseDetail?.number_of_participants}
              </span>
            </Space>
          </Card>
          <Card bordered={false} className="container-card-right">
            {dataCourseDetail?.sections?.length === 0 && <NoData />}
            {dataCourseDetail?.sections?.map((section: any, index: number) => (
              <ListCollapseSection
                item={section}
                key={section.id}
                index={index}
                onClickContent={onClickContent}
              />
            ))}
          </Card>
        </Space>
      </Space>

      {/* Modal Comment */}
      <ModalComponent
        centered
        title={translate("CommentCourse")}
        open={isModalVisibleComment}
        width={1100}
        onCancel={handleCancelComment}
        footer={null}
      >
        <CommentComponent
          dataCourseDetail={dataCourseDetail}
          isModalVisibleComment={isModalVisibleComment}
          type="course"
        />
      </ModalComponent>

      {/* Modal Share */}
      <Modal
        centered
        title="Share Course"
        open={isModalVisibleShare}
        width={1100}
        onCancel={handleCancelShare}
        footer={[
          <Button
            style={{
              backgroundColor: "#CB0B0C",
              color: "white",
            }}
            type="primary"
            loading={isLoading}
            onClick={handleCancelShare}
          >
            {translate("LabelOK")}
          </Button>,
        ]}
      >
        <Card>
          <Row gutter={30} style={{ marginBottom: 24, marginTop: 32 }}>
            {IconShare.map((item) => (
              <Col key={item.id}>{item.icon}</Col>
            ))}
          </Row>
        </Card>
      </Modal>

      {/* Modal Add Collection */}
      <ModalComponent
        width={1000}
        title="Select a Collection"
        open={isModalUploadAddCollection}
        onCancel={() => setIsModalUploadAddCollection(false)}
        footer={[
          <Flex justify="end">
            {dataCollection?.length > 0 ? (
              <Button
                icon={<SaveFilled style={{ fontSize: 20 }} />}
                type="primary"
                onClick={() => {
                  setIsModalUploadAddCollection(false);
                  handleAddContent();
                }}
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                {translate("Save")}
              </Button>
            ) : (
              <Button
                icon={<SaveFilled style={{ fontSize: 20 }} />}
                type="primary"
                onClick={() => {
                  navigate.push(DASHBOARD_MY_COLLECTION);
                }}
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                {translate("NavCollection")}
              </Button>
            )}
          </Flex>,
        ]}
      >
        <Flex
          style={{
            width: "100%",
            overflowY: "scroll",
            justifyContent: "center",
          }}
        >
          {isLoadingCollection && <LoaderSpinGif size="large" />}
          {dataCollection?.length === 0 && (
            <Col>
              <NoData />
              <span className="textfs20-fw600-black">
                {translate("CreateFolderCollection")}
              </span>
            </Col>
          )}
          {dataCollection?.length > 0 &&
            dataCollection?.map((item: any, index: any) => (
              <Space
                key={item.id}
                onClick={() => {
                  setIndexChoose(index);
                }}
                direction="horizontal"
                style={{
                  background: indexChoose === index ? "#F7ECEC" : "#FFF",
                  width: 950,
                  padding: "0px 20px",
                  gap: 20,
                  cursor: "pointer",
                }}
              >
                <Image
                  src={IconBookmark}
                  width={40}
                  height={40}
                  alt="coment-card"
                />
                <Space direction="vertical">
                  <span className="textfs20-fw600-black">{item.name}</span>
                  {/* <span className="textfs16-fw600-gray">5 Course</span> */}
                </Space>
              </Space>
            ))}
        </Flex>
      </ModalComponent>
    </div>
  );
}
