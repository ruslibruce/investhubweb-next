import { USER } from "@/shared/constants/storageStatis";
import iconAudioCourse from "@/shared/images/icon/icon_audio.webp";
import iconDocumentCourse from "@/shared/images/icon/icon_document.webp";
import iconPlayCourse from "@/shared/images/icon/icon_play_courseDetail.webp";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { FileTextFilled, UpOutlined } from "@ant-design/icons";
import { Col, Divider, Row, Space, Typography } from "antd";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import React from "react";
const { Text } = Typography;

type ListCollapseSectionProps = {
  item: any;
  index?: number;
  onClickContent: (val: any) => void;
};
export default function ListCollapseSection({
  item,
  index,
  onClickContent,
}: ListCollapseSectionProps) {
  const { t: translate } = useTranslation();
  const [isCollapse, setIsCollapse] = React.useState(item.is_collapse);
  const dataUser = storageCheck(USER);
  const handleToggleCollapse = () => {
    setIsCollapse(!isCollapse);
  };
  return (
    <Row style={{ width: 350 }}>
      {index !== 0 && <Divider />}
      <Col
        onClick={handleToggleCollapse}
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          // backgroundColor: "#F5F5F5",
        }}
      >
        <Space>
          <UpOutlined
            style={
              !isCollapse
                ? {
                    transform: "rotate(180deg)",
                    marginRight: 5,
                    fontSize: 15,
                    color: "#9F0E0F",
                  }
                : { marginRight: 5, fontSize: 15, color: "#9F0E0F" }
            }
            className="ic-arrow-courseDetail"
          />
          <Text style={{ fontWeight: 600, color: "black" }}>{item.title}</Text>
        </Space>
        {/* <Text style={{ fontWeight: 400, color: "gray" }}>{"30min"}</Text> */}
      </Col>
      {!isCollapse &&
        item.pre_test?.length > 0 &&
        item.pre_test.map((val: any) => (
          <Col span={24}>
            <Divider />
            <Row justify={"space-between"} onClick={() => onClickContent(val)}>
              <Col style={{ display: "flex", gap: 5 }}>
                <FileTextFilled style={{ color: "#9F0E0F", fontSize: 25 }} />
                <Text style={{ fontWeight: 400, color: "gray" }}>
                  {val.name}
                </Text>
                {dataUser.isLogin && (
                  <Text style={{ fontWeight: 400, color: "green" }}>{`${
                    val.attempt?.is_completed ? 100 : 0
                  }%`}</Text>
                )}
              </Col>
              <Col>
                <Text style={{ fontWeight: 400, color: "gray" }}>{`${
                  val.duration
                } ${translate("Minutes")}`}</Text>
              </Col>
            </Row>
          </Col>
        ))}
      {!isCollapse &&
        item.contents?.length > 0 &&
        item.contents.map((val: any) => (
          <Col span={24}>
            <Divider />
            <Row justify={"space-between"} onClick={() => onClickContent(val)}>
              <Col style={{ display: "flex", gap: 5 }}>
                <Image
                  src={
                    val.content_type.toLowerCase() === "video"
                      ? iconPlayCourse
                      : val.content_type.toLowerCase() === "articles"
                      ? iconDocumentCourse
                      : iconAudioCourse
                  }
                  width={25}
                  height={25}
                  alt="icon-play-course"
                />
                <Text style={{ fontWeight: 400, color: "gray" }}>
                  {val.title}
                </Text>
                {dataUser.isLogin && (
                  <Text style={{ fontWeight: 400, color: "green" }}>{`${
                    val.progress?.is_completed
                      ? 100
                      : val.progress?.progress
                      ? val.progress?.progress
                      : 0
                  }%`}</Text>
                )}
              </Col>
              <Col>
                <Text
                  style={{ fontWeight: 400, color: "gray" }}
                >{`${val.content_length} second`}</Text>
              </Col>
            </Row>
          </Col>
        ))}
      {!isCollapse &&
        item.post_test?.length > 0 &&
        item.post_test.map((val: any) => (
          <Col span={24}>
            <Divider />
            <Row justify={"space-between"} onClick={() => onClickContent(val)}>
              <Col style={{ display: "flex", gap: 5 }}>
              <FileTextFilled style={{ color: "#9F0E0F", fontSize: 25 }} />
                <Text style={{ fontWeight: 400, color: "gray" }}>
                  {val.name}
                </Text>
                {dataUser.isLogin && (
                  <Text style={{ fontWeight: 400, color: "green" }}>{`${
                    val.attempt?.is_completed ? 100 : 0
                  }%`}</Text>
                )}
              </Col>
              <Col>
                <Text style={{ fontWeight: 400, color: "gray" }}>{`${
                  val.duration
                } ${translate("Minutes")}`}</Text>
              </Col>
            </Row>
          </Col>
        ))}
      {!isCollapse &&
        item.sections.length > 0 &&
        item.sections.map((val2: any) => (
          <ListCollapseSection
            onClickContent={onClickContent}
            key={val2.id}
            item={val2}
          />
        ))}
    </Row>
  );
}
