import { DASHBOARD_NEWS_DETAIL } from "@/shared/constants/path";
import iconCalendarCard from "@/shared/images/icon/icon_calendar_card.webp";
import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import { ArrowRightOutlined, SearchOutlined } from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Flex,
  Input,
  Menu,
  Row,
  Space,
  Spin,
} from "antd";
import { MenuProps } from "antd/es/menu";
import { useRouter } from "next/router";
import React, { useState } from "react";
import useGetNews from "../hooks/useGetNews";
import useGetNewsCategory from "../hooks/useGetNewsCategory";

import NoData from "@/components/NoData";
import PaginationComponent from "@/shared/components/PaginationComponent";
import moment from "moment";
import Image from "next/image";
import { paramsToString } from "@/shared/utils/string";
import LoaderSpinGif from "@/components/LoaderSpinGif";
import { useTranslation } from "next-i18next";

export default function NewsPage() {
  const { t: translate } = useTranslation();
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [current, setCurrent] = useState("all");
  const [currentApi, setCurrentApi] = useState("");
  const { fetchQuery } = useGetNews(
    {},
    paramsToString({ page: currentPage, limit: 8, category: currentApi })
  );
  const { data: dataResultNews, isLoading } = fetchQuery;
  // console.log("dataResultNews", dataResultNews);

  const { data: dataNews } = dataResultNews;
  const { fetchQuery: fetchCategory } = useGetNewsCategory([]);
  const { isLoading: isLoadingCategory, data: dataCategory } = fetchCategory;
  const [selectedCategory, setSelectedCategory] = useState("all");
  const navigate = useRouter();
  const [dataHeadlines, setDataHeadlines] = useState<any>();
  console.log("dataNews", dataNews);

  React.useEffect(() => {
    if (dataNews && currentPage === 1) {
      setDataHeadlines(dataNews[0]);
    }
  }, [dataNews]);

  const onClick: MenuProps["onClick"] = (e) => {
    console.log("click ", e);
    if (e.key === "all") {
      setCurrent("all");
      setCurrentApi("");
      setSelectedCategory("all");
      return;
    }
    setCurrent(e.key);
    setCurrentApi(e.key);
    dataCategory.map((item: any) => {
      if (item.key == e.key) {
        setSelectedCategory(item.label);
      }
    });
  };

  const handleNews = (event: React.MouseEvent<HTMLElement>, value: any) => {
    console.log("query", value);

    if (value.source_id) {
      window.open(value.link, "_blank");
      return;
    }

    navigate.push(`${DASHBOARD_NEWS_DETAIL}/${value.id}`);
  };

  const renderCards = () => {
    if (!dataNews) {
      // Tampilkan pesan loading atau spinner
      return (
        <Col className="card-news-container">
          <LoaderSpinGif size="large" />
        </Col>
      );
    }
    if (dataNews?.length === 0) {
      // Tampilkan pesan loading atau spinner
      return <NoData />;
    }
    return dataNews?.map((newsItem: any, index: any) => (
      <Col key={index} className="card-news-container">
        <Card
          onClick={(e) => handleNews(e, newsItem)}
          hoverable
          bordered={true}
          cover={
            <div className="">
              {newsItem.source_id ? (
                <img
                  style={{ width: "100%", height: 300 }}
                  width={100}
                  height={100}
                  src={newsItem.cover_image}
                  alt="render card"
                />
              ) : (
                <Image
                  style={{ width: "100%", height: 300 }}
                  width={100}
                  height={100}
                  src={newsItem.cover_image}
                  alt="render card"
                />
              )}
            </div>
          }
          className="card-course"
        >
          <Space direction="vertical">
            <div style={{ display: "flex", flexWrap: "wrap" }}>
              <span
                className="button-card-news-container"
                style={{
                  marginTop: "12px",
                  marginRight: "25px",
                  fontSize: 10,
                }}
              >
                {newsItem.category ? newsItem.category : "RSS NEWS"}
              </span>
              <Image
                style={{ marginTop: "20px", height: 20, width: 20 }}
                src={iconCalendarCard}
                alt="calendar-card"
              />
              <span
                style={{ marginLeft: "8px", marginTop: "23px" }}
                className="text-calendar-card-courseSchedule"
              >
                {moment().format("MMM DD, YYYY")}
              </span>
            </div>

            <span
              className="text-title-card"
              style={{
                display: "-webkit-box",
                WebkitLineClamp: 2,
                WebkitBoxOrient: "vertical",
                overflow: "hidden",
                textOverflow: "ellipsis",
                height: 60,
              }}
              dangerouslySetInnerHTML={{ __html: newsItem?.subject }}
            />
            <span
              className="text-title-info"
              style={{
                display: "-webkit-box",
                WebkitLineClamp: 3,
                WebkitBoxOrient: "vertical",
                overflow: "hidden",
                textOverflow: "ellipsis",
                height: 85,
              }}
              dangerouslySetInnerHTML={{ __html: newsItem?.body }}
            />
          </Space>
        </Card>
      </Col>
    ));
  };

  const renderTodayHeadlinesCard = () => {
    const newsItem = dataHeadlines;
    return (
      <div className="today-headlines-main">
        <span className="label-today">{translate("TodayHeadlines")}</span>
        <Card
          bordered={true}
          loading={!dataHeadlines && !dataNews}
          style={{ marginBottom: 10 }}
          cover={
            newsItem?.source_id ? (
              <img
                style={{ width: "100%", height: 300 }}
                width={100}
                height={100}
                src={newsItem?.cover_image}
                alt="render today"
                className="responsive-image"
              />
            ) : (
              <Image
                style={{ width: "100%", height: 300 }}
                width={100}
                height={100}
                src={newsItem?.cover_image}
                alt="render today"
                className="responsive-image"
              />
            )
          }
          className="card-course margin-left-news-event"
        >
          {!newsItem && <NoData />}
          {newsItem && (
            <Space direction="vertical">
              <div style={{ display: "flex", flexWrap: "wrap" }}>
                <div
                  className="button-today-headlines-news"
                  style={{ marginTop: "12px", marginRight: "25px" }}
                >
                  {newsItem?.category ? newsItem?.category : "RSS NEWS"}
                </div>
                <div style={{ display: "flex" }}>
                  <Image
                    style={{ marginTop: "20px", height: 25 }}
                    src={iconCalendarCard}
                    alt="calendar-card"
                  />
                  <span
                    style={{ marginLeft: "8px", marginTop: "23px" }}
                    className="text-calendar-card-courseSchedule"
                  >
                    {moment().format("MMM DD, YYYY")}
                  </span>
                  {/* <Image
                  style={{
                    marginLeft: "25px",
                    marginTop: "20px",
                    width: "25px",
                    height: "25px",
                  }}
                  src={likeDefault}
                  alt="like-card"
                />
                <span style={{ marginTop: "20px" }}>10</span>
                <Image
                  style={{
                    width: "24px",
                    height: "24px",
                    marginLeft: "25px",
                    marginTop: "20px",
                  }}
                  src={iconCommentCard}
                  alt="comment-card"
                />
                <span
                  style={{
                    marginTop: "20px",
                    marginLeft: "4px",
                    fontSize: "14px",
                  }}
                >
                  10
                </span> */}
                </div>
              </div>
              <div className="today-headlines-main-card-footer">
                <span
                  className="text-title-card"
                  dangerouslySetInnerHTML={{ __html: newsItem?.subject }}
                />
                <span
                  className="text-desc-card-courseSchedule"
                  dangerouslySetInnerHTML={{ __html: newsItem?.body }}
                />

                <div style={{ color: "gray" }}>
                  <div style={{ display: "flex" }}>
                    <Col>
                      <span
                        style={{ paddingBottom: "12px", cursor: "pointer" }}
                        onClick={(e) => handleNews(e, newsItem)}
                      >
                        Read More{" "}
                      </span>
                    </Col>
                    <Col
                      style={{ marginTop: 4, cursor: "pointer" }}
                      onClick={(e) => handleNews(e, newsItem)}
                    >
                      {" "}
                      <ArrowRightOutlined />
                    </Col>
                  </div>
                </div>
              </div>
            </Space>
          )}
        </Card>
      </div>
    );
  };

  return (
    <div className="hero-container-background">
      <Breadcrumb
        separator=">"
        className="breadcrumb-event-news"
        items={[
          {
            href: "",
            title: (
              <>
                <Image src={iconBreadcrumb} alt="Breadcrumb Icon" />
              </>
            ),
          },
          {
            title: (
              <span className="textfs16-fw400-red">{translate("News")}</span>
            ),
          },
        ]}
      />

      <Menu
        onClick={onClick}
        selectedKeys={[current]}
        mode="horizontal"
        className="menu-event-news"
        items={dataCategory}
        style={{
          marginTop: "24px",
          backgroundColor: "#fff",
        }}
      />

      {selectedCategory === "all" && (
        <>
          <Row>
            <div className="today-headlines-news-container">
              {renderTodayHeadlinesCard()}
              {/* Sisi kanan */}
              {/* Rekomen */}
              {/* <Col className="recommended-news">
                  <Card
                    hoverable
                    bordered={false}
                    style={{
                      marginTop: "105px",
                      marginRight: "40px",
                      marginLeft: "30px",
                    }}
                  >
                    <span
                      style={{
                        marginTop: "25px",
                        fontSize: "24px",
                        fontWeight: "bold",
                      }}
                    >
                      Recommended
                    </span>
                    <br />
                    {[0, 1, 2, 3, 4, 5, 6].map((item, index) => (
                      <div key={item.id} style={{ marginTop: "30px" }}>
                        <span
                          style={{
                            fontSize: 14,
                            fontWeight: "bold",
                            marginRight: 18,
                          }}
                        >
                          Bussiness
                        </span>
                        <span
                          style={{ marginRight: 18 }}
                          className="textfs14-fw400-gray"
                        >
                          •
                        </span>
                        <span className="textfs14-fw400-gray">
                          22 Oct 2024 09:12
                        </span>

                        <Row style={{ marginTop: "15px" }}>
                          <Col span={4}>
                            <Image
                              style={{
                                width: "45px",
                                height: "45px",
                                borderRadius: "15px",
                              }}
                              src={imageDummyCard}
                              alt="comment-card"
                            />
                          </Col>
                          <Col span={20}>
                            <span
                              style={{
                                fontWeight: "bold",
                                fontSize: "18px",
                              }}
                            >
                              Class adds $30 millions to its balance sheet
                            </span>
                          </Col>
                        </Row>
                      </div>
                    ))}
                  </Card>
                </Col> */}
            </div>
          </Row>
          <Row gutter={[16, 16]} className="upcoming-event-header">
            <Col span={10}>
              <Space
                style={{
                  fontWeight: "bold",
                  fontSize: "28px",
                }}
              >
                {translate("LatestNews")}
              </Space>
            </Col>
          </Row>
          <Row
            className="padding-left-news-event"
            style={{ marginTop: 10 }}
            gutter={8}
          >
            {renderCards()}
          </Row>
          <PaginationComponent
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            total={dataResultNews.records}
            limit={dataResultNews.limit}
          />
          {/* <div className="popular-news-newsPage">
              <Col>
                <span
                  style={{
                    fontWeight: "bold",
                    fontSize: "28px",
                  }}
                >
                  Popular News
                </span>
              </Col>
              <Col className="input">
                <Input
                  prefix={<SearchOutlined />}
                  style={{
                    marginBottom: "20px",
                    width: "200px",
                    height: "50px",
                  }}
                  placeholder="Search Here"
                />
              </Col>
            </div>
            <Row style={{ marginTop: "15px" }} gutter={[8, 8]}>
              {renderCards()}
            </Row> */}
          {/* Bottom Pagination */}
          {/* <PaginationComponent
              currentPage={currentPage}
              setCurrentPage={setCurrentPage}
              total={dataResultNews.records}
              limit={dataResultNews.limit}
            /> */}
        </>
      )}
      {selectedCategory !== "all" && (
        <>
          <Row gutter={[16, 16]} className="upcoming-event-header">
            <Col span={10}>
              <Space
                style={{
                  fontWeight: "bold",
                  fontSize: 28,
                  marginTop: 10,
                }}
              >
                {selectedCategory}
              </Space>
            </Col>
          </Row>
          <Row
            className="padding-left-news-event"
            style={{ marginTop: 10 }}
            gutter={8}
          >
            {renderCards()}
          </Row>
          <PaginationComponent
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            total={dataResultNews.records}
            limit={dataResultNews.limit}
          />
        </>
      )}
    </div>
  );
}
