import iconCalendarCard from "@/shared/images/icon/icon_calendar_card.webp";
import iconBreadcrumb from "@/shared/images/news/News.webp";
import imageDummyCard from "@/shared/images/news/dummy4.webp";
import { Breadcrumb, Button, Card, Col, Row, Space } from "antd";
import { MenuProps } from "antd/es/menu";
import { useRouter } from "next/router";
import { useState } from "react";

import useGetDetailEvent from "./hooks/useGetDetailEvent";

import { DASHBOARD_EVENT } from "@/shared/constants/path";
import moment from "moment";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import LoaderSpinGif from "@/components/LoaderSpinGif";
import NoData from "@/components/NoData";

export default function NewsPage() {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const { query } = navigate;
  const { id } = query;
  const { fetchQuery } = useGetDetailEvent(id as string);
  const { isLoading, data: dataEvent } = fetchQuery;
  console.log("dataEvent detail", dataEvent);

  const renderEventDetailCard = () => {
    if (isLoading) {
      // Tampilkan pesan loading atau spinner
      return <LoaderSpinGif size="large" />;
    }

    if (`${dataEvent.id}` !== id) {
      // Tampilkan pesan loading atau spinner
      return <LoaderSpinGif size="large" />;
    }

    if (!dataEvent) {
      // Tampilkan pesan jika dataNews tidak tersedia
      return <NoData />;
    }
    
    return (
      <div className="top-detail-event-main">
        <Card
          bordered={true}
          style={{
            marginBottom: 45,
            marginTop: 20,
          }}
          className="card-course"
        >
          <div>
            <p
              className="detail-event-content-title"
              dangerouslySetInnerHTML={{ __html: dataEvent?.title }}
            />

            <Space
              direction="horizontal"
              wrap
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Button style={{ marginRight: "16px" }} danger>
                {dataEvent.category}
              </Button>

              <span
                style={{ marginTop: "23px" }}
                className="detail-event-content-location"
              >
                <span style={{ marginRight: "5px" }}>
                  {dataEvent?.location}
                </span>

                {moment(dataEvent?.publish_time).format("MMM DD, YYYY")}
              </span>
            </Space>
            <Image
              src={dataEvent?.cover_image}
              width={100}
              height={100}
              alt="image-dummy-card"
              style={{ width: "100%", height: 400, marginTop: 20 }}
            />
          </div>
          <Space direction="vertical">
            <p
              style={{ marginTop: "24px", color: "gray" }}
              dangerouslySetInnerHTML={{ __html: dataEvent?.description }}
            />
            {/* <div className="detail-event-card-interaction">
              <Image
                style={{
                  marginTop: "20px",
                  width: "25px",
                  height: "25px",
                }}
                src={likeDefault}
                alt="like-card"
              />
              <span style={{ marginTop: "20px" }}>Like</span>
              <Image
                style={{
                  width: "24px",
                  height: "24px",
                  marginLeft: "25px",
                  marginTop: "20px",
                }}
                src={iconCommentCard}
                alt="comment-card"
              />
              <span
                style={{
                  marginTop: "20px",
                  marginLeft: "4px",
                  fontSize: "14px",
                }}
              >
                Comment
              </span>
              <Image
                style={{
                  width: "24px",
                  height: "24px",
                  marginLeft: "25px",
                  marginTop: "20px",
                }}
                src={iconShareCard}
                alt="comment-card"
              />
              <span
                style={{
                  marginTop: "20px",
                  marginLeft: "4px",
                  fontSize: "14px",
                }}
              >
                Share
              </span>
              <Image
                style={{
                  width: "24px",
                  height: "24px",
                  marginLeft: "25px",
                  marginTop: "20px",
                }}
                src={iconBookmarkCard}
                alt="comment-card"
              />
              <span
                style={{
                  marginTop: "20px",
                  marginLeft: "4px",
                  fontSize: "14px",
                }}
              >
                Bookmark
              </span>
            </div> */}
          </Space>
        </Card>
      </div>
    );
  };

  return (
    <div className="hero-container-background">
      <Breadcrumb
        separator=">"
        className="breadcrumb-event-news"
        items={[
          {
            href: "/event",
            title: (
              <>
                <Image src={iconBreadcrumb} alt="Breadcrumb Icon" />
              </>
            ),
          },
          {
            title: (
              <span style={{ fontSize: "14px" }}>{translate("Event")}</span>
            ),
            href: DASHBOARD_EVENT,
          },
          {
            title: (
              <p
                style={{ fontSize: "14px", color: "#9F0E0F", paddingTop: 2 }}
                dangerouslySetInnerHTML={{ __html: dataEvent?.title }}
              />
            ),
          },
        ]}
      />

      <>
        <Row>
          <div className="top-detail-event-container margin-left-news-event">
            {renderEventDetailCard()}
            <Col className="top-detail-event-aside">
              {/* Sisi kanan */}
              {/* Rekomen */}
              {/* <Card
                  hoverable
                  bordered={false}
                  style={{ marginTop: "105px", marginRight: "40px" }}
                >
                  <span
                    style={{
                      marginTop: "25px",
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                  >
                    Top Events
                  </span>
                  <br />
                  {[1, 2, 3, 4, 5, 6].map((item, index) => (
                    <div style={{ marginTop: "30px" }}>
                      <span
                        style={{
                          fontSize: 14,
                          fontWeight: "bold",
                          marginRight: 18,
                        }}
                      >
                        Jakarta
                      </span>
                      <span
                        style={{ marginRight: 18 }}
                        className="textfs14-fw400-gray"
                      >
                        •
                      </span>
                      <span className="textfs14-fw400-gray">
                        22 Oct 2024 09:12
                      </span>

                      <Row style={{ marginTop: "15px" }}>
                        <Col span={4}>
                          <Image
                            style={{
                              width: "45px",
                              height: "45px",
                              borderRadius: "15px",
                            }}
                            src={imageDummyCard}
                            alt="comment-card"
                          />
                        </Col>
                        <Col span={20}>
                          <span
                            style={{
                              fontWeight: "bold",
                              fontSize: "18px",
                            }}
                          >
                            Class adds $30 millions to its balance sheet
                          </span>
                        </Col>
                      </Row>
                    </div>
                  ))}
                </Card> */}
            </Col>
          </div>
        </Row>
        {/* <div className="upcoming-detail-event-header">
            <div>
              <span
                style={{
                  fontWeight: "bold",
                  fontSize: "28px",
                }}
              >
                Upcoming Event
              </span>
            </div>
            <div className="upcoming-detail-event-filter">
              <Select
                placeholder="All Category"
                style={{ width: 250, height: 40 }}
                onChange={handleChange}
                options={[
                  { value: "all", label: "All Category" },
                  { value: "promotion", label: "Promotion" },
                  { value: "investasi", label: "Investasi" },
                  { value: "sport", label: "Sport" },
                  { value: "cultural event", label: "Cultural event" },
                  { value: "organizational", label: "Organizational" },
                ]}
              />
            </div>
            <div className="upcoming-detail-event-filter">
              <Select
                placeholder="Filter Event Type"
                style={{ width: 250, height: 40 }}
                onChange={handleChange}
                options={[
                  { value: "weekend", label: "Weekend" },
                  { value: "weekdays", label: "Weekdays" },
                ]}
              />
            </div>
            <div className="upcoming-detail-event-filter">
              <Select
                placeholder="Filter location"
                style={{ width: 250, height: 40 }}
                onChange={handleChange}
                options={[
                  { value: "all", label: "All" },
                  { value: "jakarta", label: "Jakarta" },
                  { value: "bandung", label: "Bandung" },
                  { value: "yogyakarta", label: "Yogyakarta" },
                  { value: "solo", label: "Solo" },
                  { value: "surabaya", label: "Surabaya" },
                ]}
              />
            </div>
          </div>
          <div
            className="upcoming-detail-event-content"
            style={{ marginTop: "30px" }}
          >
            {renderCards()}
          </div> */}
        {/* Bottom Pagination */}
        {/* <PaginationComponent
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            total={50}
          /> */}
      </>
    </div>
  );
}
