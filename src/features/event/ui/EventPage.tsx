import { DASHBOARD_EVENT_DETAIL } from "@/shared/constants/path";
import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import { ArrowRightOutlined, SearchOutlined } from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Flex,
  Input,
  Menu,
  Row,
  Space,
  Spin,
} from "antd";
import { MenuProps } from "antd/es/menu";
import { useRouter } from "next/router";
import React, { useState } from "react";
import useGetEvent from "../hooks/useGetEvent";
import useGetEventCategory from "../hooks/useGetEventCategory";

import NoData from "@/components/NoData";
import PaginationComponent from "@/shared/components/PaginationComponent";
import moment from "moment";
import Image from "next/image";
import { paramsToString } from "@/shared/utils/string";
import LoaderSpinGif from "@/components/LoaderSpinGif";
import { useTranslation } from "next-i18next";

export default function EventPage() {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const [currentPage, setCurrentPage] = React.useState<number>(1);
  const [current, setCurrent] = useState("all");
  const [currentApi, setCurrentApi] = useState("");
  const { fetchQuery } = useGetEvent(
    {},
    paramsToString({ page: currentPage, limit: 8, category: currentApi })
  );
  const { data: dataResultEvent, isLoading } = fetchQuery;
  const { data: dataEvent } = dataResultEvent;
  const { fetchQuery: fetchCategory } = useGetEventCategory();
  const { data: dataCategory } = fetchCategory;
  const [selectedCategory, setSelectedCategory] = useState("all");
  const [dataHeadlines, setDataHeadlines] = useState<any>();
  console.log("dataEvent", dataEvent);

  React.useEffect(() => {
    if (dataEvent && currentPage === 1) {
      setDataHeadlines(dataEvent[0]);
    }
  }, [dataEvent]);

  const onClick: MenuProps["onClick"] = (e) => {
    console.log("click ", e);
    if (e.key === "all") {
      setCurrent("all");
      setCurrentApi("");
      setSelectedCategory("all");
      return;
    }
    setCurrent(e.key);
    setCurrentApi(e.key);
    dataCategory.map((item: any) => {
      if (item.key == e.key) {
        setSelectedCategory(item.label);
      }
    });
  };

  const handleEvent = (event: React.MouseEvent<HTMLElement>, value: any) => {
    navigate.push(`${DASHBOARD_EVENT_DETAIL}/${value.id}`);
  };

  const renderCards = () => {
    if (!dataEvent) {
      // Tampilkan pesan loading atau spinner
      return (
        <Col className="card-event-container">
          <LoaderSpinGif size="large" />
        </Col>
      );
    }
    if (dataEvent?.length === 0) return <NoData />;
    return dataEvent?.map((data: any, index: any) => (
      <Col key={index} className="card-event-container">
        <Card
          hoverable
          loading={!dataEvent}
          onClick={(e) => handleEvent(e, data)}
          bordered={true}
          cover={
            <Image
              style={{ width: "100%", height: 300 }}
              width={100}
              height={100}
              src={data.cover_image}
              alt="image-dummy-card"
            />
          }
          className="card-course"
        >
          <Space direction="vertical">
            <div style={{ display: "flex", alignItems: "left" }}>
              <Button style={{ marginTop: "12px" }} danger>
                {data.category}
              </Button>
            </div>

            <span
              className="text-title-card"
              style={{
                display: "-webkit-box",
                WebkitLineClamp: 2,
                WebkitBoxOrient: "vertical",
                overflow: "hidden",
                textOverflow: "ellipsis",
                height: 60,
              }}
              dangerouslySetInnerHTML={{ __html: data.title }}
            />
            <span
              className="text-title-info"
              style={{
                display: "-webkit-box",
                WebkitLineClamp: 3,
                WebkitBoxOrient: "vertical",
                overflow: "hidden",
                textOverflow: "ellipsis",
                height: 85,
              }}
              dangerouslySetInnerHTML={{ __html: data.description }}
            />
            <div style={{ display: "flex", alignItems: "left" }}>
              <span
                style={{
                  fontWeight: "bold",
                  fontSize: "16px",
                  marginTop: "12px",
                }}
              >
                {data.location}
              </span>

              <span
                style={{
                  color: "gray",
                  marginLeft: "8px",
                  marginTop: "14px",
                  fontSize: "14px",
                }}
              >
                {moment(data.publish_time).format("MMM DD, YYYY")}
              </span>
            </div>
          </Space>
        </Card>
      </Col>
    ));
  };

  const renderTopEvent = () => {
    const eventItem = dataHeadlines;
    return (
      <div className="top-event-main">
        <span className="label-today">{translate("TopEvent")}</span>
        <Card
          bordered={true}
          loading={!dataHeadlines && !dataEvent}
          style={{ marginBottom: 10 }}
          cover={
            <Image
              src={eventItem?.cover_image}
              width={100}
              height={100}
              className="responsive-image"
              alt="image-event"
            />
          }
          className="card-course margin-left-news-event"
        >
          {!eventItem && <NoData />}
          {eventItem && (
            <Space direction="vertical">
              <div style={{ display: "flex", alignItems: "left" }}>
                <Button style={{ marginTop: "12px" }} danger>
                  {eventItem?.category}
                </Button>
                <span
                  className="text-calendar-card-courseSchedule"
                  style={{
                    marginLeft: "13%",
                    marginRight: "10px",
                    marginTop: "20px",
                  }}
                >
                  {eventItem?.location}
                </span>
                <span
                  style={{
                    marginLeft: "8px",
                    marginTop: "20px",
                    fontSize: "14px",
                  }}
                >
                  {moment(eventItem?.publish_time).format("MMM DD, YYYY")}
                </span>
                {/* <Image
                style={{
                  marginLeft: "25px",
                  marginTop: "20px",
                  width: "25px",
                  height: "25px",
                }}
                src={likeDefault}
                alt="like-card"
                className="icon-like-event-headline"
              />
              <span
                style={{ marginTop: "20px", fontSize: "14px" }}
                className="like-event-headline"
              >
                10
              </span>
              <Image
                style={{
                  width: "24px",
                  height: "24px",
                  marginLeft: "25px",
                  marginTop: "20px",
                }}
                src={iconCommentCard}
                alt="comment-card"
                className="icon-comment-event-headline"
              />
              <span
                style={{
                  marginTop: "20px",
                  marginLeft: "4px",
                  fontSize: "14px",
                }}
                className="comment-event-headline"
              >
                10
              </span> */}
              </div>

              <span
                className="text-title-card"
                dangerouslySetInnerHTML={{ __html: eventItem?.title }}
              />
              <span
                className="text-desc-card-courseSchedule"
                dangerouslySetInnerHTML={{ __html: eventItem?.description }}
              />
              <div style={{ color: "gray" }}>
                <Row>
                  <Col className="event-read-more">
                    <span
                      style={{ paddingBottom: "12px", cursor: "pointer" }}
                      onClick={(e) => handleEvent(e, eventItem)}
                    >
                      {translate("ReadMore")}
                    </span>
                  </Col>
                  <Col
                    className="event-read-more-icon"
                    style={{ marginTop: 4, cursor: "pointer" }}
                    onClick={(e) => handleEvent(e, eventItem)}
                  >
                    <ArrowRightOutlined />
                  </Col>
                </Row>
              </div>
            </Space>
          )}
        </Card>
      </div>
    );
  };
  return (
    <div className="hero-container-background">
      <Breadcrumb
        className="breadcrumb-event-news"
        separator=">"
        items={[
          {
            href: "",
            title: (
              <>
                <Image src={iconBreadcrumb} alt="Breadcrumb Icon" />
              </>
            ),
          },
          {
            title: (
              <span className="textfs16-fw400-red">{translate("Event")}</span>
            ),
          },
        ]}
      />

      <Menu
        onClick={onClick}
        selectedKeys={[current]}
        mode="horizontal"
        className="menu-event-news"
        items={dataCategory}
        style={{
          marginTop: "24px",
          backgroundColor: "#fff",
        }}
      />

      {selectedCategory === "all" && (
        <>
          <Row>
            <div className="top-event-container">
              {renderTopEvent()}
              {/* Sisi kanan */}
              {/* Rekomen */}
              {/* <Col className="top-event-aside">
                <Card
                  hoverable
                  bordered={false}
                  style={{ marginTop: "105px", marginRight: "40px" }}
                >
                  <span
                    style={{
                      marginTop: "25px",
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                  >
                    Top Events
                  </span>
                  <br />
                  {[1, 2, 3, 4].map((item: any) => (
                    <div key={item.id} style={{ marginTop: "30px" }}>
                      <span
                        style={{
                          fontSize: 14,
                          fontWeight: "bold",
                          marginRight: 18,
                        }}
                      >
                        Jakarta
                      </span>
                      <span
                        style={{ marginRight: 18 }}
                        className="textfs14-fw400-gray"
                      >
                        •
                      </span>
                      <span className="textfs14-fw400-gray">
                        22 Oct 2024 09:12
                      </span>

                      <Row style={{ marginTop: "15px" }}>
                        <Col span={4}>
                          <Image
                            style={{
                              width: "45px",
                              height: "45px",
                              borderRadius: "15px",
                            }}
                            src={imageDummyCard}
                            alt="comment-card"
                          />
                        </Col>
                        <Col span={20}>
                          <span
                            style={{
                              fontWeight: "bold",
                              fontSize: "18px",
                            }}
                          >
                            Class adds $30 millions to its balance sheet
                          </span>
                        </Col>
                      </Row>
                    </div>
                  ))}
                </Card>
              </Col> */}
            </div>
          </Row>
          <Row gutter={[16, 16]} className="upcoming-event-header">
            <Col span={10}>
              <Space
                style={{
                  fontWeight: "bold",
                  fontSize: "28px",
                }}
              >
                {translate("UpcomingEvent")}
              </Space>
            </Col>
            {/* <Col span={10} style={{ display: "flex" }}>
              <Col className="event-all-category">
                <Select
                  placeholder="All Category"
                  style={{ width: 250, height: 40 }}
                  onChange={handleChange}
                  options={[
                    { value: "all", label: "All Category" },
                    { value: "promotion", label: "Promotion" },
                    { value: "investasi", label: "Investasi" },
                    { value: "sport", label: "Sport" },
                    { value: "cultural event", label: "Cultural event" },
                    { value: "organizational", label: "Organizational" },
                  ]}
                />
              </Col>

              <Col className="event-filter-location">
                <Select
                  placeholder="Filter location"
                  style={{ width: 250, height: 40 }}
                  onChange={handleChange}
                  options={[
                    { value: "all", label: "All" },
                    { value: "jakarta", label: "Jakarta" },
                    { value: "bandung", label: "Bandung" },
                    { value: "yogyakarta", label: "Yogyakarta" },
                    { value: "solo", label: "Solo" },
                    { value: "surabaya", label: "Surabaya" },
                  ]}
                />
              </Col>
            </Col> */}
          </Row>
          <Row
            className="padding-left-news-event"
            style={{ marginTop: 10 }}
            gutter={8}
          >
            {renderCards()}
          </Row>
          {/* Bottom Pagination */}
          <PaginationComponent
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            total={dataResultEvent?.records || 0}
          />
          {/* <Row gutter={[16, 16]} className="upcoming-event-header">
            <Col span={10}>
              <span
                style={{
                  fontWeight: "bold",
                  fontSize: "28px",
                }}
              >
                Upcoming Event
              </span>
            </Col>
            <Col span={10} style={{ display: "flex" }}>
              <Col className="event-all-category">
                <Select
                  placeholder="All Category"
                  style={{ width: 250, height: 40 }}
                  onChange={handleChange}
                  options={[
                    { value: "all", label: "All Category" },
                    { value: "promotion", label: "Promotion" },
                    { value: "investasi", label: "Investasi" },
                    { value: "sport", label: "Sport" },
                    { value: "cultural event", label: "Cultural event" },
                    { value: "organizational", label: "Organizational" },
                  ]}
                />
              </Col>

              <Col className="event-filter-location">
                <Select
                  placeholder="Filter location"
                  style={{ width: 250, height: 40 }}
                  onChange={handleChange}
                  options={[
                    { value: "all", label: "All" },
                    { value: "jakarta", label: "Jakarta" },
                    { value: "bandung", label: "Bandung" },
                    { value: "yogyakarta", label: "Yogyakarta" },
                    { value: "solo", label: "Solo" },
                    { value: "surabaya", label: "Surabaya" },
                  ]}
                />
              </Col>
            </Col>
          </Row>
          <Row style={{ marginTop: "10px" }} gutter={[8, 8]}>
            {renderCards()}
          </Row> */}
          {/* Bottom Pagination */}
          {/* <PaginationComponent
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            total={50}
          /> */}
        </>
      )}
      {selectedCategory !== "all" && (
        <>
          <Row gutter={[16, 16]} className="upcoming-event-header">
            <Col span={10}>
              <Space
                style={{
                  fontWeight: "bold",
                  fontSize: 28,
                  marginTop: 10,
                }}
              >
                {selectedCategory}
              </Space>
            </Col>
          </Row>
          <Row
            className="padding-left-news-event"
            style={{ marginTop: 10 }}
            gutter={8}
          >
            {renderCards()}
          </Row>
          {/* Bottom Pagination */}
          <PaginationComponent
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            total={dataResultEvent?.records || 0}
          />
        </>
      )}
    </div>
  );
}
