import FormInputFormik from "@/components/FormInputFormik";
import LoaderSpinGif from "@/components/LoaderSpinGif";
import NoData from "@/components/NoData";
import ModalComponent from "@/shared/components/ModalComponent";
import PaginationComponent from "@/shared/components/PaginationComponent";
import {
  DASHBOARD_COURSE,
  DASHBOARD_MY_COLLECTION_DETAIL,
} from "@/shared/constants/path";
import iconEye from "@/shared/images/svg/ic_blue_eye_square.svg";
import iconPlus from "@/shared/images/svg/ic_red_plus_square.svg";
import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import { PlusCircleFilled, SendOutlined } from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Button as ButtonAntd,
  Col,
  Flex,
  Row,
  Space,
} from "antd";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import IconBookmark from '@/shared/images/svg/icon_bookmark.svg';
import * as yup from "yup";
import useAddCollection from "./hooks/useAddCollection";
import useGetCollection from "./hooks/useGetCollection";

export default function CollectionPage() {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const [formDataCollection, setFormDataCollection] = React.useState<any>({
    title: "",
  });
  const { fetchQuery } = useGetCollection();
  const { isLoading, data: dataCollection } = fetchQuery;
  const [isModalAddCollection, setIsModalAddCollection] =
    React.useState<boolean>(false);
  const { mutationQuery, handleAddCollection } = useAddCollection();
  const { isPending: isLoadingAddCollection, isSuccess } = mutationQuery;
  const [currentPage, setCurrentPage] = React.useState<number>(1);

  React.useEffect(() => {
    if (isSuccess) {
      setIsModalAddCollection(false);
    }
  }, [isSuccess]);

  const handleOpenModalUploadCertificate = () => {
    setIsModalAddCollection(true);
  };

  if (!dataCollection) {
    return <LoaderSpinGif size="large" isFullScreen={true} />;
  }

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    title: yup
      .string()
      .required(translate("RequireName"))
      .max(
        50,
        ({ max }) =>
          `${translate("NameLength")} ${max} ${translate("Characters")}`
      ),
  });

  return (
    <div className="hero-container-background">
      <Breadcrumb
        separator=">"
        style={{ paddingLeft: 80, paddingTop: 170 }}
        items={[
          {
            href: "",
            title: (
              <Image
                src={iconBreadcrumb}
                alt="Breadcrumb Icon"
                style={{ paddingBottom: 5 }}
              />
            ),
          },
          {
            title: (
              <span className="textfs16-fw400-red">
                {translate("MyCollection")}
              </span>
            ),
          },
        ]}
      />
      <div className="collection-header">
        <span className="textfs36-fw700-black">
          {translate("MyCollection")}
        </span>
        <ButtonAntd
          icon={<PlusCircleFilled style={{ fontSize: 20 }} />}
          loading={isLoadingAddCollection}
          type="primary"
          onClick={handleOpenModalUploadCertificate}
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            fontSize: 10,
          }}
        >
          {translate("CreateCollection")}
        </ButtonAntd>
      </div>
      <Row justify="center" className="collection-main">
        {dataCollection?.length === 0 && (
          <Col>
            <NoData />
            <span className="textfs20-fw600-black">
              {translate("CreateFolderCollection")}
            </span>
          </Col>
        )}
        {dataCollection?.length > 0 &&
          dataCollection?.map((item: any) => (
            <div className="collection-main-content" key={item.id}>
              <Space style={{ gap: 10 }}>
                <div className="collection-bookmark-icon">
                  <Image
                    src={IconBookmark}
                    alt="icon-bookmark"
                    width={20}
                    height={20}
                  />
                </div>
                <div>
                  <span className="textfs20-fw600-black">{item.name}</span>
                  {/* <span className="textfs16-fw600-gray">5 Course</span> */}
                </div>
              </Space>
              <Space>
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => navigate.push(DASHBOARD_COURSE)}
                >
                  <Image
                    width={25}
                    height={25}
                    src={iconPlus}
                    alt="icon-cardLeft-forum"
                  />
                </div>
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    navigate.push(
                      `${DASHBOARD_MY_COLLECTION_DETAIL}/${item.id}/${item.name}`
                    );
                  }}
                >
                  <Image
                    width={25}
                    height={25}
                    src={iconEye}
                    alt="icon-cardLeft-forum"
                  />
                </div>
              </Space>
            </div>
          ))}
      </Row>
      <PaginationComponent
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        total={50}
      />

      <ModalComponent
        title={translate("AddNewCollection")}
        width={570}
        open={isModalAddCollection}
        onCancel={() => setIsModalAddCollection(false)}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={{
            title: "",
          }}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleAddCollection(values.title);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterCollectionName")}
                title={translate("TitleCollectionName")}
                type={"text"}
                name={"title"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingAddCollection}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </ModalComponent>
    </div>
  );
}
