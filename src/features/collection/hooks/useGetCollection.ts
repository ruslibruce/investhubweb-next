import { getCollection } from "@/shared/api/mutation/collection";
import { USER } from "@/shared/constants/storageStatis";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = getCollection();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetCollection = (initialData?: any) => {
  const dataUser = storageCheck(USER);
  const fetchDataColors = dataUser.isLogin
    ? getCollection()
    : { key: [], api: () => Promise.resolve([]) };

  const fetchQuery = useFetchHook({
    keys: fetchDataColors.key,
    api: fetchDataColors.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetCollection;
