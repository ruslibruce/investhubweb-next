import { addCollection } from "@/shared/api/mutation/collection";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";
import { useTranslation } from "next-i18next";

const useAddCollection = () => {
  const queryClient = useQueryClient();
  const {t: translate} = useTranslation();

  const mutationQuery = useMutationHook({
    api: addCollection,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        queryClient.invalidateQueries({ queryKey: ["collection"] });
        if (data) {
          notification.success({
            message: translate("CollectionTitleSuccess"),
          });
        }
      },
    },
  });

  const handleAddCollection = (params: string) => {
    mutationQuery.mutate({
      name: params,
    });
  };

  return {
    mutationQuery,
    handleAddCollection,
  };
};

export default useAddCollection;
