import { getCourseLike } from "@/shared/api/fetch/course";
import { USER } from "@/shared/constants/storageStatis";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = getCourseLike();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetLike = (initialData?: any, id?: string) => {
  const dataUser = storageCheck(USER);
  const fetchDataColors = !dataUser.isLogin
    ? { key: [], api: () => Promise.resolve(initialData || []) }
    : id
    ? getCourseLike(id)
    : { key: [], api: () => Promise.resolve(initialData || []) };

  const fetchLikeCourse = useFetchHook({
    keys: fetchDataColors.key,
    api: fetchDataColors.api,
    initialData,
    options: {},
  });

  return {
    fetchLikeCourse,
  };
};

export default useGetLike;
