import { postCommentCourse } from "@/shared/api/fetch/course";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";
import { useTranslation } from "next-i18next";

const usePostComment = () => {
  const queryClient = useQueryClient();
  const { t: translate } = useTranslation();

  const mutationQuery = useMutationHook({
    api: postCommentCourse,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        // queryClient.invalidateQueries({ queryKey: ["course_certificate"] });
        if (data) {
          notification.success({
            message: translate("CommentSuccess"),
          });
        }
      },
    },
  });

  const handleCommentCourse = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handleCommentCourse,
  };
};

export default usePostComment;
