import { postUploadCertificate } from "@/shared/api/fetch/certificate";
import { postReportCourse } from "@/shared/api/fetch/course";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";
import { useTranslation } from "next-i18next";

const usePostReport = () => {
  const queryClient = useQueryClient();
  const { t: translate } = useTranslation();

  const mutationQuery = useMutationHook({
    api: postReportCourse,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        // queryClient.invalidateQueries({ queryKey: ["course_certificate"] });
        if (data) {
          notification.success({
            message: translate("ReportSuccess"),
          });
        }
      },
    },
  });

  const handleReportCourse = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handleReportCourse,
  };
};

export default usePostReport;
