import { postLikeCourse } from "@/shared/api/fetch/course";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";
import { useTranslation } from "next-i18next";

const usePostLike = () => {
  const queryClient = useQueryClient();
  const { t: translate } = useTranslation();

  const mutationQuery = useMutationHook({
    api: postLikeCourse,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        queryClient.invalidateQueries({ queryKey: ["courseLike"] });
        if (data) {
          notification.success({
            message: data.liked
              ? translate("DescLikeSuccess")
              : translate("DescUnLikeSuccess"),
          });
        }
      },
    },
  });

  const handleLikeCourse = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handleLikeCourse,
  };
};

export default usePostLike;
