import BackgroundHero from "@/components/BackgroundHero";
import CardCourse from "@/components/CardCourse";
import FormInputFormik from "@/components/FormInputFormik";
import FormSelect from "@/components/FormSelect";
import LoaderSpinGif from "@/components/LoaderSpinGif";
import NoData from "@/components/NoData";
import TextResponsive from "@/components/TextResponsive";
import ModalComponent from "@/shared/components/ModalComponent";
import PaginationComponent from "@/shared/components/PaginationComponent";
import { imagePORTAL } from "@/shared/constants/imageUrl";
import { CERTIFICATE_FORMAT, USER } from "@/shared/constants/storageStatis";
import IconSuccess from "@/shared/images/icon/ic_round_check_circle_green_large.webp";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { handlingErrors } from "@/shared/utils/handlingErrors";
import { paramsToString } from "@/shared/utils/string";
import {
  DeleteOutlined,
  PlusCircleFilled,
  SendOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import {
  Button,
  Button as ButtonAntd,
  Col,
  Flex,
  Row,
  Space,
  Upload,
  message,
} from "antd";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import React from "react";
import * as yup from "yup";
import useGetCourse from "./hooks/useGetCourse";
import useGetCourseCategory from "./hooks/useGetCourseCategory";
import useGetCourseContentType from "./hooks/useGetCourseContentType";
import useGetCourseLevel from "./hooks/useGetCourseLevel";
import usePostCertificate from "./hooks/usePostCertificate";
import dynamic from "next/dynamic";
const { Dragger } = Upload;
const DynamicHeaderPDF = dynamic(() => import("@/components/PDFViewer"), {
  loading: () => <LoaderSpinGif size="large" />,
});

const initialFormData = {
  provider: "",
  title: "",
  duration: "",
  description: "",
  file: "",
};
export default function CoursePage() {
  const { t: translate } = useTranslation();
  const [currentPage, setCurrentPage] = React.useState<number>(1);
  const dataUser = storageCheck(USER);
  const [formData, setFormData] = React.useState<any>({
    category: null,
    level: null,
    content_type: null,
  });
  const [formApi, setFormApi] = React.useState<any>({
    category: "",
    level: "",
    content_type: "",
  });
  const { fetchQuery } = useGetCourse(
    {},
    paramsToString({ page: currentPage, ...formApi })
  );
  const { data: dataResultCourse, isLoading } = fetchQuery;
  const { data: dataCourse } = dataResultCourse;
  const [isModalUploadCertificate, setIsModalUploadCertificate] =
    React.useState<boolean>(false);
  const [isModalSukses, setIsModalSukses] = React.useState<boolean>(false);
  const { handlePostCertificate, mutationQuery: mutationQueryPostCertificate } =
    usePostCertificate();
  const { isSuccess, isPending } = mutationQueryPostCertificate;
  const { fetchCategory } = useGetCourseCategory([]);
  const { fetchLevel } = useGetCourseLevel([]);
  const { fetchContentType } = useGetCourseContentType([]);
  const { data: dataCategory } = fetchCategory;
  const { data: dataLevel } = fetchLevel;
  const { data: dataContentType } = fetchContentType;
  console.log("dataCourse", dataCourse);

  React.useEffect(() => {
    if (isSuccess) {
      setIsModalUploadCertificate(false);
    }
  }, [isSuccess]);

  const handleChangeDropdown = (value: string, target: string) => {
    switch (target) {
      case "content_type":
        dataContentType.map((item: any) => {
          if (item.label === value) {
            setFormData(() => ({
              ...formData,
              [target]: item.label,
            }));
            setFormApi(() => ({
              ...formApi,
              [target]: item.label,
            }));
          }
        });
        break;

      case "category":
        dataCategory.map((item: any) => {
          if (item.name === value) {
            setFormData(() => ({
              ...formData,
              [target]: item.label,
            }));
            setFormApi(() => ({
              ...formApi,
              [target]: item.id,
            }));
          }
        });
        break;

      case "level":
        dataLevel.map((item: any) => {
          if (item.name === value) {
            setFormData(() => ({
              ...formData,
              [target]: item.label,
            }));
            setFormApi(() => ({
              ...formApi,
              [target]: item.id,
            }));
          }
        });
        break;

      default:
        break;
    }
  };

  const handleOpenModalUploadCertificate = () => {
    setIsModalUploadCertificate(true);
  };

  const handleUploadCertificate = (values: any) => {
    handlePostCertificate(values);
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    provider: yup.string().required(translate("CourseProviderRequired")),
    title: yup
      .string()
      .required(translate("TitleRequired"))
      .max(
        50,
        ({ max }) =>
          `${translate("TitleLength")} ${max} ${translate("Characters")}`
      ),
    duration: yup
      .number()
      .required(translate("DurationRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("DurationLength")} ${max} ${translate("Characters")}`
      ),
    description: yup
      .string()
      .required(translate("DescriptionRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("DescriptionLength")} ${max} ${translate("Characters")}`
      ),
    file: yup.string().required(translate("FileRequired")),
  });

  const handleUploadChange = (info: any, setFieldValue: any) => {
    const { status, response } = info;
    if (status !== "uploading") {
      console.log("info.file", info.file);
      console.log("info.fileList", info.fileList);
      const dataFile = info.file;

      if (dataFile.response?.status === "success") {
        setFieldValue("file", dataFile.response.data.filename);
      }
    }
    if (status === "done") {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === "error") {
      console.log(
        `${info.file.name} file upload failed because of ${info.file.error}`
      );
      if (response && response?.message == "Unauthorized") {
        handlingErrors({
          error: response,
          text: "Upload Image",
        });
      }
    }
  };

  return (
    <div className="hero-container-backgroundImg">
      <Row
        style={{
          borderBottomLeftRadius: 40,
          borderBottomRightRadius: 40,
          paddingTop: 20,
          paddingBottom: 30,
          paddingLeft: 70,
          paddingRight: 70,
          position: "relative",
          overflow: "hidden",
        }}
        gutter={[16, 16]}
        align={"middle"}
        className="hero-container-antd"
      >
        <BackgroundHero />
        <Col span={12}>
          <TextResponsive
            bigScreen={60}
            mobile={24}
            color="white"
            fontWeight={700}
            isSpan
            desktop={45}
          >
            {translate("TitleHeroCourse")}
          </TextResponsive>
        </Col>
        <Col span={12}>
          <TextResponsive
            mobile={14}
            color="white"
            fontWeight={400}
            isSpan
            desktop={16}
          >
            {translate("Desc_Hero_Home")}
          </TextResponsive>
        </Col>
      </Row>
      <Row wrap gutter={10} className="container-filter-course">
        <Col>
          {dataUser?.isLogin && (
            <FormSelect
              placeholder={translate("FilterLevel")}
              value={formData.level}
              items={dataLevel}
              handleChange={(value: string) => {
                handleChangeDropdown(value, "level");
              }}
              styleSelect={{ height: 45, width: window.innerWidth > 765 ? 300 : '85vw' }}
              showSearch={false}
              isRequired={false}
            />
          )}
        </Col>
        <Col>
          <FormSelect
            placeholder={translate("FilterCategory")}
            value={formData.category}
            items={dataCategory}
            handleChange={(value: string) => {
              handleChangeDropdown(value, "category");
            }}
            styleSelect={{ height: 45, width: window.innerWidth > 765 ? 300 : '85vw'  }}
            showSearch={false}
            isRequired={false}
          />
        </Col>
        <Col>
          <FormSelect
            placeholder={translate("FilterContentType")}
            value={formData.content_type}
            items={dataContentType}
            handleChange={(value: string) => {
              handleChangeDropdown(value, "content_type");
            }}
            styleSelect={{ height: 45, width: window.innerWidth > 765 ? 300 : '85vw'  }}
            showSearch={false}
            isRequired={false}
          />
        </Col>
        {dataUser?.isLogin && (
          <Col>
            <ButtonAntd
              icon={<PlusCircleFilled style={{ fontSize: 20 }} />}
              type="primary"
              onClick={handleOpenModalUploadCertificate}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                height: 45,
              }}
            >
              {translate("Upload_Cert")}
            </ButtonAntd>
          </Col>
        )}
      </Row>
      {!dataCourse ? (
        <LoaderSpinGif size="large" isFullScreen />
      ) : (
        <Row
          gutter={10}
          wrap
          className="container-card-course"
          style={{ marginTop: 20 }}
        >
          {dataCourse?.map((item: any, index: any) => {
            return <CardCourse key={index} item={item} isMyCourse={false} />;
          })}
        </Row>
      )}
      {dataCourse?.length === 0 && <NoData />}
      <PaginationComponent
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        total={dataResultCourse?.records}
        limit={dataResultCourse?.limit}
      />

      {/* Modal Sukses */}
      <ModalComponent
        width={400}
        open={isModalSukses}
        onCancel={() => setIsModalSukses(false)}
      >
        <div className="modalHeader">
          <h5 className="heading">{translate("Done")}</h5>
        </div>
        <div style={{ margin: "auto" }} className="containerModalImgGreen">
          <Image src={IconSuccess} alt="modal-img" className="modalImage" />
        </div>
        <div className="modalContent">
          {translate("DetailSuccessAddCollection")}
        </div>
        <div className="modalActions">
          <button
            className="successBtn"
            onClick={() => setIsModalSukses(false)}
          >
            {translate("LabelOK")}
          </button>
        </div>
      </ModalComponent>

      {/* Modal Upload */}
      <ModalComponent
        title={translate("FormUploadCert")}
        open={isModalUploadCertificate}
        onCancel={() => setIsModalUploadCertificate(false)}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialFormData}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleUploadCertificate(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, values, setFieldValue, errors }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={translate('CourseProvider')}
                title={translate('EnterCourseProvider')}
                type={"text"}
                name={"provider"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"20"}
                title={`${translate('Duration')} (${translate('Hours')})`}
                type={"number"}
                name={"duration"}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate('EnterTitleCourse')}
                title={translate('TitleCourse')}
                type={"text"}
                name={"title"}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate('EnterDescription')}
                title={translate('Description')}
                type={"text"}
                name={"description"}
                textArea={true}
              />
              <div>
                <label className="required">
                  {`${translate("Upload_Cert")} (Max. 5MB)`}
                </label>
              </div>
              {values.file ? (
                <Space
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 10,
                    position: "relative",
                  }}
                >
                  {values.file.includes("pdf") ? (
                    <DynamicHeaderPDF url={`${imagePORTAL}/${values.file}`} />
                  ) : (
                    <Image
                      src={`${imagePORTAL}/${values.file}`}
                      alt="img"
                      width={100}
                      height={100}
                      style={{
                        width: 400,
                        height: 200,
                        borderRadius: 10,
                      }}
                    />
                  )}
                  <DeleteOutlined
                    style={{
                      fontSize: 20,
                      color: "red",
                      position: "absolute",
                      top: 10,
                      right: 10,
                      backgroundColor: "white",
                      padding: 5,
                      borderRadius: 100,
                    }}
                    type="primary"
                    onClick={() => {
                      setFieldValue("file", "");
                    }}
                  />
                </Space>
              ) : (
                <Dragger
                  {...{
                    name: "file",
                    multiple: true,
                    maxCount: 1,
                    action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-portal/upload-file`,
                    headers: {
                      authorization: `Bearer ${dataUser.token}`,
                    },
                    accept: CERTIFICATE_FORMAT,
                    beforeUpload: (file) => {
                      console.log("file beforeUpload", file);
                      const isSize = file.size / 1024 / 1024 < 6;

                      if (!isSize) {
                        message.error(
                          `${file.name} size must be less than 5 MB`
                        );
                      }

                      return isSize || Upload.LIST_IGNORE;
                    },
                    onChange: (info) => handleUploadChange(info, setFieldValue),
                  }}
                  style={{ marginTop: 10 }}
                >
                  <p className="ant-upload-drag-icon">
                    <UploadOutlined />
                  </p>
                  <p className="ant-upload-text">Drag and Drop Here</p>
                  <p className="ant-upload-text">or</p>
                  <p className="ant-upload-hint">Browse Files</p>
                </Dragger>
              )}
              {errors.file && (
                <div className={"error"}>{errors.file.toString()}</div>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isPending}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </ModalComponent>
    </div>
  );
}
