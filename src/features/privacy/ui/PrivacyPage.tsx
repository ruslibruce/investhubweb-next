import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import { Breadcrumb } from "antd";
import Image from "next/image";
import useGetPrivacy from "../hooks/useGetPrivacy";
import { useTranslation } from "next-i18next";

export default function PrivacyPage() {
  const {t: translate} = useTranslation();
  const { fetchQuery } = useGetPrivacy({});
  const { isLoading, data: dataPrivacy } = fetchQuery;

  return (
    <div className="privacy">
      <div className="privacy-breadcrumb">
        <Breadcrumb
          separator=">"
          items={[
            {
              href: "",
              title: <Image src={iconBreadcrumb} alt="Breadcrumb Icon" />,
            },
            {
              title: (
                <span className="textfs16-fw400-red">
                  {translate("PrivacyPolicy")}
                </span>
              ),
            },
          ]}
        />
      </div>
      <div className="privacy-content">
        <h2 className="privacy-title">{dataPrivacy?.title}</h2>
        <div
          className="privacy-content-text"
          dangerouslySetInnerHTML={{ __html: dataPrivacy?.body }}
        />
      </div>
    </div>
  );
}
