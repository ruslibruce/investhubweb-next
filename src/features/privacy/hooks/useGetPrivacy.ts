import { fetchPrivacy } from "@/shared/api/fetch/privacy";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchPrivacy();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetPrivacy = (initialData?: any) => {
  const fetchDataPrivacy = fetchPrivacy();

  const fetchQuery = useFetchHook({
    keys: fetchDataPrivacy.key,
    api: fetchDataPrivacy.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetPrivacy;
