import NoData from "@/components/NoData";
import {
  DASHBOARD_COURSE_DETAIL,
  DASHBOARD_HOME,
  DASHBOARD_LEVEL_UP,
} from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import iconBreadcrumb from "@/shared/images/svg/icon_investment_breadcumb.svg";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { Box, CircularProgress, CircularProgressLabel } from "@chakra-ui/react";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Flex,
  Progress,
  Row,
  Space,
  Spin,
  Tooltip,
} from "antd";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import useGetLearningJourneyId from "./hooks/useGetLearningJourneyId";
import { useTranslation } from "next-i18next";
import LoaderSpinGif from "@/components/LoaderSpinGif";
import { SketchOutlined } from "@ant-design/icons";

export default function LearningJourneyDetailPage() {
  const { t: translate } = useTranslation();
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const { slug } = navigate.query;
  const id: any = slug?.[0];
  const name: any = slug?.[1];

  const { fetchGetDetailLearnJourney } = useGetLearningJourneyId(id);
  const { data: dataLearnJourney, isLoading } = fetchGetDetailLearnJourney;
  // console.log("dataLearnJourney", dataLearnJourney);

  React.useEffect(() => {
    if (!dataUser.isLogin) {
      navigate.back();
    }
  }, [dataUser]);

  const handleCourse = (value: any) => {
    console.log("query", value);
    navigate.push(`${DASHBOARD_COURSE_DETAIL}/${value.id}`);
  };

  const testCompleted =
    dataLearnJourney &&
    Object.keys(dataLearnJourney).length > 0 &&
    dataLearnJourney?.is_test_passed
      ? 1
      : 0;

  const completedCount =
    dataLearnJourney &&
    Object.keys(dataLearnJourney).length > 0 &&
    dataLearnJourney?.requirements?.length > 0
      ? dataLearnJourney?.requirements?.filter(
          (val: any) => val.progress && val.progress.is_completed
        ).length + testCompleted
      : 0;
  const coursePlusTest =
    dataLearnJourney &&
    Object.keys(dataLearnJourney).length > 0 &&
    dataLearnJourney?.requirements?.length + 1;
  const valueCircular =
    completedCount === 0 ? 0 : (completedCount / coursePlusTest) * 100;

  if (isLoading) {
    return <LoaderSpinGif size="large" isFullScreen={true} />;
  }

  if (dataLearnJourney && `${dataLearnJourney?.id}` !== id) {
    return <LoaderSpinGif size="large" isFullScreen={true} />;
  }

  return (
    <div className="hero-container-background">
      <Breadcrumb
        separator=">"
        style={{ paddingLeft: 80, paddingTop: 170 }}
        items={[
          {
            href: "",
            title: <Image src={iconBreadcrumb} alt="Breadcrumb Icon" />,
          },
          {
            title: (
              <Link href={DASHBOARD_HOME}>
                <span className="textfs16-fw400-red">{translate("Home_Capital")}</span>
              </Link>
            ),
          },
          {
            title: (
              <span className="textfs16-fw400-red">
                {translate("MyLearningJourney")}
              </span>
            ),
          },
          {
            title: <span className="textfs16-fw400-red">{name}</span>,
          },
        ]}
      />
      <Card className="card-detail-collection">
        {dataLearnJourney && Object.keys(dataLearnJourney).length > 0 && (
          <Space style={{ gap: 10 }}>
            {/* <div className="detail-collection-icon-1">
              <HiOutlineBookmark size={200} color="#AF0404" />
            </div>
            <div className="detail-collection-icon-2">
              <HiOutlineBookmark size={200} color="#AF0404" />
            </div> */}
            <CircularProgress
              size="100px"
              value={valueCircular}
              color="green.400"
            >
              <CircularProgressLabel>
                <Box display="flex" alignItems="center" justifyContent="center">
                  <SketchOutlined style={{ fontSize: 40, color: "#9F0E0F" }} />
                </Box>
              </CircularProgressLabel>
            </CircularProgress>
            <div style={{ flexDirection: "column", display: "flex" }}>
              <span className="textfs30-fw600-black">{name}</span>
              <span className="textfs24-fw600-red">{`${
                dataLearnJourney?.requirements?.length
              } ${translate("Courses")} + 1 ${translate("LabelTest")}`}</span>
            </div>
          </Space>
        )}
        {dataLearnJourney && Object.keys(dataLearnJourney).length === 0 && (
          <NoData />
        )}
        {dataLearnJourney && Object.keys(dataLearnJourney).length > 0 && (
          <Card style={{ marginTop: 20 }}>
            <Row
              gutter={[8, 8]}
              style={{ marginBottom: 20, cursor: "pointer" }}
            >
              <Col span={16}>
                <span className="textfs18-fw600-black">
                  {translate("CourseTitle")}
                </span>
              </Col>
              <Col span={8}>
                <span className="textfs18-fw600-black">
                  {translate("Progress")}
                </span>
              </Col>
            </Row>
            {dataLearnJourney?.requirements?.map((item: any) => {
              return (
                <Row
                  gutter={[8, 8]}
                  onClick={() => handleCourse(item)}
                  key={item.id}
                  style={{ marginBottom: 20, cursor: "pointer" }}
                >
                  <Col span={16}>
                    <span className="textfs16-fw400-black">{item.title}</span>
                  </Col>
                  <Col span={8}>
                    {item.progress ? (
                      <Progress
                        status="success"
                        percent={item.progress?.progress}
                        percentPosition={{ align: "center", type: "inner" }}
                        size={["100%", 20]}
                      />
                    ) : (
                      <Progress
                        status="success"
                        percent={0}
                        percentPosition={{ align: "center", type: "inner" }}
                        size={["100%", 20]}
                      />
                    )}
                  </Col>
                </Row>
              );
            })}
            {dataLearnJourney?.is_test_enabled ? (
              <Row
                gutter={[8, 8]}
                onClick={() =>
                  navigate.push(
                    `${DASHBOARD_LEVEL_UP}/${dataLearnJourney?.test_attempt_id}/${name}`
                  )
                }
                style={{ marginBottom: 20, cursor: "pointer" }}
              >
                <Col span={16}>
                  <span className="textfs16-fw400-black">
                    {translate("Level_Up_Capital")}
                  </span>
                </Col>
                <Col span={8}>
                  <Progress
                    status="success"
                    percent={dataLearnJourney?.is_test_passed ? 100 : 0}
                    percentPosition={{ align: "center", type: "inner" }}
                    size={["100%", 20]}
                  />
                </Col>
              </Row>
            ) : (
              <Tooltip title={translate("CompleteProgress")}>
                <Row
                  gutter={[8, 8]}
                  style={{ marginBottom: 20, cursor: "pointer" }}
                >
                  <Col span={16}>
                    <span className="textfs16-fw400-black">
                      {translate("Level_Up_Capital")}
                    </span>
                  </Col>
                  <Col span={8}>
                    <Progress
                      status="success"
                      percent={0}
                      percentPosition={{ align: "center", type: "inner" }}
                      size={["100%", 20]}
                    />
                  </Col>
                </Row>
              </Tooltip>
            )}
          </Card>
        )}
      </Card>
    </div>
  );
}
