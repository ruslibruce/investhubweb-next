import {
  fetchGetLearningJourney,
  fetchGetLearningJourneyId,
} from "@/shared/api/fetch/learningJourney";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchGetLearningJourney();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetLearningJourneyId = (id?: string, initialData?: any) => {
  const fetchDataInvestment = fetchGetLearningJourneyId(id)

  const fetchGetDetailLearnJourney = useFetchHook({
    keys: fetchDataInvestment.key,
    api: fetchDataInvestment.api,
    initialData,
    options: {},
  });

  return {
    fetchGetDetailLearnJourney,
  };
};

export default useGetLearningJourneyId;
