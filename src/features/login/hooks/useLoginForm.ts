import { logIn } from "@/shared/api/mutation/login";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";

const useLoginForm = () => {
  const navigate = useRouter();
  const { t: translate } = useTranslation();

  const mutationQuery = useMutationHook({
    api: logIn,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
      },
    },
  });

  const handleOnSubmit = (values: any) => {
    mutationQuery.mutate(values);
  };

  return {
    mutationQuery,
    handleOnSubmit,
  };
};

export default useLoginForm;
