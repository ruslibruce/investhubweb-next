import FooterAuth from "@/components/FooterAuth";
import FormButton from "@/components/FormButton";
import FormInputFormik from "@/components/FormInputFormik";
import ButtonFlag from "@/shared/components/layout/auth-layout/sider/ButtonFlag";
import { DASHBOARD_FORGOT, DASHBOARD_REGISTER } from "@/shared/constants/path";
import IconLogo from "@/shared/images/logo/logo_investhub.webp";
import { notification, Space } from "antd";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import ReCAPTCHA from "react-google-recaptcha";
import * as yup from "yup";
import useLoginForm from "../hooks/useLoginForm";
import useGetProfileMutate from "@/shared/components/layout/dashboard-layout/hooks/useGetProfileMutate";
import { regexEmail } from "@/shared/utils/helper";
import { jwtDecode } from "jwt-decode";

const FormLogin = () => {
  const { mutationQuery, handleOnSubmit } = useLoginForm();
  const { t: translate } = useTranslation();
  const { isPending, data: dataLogin } = mutationQuery;
  const [token, setToken] = React.useState<any>("");
  const captchaRef = React.useRef<any>(null);
  const { handleGetProfile } = useGetProfileMutate();

  React.useEffect(() => {
    if (dataLogin) {
      let resultData: any = dataLogin;
      const decodedToken: any = jwtDecode(resultData.data.authorisation.token);
      handleGetProfile({
        token: resultData.data.authorisation.token,
        user: decodedToken.user,
      });
    }
  }, [dataLogin]);

  const handleLogin = (values: { email: string; password: string }) => {
    if (isPending) {
      return;
    }

    if (!token) {
      notification.error({
        message: "reCAPTCHA",
        description: translate("Recaptcha"),
      });
      return;
    }

    setToken("");
    handleOnSubmit({
      ...values,
      "g-recaptcha-response": captchaRef.current.getValue(),
    });
    captchaRef.current.reset();
  };

  const onVerify = () => {
    const tokenCaptcha = captchaRef.current.getValue();
    // alert(tokenCaptcha);
    setToken(tokenCaptcha);
  };

  const onExpire = () => {
    notification.info({
      message: "reCAPTCHA",
      description: translate("Recaptcha_Expired"),
    });
  };

  const loginValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    email: yup
      .string()
      .matches(regexEmail(), translate("ValidEmail"))
      .max(
        30,
        ({ max }) =>
          `${translate("EmailLength")} ${max} ${translate("Characters")}`
      )
      .required(translate("RequireEmail")),
    password: yup
      .string()
      .min(
        8,
        ({ min }) =>
          `${translate("PasswordLength")} ${min} ${translate("Characters")}`
      )
      .required(translate("RequirePassword")),
  });

  return (
    <Space direction="vertical" className="form-login-container">
      <div className="language-switch">
        <ButtonFlag mobileVersion={true} />
      </div>
      <div className={"container-header"}>
        <Image
          width={200}
          src={IconLogo}
          alt="icon-logo"
          className={"icon-logo"}
        />
        <div className={"sub-right-container"}>
          <div className="text-ask">{translate("Doesnt_have_acc")}</div>
          <Link href={DASHBOARD_REGISTER} className={"text-register"}>
            {translate("Register")}
          </Link>
        </div>
      </div>
      <div className={"container-title"}>
        <div className={"text-title"}>{translate("Login!")}</div>
        <div className={"text-title-info"}>{translate("Desc_Login")}</div>
        <Formik
          validationSchema={loginValidationSchema}
          initialValues={{ email: "", password: "" }}
          onSubmit={(values) => {
            handleLogin(values);
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterEmail")}
                title={translate("LabelEmail")}
                type={"email"}
                name={"email"}
                styleForm={{ marginTop: 20 }}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterPassword")}
                title={translate("LabelPassword")}
                type={"password"}
                name={"password"}
              />
              <Link href={DASHBOARD_FORGOT} className={"text-forgot-password"}>
                {translate("ForgotPassword")}
              </Link>

              <div className={"position-recaptcha"}>
                <ReCAPTCHA
                  sitekey={process.env.NEXT_PUBLIC_SITE_KEY as string}
                  onChange={onVerify}
                  onExpired={onExpire}
                  ref={captchaRef}
                />
              </div>
              <FormButton
                styleButton={{ marginTop: 32 }}
                onClick={handleSubmit}
                disabled={isPending || !isValid}
                title={translate("Login")}
                type={"submit"}
                isLoading={isPending}
              />
            </>
          )}
        </Formik>
      </div>
      <FooterAuth
        backToLogin={false}
        hiddenButton={false}
        hiddenLineOr={false}
      />
    </Space>
  );
};

export default FormLogin;
