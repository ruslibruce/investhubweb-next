import { ComponentToken } from 'antd/es/app/style';
import { AliasToken } from 'antd/es/theme/internal';

export const ButtonTheme: Partial<ComponentToken> & Partial<AliasToken> = {
  borderRadius: 10,
  controlHeight: 44,
};
