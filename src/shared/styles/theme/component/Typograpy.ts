import { ComponentToken } from 'antd/es/app/style';
import { AliasToken } from 'antd/es/theme/internal';

export const TypographyTheme: Partial<ComponentToken> & Partial<AliasToken> = {
  // heading
  colorTextHeading: '#212121',
  fontWeightStrong: 900,

  // h3
  fontSizeHeading3: 28,
  lineHeightHeading3: 1.14,

  // h5
  fontSizeHeading5: 18,
  lineHeightHeading5: 1.44,
};
