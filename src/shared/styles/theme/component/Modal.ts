import { ComponentToken } from 'antd/es/app/style';
import { AliasToken } from 'antd/es/theme/internal';

export const ModalTheme: Partial<ComponentToken> & Partial<AliasToken> = {
  fontSizeHeading5: 20,
  fontWeightStrong: 700,
  paddingMD: 24,
  boxShadow: '0px 4px 10px 0px rgba(0, 0, 0, 0.05)',
};
