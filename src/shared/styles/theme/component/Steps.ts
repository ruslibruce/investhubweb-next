import { ComponentToken } from 'antd/es/app/style';
import { AliasToken } from 'antd/es/theme/internal';

export const StepsTheme: Partial<ComponentToken> & Partial<AliasToken> = {
  colorPrimary: '#9F0E0F',
  lineWidth: 1,
  colorTextDescription: '#6E6E6E',
  colorSplit: '#D1D5DB',
};
