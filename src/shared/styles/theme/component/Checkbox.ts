import { ComponentToken } from 'antd/es/app/style';
import { AliasToken } from 'antd/es/theme/internal';

export const CheckboxTheme: Partial<ComponentToken> & Partial<AliasToken> = {
  controlInteractiveSize: 15,
  colorPrimary: '#249F5D',
  colorPrimaryHover: '#4EC987',
  colorPrimaryBorder: '#4EC987',
  colorBorder: '#4C4E64',
  borderRadiusSM: 2,
  paddingXS: 9,
};
