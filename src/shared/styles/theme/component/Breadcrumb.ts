import { ComponentToken } from 'antd/es/app/style';
import { AliasToken } from 'antd/es/theme/internal';

export const BreadcrumbTheme: Partial<ComponentToken> & Partial<AliasToken> = {
  marginXS: 8,
};
