import { ComponentToken } from 'antd/es/app/style';
import { AliasToken } from 'antd/es/theme/internal';

export const CardTheme: Partial<ComponentToken> & Partial<AliasToken> = {
  boxShadowTertiary: '0px 4px 10px 0px rgba(0, 0, 0, 0.05)',
  colorBgContainer: '#fff',
  colorBorderSecondary: '#F3F4F6',
};
