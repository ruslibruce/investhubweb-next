import { useRouter } from "next/router";
import { LOGOUT } from "../constants/path";

const useLogoutUser = () => {
  const navigate = useRouter();

  // func
  const handleSignout = () => {
    navigate.push(LOGOUT);
  };

  return {
    handleSignout,
  };
};

export default useLogoutUser;
