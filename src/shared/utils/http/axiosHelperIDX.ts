import AxiosInstanceIDX from "@/shared/api/axiosInstanceIDX";
import { AxiosRequestConfig } from "axios";

function handleError(error: any) {
  // Handle error
  if (error.response) {
    // Server merespons dengan status selain 2xx
    console.error(
      "Error response:",
      error.response.status,
      error.response.data
    );
  } else if (error.request) {
    // Tidak ada respons dari server
    console.error("No response from server:", error.request);
    console.error("Error message:", error.message);
  } else {
    // Error lainnya
    console.error("Error:", error.message);
  }
  throw error;
}

async function getDataIDX({
  endpoint,
  config,
}: {
  endpoint: string;
  config?: AxiosRequestConfig<any>;
}): Promise<any> {
  try {
    const response = await AxiosInstanceIDX.get(endpoint, config);
    return response;
  } catch (error: any) {
    handleError(error);
  }
}

async function postDataIDX({
  endpoint,
  data,
  config,
}: {
  endpoint: string;
  data: any;
  config?: AxiosRequestConfig<any>;
}) {
  try {
    const response = await AxiosInstanceIDX.post(endpoint, data, config);
    return response;
  } catch (error: any) {
    handleError(error);
  }
}

async function putDataIDX({
  endpoint,
  data,
  config,
}: {
  endpoint: string;
  data: any;
  config?: AxiosRequestConfig<any>;
}) {
  try {
    const response = await AxiosInstanceIDX.put(endpoint, data, config);
    return response;
  } catch (error: any) {
    handleError(error);
  }
}

async function deleteDataIDX({
  endpoint,
  config,
}: {
  endpoint: string;
  config?: AxiosRequestConfig<any>;
}): Promise<any> {
  try {
    const response = await AxiosInstanceIDX.delete(endpoint, config);
    return response;
  } catch (error: any) {
    handleError(error);
  }
}

export { getDataIDX, postDataIDX, putDataIDX, deleteDataIDX };
