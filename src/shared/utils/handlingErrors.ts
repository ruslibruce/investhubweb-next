import { notification } from "antd";
import { i18n } from "next-i18next";
import Router from "next/router";
import { AUTH_ROUTES, LOGOUT } from "../constants/path";
import { USER } from "../constants/storageStatis";
import { storageSet } from "./clientStorageUtils";

type props = {
  error: any;
  text: string;
};

export function handlingErrors({ error, text }: props) {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.log("error.response.data", error.response.data);
    console.log("error.response.status", error.response.status);
    console.log("error.response.headers", error.response.headers);
    if (error.response.status === 401) {
      let check = false;
      AUTH_ROUTES.forEach((route) => {
        if (Router.asPath === route) {
          check = true;
        }
      });
      console.log("check asPath", check);
      if (check) {
        notification.error({
          message: error.response?.data?.message ?? `${text} Failed`,
          description: error?.message,
        });
        return;
      }
      Router.push(LOGOUT);
      return;
    }
    notification.error({
      message: error.response?.data?.message ?? `${text} Failed`,
      description: error?.message,
    });
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    console.log("error.request", error.request);
  } else {
    // Something happened in setting up the request that triggered an Error
    console.log("Error message", error.message);
    if (
      error.message === "Unauthorized" ||
      error.message === "Unauthenticated"
    ) {
      let check = false;
      AUTH_ROUTES.forEach((route) => {
        if (Router.asPath === route) {
          check = true;
        }
      });
      console.log("check asPath", check);
      if (check) {
        notification.error({
          message: error.response?.data?.message ?? `${text} Failed`,
          description: error?.message,
        });
        return;
      }
      let temp = {
        loading: true,
        isLogin: false,
        user: null,
        initialRoute: "Home",
        token: null,
        fcmToken: null,
      };
      storageSet(USER, temp);
      notification.error({
        message: i18n?.t("EndSession"),
        description: i18n?.t("EndSessionDesc"),
      });
      return;
    }
    notification.error({
      message: `${text} Failed`,
      description: error.message ?? `Your ${text} Failed`,
    });
  }
  console.log("error.config", error.config);
  console.log("error", error);
}
