import usePostSaveQuizLevelup from "@/features/levelupsession/hooks/usePostSaveQuizLevelup";
import React, { useCallback } from "react";
import { useTimer } from "react-timer-hook";

export default function MyTimer({
  expiryTimestamp,
  handleTimeCountDown,
  className,
  dataExercise,
  id,
}) {
  const {
    seconds,
    minutes,
    hours,
    days,
    isRunning,
    start,
    pause,
    resume,
    restart,
    totalSeconds,
  } = useTimer({
    expiryTimestamp,
    onExpire: () => {
      // console.warn('onExpire called');
      handleTimeCountDown();
    },
  });
  const { saveDataQuiz } = usePostSaveQuizLevelup();

  React.useEffect(() => {
    const intervalId = setInterval(() => {
      // Call your function here
      console.log("Function called every 5 seconds");
      handleSaveRemaining();
    }, 5000); // 5000 milliseconds = 5 seconds

    return () => {
      clearInterval(intervalId);
    };
  }, []);

  const handleSaveRemaining = useCallback(() => {
    const answer = {};
    dataExercise.map((item) => {
      if (item.id) {
        answer[item.id] = item.answer;
      }
    });
    if (answer && Object.keys(answer).length > 0) {
      saveDataQuiz(id, answer, totalSeconds);
    }
  }, [totalSeconds]);

  return (
    <div
      className={`text-countDown ${className}`}
    >{`${minutes} : ${seconds}`}</div>
  );
}
