const imageLMS = `${process.env.NEXT_PUBLIC_HOST_NAME}/lms-files`;

const imageIAM = `${process.env.NEXT_PUBLIC_HOST_NAME}/iam-files`;

const imagePORTAL = `${process.env.NEXT_PUBLIC_HOST_NAME}/portal-files`;

export { imageLMS, imageIAM, imagePORTAL };
