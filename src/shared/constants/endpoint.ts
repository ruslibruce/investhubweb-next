/**
 * BASE URL
 */
const BASE_URL: string = `${process.env.NEXT_PUBLIC_HOST_NAME}`;
const BASE_URL_IDX: string = `${process.env.NEXT_PUBLIC_HOST_NAME_IDX}`;
const URL_CALLBACK_MOBILE: string = `${process.env.NEXT_PUBLIC_CALLBACK_URL}`;

/**
 * ENDPOINTS
 */
const LOGIN = "/api-iam/login";
const LOGIN_GOOGLE = "/api-iam/auth/google";
const LOGIN_APPLE = "/api-iam/auth/apple";
const LOGIN_RDIS = "/api-iam/auth/rdis";
const LOGIN_IDX = "/api-iam/auth/webidx";
const LOGIN_TICMIEDU = "/api-iam/auth/ticmiedu";
const REGISTER = "/api-iam/register";
const LOGOUT = "/api-iam/logout";
const COURSE = (params: string = ""): string => `/api-lms/course${params}`;
const COURSE_COMMENT = (params: string = ""): string =>
  `/api-lms/course-comment${params}`;
const COURSE_REPORT = (params: string = ""): string => `/api-lms/course-report`;
const COURSE_LIKE = (params: string = ""): string =>
  `/api-lms/course-like${params}`;
const COURSE_RECOMENDATION = (params: string = ""): string =>
  `/api-lms/course/recommendation${params}`;
const MY_COURSE = (params: string = ""): string => `/api-lms/mycourse${params}`;
const COURSE_PROGRESS = (params: string = ""): string =>
  `/api-lms/course/progress${params}`;
const COURSE_ENROLL = (params: string = ""): string => "/api-lms/course/enroll";
const CATEGORY_COURSE = (params: string = ""): string => `/api-lms/category`;
const LEVEL_COURSE = (params: string = ""): string => `/api-lms/level`;
const FETCH_USERS = (params: string = ""): string => `/api-iam/update-profile`;
const INVESTMENT_PARTNER = (params: string = ""): string =>
  `/api-portal/investment-partner${params}`;
const CATEGORY_INVESTMENT = (params: string = ""): string =>
  `/api-portal/investment-partner-category`;
const NEWS = (params: string = ""): string => `/api-portal/news${params}`;
const NEWS_PORTAL = (params: string = ""): string =>
  `/api-portal/news-all${params}`;
const CATEGORY_NEWS = (params: string = ""): string =>
  `/api-portal/news-category`;
const EVENT = (params: string = ""): string => `/api-portal/event${params}`;
const EVENT_PORTAL = (params: string = ""): string =>
  `/api-portal/event-all${params}`;
const CATEGORY_EVENT = (params: string = ""): string =>
  `/api-portal/event-category`;
const FAQ_PORTAL = (params: string = ""): string =>
  `/api-portal/faq-all${params}`;
const ABOUT = (params: string = ""): string =>
  "/api-portal/general-content/id/1";
const DISCLAIMER = (params: string = ""): string =>
  "/api-portal/general-content/id/2";
const TERMSCONDITION = (params: string = ""): string =>
  "/api-portal/general-content/id/3";
const PRIVACYPOLICY = (params: string = ""): string =>
  "/api-portal/general-content/id/4";
const STOCK_SCREENER = (params: string = ""): string =>
  `/support/stock-screener/api/v1/stock-screener/get${params}`;
const STOCK_SCREENER_PROFILE_COMPANY = (params: string = ""): string =>
  `/primary/ListedCompany/GetCompanyProfilesDetail${params}`;
const CATEGORY_SECTOR = (params: string = ""): string =>
  `/support/stock-screener/api/v1/stock-screener/sector`;
const CATEGORY_SUB_SECTOR = (params: string = ""): string =>
  `/support/stock-screener/api/v1/stock-screener/sub-sector${params}`;
const STOCK_SCREENER_MIN_MAX = (params: string = ""): string =>
  "/support/stock-screener/api/v1/stock-screener/min-max";
const LEVEL_UP_QUIZ = (params: string = ""): string =>
  `/api-lms/test-attempt/${params}`;
const LEVEL_UP_SAVE = (params: string = ""): string =>
  "/api-lms/test-attempt/save";
const LEVEL_UP_RETAKE = (params: string = ""): string =>
  "/api-lms/test-attempt/retake";
const LEVEL_UP_SUBMIT = (params: string = ""): string =>
  "/api-lms/test-attempt/submit";
const CERTIFICATE = (params: string = ""): string =>
  `/api-lms/external-certificate${params}`;
const SEARCH_NEWS = (params: string = ""): string =>
  `/api-portal/search-all-news${params}`;
const SEARCH_COURSE = (params: string = ""): string =>
  `/api-lms/course/search/${params}`;
const SEARCH_ALL = (params: string = ""): string =>
  `/api-portal/search-all-portal${params}`;
const NOTIFICATION = (params: string = ""): string =>
  `/api-iam/notification${params}`;
const COLLECTION = (params: string = ""): string => "/api-lms/collection";
const COLLECTION_CONTENT = (params: string = ""): string =>
  `/api-lms/collection-content${params}`;
const COUNTRY = (params: string = ""): string => "/api-portal/countries";
const ID_PROVINCE = (params: string = ""): string =>
  `/api-portal/provinces/countriesId/ID`;
const CHANGE_PASSWORD = (params: string = ""): string =>
  `/api-iam/change-password`;

const RESEND_EMAIL = (params: string = ""): string =>
  `/api-iam/resend-email-verification`;

const CHANGE_EMAIL = (params: string = ""): string => `/api-iam/change-email`;

const FORGOT_PASSWORD = (params: string = ""): string =>
  `/api-iam/forgot-password`;

const VALIDATE_RESET_TOKEN = (params: string = ""): string =>
  `/api-iam/validate-reset-token`;

const RESET_PASSWORD = (params: string = ""): string =>
  `/api-iam/reset-password`;

const PREFERENCES = (params: string = ""): string => "/api-lms/user-preference";

const PROFILE = (params: string = ""): string => "/api-iam/profile-info";

const LEARNING_JOURNEY = (params: string = ""): string =>
  `/api-lms/user-level${params}`;

const CATEGORY_CONTENT_TYPE = (params: string = ""): string =>
  "/api-lms/content-type";

const PLACEMENT_TEST = (params: string = ""): string =>
  `/api-lms/placement-test${params}`;

const URL_WEBIDX_STOCK_SCREENER = (params: string = ""): string =>
  "/api-iam/info/webidx-base-url";

export {
  ABOUT,
  BASE_URL,
  BASE_URL_IDX,
  CATEGORY_CONTENT_TYPE,
  CATEGORY_COURSE,
  CATEGORY_EVENT,
  CATEGORY_INVESTMENT,
  CATEGORY_NEWS,
  CATEGORY_SECTOR,
  CATEGORY_SUB_SECTOR,
  CERTIFICATE,
  CHANGE_EMAIL,
  CHANGE_PASSWORD,
  COLLECTION,
  COLLECTION_CONTENT,
  COUNTRY,
  COURSE,
  COURSE_COMMENT,
  COURSE_ENROLL,
  COURSE_LIKE,
  COURSE_PROGRESS,
  COURSE_RECOMENDATION,
  COURSE_REPORT,
  DISCLAIMER,
  EVENT,
  EVENT_PORTAL,
  FAQ_PORTAL,
  FETCH_USERS,
  FORGOT_PASSWORD,
  ID_PROVINCE,
  INVESTMENT_PARTNER,
  LEARNING_JOURNEY,
  LEVEL_COURSE,
  LEVEL_UP_QUIZ,
  LEVEL_UP_RETAKE,
  LEVEL_UP_SAVE,
  LEVEL_UP_SUBMIT,
  LOGIN,
  LOGIN_APPLE,
  LOGIN_GOOGLE,
  LOGIN_IDX,
  LOGIN_RDIS,
  LOGIN_TICMIEDU,
  LOGOUT,
  MY_COURSE,
  NEWS,
  NEWS_PORTAL,
  NOTIFICATION,
  PLACEMENT_TEST,
  PREFERENCES,
  PRIVACYPOLICY,
  PROFILE,
  REGISTER,
  RESEND_EMAIL,
  RESET_PASSWORD,
  SEARCH_ALL,
  SEARCH_COURSE,
  SEARCH_NEWS,
  STOCK_SCREENER,
  STOCK_SCREENER_MIN_MAX,
  STOCK_SCREENER_PROFILE_COMPANY,
  TERMSCONDITION,
  URL_CALLBACK_MOBILE,
  URL_WEBIDX_STOCK_SCREENER,
  VALIDATE_RESET_TOKEN,
};
