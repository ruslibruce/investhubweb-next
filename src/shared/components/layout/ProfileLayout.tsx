import { Grid, Layout, Spin } from "antd";
import { useState } from "react";
import styled from "styled-components";
import SiderLayoutAuth from "./auth-layout/sider";
import Navbar from "./dashboard-layout/Navbar";
import FloatScrollToTop from "@/components/FloatScrollToTop";
import dynamic from "next/dynamic";
import LoaderSpinGif from "@/components/LoaderSpinGif";
const DynamicHeaderWhatsapp = dynamic(() => import("@/components/FloatWhatsapp"), {
  loading: () => <LoaderSpinGif size="large" />,
});

const { Content } = Layout;

const CustomLayout = styled(Layout)`
  background: #f8f8f8;
  min-height: 100vh;
`;

type Props = {
  children: React.ReactNode;
};

const ProfileLayout = ({ children }: Props) => {
  const [collapsed, setCollapsed] = useState(false);

  const screens = Grid.useBreakpoint();
  const sideBarCollapseWidth = screens.lg ? 108 : 0; // nol membuat sidebar menghilang ketika di layar kecil

  return (
    <>
      <Navbar profile={true} />
      <Layout style={{ backgroundColor: '#fff' }}>
        <Layout.Content>
          <div
            style={{ minHeight: "100vh", paddingTop: 70, backgroundColor: '#fff' }}
          >
            {children}
            <FloatScrollToTop />
            <DynamicHeaderWhatsapp />
          </div>
        </Layout.Content>

        <Layout.Sider breakpoint="lg" width={600} style={{ backgroundColor: "transparent" }}>
          <SiderLayoutAuth button={false} />
        </Layout.Sider>
      </Layout>
    </>
  );
};

export default ProfileLayout;
