import { fetchNotification } from "@/shared/api/fetch/notification";
import { USER } from "@/shared/constants/storageStatis";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchNotification();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetNotification = (initialData?: any, params?: string) => {
  const dataUser = storageCheck(USER);
  const dataNotif = dataUser.isLogin
    ? fetchNotification(params)
    : { key: [], api: () => Promise.resolve({}) };

  const fetchNotif = useFetchHook({
    keys: [dataNotif.key, params],
    api: dataNotif.api,
    initialData,
    options: {
      refetchInterval: 60_000, // 1 minute
      refetchOnWindowFocus: true,
    },
  });

  return {
    fetchNotif,
  };
};

export default useGetNotification;
