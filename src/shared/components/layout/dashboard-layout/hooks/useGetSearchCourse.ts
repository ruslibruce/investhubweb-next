import { fetchSearchCourse } from "@/shared/api/fetch/search";
import useMutationHook from "@/shared/hooks/useMutationHook";

const useGetSearchCourse = () => {
  const mutationQuery = useMutationHook({
    api: fetchSearchCourse,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
      },
    },
  });

  const handleSearchCourse = (params: string) => {
    mutationQuery.mutate(params);
  };

  return {
    mutationQuery,
    handleSearchCourse,
  };
};

export default useGetSearchCourse;
