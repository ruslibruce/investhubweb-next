import { fetchSearchAll } from "@/shared/api/fetch/search";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { paramsToString } from "@/shared/utils/string";

const useGetSearchAll = () => {
  const mutationQuery = useMutationHook({
    api: fetchSearchAll,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
      },
    },
  });

  const handleSearchAll = (object: any) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handleSearchAll,
  };
};

export default useGetSearchAll;
