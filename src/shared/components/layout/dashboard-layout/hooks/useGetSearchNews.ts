import { fetchSearchNews } from "@/shared/api/fetch/search";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { paramsToString } from "@/shared/utils/string";

const useGetSearchNews = () => {
  const mutationQuery = useMutationHook({
    api: fetchSearchNews,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
      },
    },
  });

  const handleSearchNews = (params: string) => {
    mutationQuery.mutate(
      paramsToString({
        q: params,
      })
    );
  };

  return {
    mutationQuery,
    handleSearchNews 
  };
};

export default useGetSearchNews;
