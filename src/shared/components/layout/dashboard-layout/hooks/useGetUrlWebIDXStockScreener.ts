import { getUrlIDXStockScreener } from "@/shared/api/mutation/urlWebIDXStockScreener";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { paramsToString } from "@/shared/utils/string";
import { useTranslation } from "next-i18next";

const useGetUrlWebIDXStockScreener = () => {
  const { i18n } = useTranslation();
  const mutationQuery = useMutationHook({
    api: getUrlIDXStockScreener,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables: any, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        if(data) {
          window.open(`${data}/${i18n.language}/investhub/stock-screener${paramsToString(variables)}`, "_blank");
        }
      },
    },
  });

  const handleGetUrlIDXStockScreener = (object: any) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handleGetUrlIDXStockScreener,
  };
};

export default useGetUrlWebIDXStockScreener;
