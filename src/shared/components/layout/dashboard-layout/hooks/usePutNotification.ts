import { putReadDataNotif } from "@/shared/api/fetch/notification";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";

const usePutNotification = () => {
  const queryClient = useQueryClient();
  const mutationQuery = useMutationHook({
    api: putReadDataNotif,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        queryClient.invalidateQueries({ queryKey: ["notification"] });
      },
    },
  });

  const handleReadNotification = (id: string, data: {}) => {
    mutationQuery.mutate({ id, data });
  };

  return {
    mutationQuery,
    handleReadNotification,
  };
};

export default usePutNotification;
