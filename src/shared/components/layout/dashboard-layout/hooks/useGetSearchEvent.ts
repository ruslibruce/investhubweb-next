import { fetchSearchEvent } from "@/shared/api/fetch/search";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { paramsToString } from "@/shared/utils/string";

const useGetSearchEvent = () => {
  const mutationQuery = useMutationHook({
    api: fetchSearchEvent,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
      },
    },
  });

  const handleSearchEvent = (params: string) => {
    mutationQuery.mutate(
      paramsToString({
        title: params,
      })
    );
  };

  return {
    mutationQuery,
    handleSearchEvent,
  };
};

export default useGetSearchEvent;
