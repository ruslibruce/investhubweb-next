import { getProfileInfoMutation } from "@/shared/api/mutation/user";
import {
  DASHBOARD_EMAIL_CONFIRM,
  DASHBOARD_HOME,
  DASHBOARD_PREFERENCES,
  DASHBOARD_PRELIMINARY,
  DASHBOARD_UPDATE_PROFILE,
} from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { storageSet } from "@/shared/utils/clientStorageUtils";
import { notification } from "antd";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
const useGetProfileMutate = () => {
  const navigate = useRouter();
  const { t: translate } = useTranslation();
  const mutationQuery = useMutationHook({
    api: getProfileInfoMutation,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables: any, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);

        const { user, token } = variables;

        if (!user.is_email_verified) {
          let temp = {
            loading: true,
            isLogin: false,
            user: user,
            initialRoute: "Home",
            token: token,
            fcmToken: null,
          };

          notification.info({
            message: translate("VerifyEmail"),
            description: translate("VerifyEmailDesc"),
            placement: "bottom",
          });

          storageSet(USER, temp);
          navigate.push(DASHBOARD_EMAIL_CONFIRM);
          return;
        }

        let temp = {
          loading: true,
          isLogin: true,
          user: user,
          initialRoute: "Home",
          token: token,
          fcmToken: null,
          profile: data,
        };

        storageSet(USER, temp);

        if (
          user.is_profile_updated &&
          user.is_placement_test_taken &&
          user.is_preference_updated
        ) {
          navigate.replace(DASHBOARD_HOME);
          return;
        }

        if (!user.is_profile_updated) {
          navigate.replace(DASHBOARD_UPDATE_PROFILE);
        } else if (!user.is_preference_updated) {
          navigate.replace(DASHBOARD_PREFERENCES);
        } else {
          navigate.replace(DASHBOARD_PRELIMINARY);
        }
      },
    },
  });

  const handleGetProfile = (data: {}) => {
    mutationQuery.mutate(data);
  };

  return {
    mutationQuery,
    handleGetProfile,
  };
};

export default useGetProfileMutate;
