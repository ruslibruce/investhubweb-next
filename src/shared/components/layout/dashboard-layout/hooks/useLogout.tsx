import { logout } from "@/shared/api/mutation/logout";
import { USER } from "@/shared/constants/storageStatis";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { storageSet } from "@/shared/utils/clientStorageUtils";

const useLogout = () => {
  const mutationQuery = useMutationHook({
    api: logout,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        console.log("data logout", data);
        // console.log("variables", variables);
        if (data) {
          let temp = {
            loading: true,
            isLogin: false,
            user: null,
            initialRoute: "Home",
            token: null,
            fcmToken: null,
          };
          storageSet(USER, temp);
        }
      },
    },
  });

  const handleLogout = () => {
    mutationQuery.mutate({});
  };

  return {
    mutationQuery,
    handleLogout,
  };
};

export default useLogout;
