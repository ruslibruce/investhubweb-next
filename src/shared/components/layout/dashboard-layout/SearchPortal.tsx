import {
  DASHBOARD_COURSE_DETAIL,
  DASHBOARD_EVENT_DETAIL,
  DASHBOARD_NEWS_DETAIL
} from "@/shared/constants/path";
import useWindowResize from "@/shared/hooks/useWindowResize";
import { paramsToString } from "@/shared/utils/string";
import {
  CloseOutlined,
  SearchOutlined
} from "@ant-design/icons";
import { Col, Input, Row, Space } from "antd";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import React from "react";
import styled from "styled-components";
import PaginationComponent from "../../PaginationComponent";
import useGetSearchAll from "./hooks/useGetSearchAll";

const StyledDiv = styled((props: any) => <Space {...props} />)`
  && div:hover {
    background: #f2f2f2;
    cursor: pointer;
  }
`;

export function SearchPortal() {
  const { width } = useWindowResize({ defaultWidth: 300, persentage: 0.4 });
  const navigate = useRouter();
  const { t: translate } = useTranslation();
  const [currentPage, setCurrentPage] = React.useState<number>(1);
  const [search, setSearch] = React.useState<string>("");
  const [dataSearch, setDataSearch] = React.useState<any>([]);
  const { handleSearchAll, mutationQuery: mutationQuerySearchAll } =
    useGetSearchAll();
  const { data: dataSearchAll } = mutationQuerySearchAll;
  const dataResultSearch = dataSearchAll as any;

  React.useEffect(() => {
    if (dataSearchAll && search) {
      setDataSearch(dataResultSearch.data);
    }
  }, [dataSearchAll]);

  React.useEffect(() => {
    if (search.length > 2 && currentPage) {
      handleSearchAll(paramsToString({ search, page: currentPage }));
    } else {
      setDataSearch([]);
    }
  }, [search, currentPage]);

  const onSearch = (event: any) => {
    setSearch(event.target.value);
  };

  const handleNavigateFromSearch = (item: any) => {
    setSearch("");
    switch (item.source) {
      case "investments":
        window.open(item.url, "_blank");
        break;
      case "news":
        if (item.source_id) {
          window.open(item.link, "_blank");
          return;
        }
        navigate.replace(`${DASHBOARD_NEWS_DETAIL}/${item.id}`);
        break;

      case "events":
        navigate.replace(`${DASHBOARD_EVENT_DETAIL}/${item.id}`);
        break;

      case "course":
        navigate.replace(`${DASHBOARD_COURSE_DETAIL}/${item.id}`);
        break;

      default:
        break;
    }
  };

  return (
    <Space.Compact
      style={{
        alignItems: "center",
        position: "relative",
        width: "40%",
        backgroundColor: "#f2f2f2",
        borderRadius: 5,
        height: 44,
      }}
      className="navbar-search"
    >
      <Input
        prefix={<SearchOutlined style={{ color: "#656565" }} />}
        placeholder={`${translate("Search")}`}
        value={search}
        onChange={onSearch}
        variant="borderless"
        className="navbar-search-custom"
        suffix={
          search && search.length > 0 ? (
            <CloseOutlined onClick={() => setSearch("")} color="#656565" />
          ) : null
        }
      />
      {dataSearch.length > 0 && (
        <Row
          style={{
            backgroundColor: "white",
            width: "100%",
            display: "block",
            overflowY: "scroll",
            padding: 5,
            position: "absolute",
            zIndex: 999,
            top: 44,
          }}
          className="navbar-shadow"
        >
          <Col
            style={{
              backgroundColor: "white",
              maxHeight: 300,
              display: "block",
              overflowY: "scroll",
              padding: 5,
            }}
            span={24}
          >
            {dataSearch.map((item: any, index: any) => {
              return (
                <StyledDiv
                  onClick={() => handleNavigateFromSearch(item)}
                  key={item.id}
                >
                  <Space
                    style={{
                      flexDirection: "column",
                      display: "block",
                      padding: 5,
                      width: width,
                    }}
                  >
                    {item.title && (
                      <span
                        style={{
                          display: "-webkit-box",
                          WebkitLineClamp: 1,
                          WebkitBoxOrient: "vertical",
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                          height: 20,
                        }}
                        className="textfs14-fw600-black"
                        dangerouslySetInnerHTML={{ __html: item.title }}
                      />
                    )}
                    {item.subject && (
                      <span
                        style={{
                          display: "-webkit-box",
                          WebkitLineClamp: 1,
                          WebkitBoxOrient: "vertical",
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                          height: 20,
                        }}
                        className="textfs14-fw600-black"
                        dangerouslySetInnerHTML={{ __html: item.subject }}
                      />
                    )}
                    {item.name && (
                      <span
                        style={{
                          display: "-webkit-box",
                          WebkitLineClamp: 1,
                          WebkitBoxOrient: "vertical",
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                          height: 20,
                        }}
                        className="textfs14-fw600-black"
                        dangerouslySetInnerHTML={{ __html: item.name }}
                      />
                    )}
                    {item.description && (
                      <span
                        style={{
                          display: "-webkit-box",
                          WebkitLineClamp: 1,
                          WebkitBoxOrient: "vertical",
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                          height: 20,
                        }}
                        className="textfs14-fw400-gray truncate"
                        dangerouslySetInnerHTML={{
                          __html: item.description,
                        }}
                      />
                    )}
                    {item.body && (
                      <span
                        style={{
                          display: "-webkit-box",
                          WebkitLineClamp: 1,
                          WebkitBoxOrient: "vertical",
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                          height: 20,
                        }}
                        className="textfs14-fw400-gray truncate"
                        dangerouslySetInnerHTML={{
                          __html: item.body,
                        }}
                      />
                    )}
                    {item.url && (
                      <span
                        style={{
                          display: "-webkit-box",
                          WebkitLineClamp: 1,
                          WebkitBoxOrient: "vertical",
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                          height: 20,
                        }}
                        className="textfs14-fw400-gray truncate"
                        dangerouslySetInnerHTML={{
                          __html: item.url,
                        }}
                      />
                    )}
                  </Space>
                </StyledDiv>
              );
            })}
          </Col>
          <Col
            span={24}
            style={{
              backgroundColor: "white",
              height: 30,
              display: "block",
            }}
          >
            <PaginationComponent
              currentPage={currentPage}
              setCurrentPage={setCurrentPage}
              total={dataResultSearch?.records}
              limit={dataResultSearch?.limit}
            />
          </Col>
        </Row>
      )}
    </Space.Compact>
  );
}
