import ButtonUserAvatar from "@/components/ButtonUserAvatar";
import Hamburger from "@/components/Hamburger";
import LoaderSpinGif from "@/components/LoaderSpinGif";
import RadioButton from "@/components/RadioLanguage";
import ResponsiveSearch from "@/components/ResponsiveSearch";
import TextResponsive from "@/components/TextResponsive";
import {
  DASHBOARD_ABOUT,
  DASHBOARD_CLASS,
  DASHBOARD_CLASS_DETAIL,
  DASHBOARD_COURSE,
  DASHBOARD_COURSE_DETAIL,
  DASHBOARD_EVENT,
  DASHBOARD_EVENT_DETAIL,
  DASHBOARD_FORUM,
  DASHBOARD_HOME,
  DASHBOARD_INVESTMENT,
  DASHBOARD_LOGIN,
  DASHBOARD_MY_CERTIFICATE,
  DASHBOARD_MY_COLLECTION,
  DASHBOARD_MY_COURSE,
  DASHBOARD_MY_PROFILE,
  DASHBOARD_NEWS,
  DASHBOARD_NEWS_DETAIL,
  DASHBOARD_PREFERENCES,
  DASHBOARD_REGISTER,
  DASHBOARD_STOCK_SCREENER
} from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import useLogoutUser from "@/shared/hooks/useLogoutUser";
import iconLogout from "@/shared/images/icon/icon_Logout.webp";
import iconAvatar from "@/shared/images/icon/icon_myProfile.webp";
import IconMyCertificate from "@/shared/images/icon/icon_my_certificate.webp";
import IconMyCollection from "@/shared/images/icon/icon_my_collection.webp";
import iconMyCourse from "@/shared/images/icon/icon_my_course.webp";
import IconMyPreference from "@/shared/images/icon/icon_my_preference.webp";
import iconNotification from "@/shared/images/svg/icon_notification.svg";
import IconLogo from "@/shared/images/svg/logo_investhub_navbar.svg";
import { COLORS } from "@/shared/styles/color";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { paramsToString } from "@/shared/utils/string";
import {
  Badge,
  Button,
  Col,
  Divider,
  Dropdown,
  Flex,
  Row,
  Space,
  type MenuProps
} from "antd";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import PaginationComponent from "../../PaginationComponent";
import { SearchPortal } from "./SearchPortal";
import useGetNotification from "./hooks/useGetNotification";
import useGetUrlWebIDXStockScreener from "./hooks/useGetUrlWebIDXStockScreener";
import usePutNotification from "./hooks/usePutNotification";
dayjs.extend(utc);
dayjs.extend(timezone);
type profileProps = {
  profile: boolean;
};

function Navbar({ profile = false }: profileProps) {
  const navigate = useRouter();
  const { asPath } = navigate;
  const { t: translation, i18n } = useTranslation();
  const dataUser = storageCheck(USER);
  const [currentPage, setCurrentPage] = React.useState<number>(1);
  const { handleGetUrlIDXStockScreener, mutationQuery } =
    useGetUrlWebIDXStockScreener();
  const { isPending } = mutationQuery;
  // console.log("dataUser", dataUser);

  const { fetchNotif } = useGetNotification(
    [],
    paramsToString({
      page: currentPage,
      limit: 10,
      app: "portal",
      lan: navigate.locale,
    })
  );
  const { data: dataResultNotification } = fetchNotif;
  const { data: dataNotification = [] } = dataResultNotification || {};
  const { handleReadNotification } = usePutNotification();
  const [myProfile, setMyProfile] = React.useState<boolean>(false);
  const {handleSignout} = useLogoutUser();

  const handleMarkAllRead = () => {
    dataNotification.map((item: any) => {
      handleReadNotification(item.id, { is_read: true });
    });
  };

  //icon my acount dropdown
  const items: MenuProps["items"] = [
    {
      label: translation("My_Profile"),
      key: "My Profile",
      icon: <Image width={20} height={20} src={iconAvatar} alt="icon-logout" />,
    },
    {
      label: translation("My_Course"),
      key: "course",
      icon: (
        <Image width={20} height={20} src={iconMyCourse} alt="icon-my-course" />
      ),
    },
    {
      label: translation("Preferences"),
      key: "preferences",
      icon: (
        <Image
          width={20}
          height={20}
          src={IconMyPreference}
          alt="icon-my-preferences"
        />
      ),
    },
    {
      label: translation("MyCertificate"),
      key: "certificate",
      icon: (
        <Image
          width={20}
          height={20}
          src={IconMyCertificate}
          alt="icon-my-certificate"
        />
      ),
    },
    {
      label: translation("MyCollection"),
      key: "collection",
      icon: (
        <Image
          width={20}
          height={20}
          src={IconMyCollection}
          alt="icon-my-collection"
        />
      ),
    },
    {
      label: translation("Logout"),
      key: "logout",
      icon: <Image width={20} height={20} src={iconLogout} alt="icon-logout" />,
    },
  ];

  const handleMenuClick: MenuProps["onClick"] = (e) => {
    if (e.key === "logout") {
      handleSignout();
      navigate.replace(DASHBOARD_HOME);
      return;
    }

    if (e.key === "collection") {
      navigate.push(DASHBOARD_MY_COLLECTION);
      return;
    }

    if (e.key === "preferences") {
      navigate.push(DASHBOARD_PREFERENCES);
      return;
    }

    if (e.key === "course") {
      navigate.push(DASHBOARD_MY_COURSE);
      return;
    }

    if (e.key === "certificate") {
      navigate.push(DASHBOARD_MY_CERTIFICATE);
      return;
    }

    navigate.push(DASHBOARD_MY_PROFILE);
  };

  const menuProps = {
    items,
    onClick: handleMenuClick,
  };

  return (
    <div className="navbar">
      <div className="navbar-top-container">
        <Link href={profile ? "#" : DASHBOARD_HOME} className="navbar-logo">
          <Image
            src={IconLogo}
            alt="logo-icon"
            className="logo-icon"
            style={{ width: "auto", height: "auto" }}
          />
        </Link>
        <h3 className="navbar-time">
          {`${dayjs().tz("Asia/Jakarta").format("DD MMMM YYYY")} | ${dayjs()
            .tz("Asia/Jakarta")
            .format("HH:mm")} WIB`}
        </h3>
        <SearchPortal />
        <div className="navbar-tablet">
          <div className="responsive-search">
            <ResponsiveSearch />
          </div>
          {dataUser?.isLogin ? (
            <>
              <Dropdown
                // className="navbar-notification"
                placement="bottom"
                dropdownRender={(menu) => (
                  <Flex
                    justify="space-between"
                    style={{
                      backgroundColor: "white",
                      flexDirection: "column",
                      padding: 10,
                      width: 400,
                    }}
                  >
                    <Row
                      style={{
                        justifyContent:
                          dataNotification.length > 0
                            ? "space-between"
                            : "center",
                        alignItems: "center",
                        marginBottom: 10,
                      }}
                      gutter={[16, 16]}
                    >
                      <Col>
                        <TextResponsive
                          bigScreen={18}
                          mobile={14}
                          isSpan
                          fontWeight={700}
                          desktop={16}
                          color={COLORS.BLACK}
                        >
                          {translation("Notifications")}
                        </TextResponsive>
                      </Col>
                      {dataNotification.length > 0 && (
                        <Col
                          style={{ cursor: "pointer" }}
                          onClick={handleMarkAllRead}
                        >
                          <TextResponsive
                            bigScreen={16}
                            mobile={12}
                            isSpan
                            fontWeight={400}
                            desktop={14}
                            color={COLORS.PRIMARY}
                          >
                            {translation("Mark_All")}
                          </TextResponsive>
                        </Col>
                      )}
                    </Row>
                    <Divider style={{ margin: 0 }} />
                    <Row
                      style={{
                        width: "100%",
                        overflowY: "scroll",
                        maxHeight: 300,
                        marginTop: 10,
                        padding: "15px 10px",
                        // Hide scrollbar for WebKit-based browsers (Chrome, Safari, etc.)
                        scrollbarWidth: "none", // For Firefox
                        msOverflowStyle: "none", // For Internet Explorer and Edge
                      }}
                    >
                      {dataNotification.length === 0 && (
                        <Col span={24}>
                          <TextResponsive
                            bigScreen={16}
                            mobile={12}
                            isSpan
                            fontWeight={400}
                            desktop={14}
                            color={COLORS.WHITE}
                          >
                            {translation("No_Notification")}
                          </TextResponsive>
                        </Col>
                      )}
                      {dataNotification.map((item: any, index: any) => {
                        return (
                          <Col
                            style={{
                              backgroundColor: COLORS.WHITE,
                              justifyContent: "space-between",
                              padding: 10,
                              cursor: "pointer",
                              boxShadow: "0 8px 16px rgba(0, 0, 0, 0.05)",
                              marginBottom: 15,
                            }}
                            onClick={() => {
                              handleReadNotification(item.id, {
                                is_read: true,
                              });
                              console.log("url notif", item.url);
                              if (item.url) {
                                navigate.push(item.url);
                              }
                            }}
                            key={item.id}
                            span={24}
                          >
                            {item.is_read ? (
                              <TextResponsive
                                bigScreen={16}
                                mobile={12}
                                isSpan
                                fontWeight={400}
                                desktop={14}
                                color={COLORS.BLACK}
                              >
                                {item.message}
                              </TextResponsive>
                            ) : (
                              <TextResponsive
                                bigScreen={16}
                                mobile={12}
                                isSpan
                                fontWeight={800}
                                desktop={14}
                                color={COLORS.BLACK}
                              >
                                {item.message}
                              </TextResponsive>
                            )}
                            {/* <Space style={{ flexDirection: "row" }}>
                                <span className="textfs14-fw400-gray">
                                  {moment(item.created_at).fromNow()}
                                </span>
                                <LuDot />
                                <span className="textfs14-fw600-gray">
                                  {item.category}
                                </span>
                              </Space> */}
                          </Col>
                        );
                      })}
                    </Row>
                    {dataNotification.length > 0 && (
                      <Col span={24} style={{ marginTop: "0 auto" }}>
                        <PaginationComponent
                          currentPage={currentPage}
                          setCurrentPage={setCurrentPage}
                          total={dataResultNotification.records}
                          limit={dataResultNotification.limit}
                        />
                      </Col>
                    )}
                  </Flex>
                )}
              >
                {dataResultNotification.unread > 0 ? (
                  <Space style={{ marginRight: 15 }}>
                    <Badge count={dataResultNotification.unread}>
                      <Image alt="icon notif" src={iconNotification} />
                    </Badge>
                  </Space>
                ) : (
                  <Space style={{ marginRight: 15 }}>
                    <Image alt="icon notif" src={iconNotification} />
                  </Space>
                )}
              </Dropdown>
              <div className="button-my-account">
                <ButtonUserAvatar
                  className={""}
                  menuProps={menuProps}
                  setMyProfile={setMyProfile}
                  myProfile={myProfile}
                  user={dataUser?.profile?.photo}
                />
              </div>
            </>
          ) : (
            <>
              <Button
                style={{ color: "#9f0e0f", width: 100 }}
                className="navbar-btn-login"
                onClick={() => navigate.push(DASHBOARD_LOGIN)}
                onTouchStart={() => <LoaderSpinGif />}
              >
                {translation("Login")}
              </Button>
              <Button
                style={{ color: "white", width: 100 }}
                type="primary"
                className="navbar-btn-register"
                onClick={() => navigate.push(DASHBOARD_REGISTER)}
                onTouchStart={() => <LoaderSpinGif />}
              >
                {translation("Register")}
              </Button>
            </>
          )}
          <div className="hamburger">
            <Hamburger />
          </div>
          <RadioButton classNameView="radioNavbar" />
        </div>
      </div>
      {!profile && (
        <div className="navbar-bottom-container">
          <ul className="navbar-bottom-menu">
            <li
              className={
                asPath === DASHBOARD_HOME ? "navbar-bottom-menu-active" : ""
              }
            >
              <Link href={DASHBOARD_HOME}>{translation("Home")}</Link>
            </li>
            <li
              className={
                asPath === DASHBOARD_COURSE ||
                asPath === DASHBOARD_COURSE_DETAIL ||
                asPath === DASHBOARD_MY_COURSE
                  ? "navbar-bottom-menuParent-active"
                  : "navbar-bottom-menuParent"
              }
            >
              {translation("Course_Tab")}
              <div className="mega-box">
                <div className="content-mega-box">
                  <Link
                    className={
                      asPath === DASHBOARD_COURSE
                        ? "row-mega-box-active"
                        : "row-mega-box"
                    }
                    href={DASHBOARD_COURSE}
                  >
                    {translation("Course_Tab")}
                  </Link>
                  {dataUser?.isLogin && (
                    <Link
                      className={
                        asPath === DASHBOARD_MY_COURSE
                          ? "row-mega-box-active"
                          : "row-mega-box"
                      }
                      href={DASHBOARD_MY_COURSE}
                    >
                      {translation("My_Course")}
                    </Link>
                  )}
                  <Link className="row-mega-box" href="#">
                    {translation("Ebook")}
                  </Link>
                  <Link className="row-mega-box" href="#">
                    {translation("Glossarium")}
                  </Link>
                </div>
              </div>
            </li>
            <li
              className={
                asPath === DASHBOARD_CLASS || asPath === DASHBOARD_CLASS_DETAIL
                  ? "navbar-bottom-menu-active"
                  : ""
              }
            >
              {/* <Link href={DASHBOARD_CLASS}>{translation("Class")}</Link> */}
              <Link href={"#"}>{translation("Class")}</Link>
            </li>
            <li
              className={
                asPath === DASHBOARD_EVENT || asPath === DASHBOARD_EVENT_DETAIL
                  ? "navbar-bottom-menu-active"
                  : ""
              }
            >
              <Link href={DASHBOARD_EVENT}>{translation("Event_Tab")}</Link>
            </li>
            <li
              className={
                asPath === DASHBOARD_NEWS || asPath === DASHBOARD_NEWS_DETAIL
                  ? "navbar-bottom-menu-active"
                  : ""
              }
            >
              <Link href={DASHBOARD_NEWS}>{translation("News_Tab")}</Link>
            </li>
            <li
              className={
                asPath === DASHBOARD_FORUM ? "navbar-bottom-menu-active" : ""
              }
            >
              <Link href="" className="desktop-item">
                {translation("Forum_Tab")}
              </Link>
              <div className="mega-box">
                <div className="content-mega-box">
                  <div className="row-mega-box">
                    {/* <Link href={DASHBOARD_FORUM}> */}
                    <Link href={"#"}>{translation("Forum_Tab")}</Link>
                  </div>
                  {dataUser?.isLogin && (
                    <div className="row-mega-box">
                      <Link href={""}>{translation("My_Forum")}</Link>
                    </div>
                  )}
                </div>
              </div>
            </li>
            <li
              className={
                asPath === DASHBOARD_STOCK_SCREENER
                  ? "navbar-bottom-menu-active"
                  : ""
              }
            >
              <Link
                href={"#"}
                onClick={() =>
                  handleGetUrlIDXStockScreener({
                    tokenInvesthub: dataUser?.token,
                  })
                }
              >
                {isPending ? <LoaderSpinGif /> : translation("Stock")}
              </Link>
            </li>
            <li
              className={
                asPath === DASHBOARD_INVESTMENT
                  ? "navbar-bottom-menu-active"
                  : ""
              }
            >
              <Link href={DASHBOARD_INVESTMENT}>
                {translation("Investment")}
              </Link>
            </li>
            {/* {dataUser?.isLogin && (
              <li
                className={
                  asPath === DASHBOARD_LEVEL_UP
                    ? "navbar-bottom-menuParent-active"
                    : "navbar-bottom-menuParent"
                }
              >
                {translation("Learning")}
                <div className="mega-box">
                  <div className="content-mega-box">
                    <Link
                      className={
                        asPath === DASHBOARD_LEVEL_UP
                          ? "row-mega-box-active"
                          : "row-mega-box"
                      }
                      href={DASHBOARD_LEVEL_UP}
                    >
                      {translation("Level_Up")}
                    </Link>
                  </div>
                </div>
              </li>
            )} */}
            <li
              className={
                asPath === DASHBOARD_ABOUT ? "navbar-bottom-menu-active" : ""
              }
            >
              <Link href={DASHBOARD_ABOUT}>{translation("About")}</Link>
            </li>
          </ul>
        </div>
      )}
    </div>
  );
}

export default Navbar;
