import React from "react";

// styled
import styled from "styled-components";

// image
// import LogoCms from 'src/shared/images/logo-cms.png';

// hooks
import useWindowResize from "@/shared/hooks/useWindowResize";
import FloatScrollToTop from "@/components/FloatScrollToTop";
import dynamic from "next/dynamic";
import { Spin } from "antd";
import LoaderSpinGif from "@/components/LoaderSpinGif";
const DynamicHeaderWhatsapp = dynamic(() => import("@/components/FloatWhatsapp"), {
  loading: () => <LoaderSpinGif size="large" />,
});

type ContentLayoutAuthProps = {
  children: React.ReactNode;
};

const StyledDiv = styled.div`
  min-height: 100vh;
  background: #ffffff;
  justify-content: center;
  align-items: center;
`;

const ContentLayoutAuth = (props: ContentLayoutAuthProps) => {
  // hooks
  const { width } = useWindowResize({ defaultWidth: 400, persentage: 0.6 });

  return (
    <StyledDiv>
        {props.children}
        <FloatScrollToTop />
        <DynamicHeaderWhatsapp />
    </StyledDiv>
  );
};

export default ContentLayoutAuth;
