import { CSSProperties } from "react";

type StylesType = {
  ButtonFloat: CSSProperties;
  ButtonFloatMobile: CSSProperties;
  ButtonFloatMobileCourse: CSSProperties;
};

/**
 * @return {StylesType} styles
 */
function StylesSider(): StylesType {
  const ButtonFloat: CSSProperties = {
    position: "absolute",
    top: 20,
    left: 500,
    padding: "18px 12px",
    height: "42px",
    display: "flex",
    borderRadius: "100px",
    backgroundColor: "#e8e8e8",
    gap: "8px",
    maxWidth: "72px",
    alignItems: "center",
    justifyContent: "center",
    zIndex: 999,
  };

  const ButtonFloatMobile: CSSProperties = {
    position: "absolute",
    right: 20,
    top: 100,
    padding: "18px 12px",
    height: "42px",
    display: "flex",
    borderRadius: "100px",
    backgroundColor: "#e8e8e8",
    gap: "8px",
    maxWidth: "72px",
    alignItems: "center",
    justifyContent: "center",
    zIndex: 999,
  };

  const ButtonFloatMobileCourse: CSSProperties = {
    position: "absolute",
    right: 35,
    top: -7,
    padding: "0px 20px",
    height: "42px",
    display: "flex",
    borderRadius: "100px",
    backgroundColor: "#fff",
    gap: "8px",
    maxWidth: "72px",
    alignItems: "center",
    justifyContent: "center",
    zIndex: 999,
  };

  return {
    ButtonFloat,
    ButtonFloatMobile,
    ButtonFloatMobileCourse,
  };
}

export default StylesSider;
