// styled
import styled from "styled-components";

// style
import StylesSider from "./Style";

// next
import Image from "next/image";

// image
import IconFlagIND from "@/shared/images/icon/indonesia_round_icon.webp";
import IconFlag from "@/shared/images/icon/united_kingdom_round_icon.webp";

// icon
import { Dropdown, Space } from "antd";
import { useTranslation } from "next-i18next";
import Link from "next/link";
import { useRouter } from "next/router";
import { DownOutlined } from "@ant-design/icons";

const StyledDiv = styled.div;

export default function ButtonFlag(props: any) {
  // style
  const stylesSider = StylesSider();
  const navigate = useRouter();
  const { asPath } = navigate;
  const { i18n } = useTranslation();

  return (
    <Dropdown
      placement="bottom"
      trigger={["click"]}
      dropdownRender={(menu) => (
        <Space
          style={{
            backgroundColor: "white",
            flexDirection: "column",
            borderRadius: 10,
          }}
        >
          <Link href={asPath} locale="en">
            <Space
              style={{
                padding: "2px 8px",
              }}
              direction="horizontal"
            >
              <Image style={{ width: 30 }} src={IconFlag} alt="flag-icon" />
              <span className="textfs16-fw700-black">{"EN"}</span>
            </Space>
          </Link>
          <Link href={asPath} locale="id">
            <Space
              style={{
                padding: "2px 8px",
              }}
              direction="horizontal"
            >
              <Image style={{ width: 30 }} src={IconFlagIND} alt="flag-icon" />
              <span className="textfs16-fw700-black">{"ID"}</span>
            </Space>
          </Link>
        </Space>
      )}
    >
      <div
        style={
          props.course
            ? stylesSider.ButtonFloatMobileCourse
            : props.mobileVersion
            ? stylesSider.ButtonFloatMobile
            : stylesSider.ButtonFloat
        }
      >
        <Image
          style={{ width: 30, marginRight: -10 }}
          src={navigate.locale == "en" ? IconFlag : IconFlagIND}
          alt="flag-icon"
          className="flag-icon"
        />
        <DownOutlined
          style={{ fontSize: 15, marginLeft: 8, color: "#9F0E0F" }}
        />
      </div>
    </Dropdown>
  );
}
