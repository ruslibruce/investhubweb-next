import { checkIsAuthRoute, checkIsPrivateRoute, checkIsProfileRoute } from "@/shared/utils/route";
import { useRouter } from "next/router";
import React, { useState } from 'react';
import AuthLayout from "./AuthLayout";
import DashboardLayout from './DashboardLayout';
import ProfileLayout from './ProfileLayout';

type LayoutProps = {
  children: React.ReactNode;
};

const Layout = ({ children }: LayoutProps) => {
  const navigate = useRouter();

  const [initialRenderComplete, setInitialRenderComplete] = useState(false);

  React.useEffect(() => {
    setInitialRenderComplete(true);
  }, []);

  if (!initialRenderComplete) {
    return null;
  }

  if (checkIsPrivateRoute(navigate.route)) {
    return <DashboardLayout>{children}</DashboardLayout>;
  }

  if (checkIsAuthRoute(navigate.route)) {
    return <AuthLayout>{children}</AuthLayout>;
  }

  if(checkIsProfileRoute(navigate.route)){
    return <ProfileLayout>{children}</ProfileLayout>
  }

  return <>{children}</>;
};

export default Layout;