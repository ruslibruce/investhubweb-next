import FloatScrollToTop from '@/components/FloatScrollToTop';
import { Grid, Layout, Spin } from 'antd';
import { useRouter } from 'next/router';
import { useState } from 'react';
import styled from 'styled-components';
import FooterContentPrivateLayout from './private-layout/FooterContentPrivateLayout';
import dynamic from "next/dynamic";
import LoaderSpinGif from '@/components/LoaderSpinGif';
import Navbar from './dashboard-layout/Navbar';
const DynamicHeaderWhatsapp = dynamic(() => import("@/components/FloatWhatsapp"), {
  loading: () => <LoaderSpinGif size="large" />,
});

const { Content } = Layout;

const CustomLayout = styled(Layout)`
	background: #f8f8f8;
	min-height: 100vh;
`;

type Props = {
  children: React.ReactNode;
};

const DashboardLayout = ({ children }: Props) => {
  const [collapsed, setCollapsed] = useState(false);
  const navigate = useRouter();

  const screens = Grid.useBreakpoint();
  const sideBarCollapseWidth = screens.lg ? 108 : 0; // nol membuat sidebar menghilang ketika di layar kecil

  return (
    <>
      <Layout>
        <CustomLayout>
          <Content>
            <Navbar profile={false} />

            <div>
              {children}
              <FloatScrollToTop />
              <DynamicHeaderWhatsapp />
            </div>

            <FooterContentPrivateLayout />
          </Content>
        </CustomLayout>

      </Layout>
    </>
  );
};

export default DashboardLayout;