import { useState } from "react";

// antd
import {
  DASHBOARD_DISCLAIMER,
  DASHBOARD_FAQ,
  DASHBOARD_PRIVACY,
} from "@/shared/constants/path";
import GetIOS from "@/shared/images/svg/logo_footer/Apple_Card.svg";
import GetGoogle from "@/shared/images/svg/logo_footer/Google_Card.svg";
import {
  FacebookFilled,
  InstagramOutlined,
  TikTokOutlined,
  WhatsAppOutlined,
  XOutlined,
  YoutubeFilled,
} from "@ant-design/icons";
import { Col, Flex, Row, Space } from "antd";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import IconEmail from "@/shared/images/svg/icon_email.svg";
import IconEmailActive from "@/shared/images/svg/icon_email_active.svg";
import IconPhone from "@/shared/images/svg/icon_phone.svg";
import IconPhoneActive from "@/shared/images/svg/icon_phone_active.svg";
import { StaticImport } from "next/dist/shared/lib/get-img-props";

// hooks
// import useGetListSettings from 'src/features/konfigurasi/settings/hooks/useGetListSettings';

const FooterContentPrivateLayout = () => {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const [valueImagePhone, setValueImagePhone] = useState<string | StaticImport>(
    IconPhone
  );
  const [valueImageEmail, setValueImageEmail] = useState<string | StaticImport>(
    IconEmail
  );
  const [color, setColor] = useState<string>("#6D737A");

  return (
    <footer>
      <Row
        wrap
        className="footer-section"
        style={{ padding: window.innerWidth < 800 ? "16px 25px" : "16px 72px" }}
      >
        <Col xs={20} sm={16} md={12} lg={8} className="footer-widget">
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              cursor: "pointer",
            }}
            onMouseEnter={() => setColor("#9F0E0F")}
            onMouseLeave={() => setColor("#6D737A")}
          >
            <Link
              href="https://www.idx.co.id/id/tentang-bei/hubungi-kami/"
              target="_blank"
              className="textfs16-fw400-gray"
              style={{ color: color }}
            >
              {translate("BEIAddress")}
            </Link>
            <Link
              href="https://www.idx.co.id/id/tentang-bei/hubungi-kami/"
              target="_blank"
              className="textfs16-fw400-gray"
              style={{ color: color }}
            >
              {translate("BEIAddress2")}
            </Link>
          </div>
          <Link
            href="tel:150515"
            target="_blank"
            style={{ cursor: "pointer", gap: 5 }}
            className="text-phonefooter-idx"
            onMouseEnter={() => setValueImagePhone(IconPhoneActive)}
            onMouseLeave={() => setValueImagePhone(IconPhone)}
          >
            <Image
              src={valueImagePhone}
              alt="icon-email"
              width={20}
              height={20}
            />
            {`150515 (${translate("NationalID")})`}
          </Link>
          <Link
            href="mailto:contactcenter@idx.co.id"
            target="_blank"
            style={{ cursor: "pointer", gap: 5 }}
            className="text-emailfooter-idx"
            onMouseEnter={() => setValueImageEmail(IconEmailActive)}
            onMouseLeave={() => setValueImageEmail(IconEmail)}
          >
            <Image
              src={valueImageEmail}
              alt="icon-email"
              width={20}
              height={20}
            />
            {"Email: contactcenter@idx.co.id"}
          </Link>
          <Link
            href={"https://wa.me/+6281181150515"}
            target="_blank"
            style={{ cursor: "pointer", gap: 5 }}
            className="text-wafooter-idx"
          >
            <WhatsAppOutlined style={{ fontSize: 20 }} />
            {"+6281181150515"}
          </Link>
        </Col>
        <Col
          xs={20}
          sm={16}
          md={12}
          lg={8}
          className="footer-widget"
          style={{
            alignItems: "center",
            justifyContent: window.innerWidth < 800 ? "flex-start" : "center",
            marginTop: window.innerWidth < 800 ? 20 : 0,
          }}
        >
          <ul className="footer-link-right">
            <li>
              <Link href={DASHBOARD_DISCLAIMER}>Disclaimer</Link>
            </li>
            <li>
              <Link href={DASHBOARD_FAQ}>{translate("Faq")}</Link>
            </li>
            <li>
              <Link href={DASHBOARD_PRIVACY}>{translate("PrivacyPolicy")}</Link>
            </li>
          </ul>
        </Col>
        {/* <Col xs={20} sm={16} md={12} lg={8} >
          <div wrap>
            <ul className="footer-link-left">
              <li>
                <Link href={DASHBOARD_PRIVACY}>
                  {translate("PrivacyPolicy")}
                </Link>
              </li>
            </ul>
          </div>
        </Col> */}
        <Col
          xs={20}
          sm={16}
          md={12}
          lg={8}
          style={{
            alignItems: "center",
            justifyContent:
              window.innerWidth < 1000 ? "flex-start" : "flex-end",
            marginTop: window.innerWidth < 1000 ? 20 : 0,
          }}
          className="footer-widget"
        >
          <Flex justify="space-between" gap={10} className="footer-sosmed">
            <Link href="#" className="fil-fb">
              <FacebookFilled style={{ fontSize: 25 }} />
            </Link>
            <Link href="#" className="fil-twitter">
              <XOutlined style={{ fontSize: 25 }} />
            </Link>
            <Link href="#" className="fil-ig">
              <InstagramOutlined style={{ fontSize: 25 }} />
            </Link>
            <Link href="#" className="fil-youtube">
              <YoutubeFilled style={{ fontSize: 25 }} />
            </Link>
            <Link href="#" className="fil-wa">
              <WhatsAppOutlined style={{ fontSize: 25 }} />
            </Link>
            <Link href="#" className="fil-tiktok">
              <TikTokOutlined style={{ fontSize: 25 }} />
            </Link>
          </Flex>
          <Flex
            style={{
              marginTop: 10,
              marginBottom: 10,
              justifyContent:
                window.innerWidth < 1000 ? "flex-start" : "flex-end",
            }}
          >
            <Link href="#" style={{ marginRight: 10 }}>
              <Image
                width={100}
                height={100}
                className="android"
                alt="Get it on Google Play"
                src={GetGoogle}
                style={{ width: 150, height: 50 }}
              />
            </Link>
            <Link href="#">
              <Image
                width={100}
                height={100}
                className="apple"
                src={GetIOS}
                alt="Download on the App Store"
                style={{ width: 150, height: 50 }}
              />
            </Link>
          </Flex>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-end",
            }}
          >
            <span className="textfs12-fw400-gray">
              {`Copyright © 2022 - 2024. ${translate("BEI")}`}
            </span>
            <span className="textfs12-fw400-gray">All rights reserved.</span>
          </div>
        </Col>
      </Row>
    </footer>
  );
};

export default FooterContentPrivateLayout;
