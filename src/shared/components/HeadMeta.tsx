import Head from "next/head";
import React from "react";
import bgHero from "@/shared/images/icon/Hero_Section.webp";

type Props = {
  title: string;
  children?: React.ReactNode;
}

const HeadMeta = ({ title, children }: Props) => {
  return (
    <Head>
      <link rel="icon" href="/favicon.ico" sizes="any" />
      <link
        rel="icon"
        href="/icon?<generated>"
        type="image/<generated>"
        sizes="<generated>"
      />
      <link
        rel="apple-touch-icon"
        href="/apple-icon?<generated>"
        type="image/<generated>"
        sizes="<generated>"
      />
      <link rel="preload" href={bgHero.src} as="image" />
      <title>{title}</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      {children}
    </Head>
  );
};

export default HeadMeta;
