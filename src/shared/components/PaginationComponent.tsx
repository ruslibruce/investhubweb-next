import React, { use, useState } from "react";
import { Pagination, PaginationProps } from "antd";
import { useTranslation } from "next-i18next";
type PaginationComponentProps = {
  total: number;
  setCurrentPage: (value: any) => void;
  currentPage: number;
  limit?: number;
  stylePagination?: React.CSSProperties;
};

export default function PaginationComponent({
  total,
  currentPage,
  setCurrentPage,
  limit = 10,
  stylePagination,
}: PaginationComponentProps) {
  const {i18n} = useTranslation();
  const onChange: PaginationProps["onChange"] = (page) => {
    console.log("Pagination", page);
    setCurrentPage(page);
  };

  const showTotal: PaginationProps["showTotal"] = (total) =>
    `Total ${total} ${i18n.language === "en" ? "items" : "item"}`;

  return (
    <Pagination
      responsive
      style={stylePagination}
      className="bottom-pagination-card"
      total={total}
      current={currentPage}
      onChange={onChange}
      pageSize={limit}
      showSizeChanger={false}
      showTotal={showTotal}
      showQuickJumper
    />
  );
} // Path: src/shared/utils/pagination.ts
