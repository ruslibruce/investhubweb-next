import axios, { AxiosInstance, CreateAxiosDefaults } from "axios";
import { BASE_URL_IDX } from "../constants/endpoint";

const AxiosInstanceConfig: CreateAxiosDefaults = {
  timeout: 20000,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
  },
  baseURL: BASE_URL_IDX,
};

const AxiosInstanceIDX: AxiosInstance = axios.create(AxiosInstanceConfig);

export default AxiosInstanceIDX;
