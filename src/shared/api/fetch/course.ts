import {
  CATEGORY_CONTENT_TYPE,
  CATEGORY_COURSE,
  COURSE,
  COURSE_COMMENT,
  COURSE_ENROLL,
  COURSE_LIKE,
  COURSE_PROGRESS,
  COURSE_RECOMENDATION,
  COURSE_REPORT,
  LEVEL_COURSE,
  MY_COURSE
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData, postData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchCourses(params?: string): Props {
  const KEY = ["course"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataCourse = await getData({
      endpoint: COURSE(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataCourse?.data;
  };

  return { key: KEY, api: API };
}

function fetchDetailCourses(params?: string): Props {
  const KEY = ["courseDetail"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataCourse = await getData({
      endpoint: COURSE(`/id/${params}`),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataCourse?.data.data;
  };

  return { key: KEY, api: API };
}

function fetchCategoryCourses(params?: string): Props {
  const KEY = ["courseCategory"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataCourse = await getData({
      endpoint: CATEGORY_COURSE(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    dataCourse?.data.data.map((item: any) => {
      item.key = item.id;
      item.label = item.name;
      item.value = item.name;
    });

    return dataCourse?.data.data;
  };

  return { key: KEY, api: API };
}

function fetchLevelCourses(params?: string): Props {
  const KEY = ["courseLevel"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataCourse = await getData({
      endpoint: LEVEL_COURSE(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    dataCourse?.data.data.map((item: any) => {
      item.key = item.id;
      item.label = item.name;
      item.value = item.name;
    });

    return dataCourse?.data.data;
  };

  return { key: KEY, api: API };
}

function fetchCourseContentType(params?: string): Props {
  const KEY = ["courseContentType"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataCourse = await getData({
      endpoint: CATEGORY_CONTENT_TYPE(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    dataCourse?.data.data.map((item: any) => {
      item.key = item.id;
      item.label = item.name;
      item.value = item.name;
    });

    return dataCourse?.data.data;
  };

  return { key: KEY, api: API };
}

function fetchDetailCoursesProgress(params?: string): Props {
  const KEY = ["courseDetail"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataCourse = await getData({
      endpoint: COURSE_PROGRESS(`/${params}`),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataCourse?.data.data;
  };

  return { key: KEY, api: API };
}

const postEnrollCourse = async (data: any) => {
  const user = storageCheck(USER);
  const res = await postData({
    endpoint: COURSE_ENROLL(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data;
};

const postProgress = async (data: any) => {
  const user = storageCheck(USER);
  const res = await postData({
    endpoint: COURSE_PROGRESS(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data;
};

function fetchMyCourses(params?: string): Props {
  const KEY = ["mycourse"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataCourse = await getData({
      endpoint: MY_COURSE(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataCourse?.data;
  };

  return { key: KEY, api: API };
}

function getCourseRecomendations(params?: string): Props {
  const KEY = ["courseRecomendations"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataCourseRecomend = await getData({
      endpoint: COURSE_RECOMENDATION(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataCourseRecomend?.data;
  };

  return { key: KEY, api: API };
}

const postReportCourse = async (data: any) => {
  const user = storageCheck(USER);
  const res = await postData({
    endpoint: COURSE_REPORT(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data;
};

const postCommentCourse = async (data: any) => {
  const user = storageCheck(USER);
  const res = await postData({
    endpoint: COURSE_COMMENT(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data;
};

const postLikeCourse = async (data: any) => {
  const user = storageCheck(USER);
  const res = await postData({
    endpoint: COURSE_LIKE(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data;
};

const getCommentCourse = async ({id, type}: {id: string; type: string}) => {
  const user = storageCheck(USER);
  const res = await getData({
    endpoint: COURSE_COMMENT(`/${id}?type=${type}`),
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data.data;
};

function getCourseLike(id?: string): Props {
  const KEY = ["courseLike"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataCourseLike = await getData({
      endpoint: COURSE_LIKE(`/${id}`),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataCourseLike?.data;
  };

  return { key: KEY, api: API };
}

export {
  fetchCategoryCourses, fetchCourseContentType, fetchCourses,
  fetchDetailCourses,
  fetchDetailCoursesProgress,
  fetchLevelCourses,
  fetchMyCourses,
  getCourseRecomendations,
  postEnrollCourse,
  postProgress,
  postReportCourse,
  postCommentCourse,
  postLikeCourse,
  getCommentCourse,
  getCourseLike,
};

