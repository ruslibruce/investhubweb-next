import {
    CERTIFICATE
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData, postData } from "@/shared/utils/http/axiosHelper";
  
  type Props = {
    key: string[];
    api: () => Promise<any>;
  };
  
  function getCertificate(params?: string): Props {
    const KEY = ["certificate"];
    const user = storageCheck(USER);
  
    const API = async () => {
      const dataCourseRecomend = await getData({
        endpoint: CERTIFICATE(params),
        config: {
          headers: {
            Authorization: `Bearer ${user.token}`,
          },
        },
      });
  
      return dataCourseRecomend?.data;
    };
  
    return { key: KEY, api: API };
  }

  async function postUploadCertificate(data: any) {
    console.log("cek postUploadCertificate", data);
    const user = storageCheck(USER);
  
    const dataPostUpload = await postData({
      endpoint: CERTIFICATE(),
      data: data,
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });
    return dataPostUpload?.data;
  }
  
  export {
    getCertificate, postUploadCertificate
};
  