import {
  EVENT_PORTAL,
  SEARCH_ALL,
  SEARCH_COURSE,
  SEARCH_NEWS,
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

async function fetchSearchNews(params?: string) {
  const user = storageCheck(USER);
  const dataSearch = await getData({
    endpoint: SEARCH_NEWS(params),
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });

  return dataSearch?.data.data;
}

async function fetchSearchEvent(params?: string) {
  const user = storageCheck(USER);
  const dataSearch = await getData({
    endpoint: EVENT_PORTAL(params),
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });

  return dataSearch?.data.data;
}

async function fetchSearchCourse(params?: string) {
  const user = storageCheck(USER);
  const dataSearch = await getData({
    endpoint: SEARCH_COURSE(params),
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });

  return dataSearch?.data.data;
}

async function fetchSearchAll(params?: string) {
  const user = storageCheck(USER);
  const dataSearch = await getData({
    endpoint: SEARCH_ALL(params),
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });

  let dataFilter = dataSearch?.data?.data?.filter((item: any) => {
    return !item.question;
  });

  let dataResult = {
    ...dataSearch?.data,
    data: dataFilter,
  }

  return dataResult;
}

export { fetchSearchNews, fetchSearchCourse, fetchSearchEvent, fetchSearchAll };
