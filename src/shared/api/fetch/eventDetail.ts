import { EVENT } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchDetailEvent(id?: string): Props {
  const KEY = ["eventDetail"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataEvent = await getData({
      endpoint: EVENT(`/id/${id}`),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`
        }
      }
    });
    console.log("dataEvent api", dataEvent?.data.data);
    

    return dataEvent?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchDetailEvent };
