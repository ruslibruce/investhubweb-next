import {
  CATEGORY_INVESTMENT,
  INVESTMENT_PARTNER,
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";
import { paramsToString } from "@/shared/utils/string";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchInvestment(params?: string): Props {
  const KEY = ["investment"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataInvestment = await getData({
      endpoint: INVESTMENT_PARTNER(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataInvestment?.data.data;
  };

  return { key: KEY, api: API };
}

function fetchInvestmentCategory(params?: string): Props {
  const KEY = ["category_investment"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataCategory = await getData({
      endpoint: CATEGORY_INVESTMENT(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    let dataTemp = [
      {
        key: 0,
        label: "Semua Kategori",
      },
    ];

    dataCategory?.data.data.map((item: any) => {
      dataTemp.push({
        key: item.id,
        label: item.name,
      });
    });

    return dataTemp;
  };

  return { key: KEY, api: API };
}

export { fetchInvestment, fetchInvestmentCategory };
