import { PREFERENCES } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData, putData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchPreferences(params?: string): Props {
  const KEY = ["preferences"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataPreferences = await getData({
      endpoint: PREFERENCES(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    dataPreferences?.data.data.map((item: any) => {
      item.choosen = item.check === 1 ? true : false;
    });

    return dataPreferences.data.data;
  };

  return { key: KEY, api: API };
}

const putUpdatePreferences = async (data: {}) => {
  const user = storageCheck(USER);
  const res = await putData({
    endpoint: PREFERENCES(),
    data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  
  return res?.data;
};

export { fetchPreferences, putUpdatePreferences };
