import { LEARNING_JOURNEY } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchGetLearningJourney(params?: string): Props {
  const KEY = ["learning-journey"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataLearningJourney = await getData({
      endpoint: LEARNING_JOURNEY(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataLearningJourney?.data;
  };

  return { key: KEY, api: API };
}

function fetchGetLearningJourneyId(id?: string): Props {
  const KEY = ["learning-journeyDetail"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataLearningJourney = await getData({
      endpoint: LEARNING_JOURNEY(),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });
    

    let dataJourney = {};

    dataLearningJourney?.data.data.map((item: any) => {
      if (item.id == id) {
        dataJourney = item;
      }
    });
    

    return dataJourney;
  };

  return { key: KEY, api: API };
}

export { fetchGetLearningJourney, fetchGetLearningJourneyId };
