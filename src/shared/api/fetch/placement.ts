import { PLACEMENT_TEST } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchPlacement(params?: string): Props {
  const KEY = ["placement"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataPlacement = await getData({
      endpoint: PLACEMENT_TEST(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`
        }
      }
    });

    return dataPlacement?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchPlacement };
