import { PRIVACYPOLICY } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchPrivacy(params?: string): Props {
  const KEY = ["privacy"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataPrivacy = await getData({
      endpoint: PRIVACYPOLICY(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataPrivacy?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchPrivacy };
