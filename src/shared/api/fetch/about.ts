import { ABOUT } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchAbout(params?: string): Props {
  const KEY = ["about"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataAbout = await getData({
      endpoint: ABOUT(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`
        }
      }
    });

    return dataAbout?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchAbout };
