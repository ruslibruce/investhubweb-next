import { DISCLAIMER } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchDisclaimer(params?: string): Props {
  const KEY = ["disclaimer"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataDisclaimer = await getData({
      endpoint: DISCLAIMER(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataDisclaimer?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchDisclaimer };
