import { TERMSCONDITION } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchTerms(params?: string): Props {
  const KEY = ["terms"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataTerms = await getData({
      endpoint: TERMSCONDITION(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataTerms?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchTerms };
