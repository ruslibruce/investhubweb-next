import { NEWS, NEWS_PORTAL } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchNews(params?: string): Props {
  const KEY = ["news"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataNews = await getData({
      endpoint: NEWS_PORTAL(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataNews?.data;
  };

  return { key: KEY, api: API };
}

function fetchDetailNews(id?: string): Props {
  const KEY = ["newsDetail"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataNews = await getData({
      endpoint: NEWS(`/id/${id}`),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataNews?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchNews, fetchDetailNews };
