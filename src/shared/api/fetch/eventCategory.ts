import {
  CATEGORY_EVENT
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchEventCategory(params?: string): Props {
  const KEY = ["category_event"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataCategory = await getData({
      endpoint: CATEGORY_EVENT(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`
        }
      }
    });

    let dataTemp = [
      {
        label: "All",
        key: "all",
      },
    ];

    dataCategory?.data.data.map((item: any) => {
      dataTemp.push({
        key: item.id,
        label: item.name,
      });
    });

    return dataTemp;
  };

  return { key: KEY, api: API };
}

export { fetchEventCategory };
