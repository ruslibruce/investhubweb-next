import {
  CATEGORY_SECTOR,
  CATEGORY_SUB_SECTOR,
  STOCK_SCREENER,
  STOCK_SCREENER_MIN_MAX,
  STOCK_SCREENER_PROFILE_COMPANY,
} from "@/shared/constants/endpoint";
import { getDataIDX } from "@/shared/utils/http/axiosHelperIDX";
import { paramsToString } from "@/shared/utils/string";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchStockScreener(params?: string): Props {
  const KEY = ["stock_screener"];

  const API = async () => {
    const dataStockScreener = await getDataIDX({
      endpoint: STOCK_SCREENER(params),
    });

    return dataStockScreener?.data.results;
  };

  return { key: KEY, api: API };
}

function fetchStockScreenerProfileCompany(params?: string): Props {
  const KEY = ["stock_screener_profile_company"];

  const API = async () => {
    const dataStockScreenerProfileCompany = await getDataIDX({
      endpoint: STOCK_SCREENER_PROFILE_COMPANY(
        paramsToString({
          KodeEmiten: params,
          language: "id-id",
        })
      ),
    });

    return dataStockScreenerProfileCompany?.data;
  };

  return { key: KEY, api: API };
}

async function fetchStockScreenerParams(data: any) {
  const dataStockScreenerParams = await getDataIDX({
    endpoint: STOCK_SCREENER(data),
  });
  return dataStockScreenerParams?.data.results;
}

function fetchStockScreenerCategorySector(): Props {
  const KEY = ["category_sector"];

  const API = async () => {
    const dataCategorySector = await getDataIDX({
      endpoint: CATEGORY_SECTOR(),
      config: {
        headers: {
          Authorization: `Bearer ${process.env.NEXT_PUBLIC_TOKEN_IDX}`,
        },
      },
    });
    return dataCategorySector?.data;
  };

  return { key: KEY, api: API };
}

async function fetchStockScreenerCategorySubSectorParams(params: string) {
  const dataCategorySector = await getDataIDX({
    endpoint: CATEGORY_SUB_SECTOR(params),
    config: {
      headers: {
        Authorization: `Bearer ${process.env.NEXT_PUBLIC_TOKEN_IDX}`,
      },
    },
  });
  return dataCategorySector?.data;
}

function fetchStockScreenerCategorySubSector(): Props {
  const KEY = ["category_sub_sector"];

  const API = async () => {
    const dataCategorySubSector = await getDataIDX({
      endpoint: CATEGORY_SUB_SECTOR(),
      config: {
        headers: {
          Authorization: `Bearer ${process.env.NEXT_PUBLIC_TOKEN_IDX}`,
        },
      },
    });
    return dataCategorySubSector?.data;
  };

  return { key: KEY, api: API };
}

function fetchStockScreenerMinMax(): Props {
  const KEY = ["filter_min_max"];

  const API = async () => {
    const dataFilterMinMax = await getDataIDX({
      endpoint: STOCK_SCREENER_MIN_MAX(),
      config: {
        headers: {
          Authorization: `Bearer ${process.env.NEXT_PUBLIC_TOKEN_IDX}`,
        },
      },
    });
    return dataFilterMinMax?.data;
  };

  return { key: KEY, api: API };
}

export {
  fetchStockScreener,
  fetchStockScreenerProfileCompany,
  fetchStockScreenerParams,
  fetchStockScreenerCategorySector,
  fetchStockScreenerCategorySubSectorParams,
  fetchStockScreenerCategorySubSector,
  fetchStockScreenerMinMax,
};
