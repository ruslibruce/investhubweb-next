import axios, { CreateAxiosDefaults } from 'axios';
import type { AxiosInstance } from 'axios'; // Add type-only import for AxiosInstance
import { BASE_URL } from '../constants/endpoint';

const AxiosInstanceConfig: CreateAxiosDefaults = {
  timeout: 20000,
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  },
  baseURL: BASE_URL
};

const AxiosInstance: AxiosInstance = axios.create(AxiosInstanceConfig);

export default AxiosInstance;