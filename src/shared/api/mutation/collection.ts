import { COLLECTION, COLLECTION_CONTENT } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData, postData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function getCollection(params?: string): Props {
  const KEY = ["collection"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataNews = await getData({
      endpoint: COLLECTION(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataNews?.data.data;
  };

  return { key: KEY, api: API };
}

function getCollectionContent(params?: string): Props {
  const KEY = ["collection_content"];
  const user = storageCheck(USER);
  console.log('params?:',params);
  

  const API = async () => {
    const dataNews = await getData({
      endpoint: COLLECTION_CONTENT(`/id/${params}`),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataNews?.data.data;
  };

  return { key: KEY, api: API };
}

const addCollection = async (data: {}) => {
  const user = storageCheck(USER);
  const res = await postData({
    endpoint: COLLECTION(),
    data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data.data;
};

const addContentCollection = async (data: {}) => {
  const user = storageCheck(USER);
  const res = await postData({
    endpoint: COLLECTION_CONTENT(),
    data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data.data;
};

export {
  getCollection,
  getCollectionContent,
  addCollection,
  addContentCollection,
};
