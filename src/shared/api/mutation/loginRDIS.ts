import { LOGIN_RDIS } from '@/shared/constants/endpoint';
import { getData } from '@/shared/utils/http/axiosHelper';

const loginRDIS = async (params: any) => {
  console.log('params', params.code);
  
  const res = await getData({
    endpoint: `${LOGIN_RDIS}/callback?code=${encodeURIComponent(params.code)}`,
    config: {
      headers: {
        'Accept': 'application/json',
      },
    },
  });
  return res?.data;
};

export { loginRDIS };
