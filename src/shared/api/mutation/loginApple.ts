import { LOGIN_APPLE, LOGIN_GOOGLE } from "@/shared/constants/endpoint";
import { getData } from "@/shared/utils/http/axiosHelper";

const loginApple = async (code: string) => {
  console.log("params", code);

  const res = await getData({
    endpoint: `${LOGIN_APPLE}/callback?code=${encodeURIComponent(code)}`,
    config: {
      headers: {
        Accept: "application/json",
      },
    },
  });
  return res?.data;
};

export { loginApple };
