import {
  LEVEL_UP_QUIZ,
  LEVEL_UP_RETAKE,
  LEVEL_UP_SAVE,
  LEVEL_UP_SUBMIT,
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData, postData } from "@/shared/utils/http/axiosHelper";

const getQuizLevelup = async (id: string) => {
  const user = storageCheck(USER);
  const res = await getData({
    endpoint: LEVEL_UP_QUIZ(`${id}`),
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data.data;
};

const postQuizLevelupSubmit = async (data: {}) => {
  const user = storageCheck(USER);
  const res = await postData({
    endpoint: LEVEL_UP_SUBMIT(),
    data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data.data;
};

const postSaveQuizLevelup = async (data: {}) => {
  const user = storageCheck(USER);
  const res = await postData({
    endpoint: LEVEL_UP_SAVE(),
    data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data.data;
};

const postRetakeQuizLevelup = async (data: {}) => {
  const user = storageCheck(USER);
  const res = await postData({
    endpoint: LEVEL_UP_RETAKE(),
    data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data.data;
};

export {
  getQuizLevelup,
  postQuizLevelupSubmit,
  postSaveQuizLevelup,
  postRetakeQuizLevelup,
};
