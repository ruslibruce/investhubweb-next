import { LOGIN_IDX } from '@/shared/constants/endpoint';
import { getData } from '@/shared/utils/http/axiosHelper';

const loginIDX = async (params: any) => {
  console.log('params', params.token);
  
  const res = await getData({
    endpoint: `${LOGIN_IDX}/callback?token=${encodeURIComponent(params.token)}`,
    config: {
      headers: {
        'Accept': 'application/json',
      },
    },
  });
  return res?.data;
};

export { loginIDX };
