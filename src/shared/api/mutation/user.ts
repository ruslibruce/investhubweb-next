import { COUNTRY, FETCH_USERS, PROFILE } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { deleteData, getData, postData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

const saveUser = async (data: any) => {
  console.log("cekkkk data", data);

  const user = storageCheck(USER);
  const res = await postData({
    endpoint: FETCH_USERS(),
    data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data;
};

function getCountry(params?: string): Props {
  const KEY = ["country"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataCountry = await getData({
      endpoint: COUNTRY(),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    dataCountry?.data.data.map((item: any) => {
      item.key = item.id;
      item.value = item.name;
      item.label = item.name;
    });

    return dataCountry?.data.data;
  };

  return { key: KEY, api: API };
}

function getProfileInfo(params?: string): Props {
  const KEY = ["profile"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataProfile = await getData({
      endpoint: PROFILE(),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataProfile?.data.data;
  };

  return { key: KEY, api: API };
}

const getProfileInfoMutation = async (data: any) => {
  const dataUser = storageCheck(USER);
  const dataProfile = await getData({
    endpoint: PROFILE(),
    config: {
      headers: {
        Authorization: `Bearer ${data.token}`,
      },
    },
  });

  return dataProfile?.data.data;
};

export {
  getCountry,
  getProfileInfo,
  getProfileInfoMutation,
  saveUser,
};
