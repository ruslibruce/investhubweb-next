import {
  CHANGE_EMAIL
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  postData
} from "@/shared/utils/http/axiosHelper";

const changeMail = async (data: any) => {
  const user = storageCheck(USER);
  
  const res = await postData({
    endpoint: CHANGE_EMAIL(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data;
};

export { changeMail };
