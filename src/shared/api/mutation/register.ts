import { REGISTER } from '@/shared/constants/endpoint';
import { postData } from '@/shared/utils/http/axiosHelper';

const register = async (data: any) => {
  const res = await postData({
    endpoint: REGISTER,
    data,
  });
  return res?.data;
};

export { register };
