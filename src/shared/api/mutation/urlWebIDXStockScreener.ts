import {
  URL_WEBIDX_STOCK_SCREENER
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  getData
} from "@/shared/utils/http/axiosHelper";

const getUrlIDXStockScreener = async (data: any) => {
  const user = storageCheck(USER);
  
  const res = await getData({
    endpoint: URL_WEBIDX_STOCK_SCREENER(),
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data;
};

export { getUrlIDXStockScreener };
