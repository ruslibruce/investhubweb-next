import { LOGIN_TICMIEDU } from "@/shared/constants/endpoint";
import { getData } from "@/shared/utils/http/axiosHelper";

const loginTICMI = async (params: any) => {
  console.log("params", params);

  const res = await getData({
    endpoint: `${LOGIN_TICMIEDU}/callback?jwt_token=${encodeURIComponent(
      params.jwt_token
    )}`,
    config: {
      headers: {
        Accept: "application/json",
      },
    },
  });
  return res?.data;
};

export { loginTICMI };
