import { LOGIN_GOOGLE } from '@/shared/constants/endpoint';
import { getData } from '@/shared/utils/http/axiosHelper';

const loginGoogle = async (params: any) => {
  console.log('params', params.code);
  
  const res = await getData({
    endpoint: `${LOGIN_GOOGLE}/callback?code=${encodeURIComponent(params.code)}&scope=${params.scope}&authuser=${params.authuser}&prompt=${params.prompt}`,
    config: {
      headers: {
        'Accept': 'application/json',
      },
    },
  });
  return res?.data;
};

export { loginGoogle };
