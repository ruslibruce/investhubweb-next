import MetaHead from "@/components/MetaHead";
import FaqPage from "@/features/faq/ui/FaqPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function Faq() {
  return (
    <>
      <MetaHead
        title={"FAQ Page"}
        description={"Welcome to FAQ page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <FaqPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
