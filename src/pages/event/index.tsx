import MetaHead from "@/components/MetaHead";
import EventPage from "@/features/event/ui/EventPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function Event() {
  return (
    <>
      <MetaHead
        title={"Event Page"}
        description={"Welcome to Event page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <EventPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
