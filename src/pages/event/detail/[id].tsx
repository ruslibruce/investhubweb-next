import MetaHead from "@/components/MetaHead";
import EventDetailPage from "@/features/eventdetail/EventDetailPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function EventDetail() {
  return (
    <>
      <MetaHead
        title={"Detail Event Page"}
        description={"Welcome to Detail Event page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <EventDetailPage />
    </>
  );
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
