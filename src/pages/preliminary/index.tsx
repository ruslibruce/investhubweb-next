import MetaHead from "@/components/MetaHead";
import PreliminaryPage from "@/features/preliminary/ui/PreliminaryPage";
import UpdateProfileScreen from "@/features/updateprofile/ui/UpdateProfilePage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function UpdateProfile() {
  return (
    <>
      <MetaHead
        title={"Preliminary Page"}
        description={"Welcome to Preliminary page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <PreliminaryPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
