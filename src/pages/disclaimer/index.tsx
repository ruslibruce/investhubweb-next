import MetaHead from "@/components/MetaHead";
import DisclaimerPage from "@/features/disclaimer/ui/DisclaimerPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function Disclaimer() {
  return (
    <>
      <MetaHead
        title={"Disclaimer Page"}
        description={"Welcome to Disclaimer page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <DisclaimerPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
