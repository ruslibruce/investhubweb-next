import MetaHead from "@/components/MetaHead";
import CourseDetailProgressPage from "@/features/coursedetailprogress/ui/CourseDetailProgressPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function ProgressCourse() {
  return (
    <>
      <MetaHead
        title={"Detail Course Page"}
        description={"Welcome to Detail Course page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <CourseDetailProgressPage />
    </>
  );
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
