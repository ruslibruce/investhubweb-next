import MetaHead from "@/components/MetaHead";
import FormRegister from "@/features/register/ui/FormRegister";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function Register() {
  return (
    <>
      <MetaHead
        title={"Register Page"}
        description={"Welcome to register page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <FormRegister />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
