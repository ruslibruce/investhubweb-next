import MetaHead from "@/components/MetaHead";
import EmailConfirm from "@/features/emailconfirm/EmailConfirm";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function EmailConfirmScreen() {
  return (
    <>
      <MetaHead
        title={"Email Confirm Page"}
        description={"Welcome to email confirm page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <EmailConfirm />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
