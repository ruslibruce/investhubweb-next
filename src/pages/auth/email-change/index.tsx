import MetaHead from "@/components/MetaHead";
import EmailChange from "@/features/emailchange/EmailChange";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function EmailChangeScreen() {
  return (
    <>
      <MetaHead
        title={"Email Change Page"}
        description={"Welcome to email change page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <EmailChange />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
