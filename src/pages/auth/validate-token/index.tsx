import MetaHead from "@/components/MetaHead";
import ValidateTokenForm from "@/features/validatetokenform/ValidateTokenForm";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function ValidateTokenFormScreen() {
  return (
    <>
      <MetaHead
        title={"Validate Token Page"}
        description={"Welcome to Validate Token Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <ValidateTokenForm />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
