import MetaHead from "@/components/MetaHead";
import ForgotPassword from "@/features/forgotpassword/ForgotPassword";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function ForgotPasswordScreen() {
  return (
    <>
      <MetaHead
        title={"Forgot Password Page"}
        description={"Welcome to forgot password page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <ForgotPassword />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
