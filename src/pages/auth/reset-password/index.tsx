import MetaHead from "@/components/MetaHead";
import ResetPasswordForm from "@/features/forgotpasswordform/ResetPasswordForm";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function ResetPasswordFormScreen() {
  return (
    <>
      <MetaHead
        title={"Reset Password Page"}
        description={"Welcome to Reset Password Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <ResetPasswordForm />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
