import { loginApple } from "@/shared/api/mutation/loginApple";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useRouter } from "next/router";

const useLoginApple = () => {
  const navigate = useRouter();

  const mutationQuery = useMutationHook({
    api: loginApple,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
      },
    },
  });

  const handleOnSubmit = (code: string) => {
    mutationQuery.mutate(code);
  };

  return {
    mutationQuery,
    handleOnSubmit,
  };
};

export default useLoginApple;
