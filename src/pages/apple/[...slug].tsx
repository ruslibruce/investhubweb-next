import LoaderSpinGif from "@/components/LoaderSpinGif";
import useIsMobile from "@/components/useIsMobile";
import useGetProfileMutate from "@/shared/components/layout/dashboard-layout/hooks/useGetProfileMutate";
import { URL_CALLBACK_MOBILE } from "@/shared/constants/endpoint";
import { Flex } from "antd";
import { jwtDecode } from "jwt-decode";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import React from "react";
import useLoginApple from "./hooks/useLoginApple";
import { parseBody } from "@/shared/utils/parseBody";
import { NextApiRequest } from "next";

type CallbackProps = {
  code: string;
};

export default function AppleCallback({ code }: CallbackProps) {
  const navigate = useRouter();
  const { handleOnSubmit, mutationQuery } = useLoginApple();
  const { data: dataLogin } = mutationQuery;
  const { handleGetProfile } = useGetProfileMutate();
  const { resultMobile } = useIsMobile();

  React.useEffect(() => {
    if (dataLogin) {
      let resultData: any = dataLogin;
      const decodedToken: any = jwtDecode(resultData.data.authorisation.token);
      handleGetProfile({
        token: resultData.data.authorisation.token,
        user: {
          ...decodedToken.user,
          is_email_verified: true,
        },
      });
    }
  }, [dataLogin]);

  React.useEffect(() => {
    console.log("navigate.query.code", navigate.query.code);
    if (navigate.query.code && resultMobile) {
      handleCheckMobile(resultMobile, navigate.query.code as string);
    }
  }, [navigate]);

  React.useEffect(() => {
    console.log("code apple in page =======>", code);
    if (resultMobile && code) {
      handleCheckMobile(resultMobile, code);
    }
  }, [resultMobile, code]);

  const handleCheckMobile = async (resultMobile: any, code: string) => {
    await new Promise((resolve) => setTimeout(resolve, 2000));
    console.log("resultMobile", resultMobile);
    // if (resultMobile === "mobile") {
    //   window.location.href = `${URL_CALLBACK_MOBILE}/apple/callback?code=${code}`;
    //   return;
    // }
    console.log("masuk webbb");
    handleOnSubmit(code);
  };

  return (
    <Flex
      style={{
        display: "block",
        boxSizing: "border-box",
        height: "100vh",
        width: "100vw",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <LoaderSpinGif size="large" isFullScreen={true} />
    </Flex>
  );
}

export async function getServerSideProps({
  locale,
  req,
}: {
  locale: string;
  req: NextApiRequest;
}) {
  if (req.method === "POST") {
    const body = await parseBody(req);
    const { code }: any = body;
    console.log("code apple getServerSideProps =======>", code);
    return {
      props: {
        ...(await serverSideTranslations(locale, ["common"])),
        code: code || "",
      },
    };
  }
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
