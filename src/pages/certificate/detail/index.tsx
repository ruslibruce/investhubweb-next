import MetaHead from "@/components/MetaHead";
import CollectionDetailPage from "@/features/collectiondetail/CollectionDetailPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function CourseDetail() {
  return (
    <>
      <MetaHead
        title={"Detail Collection Page"}
        description={"Welcome to Detail Collection page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <CollectionDetailPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
