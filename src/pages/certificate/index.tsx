import MetaHead from "@/components/MetaHead";
import CertificatePage from "@/features/certificate/CertificatePage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function CertificateMain() {
  return (
    <>
      <MetaHead
        title={"Certificate Page"}
        description={"Welcome to Certificate page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <CertificatePage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
