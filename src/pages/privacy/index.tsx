import MetaHead from "@/components/MetaHead";
import PrivacyPage from "@/features/privacy/ui/PrivacyPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function Privacy() {
  return (
    <>
      <MetaHead
        title={"Privacy Policy Page"}
        description={"Welcome to Privacy Policy page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <PrivacyPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
