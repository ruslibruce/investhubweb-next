import MetaHead from "@/components/MetaHead";
import ProfilePage from "@/features/profile/ui/ProfilePage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function UpdateProfile() {
  return (
    <>
      <MetaHead
        title={"My Profile Page"}
        description={"Welcome to My profile page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <ProfilePage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
