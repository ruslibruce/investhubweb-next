import MetaHead from "@/components/MetaHead";
import UpdateProfilePage from "@/features/updateprofile/ui/UpdateProfilePage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function ProfileUpdate() {
  return (
    <>
      <MetaHead
        title={"Update Profile Page"}
        description={"Welcome to update profile page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <UpdateProfilePage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
