import MetaHead from "@/components/MetaHead";
import ForumPage from "@/features/forumscreen/ForumPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function Forum() {
  return (
    <>
      <MetaHead
        title={"Forum Page"}
        description={"Welcome to Forum page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <ForumPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
