import MetaHead from "@/components/MetaHead";
import AboutPage from "@/features/about/ui/AboutPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function About() {
  return (
    <>
      <MetaHead
        title={"About Page"}
        description={"Welcome to About page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <AboutPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
