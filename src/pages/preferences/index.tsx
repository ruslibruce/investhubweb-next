import MetaHead from "@/components/MetaHead";
import PreferencesPage from "@/features/preferences/ui/PreferencesPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function UpdateProfile() {
  return (
    <>
      <MetaHead
        title={"Preferences Page"}
        description={"Welcome to Preferences page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <PreferencesPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
