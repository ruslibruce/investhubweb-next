import MetaHead from "@/components/MetaHead";
import LevelUpPage from "@/features/levelupsession/ui/LevelUpPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function LevelUp() {
  return (
    <>
      <MetaHead
        title={"Level Up Test Page"}
        description={"Welcome to Level Up Test page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <LevelUpPage />
    </>
  );
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
