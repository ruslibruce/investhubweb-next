import MetaHead from "@/components/MetaHead";
import ClassSchedulePage from "@/features/classschedule/ClassSchedulePage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function ClassSchedule() {
  return (
    <>
      <MetaHead
        title={"Class Schedule Page"}
        description={"Welcome to Class Schedule page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <ClassSchedulePage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
