import MetaHead from "@/components/MetaHead";
import StockScreenerDetailPage from "@/features/stockscreenerdetail/ui/StockScreenerDetailPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function StockScreenerDetail() {
  return (
    <>
      <MetaHead
        title={"StockScreener Detail Page"}
        description={"Welcome to StockScreener Detail page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <StockScreenerDetailPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
