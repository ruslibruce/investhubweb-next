import MetaHead from "@/components/MetaHead";
import StockScreenerPage from "@/features/stockscreener/ui/StockScreenerPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function StockScreener() {
  return (
    <>
      <MetaHead
        title={"StockScreener Page"}
        description={"Welcome to StockScreener page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <StockScreenerPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
