import MetaHead from "@/components/MetaHead";
import InvestmentPartnerPage from "@/features/investment/ui/InvestmentPartnerPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function Investment() {
  return (
    <>
      <MetaHead
        title={"Investment Partner Page"}
        description={"Welcome to Investment Partner page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <InvestmentPartnerPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
