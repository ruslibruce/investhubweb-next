import { loginGoogle } from "@/shared/api/mutation/loginGoogle";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useRouter } from "next/router";

const useLoginGoogle = () => {
  const navigate = useRouter();

  const mutationQuery = useMutationHook({
    api: loginGoogle,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
      },
    },
  });

  const handleOnSubmit = (value: {}) => {
    mutationQuery.mutate(value);
  };

  return {
    mutationQuery,
    handleOnSubmit,
  };
};

export default useLoginGoogle;
