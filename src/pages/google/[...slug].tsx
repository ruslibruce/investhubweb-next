import LoaderSpinGif from "@/components/LoaderSpinGif";
import useGetProfileMutate from "@/shared/components/layout/dashboard-layout/hooks/useGetProfileMutate";
import { Flex } from "antd";
import { jwtDecode } from "jwt-decode";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import React from "react";
import useLoginGoogle from "./hooks/useLoginGoogle";

export default function GoogleCallback() {
  const navigate = useRouter();
  const { slug } = navigate.query;
  const { handleOnSubmit, mutationQuery } = useLoginGoogle();
  const { data: dataLogin } = mutationQuery;
  const { handleGetProfile } = useGetProfileMutate();

  React.useEffect(() => {
    if (dataLogin) {
      let resultData: any = dataLogin;
      const decodedToken: any = jwtDecode(resultData.data.authorisation.token);
      handleGetProfile({
        token: resultData.data.authorisation.token,
        user: {
          ...decodedToken.user,
          is_email_verified: true,
        },
      });
    }
  }, [dataLogin]);

  React.useEffect(() => {
    if (slug) {
      handleOnSubmit(navigate.query);
    }
  }, [slug]);

  return (
    <Flex
      style={{
        display: "block",
        boxSizing: "border-box",
        height: "100vh",
        width: "100vw",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <LoaderSpinGif size="large" isFullScreen={true} />
    </Flex>
  );
}

export async function getServerSideProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
