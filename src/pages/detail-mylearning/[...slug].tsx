import MetaHead from "@/components/MetaHead";
import LearningJourneyDetailPage from "@/features/learningjourneydetail/LearningJourneyDetailPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function DetailMyLearningPage() {
  return (
    <>
      <MetaHead
        title={"Detail My Learning Page"}
        description={"Welcome to Detail My Learning page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <LearningJourneyDetailPage />
    </>
  );
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
