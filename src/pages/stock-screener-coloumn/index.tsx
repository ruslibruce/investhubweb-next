import MetaHead from "@/components/MetaHead";
import StockScreenerColoumnPage from "@/features/stockscreenercoloumn/ui/StockScreenerColoumnPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function StockScreenerColoumn() {
  return (
    <>
      <MetaHead
        title={"Stock Screener Coloumn Page"}
        description={"Welcome to Stock Screener Coloumn page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <StockScreenerColoumnPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
