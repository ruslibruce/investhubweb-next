import MetaHead from "@/components/MetaHead";
import NewsDetailPage from "@/features/newsdetail/NewsDetailPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function NewsDetail() {
  return (
    <>
      <MetaHead
        title={"Detail News Page"}
        description={"Welcome to Detail News page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <NewsDetailPage />
    </>
  );
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
