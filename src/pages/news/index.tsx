import MetaHead from "@/components/MetaHead";
import NewsPage from "@/features/news/ui/NewsPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function News() {
  return (
    <>
      <MetaHead
        title={"News Page"}
        description={"Welcome to News page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <NewsPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
