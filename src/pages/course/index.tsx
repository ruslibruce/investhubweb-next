import MetaHead from "@/components/MetaHead";
import CoursePage from "@/features/course/CoursePage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function Course() {
  return (
    <>
      <MetaHead
        title={"Course Page"}
        description={"Welcome to Course page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <CoursePage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
