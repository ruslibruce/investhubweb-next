import { loginRDIS } from "@/shared/api/mutation/loginRDIS";
import useMutationHook from "@/shared/hooks/useMutationHook";

const useLoginRDIS = () => {
  const mutationQuery = useMutationHook({
    api: loginRDIS,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
      },
    },
  });

  const handleOnSubmit = (value: {}) => {
    mutationQuery.mutate(value);
  };

  return {
    mutationQuery,
    handleOnSubmit,
  };
};

export default useLoginRDIS;
