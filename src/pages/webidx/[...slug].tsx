import LoaderSpinGif from "@/components/LoaderSpinGif";
import useIsMobile from "@/components/useIsMobile";
import useGetProfileMutate from "@/shared/components/layout/dashboard-layout/hooks/useGetProfileMutate";
import { URL_CALLBACK_MOBILE } from "@/shared/constants/endpoint";
import { Flex } from "antd";
import { jwtDecode } from "jwt-decode";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import React from "react";
import useLoginIDX from "./hooks/useLoginIDX";

export default function WebIDXCallback() {
  const navigate = useRouter();
  const { token } = navigate.query;
  const { handleOnSubmit, mutationQuery } = useLoginIDX();
  const { data: dataLogin } = mutationQuery;
  const { handleGetProfile } = useGetProfileMutate();
  const { resultMobile } = useIsMobile();

  React.useEffect(() => {
    if (dataLogin) {
      let resultData: any = dataLogin;
      const decodedToken: any = jwtDecode(resultData.data.authorisation.token);
      handleGetProfile({
        token: resultData.data.authorisation.token,
        user: {
          ...decodedToken.user,
          is_email_verified: true,
        },
      });
    }
  }, [dataLogin]);

  React.useEffect(() => {
    if (resultMobile) {
      handleCheckMobile(resultMobile);
    }
  }, [resultMobile]);

  const handleCheckMobile = async (resultMobile: any) => {
    await new Promise((resolve) => setTimeout(resolve, 2000));
    console.log("resultMobile", resultMobile);
    if (resultMobile === "mobile") {
      window.location.href = `${URL_CALLBACK_MOBILE}/webidx/callback?token=${token}`;
      return;
    }
    console.log("masuk webbb");
    handleOnSubmit(navigate.query);
  };

  return (
    <Flex
      style={{
        display: "block",
        boxSizing: "border-box",
        height: "100vh",
        width: "100vw",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <LoaderSpinGif size="large" isFullScreen={true} />
    </Flex>
  );
}

export async function getServerSideProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
