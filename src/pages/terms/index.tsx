import MetaHead from "@/components/MetaHead";
import TermsPage from "@/features/termofuse/ui/TermsPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function Privacy() {
  return (
    <>
      <MetaHead
        title={"Terms Page"}
        description={"Welcome to Terms page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <TermsPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
