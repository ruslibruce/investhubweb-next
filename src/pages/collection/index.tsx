import MetaHead from "@/components/MetaHead";
import CollectionPage from "@/features/collection/CollectionPage";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function Course() {
  return (
    <>
      <MetaHead
        title={"Collection Page"}
        description={"Welcome to Collection page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
      />
      <CollectionPage />
    </>
  );
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
