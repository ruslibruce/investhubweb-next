import LoaderSpinGif from "@/components/LoaderSpinGif";
import useLogout from "@/shared/components/layout/dashboard-layout/hooks/useLogout";
import { DASHBOARD_HOME } from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck, storageSet } from "@/shared/utils/clientStorageUtils";
import { Flex } from "antd";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import React from "react";

export default function Page() {
  const navigate = useRouter();
  const { handleLogout, mutationQuery } = useLogout();
  const { data: dataLogout, error } = mutationQuery;
  const dataUser = storageCheck(USER);

  React.useEffect(() => {
    if (dataLogout) {
      navigate.replace(DASHBOARD_HOME);
    }

    if (error) {
      let dataError: any = error;
      if (dataError.response.status === 401) {
        let temp = {
          loading: true,
          isLogin: false,
          user: null,
          initialRoute: "Home",
          token: null,
          fcmToken: null,
        };
        storageSet(USER, temp);
        navigate.replace(DASHBOARD_HOME);
      }
    }
  }, [dataLogout, error]);

  React.useEffect(() => {
    console.log("dataUser", dataUser);
    if (!dataUser.token) {
      navigate.replace(DASHBOARD_HOME);
      return;
    }
    handleLogout();
  }, []);

  return (
    <Flex
      style={{
        display: "block",
        boxSizing: "border-box",
        height: "100vh",
        width: "100vw",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <LoaderSpinGif size="large" isFullScreen={true} />
    </Flex>
  );
}

export async function getServerSideProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
