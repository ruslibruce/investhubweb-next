import LogoIDX from "@/shared/images/logo/logo_avatar_wa.webp";
import { FloatingWhatsApp } from "react-floating-whatsapp";
import { useTranslation } from "next-i18next";

export default function FloatWhatsapp() {
  const { t: translate } = useTranslation();
  return (
    <div aria-hidden="true" style={{ width: "45px", height: "45px" }}>
      <FloatingWhatsApp
        statusMessage={translate("StatusFloatWA")}
        chatMessage={translate("WelcomeMessage")}
        placeholder={translate("TypeHere")}
        allowClickAway={true}
        avatar={LogoIDX.src}
        phoneNumber={"+6281181150515"}
        accountName={"IDX"}
        notification={false}
        // buttonStyle={{ width: "45px", height: "45px" }}
      />
    </div>
  );
}
