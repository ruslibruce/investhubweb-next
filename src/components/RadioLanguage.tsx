import { Button, Col, Row } from "antd";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";

const optionsWithDisabled = [
  { label: "EN", value: "en" },
  { label: "ID", value: "id" },
];

const RadioGroupPage = ({ classNameView }: { classNameView: string }) => {
  const [isSmallScreen, setIsSmallScreen] = useState(false);
  const navigate = useRouter();
  const { asPath } = navigate;
  

  React.useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth <= 450);
    };

    // Pertama kali, panggil handleResize untuk mengatur isSmallScreen
    handleResize();

    // Tambahkan event listener untuk memantau perubahan ukuran layar
    window.addEventListener("resize", handleResize);

    // Clean up event listener saat komponen dibongkar
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []); // Dependensi kosong agar efek hanya berjalan sekali saat komponen dipasang

  return (
    <Row>
      {optionsWithDisabled.map((option, index) => {
        const buttonStyle = {
          padding: "12px 9px",
          backgroundColor:
            navigate.locale === option.value ? "#e8e8e8" : "#fff",
          color: navigate.locale === option.value ? "#9F0E0F" : "#000",
          border: "1px solid #e8e8e8",
          borderRadius: index === 0 ? "4px 0 0 4px" : "0 4px 4px 0",
          cursor: "pointer",
          ...(isSmallScreen && {
            padding: "7px 4px",
            // Tambahkan penyesuaian gaya khusus untuk layar kecil di sini
          }),
        };

        return (
          <Col key={option.value}>
            <Link href={asPath} locale={option.value}>
              <Button className={classNameView} style={buttonStyle}>
                {option.label}
              </Button>
            </Link>
          </Col>
        );
      })}
    </Row>
  );
};

export default RadioGroupPage;
