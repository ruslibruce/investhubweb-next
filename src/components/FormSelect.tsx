import { CustomFormItem } from "@/shared/components/Form/CustomForm";
import { DownOutlined } from "@ant-design/icons";
import { Select } from "antd";
import { CSSProperties } from "styled-components";

type PropsForm = {
  styleForm?: CSSProperties;
  title?: string;
  placeholder: string;
  input?: [key: string];
  handleChange?: (e: any) => void;
  items: any[];
  //   onClick?: (e: React.MouseEvent<HTMLInputElement>) => void;
  value?: string;
  buttonStyle?: string;
  styleSelect?: React.CSSProperties;
  showSearch?: boolean;
  isRequired?: boolean;
};

export default function FormSelect({
  styleForm,
  title,
  placeholder,
  handleChange,
  value,
  buttonStyle,
  items,
  styleSelect,
  showSearch = true,
  isRequired=true,
}: PropsForm) {
  const labelRender = (props: any) => {
    const { label, value } = props;
    return <span style={{ paddingLeft: 10 }}>{value}</span>;
  };
  return (
    <CustomFormItem style={styleForm}>
      <label className={isRequired ? "required" : ""}>{title}</label>
      <Select
        className={`${buttonStyle} form-button__button__select`}
        placeholder={<span style={{ paddingLeft: 10 }}>{placeholder}</span>}
        onChange={handleChange}
        showSearch={showSearch}
        options={items}
        labelRender={labelRender}
        suffixIcon={<DownOutlined size={5} color="#000000" />}
        value={value}
        style={styleSelect}
      />
    </CustomFormItem>
  );
}
