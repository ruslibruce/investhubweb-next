import iconEmpty from "@/shared/images/emptyData/icon_empty_table.webp";
import { Col, Row, Spin } from "antd";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import LoaderSpinGif from "./LoaderSpinGif";

function NoData({ loading, span }: { loading?: boolean; span?: number }) {
  const { t: translate } = useTranslation();
  return (
    <Row justify={"center"}>
      <Col
        style={{
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
          flexDirection: "column",
        }}
        span={span ? span : 24}
      >
        {loading ? (
          <LoaderSpinGif size="large" />
        ) : (
          <>
            <Image src={iconEmpty} alt="empty-data" />
            <div style={{ height: 10 }} />
            <span className="textfs32-fw600-red">
              {translate("SorryNoData")}
            </span>
            <div style={{ height: 30 }} />
          </>
        )}
      </Col>
    </Row>
  );
}

export default NoData;
