import { DASHBOARD_NEWS_DETAIL } from "@/shared/constants/path";
import content2 from "@/shared/images/news/content2.jpg";
import { Flex, Spin } from "antd";
import moment from "moment";
import Image from "next/image";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import IconDot from "@/shared/images/svg/icon_dot.svg";
import SwiperCore from "swiper";
import "swiper/css";
import { Pagination } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import LoaderSpinGif from "./LoaderSpinGif";
import { RightOutlined } from "@ant-design/icons";

SwiperCore.use([Pagination]);

type NewsCarouselProps = {
  dataNews: {}[];
  isLoading: boolean;
};

const NewsCarousel = ({ dataNews, isLoading }: NewsCarouselProps) => {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const handleNews = (event: React.MouseEvent<HTMLElement>, value: any) => {
    navigate.push(`${DASHBOARD_NEWS_DETAIL}/${value.id}`);
  };
  if (!dataNews) return <LoaderSpinGif size="large" />;
  if (dataNews?.length === 0) return null;
  return (
    <Swiper
      slidesPerView={1}
      freeMode={true}
      pagination={{ dynamicBullets: true }}
      scrollbar={{ draggable: true }}
      navigation
    >
      {dataNews?.length > 0 &&
        dataNews?.map((newsItem: any, index) => (
          <SwiperSlide key={newsItem.id}>
            <div className="news-container-content-carousel">
              <div className="news-head-content">
                {newsItem.source_id ? (
                  <img
                    src={newsItem.cover_image}
                    alt="image-news"
                    width={30}
                    height={30}
                    style={{
                      objectFit: "cover",
                      borderRadius: "100px",
                    }}
                  />
                ) : (
                  <Image
                    src={newsItem.cover_image}
                    alt="image-news"
                    width={30}
                    height={30}
                    style={{
                      objectFit: "cover",
                      borderRadius: "100px",
                    }}
                  />
                )}
                <Image src={IconDot} alt="icon-dot" style={{ marginLeft: 5 }} />
                <h4 style={{ marginLeft: 5 }}>
                  {moment(newsItem.publish_time).format("DD MMM YYYY")}
                </h4>
                <Image src={IconDot} alt="icon-dot" />
              </div>
              <div className="news-main-content">
                <div className="news-main-title">
                  <Image
                    src={content2}
                    alt="Hotel"
                    width={60}
                    height={60}
                    style={{
                      objectFit: "cover",
                      borderRadius: "10px",
                    }}
                  />
                  <h3 dangerouslySetInnerHTML={{ __html: newsItem?.subject }} />
                </div>
                <div
                  className="news-main-text"
                  dangerouslySetInnerHTML={{ __html: newsItem?.body }}
                />
              </div>
              <Flex
                onClick={(e) => handleNews(e, newsItem)}
                style={{ cursor: "pointer" }}
                justify="end"
                className="news-footer-content"
              >
                <h4 className="news-see-detail">
                  {`${translate("SeeDetail")}`}
                  <RightOutlined style={{ color: "#9F0E0F" }} />
                </h4>
              </Flex>
            </div>
          </SwiperSlide>
        ))}
    </Swiper>
  );
};

export default NewsCarousel;
