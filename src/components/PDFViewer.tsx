// components/PdfViewer.jsx
import usePostProgress from "@/features/coursedetail/hooks/usePostProgress";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { Viewer, Worker } from "@react-pdf-viewer/core";
import "@react-pdf-viewer/core/lib/styles/index.css";
import { Col } from "antd";
import { useCallback } from "react";

type PdfViewerProps = {
  url: string;
  content?: any;
  idProgress?: string;
};
const PDFViewer = ({ url, content, idProgress }: PdfViewerProps) => {
  const dataUser = storageCheck(USER);
  const { handlePostProgress } = usePostProgress();
  console.log("url PDF", url);
  console.log("content", content);

  const handleAccesssProgress = useCallback(
    (currentPage: number) => {
      if (!dataUser.isLogin) {
        return;
      }

      if (!idProgress && !content) {
        return;
      }

      handlePostProgress({
        participant_id: idProgress,
        content_id: content?.id,
        position: currentPage,
      });
    },
    [idProgress, content, dataUser, handlePostProgress]
  );

  return (
    <Col
      span={24}
      style={{
        border: "1px solid rgba(0, 0, 0, 0.3)",
        height: "750px",
      }}
    >
      <Worker workerUrl="https://unpkg.com/pdfjs-dist@3.4.120/build/pdf.worker.min.js">
        <Viewer
          fileUrl={url}
          httpHeaders={{
            Authorization: `Bearer ${dataUser?.token}`,
          }}
          withCredentials={true}
          initialPage={content?.progress?.position ?? 0}
          onPageChange={(e)=> handleAccesssProgress(e?.currentPage ?? 0)}
        />
      </Worker>
    </Col>
  );
};
export default PDFViewer;
