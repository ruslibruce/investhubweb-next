import {
  BASE_URL,
  LOGIN_GOOGLE,
  LOGIN_IDX,
  LOGIN_RDIS,
  LOGIN_TICMIEDU,
} from "@/shared/constants/endpoint";
import { DASHBOARD_LOGIN } from "@/shared/constants/path";
import IconGoogle from "@/shared/images/logo/Google_Logo.webp";
import IconIDX from "@/shared/images/logo/IDX_Logo.webp";
import IconIDXMobile from "@/shared/images/logo/IDX_Mobile_Logo.webp";
import IconRDIS from "@/shared/images/logo/RDIS_Logo.webp";
import IconTicmi from "@/shared/images/logo/TICMI_Logo.webp";
import { LockFilled } from "@ant-design/icons";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";

type PropsForm = {
  backToLogin: boolean;
  hiddenButton: boolean;
  hiddenLineOr: boolean;
};

function FooterAuth({ backToLogin, hiddenButton, hiddenLineOr }: PropsForm) {
  const navigate = useRouter();
  const { t: translate, i18n } = useTranslation();

  const navigateToUrl = (url: string) => {
    const cleanedUrl = url.replace("/id", "");
    console.log('Navigating to:', cleanedUrl); // Debugging log
    window.location.href = (cleanedUrl);
  };

  const handleGoogleLogin = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    navigateToUrl(`${BASE_URL}${LOGIN_GOOGLE}`);
  };

  const handleRDISLogin = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    navigateToUrl(`${BASE_URL}${LOGIN_RDIS}`);
  };

  const handleIDXLogin = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    navigateToUrl(`${BASE_URL}${LOGIN_IDX}?lan=${i18n.language}`);
  };

  const handleTICMILogin = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    navigateToUrl(`${BASE_URL}${LOGIN_TICMIEDU}`);
  };

  return (
    <>
      <div className={"container-footer-login"}>
        {hiddenButton ? (
          <div className={"group-info-secure"}></div>
        ) : (
          <div className={"group-info-secure"}>
            <LockFilled className={"group-info-secure-lock"} />
            {translate("Your_Info_Footer_Auth")}
          </div>
        )}
        {hiddenLineOr ? (
          <div className={"line-or-container"}></div>
        ) : (
          <div className={"line-or-container"}>
            <div className={"line-or"}></div>
            {backToLogin ? (
              <>
                <div className={"text-backTo"}>{translate("Back_To")}</div>
                <Link className={"nav-link"} href={DASHBOARD_LOGIN}>
                  <div className={"text-linkRed-login"}>
                    {translate("Login")}
                  </div>
                </Link>
              </>
            ) : (
              <div className={"text-or"}>{translate("Or")}</div>
            )}
            <div className={"line-or"}></div>
          </div>
        )}
        {hiddenButton ? (
          <>
            <div className={"group-button-else-top"} />
            <div className={"group-button-else-bottom"} />
          </>
        ) : (
          <>
            <div className={"group-button-else-top"}>
              <button onClick={handleGoogleLogin} className={"btn-item"}>
                <Image
                  src={IconGoogle}
                  alt={"btn-item-icon"}
                  className={"btn-item-icon"}
                />
                {"Google"}
              </button>
              <button onClick={handleIDXLogin} className={"btn-item"}>
                <Image
                  src={IconIDX}
                  alt={"btn-item-icon"}
                  className={"btn-item-icon"}
                />
                {"IDX"}
              </button>
              <button className={"btn-item"}>
                <Image
                  src={IconIDXMobile}
                  alt={"btn-item-icon"}
                  className={"btn-item-icon"}
                />
                {"IDX Mobile"}
              </button>
            </div>
            <div className={"group-button-else-bottom"}>
              <button onClick={handleRDISLogin} className={"btn-item"}>
                <Image
                  src={IconRDIS}
                  alt={"btn-item-icon"}
                  className={"btn-item-icon"}
                />
                {"RDIS"}
              </button>
              <button onClick={handleTICMILogin} className={"btn-item"}>
                <Image
                  src={IconTicmi}
                  alt={"btn-item-icon"}
                  className={"btn-item-icon"}
                />
                {"TICMI"}
              </button>
            </div>
          </>
        )}
      </div>
      <div className={"line-dashed"} />
      <div className={"container-text-footer"}>
        <div className={"text-footer-top"}>{translate("Desc_Footer_Auth")}</div>
      </div>
      <div className={"container-text-footer-bottom"}>
        <div className={"text-footer-bottom"}>
          {"© 2023"}
          <div>{translate("Bei")}</div>
        </div>
      </div>
    </>
  );
}

export default FooterAuth;
