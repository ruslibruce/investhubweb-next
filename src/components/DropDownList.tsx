import useLogoutUser from "@/shared/hooks/useLogoutUser";
import Image, { StaticImageData } from "next/image";
import Link from "next/link";

type PropsDropdown = {
  listDropdown: {
    url: string;
    icon?: StaticImageData | string;
    value?: string;
    callback: boolean;
    id: number;
    answer?: {
      id: number;
      content: string;
    }[];
    excersise?: string;
  }[];
  className: string;
  handleClick: (event: any, value: any) => void;
};

function DropDownList({ listDropdown, className, handleClick }: PropsDropdown) {
  const {handleSignout} = useLogoutUser();

  const submitLogout = async () => {
    handleSignout();
  };
  return (
    <div className={`${className} container-dropdown`}>
      <ul
        className={`${
          listDropdown.length > 5 ? "list-dropdown-scrollable" : ""
        } list-dropdown`}
      >
        {listDropdown.map((item, index) => (
          <li key={index}>
            {item.url === "none" ? (
              <>
                <span>
                  {item.icon ? (
                    <Image src={item.icon} alt="icon-logout" />
                  ) : null}
                  {item.value}
                </span>
              </>
            ) : (
              <Link
                href=""
                onClick={
                  item.value === "Logout"
                    ? submitLogout
                    : item.callback
                    ? (event) => handleClick(event, item.url)
                    : () => {}
                }
              >
                {item.icon ? <Image src={item.icon} alt="icon-logout" /> : null}
                {item.value}
              </Link>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default DropDownList;
