import useGetCollection from "@/features/collection/hooks/useGetCollection";
import useAddCollectionContent from "@/features/collectiondetail/hooks/useAddCollectionContent";
import usePostReport from "@/features/course/hooks/usePostReport";
import ModalComponent from "@/shared/components/ModalComponent";
import {
  DASHBOARD_COURSE_DETAIL,
  DASHBOARD_MY_COLLECTION,
} from "@/shared/constants/path";
import { BUTTON_COLOR, USER } from "@/shared/constants/storageStatis";
import useWindowResize from "@/shared/hooks/useWindowResize";
import iconCommentCard from "@/shared/images/icon/icon_comment_card.webp";
import iconLikeCard from "@/shared/images/icon/icon_like_card.webp";
import iconMenuCard from "@/shared/images/icon/icon_menu_card.webp";
import IconReportCard from "@/shared/images/icon/icon_report_card.webp";
import iconShareCard from "@/shared/images/icon/icon_share_card.webp";
import iconStudentCard from "@/shared/images/icon/icon_student_card.webp";
import iconCalendarCard from "@/shared/images/svg/ic_red_calendar_month.svg";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { checkIsEmptyObject } from "@/shared/utils/helper";
import {
  ExclamationCircleFilled,
  PlusCircleFilled,
  SaveFilled,
} from "@ant-design/icons";
import { Button } from "@chakra-ui/react";
import {
  Button as ButtonAntd,
  Card,
  Col,
  Flex,
  Input,
  Progress,
  Row,
  Space,
  Tooltip,
  notification,
} from "antd";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import React, { useCallback } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import IconBookmark from "@/shared/images/svg/icon_bookmark.svg";
import LoaderSpinGif from "./LoaderSpinGif";
import NoData from "./NoData";

type propsCard = {
  item: any;
  isMyCourse?: boolean;
  isCarousel?: boolean;
};
export default function CardCourse({
  item,
  isMyCourse,
  isCarousel,
}: propsCard) {
  const { t: translate } = useTranslation();
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const [formReport, setFormReport] = React.useState<any>({
    report: "",
  });
  const [isModalUploadAddCollection, setIsModalUploadAddCollection] =
    React.useState<boolean>(false);
  const { handleAddCollectionContent } = useAddCollectionContent();
  const { fetchQuery: fetchCollection } = useGetCollection();
  const { data: dataCollection, isLoading: isLoadingCollection } =
    fetchCollection;
  const [isModalReport, setIsModalReport] = React.useState<boolean>(false);
  const [indexChoose, setIndexChoose] = React.useState(0);
  const [idCourseChoose, setIdCourseChoose] = React.useState();
  const [token, setToken] = React.useState<any>("");
  const captchaRef = React.useRef<any>(null);
  const { handleReportCourse, mutationQuery: mutationReport } = usePostReport();
  const { isSuccess: isSuccessReport } = mutationReport;
  const { width } = useWindowResize({ defaultWidth: 250, persentage: 0.23 });

  React.useEffect(() => {
    if (isSuccessReport) {
      setIsModalReport(false);
    }
  }, [isSuccessReport]);

  const handleCourse = (value: any) => {
    navigate.push(`${DASHBOARD_COURSE_DETAIL}/${value.id}`);
  };

  const handleAddContent = () => {
    let data = {
      collection_id: dataCollection[indexChoose].id,
      course_id: idCourseChoose,
    };
    handleAddCollectionContent(data);
  };

  const onVerify = () => {
    const tokenCaptcha = captchaRef.current.getValue();
    // alert(tokenCaptcha);
    setToken(tokenCaptcha);
  };

  const onExpire = () => {
    notification.info({
      message: "reCAPTCHA",
      description: translate("Recaptcha_Expired"),
    });
  };

  const showModalReport = (id: string) => {
    setFormReport({ report: "", course_id: id });
    setIsModalReport(true);
  };

  const handleReport = useCallback(() => {
    if (!token) {
      notification.error({
        message: "reCAPTCHA",
        description: translate("Recaptcha"),
      });
      return;
    }

    setToken("");
    handleReportCourse({
      ...formReport,
      "g-recaptcha-response": captchaRef.current.getValue(),
    });
    captchaRef.current.reset();
  }, [token]);

  return (
    <Col
      xs={isCarousel ? undefined : 24}
      sm={isCarousel ? undefined : 24}
      md={isCarousel ? undefined : 8}
      xl={isCarousel ? undefined : 6}
      style={{ marginRight: isCarousel ? 20 : 0 }}
    >
      <Card
        style={{ width: "100%", marginBottom: 20 }}
        bordered={false}
        className="card-course"
      >
        <div className="top-card-position">
          <div className="top-left-position-card">
            <Image src={iconMenuCard} alt="menu-card" />
            <div className="column-title-card">
              <span className="text-top-card">{item.conten_provider}</span>
            </div>
          </div>
          <Space className="top-calendar-card">
            <Image src={iconCalendarCard} alt="calendar-card" />
            <span className="text-calendar-card">{item.publish_date}</span>
          </Space>
        </div>
        <div
          style={{ width: "100%", height: isCarousel ? 150 : "100%" }}
          className="image-card"
        >
          <Image
            src={item.cover_image_file}
            width={300}
            height={300}
            alt="image-dummy-card"
          />
        </div>
        <div style={{ height: "16px" }} />
        <span
          style={{
            display: "-webkit-box",
            WebkitLineClamp: 1,
            WebkitBoxOrient: "vertical",
            overflow: "hidden",
            textOverflow: "ellipsis",
          }}
          className="text-title-card"
        >
          {item.title}
        </span>
        <div style={{ marginTop: 5 }} className="selection-button">
          <Button
            colorScheme={BUTTON_COLOR[0].color}
            style={{ cursor: "default" }}
            bg={BUTTON_COLOR[0].color}
            color="white"
            disabled
          >
            {item.category}
          </Button>
        </div>
        <Row
          gutter={10}
          style={{ marginTop: 10, justifyContent: "space-between" }}
        >
          <Col style={{ display: "flex", alignItems: "center", gap: 5 }}>
            <Image
              width={25}
              height={25}
              src={iconStudentCard}
              alt="student-card"
            />
            <span className="text-bottom-card">
              {item.number_of_participants}
            </span>
          </Col>
          <Col style={{ display: "flex", alignItems: "center", gap: 5 }}>
            <Image width={25} height={25} src={iconLikeCard} alt="like-card" />
            <span className="text-bottom-card">{item.number_of_likes}</span>
          </Col>
          <Col style={{ display: "flex", alignItems: "center", gap: 5 }}>
            <Image
              width={25}
              height={25}
              src={iconCommentCard}
              alt="coment-card"
            />
            <span className="text-bottom-card">{item.number_of_comments}</span>
          </Col>
          <Col style={{ display: "flex", alignItems: "center", gap: 5 }}>
            <Image
              width={25}
              height={25}
              src={IconReportCard}
              alt="coment-card"
            />
            <span className="text-bottom-card">{item.number_of_reports}</span>
          </Col>
          <Col style={{ display: "flex", alignItems: "center", gap: 5 }}>
            <Image
              width={25}
              height={25}
              src={iconShareCard}
              alt="share-card"
            />
            <span className="text-bottom-card">{item.number_of_shares}</span>
          </Col>
        </Row>
        {isMyCourse && (
          <Progress
            status="success"
            percent={Number(item?.progress)}
            percentPosition={{ align: "center", type: "inner" }}
            size={["100%", 20]}
            style={{ marginTop: 10 }}
          />
        )}
        <Flex
          justify={dataUser?.isLogin ? "space-between" : "end"}
          style={{
            marginTop: 20,
          }}
        >
          {dataUser?.isLogin && (
            <Space>
              <Tooltip title={translate("AddToCollection")}>
                <PlusCircleFilled
                  onClick={() => {
                    setIsModalUploadAddCollection(true);
                    setIdCourseChoose(item.id);
                  }}
                  style={{ cursor: "pointer", color: "#9F0E0F", fontSize: 30 }}
                />
              </Tooltip>
              <Tooltip title={translate("ReportCourse")}>
                <ExclamationCircleFilled
                  onClick={() => showModalReport(item.id)}
                  style={{ cursor: "pointer", color: "#9F0E0F", fontSize: 30 }}
                />
              </Tooltip>
            </Space>
          )}
          <ButtonAntd type="primary" onClick={() => handleCourse(item)}>
            {translate("ViewCourse")}
          </ButtonAntd>
        </Flex>
      </Card>

      {/* Modal Add Collection */}
      <ModalComponent
        width={1000}
        title={translate("TitleCollection")}
        open={isModalUploadAddCollection}
        onCancel={() => setIsModalUploadAddCollection(false)}
        footer={[
          <Flex justify="end">
            {dataCollection?.length > 0 ? (
              <ButtonAntd
                icon={<SaveFilled style={{ fontSize: 20 }} />}
                type="primary"
                onClick={() => {
                  setIsModalUploadAddCollection(false);
                  handleAddContent();
                }}
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                {translate("Save")}
              </ButtonAntd>
            ) : (
              <ButtonAntd
                icon={<SaveFilled style={{ fontSize: 20 }} />}
                type="primary"
                onClick={() => {
                  navigate.push(DASHBOARD_MY_COLLECTION);
                }}
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                {translate("NavCollection")}
              </ButtonAntd>
            )}
          </Flex>,
        ]}
      >
        <Flex
          style={{
            width: "100%",
            overflowY: "scroll",
            justifyContent: "center",
          }}
        >
          {isLoadingCollection && <LoaderSpinGif />}
          {dataCollection?.length === 0 && (
            <Col>
              <NoData />
              <span className="textfs20-fw600-black">
                {translate("CreateFolderCollection")}
              </span>
            </Col>
          )}
          {dataCollection?.length > 0 &&
            dataCollection?.map((item: any, index: any) => (
              <Space
                key={item.id}
                onClick={() => {
                  setIndexChoose(index);
                }}
                direction="horizontal"
                style={{
                  background: indexChoose === index ? "#F7ECEC" : "#FFF",
                  width: 950,
                  padding: "0px 20px",
                  gap: 20,
                  cursor: "pointer",
                }}
              >
                <Image
                  src={IconBookmark}
                  width={30}
                  height={30}
                  alt="bookmark"
                />
                <Space direction="vertical">
                  <span className="textfs20-fw600-black">{item.name}</span>
                  {/* <span className="textfs16-fw600-gray">5 Course</span> */}
                </Space>
              </Space>
            ))}
        </Flex>
      </ModalComponent>

      {/* Modal Report */}
      <ModalComponent
        width={450}
        title={translate("ReportCourse")}
        open={isModalReport}
        onCancel={() => setIsModalReport(false)}
        footer={[
          <ButtonAntd
            icon={<ExclamationCircleFilled style={{ fontSize: 20 }} />}
            type="primary"
            onClick={handleReport}
            disabled={checkIsEmptyObject(formReport)}
            style={{
              float: "right",
              alignItems: "center",
              justifyContent: "center",
              display: "flex",
            }}
          >
            {translate("Report")}
          </ButtonAntd>,
        ]}
      >
        <Space direction="vertical">
          <Space
            style={{ display: "flex", justifyContent: "space-between" }}
            wrap
          >
            <Col span={24}>
              <p className="textfs16-fw400-black">{translate("DescReport")}</p>
              <Input.TextArea
                style={{ height: 100, width: 400 }}
                value={formReport.report}
                onChange={(event) =>
                  setFormReport((prev: {}) => ({
                    ...prev,
                    report: event.target.value,
                  }))
                }
                placeholder={translate("EnterReport")}
              />
            </Col>
          </Space>
          <div style={{ height: 5 }} />
          <div className={"position-recaptcha"}>
            <ReCAPTCHA
              sitekey={process.env.NEXT_PUBLIC_SITE_KEY as string}
              onChange={onVerify}
              onExpired={onExpire}
              ref={captchaRef}
            />
          </div>
        </Space>
      </ModalComponent>
    </Col>
  );
}
