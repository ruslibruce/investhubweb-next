import { CustomFormItem } from "@/shared/components/Form/CustomForm";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import { Input } from "antd";
import { SizeType } from "antd/es/config-provider/SizeContext";
import React from "react";
import { CSSProperties } from "styled-components";

type PropsForm = {
  styleForm?: CSSProperties;
  title: string;
  type: string;
  placeholder: string;
  input?: [key: string];
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onChangeArea?: (e: React.ChangeEvent<HTMLTextAreaElement>) => void;
  name: string;
  value: string | number;
  textArea?: boolean;
  readOnly?: boolean;
  isRequired?: boolean;
};

function FormInput({
  title,
  type,
  placeholder,
  input,
  onChange,
  onChangeArea,
  name,
  value,
  textArea,
  readOnly,
  styleForm,
  isRequired = true,
}: PropsForm) {
  const [visible, setVisible] = React.useState(true);

  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  let inputElement;
  if (type === "password") {
    inputElement = (
      <Input
        type={visible ? type : "text"}
        name={name}
        value={value}
        placeholder={placeholder}
        {...input}
        onChange={onChange}
        readOnly={readOnly}
        suffix={
          <>
            {visible ? (
              <EyeTwoTone onClick={() => setVisible(false)} />
            ) : (
              <EyeInvisibleOutlined onClick={() => setVisible(!visible)} />
            )}
          </>
        }
        size={formSize}
      />
    );
  } else if (textArea) {
    inputElement = (
      <Input.TextArea
        name={name}
        value={value}
        placeholder={placeholder}
        {...input}
        onChange={onChangeArea}
        readOnly={readOnly}
        rows={4}
        size={formSize}
      />
    );
  } else {
    inputElement = (
      <Input
        type={type}
        name={name}
        value={value}
        placeholder={placeholder}
        {...input}
        onChange={onChange}
        readOnly={readOnly}
        size={formSize}
      />
    );
  }

  return (
    <CustomFormItem style={styleForm}>
      <label className={isRequired ? "required" : ""}>{title}</label>
      {inputElement}
    </CustomFormItem>
  );
}

export default FormInput;
