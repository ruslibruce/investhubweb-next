import "swiper/css";
import React from "react";
import Image from "next/image";
import ebook1 from "@/shared/images/ebook/ebook1.jpg";

// Swiper
import { Pagination } from "swiper/modules";
import SwiperCore from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

SwiperCore.use([Pagination]);

const EbookCarousel: React.FC = () => {
  return (
    <Swiper
      spaceBetween={4}
      slidesPerView={3}
      freeMode={true}
      navigation
      pagination={{ dynamicBullets: true }}
      scrollbar={{ draggable: true }}
      breakpoints={{
        460: {
          slidesPerView: 3,
        },
        0: {
          slidesPerView: 2,
        },
      }}
    >
      {[1, 2, 3, 4, 5].map((item) => (
        <SwiperSlide>
          <Image
            src={ebook1}
            alt="buku trader"
            style={{
              objectFit: "cover",
              borderRadius: "0 8px 0 8px",
              margin: "0 1rem 1rem 0",
              width: "150px",
              height: "180px",
            }}
          />
        </SwiperSlide>
      ))}
    </Swiper>
  );
};

export default EbookCarousel;
