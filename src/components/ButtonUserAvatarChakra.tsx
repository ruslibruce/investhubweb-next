import {
  DASHBOARD_MY_CERTIFICATE,
  DASHBOARD_MY_COLLECTION,
  DASHBOARD_MY_COURSE,
  DASHBOARD_MY_PROFILE,
  DASHBOARD_PREFERENCES,
} from "@/shared/constants/path";
import useLogoutUser from "@/shared/hooks/useLogoutUser";
import iconLogout from "@/shared/images/icon/icon_Logout.webp";
import iconAvatar from "@/shared/images/icon/icon_myProfile.webp";
import IconMyCertificate from "@/shared/images/icon/icon_my_certificate.webp";
import IconMyCollection from "@/shared/images/icon/icon_my_collection.webp";
import iconMyCourse from "@/shared/images/icon/icon_my_course.webp";
import IconMyPreference from "@/shared/images/icon/icon_my_preference.webp";
import { DownOutlined, UserOutlined } from "@ant-design/icons";
import {
  Button,
  Icon,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
} from "@chakra-ui/react";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";

type ButtonUserAvatarChakraProps = {
  onClick?: () => void;
};

const ButtonUserAvatarChakra: React.FC<ButtonUserAvatarChakraProps> = ({
  onClick,
}: ButtonUserAvatarChakraProps) => {
  const { t: translation } = useTranslation();
  const navigate = useRouter();
  const { handleSignout } = useLogoutUser();

  const handleMenuClick = (item: any) => {
    switch (item) {
      case "logout":
        onClick && onClick();
        handleSignout();
        return;
      case "collection":
        onClick && onClick();
        navigate.push(DASHBOARD_MY_COLLECTION);
        return;
      case "preferences":
        onClick && onClick();
        navigate.push(DASHBOARD_PREFERENCES);
        return;
      case "course":
        onClick && onClick();
        navigate.push(DASHBOARD_MY_COURSE);
        return;
      case "certificate":
        onClick && onClick();
        navigate.push(DASHBOARD_MY_CERTIFICATE);
        return;
      default:
        onClick && onClick();
        navigate.push(DASHBOARD_MY_PROFILE);
        break;
    }
  };

  return (
    <Menu>
      <MenuButton
        as={Button}
        backgroundColor={"white"}
        border={"1px"}
        borderColor={"#9F0E0F"}
        className="menu-my-account-chakra"
        _hover={{ bg: "white", color: "black", border: "0.5px solid #9F0E0F" }}
        _expanded={{
          bg: "white",
          color: "black",
          border: "0.5px solid #9F0E0F",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          {/* <Image src={IconAvatar} alt="flag-icon" className="flag-icon" /> */}
          <UserOutlined style={{ fontSize: 20, color: "#9F0E0F" }} />
          {translation("My_Account")} <Icon as={DownOutlined} />
        </div>
      </MenuButton>
      <MenuList>
        <MenuItem key="My Profile" onClick={() => handleMenuClick("profile")}>
          <Image
            width={20}
            height={20}
            src={iconAvatar}
            alt="icon-my-profile"
            className="icon-menu-item-my-account-chakra"
          />
          {translation("My_Profile")}
        </MenuItem>
        <MenuItem key="My Course" onClick={() => handleMenuClick("course")}>
          <Image
            width={20}
            height={20}
            src={iconMyCourse}
            alt="icon-my-course"
            className="icon-menu-item-my-account-chakra"
          />
          {translation("My_Course")}
        </MenuItem>
        <MenuItem
          key="preferences"
          onClick={() => handleMenuClick("preferences")}
        >
          <Image
            width={20}
            height={20}
            src={IconMyPreference}
            alt="icon-my-preferences"
            className="icon-menu-item-my-account-chakra"
          />
          {translation("Preferences")}
        </MenuItem>
        <MenuItem
          key="certificate"
          onClick={() => handleMenuClick("certificate")}
        >
          <Image
            width={20}
            height={20}
            src={IconMyCertificate}
            alt="icon-my-certificate"
            className="icon-menu-item-my-account-chakra"
          />
          {translation("MyCertificate")}
        </MenuItem>
        <MenuItem
          key="collection"
          onClick={() => handleMenuClick("collection")}
        >
          <Image
            width={20}
            height={20}
            src={IconMyCollection}
            alt="icon-my-collection"
            className="icon-menu-item-my-account-chakra"
          />
          {translation("MyCollection")}
        </MenuItem>
        <MenuItem key="logout" onClick={() => handleMenuClick("logout")}>
          <Image
            width={20}
            height={20}
            src={iconLogout}
            alt="icon-logout"
            className="icon-menu-item-my-account-chakra"
          />
          {translation("Logout")}
        </MenuItem>
      </MenuList>
    </Menu>
  );
};

export default ButtonUserAvatarChakra;
