import "swiper/css";
import React from "react";
import image1 from "@/shared/images/forum/image1.jpg";
import image2 from "@/shared/images/forum/image2.jpg";
import image3 from "@/shared/images/forum/image3.jpg";
import image4 from "@/shared/images/forum/image4.jpg";

// Swiper
import { Pagination } from "swiper/modules";
import SwiperCore from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

// Avatar ANTD
import { AntDesignOutlined, UserOutlined } from "@ant-design/icons";
import { Avatar, Divider, Tooltip } from "antd";
import Link from "next/link";
import Image from "next/image";
import { useTranslation } from "next-i18next";

SwiperCore.use([Pagination]);

const ClassPickCarousel: React.FC = () => {
  const { t: translate } = useTranslation();
  return (
    <Swiper
      spaceBetween={3}
      slidesPerView={2}
      freeMode={true}
      navigation
      breakpoints={{
        930: {
          slidesPerView: 2,
        },
        0: {
          spaceBetween: 50,
          slidesPerView: 1,
        },
      }}
    >
      {[1, 2, 3, 4, 5].map((item) => (
        <SwiperSlide>
          <div className="card-forum">
            <div className="card-forum-image">
              <Image
                width={100}
                height={100}
                src={image1.src}
                alt="Image 1"
                style={{ objectFit: "cover", borderRadius: "20px 0 0 20px" }}
              />
              <div className="card-forum-btn">
                <Link href={""}>{`${translate("EnterForum")} &gt;`}</Link>
              </div>
            </div>
            <div className="card-forum-content">
              <div className="card-forum-text">
                <h4>investing forum</h4>
                <h3>Get rich quick by investing in shares</h3>
                <p>
                  Lorem ipsum dolor sit amet consectetur. Volutpat eget vitae
                  viverra scelerisque. Massa diam proin est fusce velit enim
                  massa.
                </p>
              </div>
              <div className="card-forum-avatar">
                <Avatar.Group
                  className="avatar-group"
                  max={{
                    count: 2,
                    style: {
                      color: "#f56a00",
                      backgroundColor: "#fde3cf",
                      cursor: "pointer",
                    },
                  }}
                >
                  <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                  <Avatar style={{ backgroundColor: "#f56a00" }}>K</Avatar>
                  <Tooltip title="Ant User" placement="top">
                    <Avatar
                      style={{ backgroundColor: "#87d068" }}
                      icon={<UserOutlined />}
                    />
                  </Tooltip>
                  <Avatar
                    style={{ backgroundColor: "#1677ff" }}
                    icon={<AntDesignOutlined />}
                  />
                </Avatar.Group>
              </div>
            </div>
          </div>
        </SwiperSlide>
      ))}
    </Swiper>
  );
};

export default ClassPickCarousel;
