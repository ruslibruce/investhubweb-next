import { UpOutlined } from "@ant-design/icons";
import { FloatButton } from "antd";
import { useTranslation } from "next-i18next";
import React from "react";

type FloatScrollToTopProps = {
  isVisibleScroll?: boolean;
};

export default function FloatScrollToTop() {
  const {t: translate} = useTranslation();
  const [isVisibleScroll, setIsVisibleScroll] = React.useState(false);

  React.useEffect(() => {
    const toggleVisibility = () => {
      // if the user scrolls down, show the button
      window.scrollY > 500
        ? setIsVisibleScroll(true)
        : setIsVisibleScroll(false);
    };
    // listen for scroll events
    window.addEventListener("scroll", toggleVisibility);

    // clear the listener on component unmount
    return () => {
      window.removeEventListener("scroll", toggleVisibility);
    };
  }, []);
  const scrollToTop = () => {
    isVisibleScroll &&
      window.scrollTo({
        top: 0,
        behavior: "auto",
      });
  };

  if (!isVisibleScroll) return null;

  return (
    <FloatButton
      onClick={scrollToTop}
      tooltip={translate("ScrollToTop")}
      icon={<UpOutlined style={{ color: "#9F0E0F" }} />}
      type="default"
      style={{ right: 75, bottom: 25, border: "1px solid #9F0E0F" }}
    />
  );
}
