import React, { useState } from "react";
import dayjs, { Dayjs } from "dayjs";
import "dayjs/locale/id"; // Import lokalisasi bahasa Indonesia
import { Calendar, Button, Row, Col, Typography, theme } from "antd";
import type { CalendarProps } from "antd";
import { useTranslation } from "next-i18next";

dayjs.locale("id"); // Set lokalisasi dayjs menjadi "id"

const App: React.FC = () => {
  const { token } = theme.useToken();
  const [currentValue, setCurrentValue] = useState<Dayjs>(dayjs());
  const {t: translate} = useTranslation();

  const handleBackForward = (change: number) => {
    const newDate = currentValue.clone().add(change, "month");
    setCurrentValue(newDate);
  };

  return (
    <div
      style={{
        width: 300,
        border: `1px solid ${token.colorBorderSecondary}`,
        borderRadius: token.borderRadiusLG,
      }}
    >
      <Calendar
        fullscreen={false}
        value={currentValue}
        headerRender={({ type, onTypeChange }) => (
          <div style={{ padding: 8 }}>
            <Typography.Title level={4}>{translate("Calendar")}</Typography.Title>
            <Row
              style={{ justifyContent: "space-between", alignItems: "center" }}
              gutter={8}
            >
              <Col>
                <span
                  style={{
                    borderRadius: 8,
                    display: "block",
                    justifyItems: "center",
                    alignItems: "center",
                    background: "#9F0E0F",
                    padding: "8px 16px",
                  }}
                  className="textfs14-fw600-white"
                >
                  {dayjs().month(currentValue.month()).format("MMM YYYY")}
                </span>
              </Col>
              <Col>
                <Button size="small" onClick={() => handleBackForward(-1)}>
                  &lt;
                </Button>
                <Button size="small" onClick={() => handleBackForward(1)}>
                  &gt;
                </Button>
              </Col>
            </Row>
          </div>
        )}
        onPanelChange={(value, mode) =>
          console.log(value.format("YYYY-MM-DD"), mode)
        }
        onSelect={(date) => setCurrentValue(date)}
      />
    </div>
  );
};

export default App;
