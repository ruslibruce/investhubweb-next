import { CustomFormItem } from "@/shared/components/Form/CustomForm";
import { DownOutlined } from "@ant-design/icons";
import { MenuProps } from "antd";
import { Button, Dropdown } from "antd";
import { MenuInfo } from "rc-menu/lib/interface";
import { CSSProperties } from "styled-components";

type PropsForm = {
  styleForm?: CSSProperties;
  title?: string;
  placeholder: string;
  input?: [key: string];
  onClick?: (e: MenuInfo) => void;
  //   onClick?: (e: React.MouseEvent<HTMLInputElement>) => void;
  value: string;
  data: {
    key: string;
    label: string;
  }[];
  buttonStyle?: string;
  isRequired?: boolean;
};

export default function FormDropdown({
  styleForm,
  title,
  placeholder,
  onClick,
  value,
  data,
  buttonStyle,
  isRequired = true,
}: PropsForm) {
  const items: MenuProps["items"] = data;

  return (
    <CustomFormItem style={styleForm}>
      <label className={isRequired ? "required" : ""}>{title}</label>
      <Dropdown
        menu={{ items, onClick }}
        placement="bottom"
        arrow={{ pointAtCenter: true }}
        trigger={["click"]}
      >
        <Button className={`${buttonStyle} form-button__button__label`}>
          {value ? value : placeholder}
          <DownOutlined />
        </Button>
      </Dropdown>
    </CustomFormItem>
  );
}
