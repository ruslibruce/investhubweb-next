import React from "react";
import { useTranslation } from "next-i18next";
import { CSSProperties } from "styled-components";

type FormRadio = {
  styleForm?: CSSProperties;
  title: string;
  input?: [key: string];
  checked: string;
  handleChangeRadio: (event: any) => void;
};

function FormRadioInput({
  styleForm,
  title,
  input,
  checked,
  handleChangeRadio,
}: FormRadio) {
  const { t: translate } = useTranslation();
  return (
    <div style={styleForm} className={"form-input"}>
      <label className="form-input__label">{title}</label>
      <div className="form-input__radio">
        <label className="form-input__radio__text">
          <input
            type="radio"
            value={"L"}
            checked={checked === "L"}
            onChange={handleChangeRadio}
            {...input}
          />
          {translate("Male")}
        </label>
        <label className="form-input__radio__text">
          <input
            type="radio"
            value={"F"}
            checked={checked === "F"}
            onChange={handleChangeRadio}
            {...input}
          />
          {translate("Female")}
        </label>
      </div>
    </div>
  );
}

export default FormRadioInput;
