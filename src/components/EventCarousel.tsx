import { DASHBOARD_EVENT_DETAIL } from "@/shared/constants/path";
import content2 from "@/shared/images/news/content2.jpg";
import IconDot from "@/shared/images/svg/icon_dot.svg";
import { RightOutlined } from "@ant-design/icons";
import { Flex } from "antd";
import moment from "moment";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import SwiperCore from "swiper";
import "swiper/css";
import { Pagination } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import LoaderSpinGif from "./LoaderSpinGif";

SwiperCore.use([Pagination]);

type EventCarouselProps = {
  dataEvent: {}[];
  isLoading: boolean;
};

const EventCarousel = ({ dataEvent, isLoading }: EventCarouselProps) => {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const handleEvent = (event: React.MouseEvent<HTMLElement>, value: any) => {
    navigate.push(`${DASHBOARD_EVENT_DETAIL}/${value.id}`);
  };
  if (!dataEvent) return <LoaderSpinGif size="large" />;
  if (dataEvent?.length === 0) return null;
  return (
    <Swiper
      slidesPerView={1}
      freeMode={true}
      pagination={{ dynamicBullets: true }}
      scrollbar={{ draggable: true }}
      navigation
    >
      {dataEvent?.length > 0 &&
        dataEvent?.map((eventItem: any, index) => (
          <SwiperSlide key={eventItem.id}>
            <div className="news-container-content-carousel">
              <div className="news-head-content">
                <Image
                  src={eventItem.cover_image}
                  alt="image-news"
                  width={30}
                  height={30}
                  style={{
                    objectFit: "cover",
                    borderRadius: "100px",
                  }}
                />
                <Image src={IconDot} alt="icon-dot" style={{ marginLeft: 5 }} />
                <h4 style={{ marginLeft: 5 }}>
                  {moment(eventItem.start_time).format("DD MMM YYYY")}
                </h4>
                <Image src={IconDot} alt="icon-dot" />
              </div>
              <div className="news-main-content">
                <div className="news-main-title">
                  <Image
                    src={content2}
                    alt="Hotel"
                    width={60}
                    height={60}
                    style={{
                      objectFit: "cover",
                      borderRadius: "10px",
                    }}
                  />
                  <h3 dangerouslySetInnerHTML={{ __html: eventItem?.title }} />
                </div>
                <div
                  className="news-main-text"
                  dangerouslySetInnerHTML={{ __html: eventItem?.description }}
                />
              </div>
              <Flex
                onClick={(e) => handleEvent(e, eventItem)}
                style={{ cursor: "pointer" }}
                justify="end"
                className="news-footer-content"
              >
                <h4 className="news-see-detail">
                  {`${translate("SeeDetail")}`}
                  <RightOutlined style={{ color: "#9F0E0F" }} />
                </h4>
              </Flex>
            </div>
          </SwiperSlide>
        ))}
    </Swiper>
  );
};

export default EventCarousel;
