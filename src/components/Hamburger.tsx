import { HamburgerIcon } from "@chakra-ui/icons";
import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Button,
  Drawer,
  DrawerBody,
  DrawerContent,
  IconButton,
} from "@chakra-ui/react";
import React, { useState } from "react";

// Import gambar yang diperlukan

// Import komponen dan hook yang dibutuhkan
import ButtonUserAvatarChakra from "@/components/ButtonUserAvatarChakra";
import {
  DASHBOARD_ABOUT,
  DASHBOARD_CLASS,
  DASHBOARD_CLASS_DETAIL,
  DASHBOARD_COURSE,
  DASHBOARD_COURSE_DETAIL,
  DASHBOARD_EVENT,
  DASHBOARD_FORUM,
  DASHBOARD_HOME,
  DASHBOARD_INVESTMENT,
  DASHBOARD_LOGIN,
  DASHBOARD_NEWS,
  DASHBOARD_REGISTER,
  DASHBOARD_STOCK_SCREENER,
} from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { useDisclosure } from "@chakra-ui/react";
import { useTranslation } from "next-i18next";
import Link from "next/link";
import { useRouter } from "next/router";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";
import useGetUrlWebIDXStockScreener from "@/shared/components/layout/dashboard-layout/hooks/useGetUrlWebIDXStockScreener";
import LoaderSpinGif from "./LoaderSpinGif";
import RadioButton from "@/components/RadioLanguage";
import { Row } from "antd";
dayjs.extend(utc);
dayjs.extend(timezone);

function Hamburger() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const navigate = useRouter();
  const { asPath } = navigate;
  const { t: translation, i18n } = useTranslation();
  const dataUser = storageCheck(USER);
  const [isSmallScreen, setIsSmallScreen] = useState(false);
  const { handleGetUrlIDXStockScreener, mutationQuery } =
    useGetUrlWebIDXStockScreener();
  const { isPending } = mutationQuery;

  React.useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth <= 450);
    };

    // Panggil handleResize pertama kali untuk mengatur isSmallScreen
    handleResize();

    // Tambahkan event listener untuk memantau perubahan ukuran layar
    window.addEventListener("resize", handleResize);

    // Clean up event listener saat komponen dibongkar
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []); // Dependensi kosong agar efek hanya berjalan sekali saat komponen dipasang

  const buttonStyle = {
    width: "50px", // Lebar default
    height: "50px", // Tinggi default
  };

  const smallScreenButtonStyle = {
    width: "39px", // Lebar pada layar kecil
    height: "39px", // Tinggi pada layar kecil
  };

  return (
    <>
      <Button
        as={IconButton}
        aria-label="Options"
        icon={<HamburgerIcon w="25px" h="25px" color="#999" />}
        onClick={onOpen}
        style={{
          ...buttonStyle,
          ...(isSmallScreen && smallScreenButtonStyle),
        }}
      />
      <Drawer placement="top" onClose={onClose} isOpen={isOpen}>
        <DrawerContent mt="65px">
          <DrawerBody overflowY="auto">
            <div className="hamburger-menu">
              <div className="hamburger-time">{`${dayjs()
                .tz("Asia/Jakarta")
                .format("DD MMMM YYYY")} | ${dayjs()
                .tz("Asia/Jakarta")
                .format("HH:mm")} WIB`}</div>
              <Row
                style={{
                  justifyContent: dataUser?.isLogin
                    ? "space-between"
                    : "center",
                  alignItems: "center",
                  marginTop: 15,
                  gap: 10,
                }}
              >
                {dataUser?.isLogin ? (
                  <ButtonUserAvatarChakra onClick={onClose} />
                ) : (
                  <div className="hamburger-btn">
                    <Link
                      style={{ color: "#9f0e0f" }}
                      className="hamburger-btn-login"
                      href={DASHBOARD_LOGIN}
                      onClick={onClose}
                    >
                      {translation("Login")}
                    </Link>
                    <Link
                      style={{ color: "white" }}
                      className="hamburger-btn-register"
                      href={DASHBOARD_REGISTER}
                      onClick={onClose}
                    >
                      {translation("Register")}
                    </Link>
                  </div>
                )}
                {window.innerWidth < 450 && (
                  <RadioButton classNameView="radioHamburger" />
                )}
              </Row>
              <ul className="hamburger-menu-list">
                <li
                  className={
                    asPath === DASHBOARD_HOME
                      ? "hamburger-menu-list-active"
                      : ""
                  }
                >
                  <Link onClick={onClose} href={DASHBOARD_HOME}>
                    {translation("Home")}
                  </Link>
                </li>
                <li className="accordion-hamburger-list">
                  <Accordion allowToggle>
                    <AccordionItem borderWidth={0}>
                      <AccordionButton
                        px={0}
                        _hover={{ bg: "none" }}
                        py={4}
                        fontSize={15}
                        color={
                          asPath === DASHBOARD_COURSE ||
                          asPath === DASHBOARD_COURSE_DETAIL
                            ? "#9f0e0fe5"
                            : "#666"
                        }
                      >
                        <Box as="span" flex="1" textAlign="left">
                          {translation("Course_Tab")}
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>

                      <AccordionPanel pb={4} px={0}>
                        <Link href={DASHBOARD_COURSE}>
                          <Button
                            colorScheme="gray"
                            variant="solid"
                            mr={5}
                            mb={2}
                            size="md"
                            color={
                              asPath === DASHBOARD_COURSE ||
                              asPath === DASHBOARD_COURSE_DETAIL
                                ? "#9f0e0fe5"
                                : "#666"
                            }
                            onClick={onClose}
                          >
                            {translation("Course_Tab")}
                          </Button>
                        </Link>
                        <Button
                          colorScheme="gray"
                          variant="solid"
                          mr={5}
                          mb={2}
                          size="md"
                          color="#666"
                          onClick={onClose}
                        >
                          {translation("Ebook")}
                        </Button>
                        <Button
                          colorScheme="gray"
                          variant="solid"
                          mr={0}
                          mb={2}
                          size="md"
                          color="#666"
                          onClick={onClose}
                        >
                          {translation("Glossarium")}
                        </Button>
                      </AccordionPanel>
                    </AccordionItem>
                  </Accordion>
                </li>
                <li
                  className={
                    asPath === DASHBOARD_CLASS ||
                    asPath === DASHBOARD_CLASS_DETAIL
                      ? "hamburger-menu-list-active"
                      : ""
                  }
                >
                  {/* <Link onClick={onClose} href={DASHBOARD_CLASS}>
                    {translation("Class")}
                  </Link> */}
                  <Link onClick={onClose} href={"#"}>
                    {translation("Class")}
                  </Link>
                </li>
                <li
                  className={
                    asPath === DASHBOARD_EVENT
                      ? "hamburger-menu-list-active"
                      : ""
                  }
                >
                  <Link onClick={onClose} href={DASHBOARD_EVENT}>
                    {translation("Event_Tab")}
                  </Link>
                </li>
                <li
                  className={
                    asPath === DASHBOARD_NEWS
                      ? "hamburger-menu-list-active"
                      : ""
                  }
                >
                  <Link onClick={onClose} href={DASHBOARD_NEWS}>
                    {translation("News_Tab")}
                  </Link>
                </li>
                <li className="accordion-hamburger-list">
                  <Accordion allowToggle>
                    <AccordionItem borderWidth={0}>
                      <AccordionButton
                        px={0}
                        _hover={{ bg: "none" }}
                        py={4}
                        fontSize={15}
                        color={
                          asPath === DASHBOARD_FORUM ? "#9f0e0fe5" : "#666"
                        }
                      >
                        <Box as="span" flex="1" textAlign="left">
                          {translation("Forum_Tab")}
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>

                      <AccordionPanel pb={4} px={0}>
                        {/* <Link href={DASHBOARD_FORUM}> */}
                        <Link href={"#"}>
                          <Button
                            colorScheme="gray"
                            variant="solid"
                            mr={5}
                            mb={2}
                            size="md"
                            color={
                              asPath === DASHBOARD_FORUM ? "#9f0e0fe5" : "#666"
                            }
                            onClick={onClose}
                          >
                            {translation("Forum_Tab")}
                          </Button>
                        </Link>
                        <Button
                          colorScheme="gray"
                          variant="solid"
                          mr={0}
                          mb={2}
                          size="md"
                          color="#666"
                          onClick={onClose}
                        >
                          {translation("My_Forum")}
                        </Button>
                      </AccordionPanel>
                    </AccordionItem>
                  </Accordion>
                </li>
                <li
                  className={
                    asPath === DASHBOARD_STOCK_SCREENER
                      ? "hamburger-menu-list-active"
                      : ""
                  }
                >
                  <Link
                    href={"#"}
                    onClick={() =>
                      handleGetUrlIDXStockScreener({
                        tokenInvesthub: dataUser?.token,
                      })
                    }
                  >
                    {isPending ? <LoaderSpinGif /> : translation("Stock")}
                  </Link>
                </li>
                <li
                  className={
                    asPath === DASHBOARD_INVESTMENT
                      ? "hamburger-menu-list-active"
                      : ""
                  }
                >
                  <Link onClick={onClose} href={DASHBOARD_INVESTMENT}>
                    {translation("Investment")}
                  </Link>
                </li>
                {/* {dataUser?.isLogin && (
                  <li className="accordion-hamburger-list">
                    <Accordion allowToggle>
                      <AccordionItem borderWidth={0}>
                        <AccordionButton
                          px={0}
                          _hover={{ bg: "none" }}
                          py={4}
                          fontSize={15}
                          color={
                            asPath === DASHBOARD_LEVEL_UP
                              ? "#9f0e0fe5"
                              : "#666"
                          }
                        >
                          <Box as="span" flex="1" textAlign="left">
                            {translation("Learning")}
                          </Box>
                          <AccordionIcon />
                        </AccordionButton>

                        <AccordionPanel pb={4} px={0}>
                          <Link href={DASHBOARD_LEVEL_UP}>
                            <Button
                              colorScheme="gray"
                              variant="solid"
                              mr={10}
                              size="lg"
                              color={
                                asPath === DASHBOARD_LEVEL_UP
                                  ? "#9f0e0fe5"
                                  : "#666"
                              }
                              onClick={onClose}
                            >
                              {translation("Level_Up")}
                            </Button>
                          </Link>
                        </AccordionPanel>
                      </AccordionItem>
                    </Accordion>
                  </li>
                )} */}
                <li>
                  <Link onClick={onClose} href={DASHBOARD_ABOUT}>
                    {translation("About")}
                  </Link>
                </li>
              </ul>
            </div>
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
}

export default Hamburger;
