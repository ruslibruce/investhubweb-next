import useGetSearchAll from "@/shared/components/layout/dashboard-layout/hooks/useGetSearchAll";
import PaginationComponent from "@/shared/components/PaginationComponent";
import {
  DASHBOARD_COURSE_DETAIL,
  DASHBOARD_EVENT_DETAIL,
  DASHBOARD_NEWS_DETAIL,
} from "@/shared/constants/path";
import { paramsToString } from "@/shared/utils/string";
import { CloseIcon, SearchIcon } from "@chakra-ui/icons";
import {
  Button,
  Drawer,
  DrawerBody,
  DrawerContent,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  useDisclosure,
} from "@chakra-ui/react";
import { Col, PaginationProps, Row, Space } from "antd";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import React from "react";
import styled from "styled-components";

const StyledDiv = styled((props: any) => <Space {...props} />)`
  && div:hover {
    background: #f2f2f2;
  }
`;

function ResponsiveSearch() {
  const navigate = useRouter();
  const { t: translate } = useTranslation();
  const [currentPage, setCurrentPage] = React.useState<number>(1);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const placement = "top";
  const [search, setSearch] = React.useState<string>("");
  const [dataSearch, setDataSearch] = React.useState<any>([]);
  const { handleSearchAll, mutationQuery: mutationQuerySearchAll } =
    useGetSearchAll();
  const { data: dataSearchAll } = mutationQuerySearchAll;
  const dataResultSearch = dataSearchAll as any;

  React.useEffect(() => {
    if (dataSearchAll && search) {
      setDataSearch(dataResultSearch.data);
    }
  }, [dataSearchAll]);

  React.useEffect(() => {
    if (search.length > 2 && currentPage) {
      handleSearchAll(paramsToString({ search, page: currentPage }));
    } else {
      setDataSearch([]);
    }
  }, [search, currentPage]);

  const onSearch = (event: any) => {
    setSearch(event.target.value);
  };

  const handleNavigateFromSearch = (item: any) => {
    setSearch("");
    setDataSearch([]);
    onClose();
    switch (item.source) {
      case "investments":
        window.open(item.url, "_blank");
        break;
      case "news":
        if (item.source_id) {
          window.open(item.link, "_blank");
          return;
        }
        navigate.push(`${DASHBOARD_NEWS_DETAIL}/${item.id}`);
        break;

      case "events":
        navigate.push(`${DASHBOARD_EVENT_DETAIL}/${item.id}`);
        break;

      case "course":
        navigate.push(`${DASHBOARD_COURSE_DETAIL}/${item.id}`);
        break;

      default:
        break;
    }
  };

  const onChange: PaginationProps["onChange"] = (page) => {
    setCurrentPage(page);
  };

  return (
    <>
      <SearchIcon
        cursor="pointer"
        w="25px"
        h="25px"
        color="#999"
        aria-label="Options"
        onClick={onOpen}
        mr="15px"
      />
      <Drawer placement={placement} onClose={onClose} isOpen={isOpen}>
        <DrawerContent>
          <DrawerBody>
            <div className="responsive-search-container">
              <div className="search-component-container">
                <div className="search-responsive-component">
                  <InputGroup
                    border={"solid #999"}
                    borderRadius={5}
                    overflow={"hidden"}
                  >
                    <InputLeftElement pointerEvents="none" pl="3px">
                      <SearchIcon color="#999" />
                    </InputLeftElement>
                    <Input
                      placeholder={translate("Search")}
                      onChange={onSearch}
                      value={search}
                      size="large"
                      _focus={{
                        boxShadow: "none",
                        outline: "none",
                      }}
                    />
                    {search.length > 0 && (
                      <InputRightElement>
                        <CloseIcon onClick={() => setSearch("")} color="#999" />
                      </InputRightElement>
                    )}
                  </InputGroup>
                </div>
                <div className="search-responsive-close">
                  <Button
                    color={"#fff"}
                    background={"#9f0e0fe5"}
                    onClick={onClose}
                  >
                    {translate("Cancel")}
                  </Button>
                </div>
              </div>
              {dataSearch.length > 0 && (
                <Row
                  style={{
                    backgroundColor: "white",
                    width: "100%",
                    display: "block",
                    overflowY: "scroll",
                    padding: 5,
                    position: "absolute",
                    top: 60,
                    zIndex: 999,
                  }}
                  className="navbar-shadow"
                >
                  <Col
                    style={{
                      backgroundColor: "white",
                      maxHeight: 300,
                      display: "block",
                      overflowY: "scroll",
                      padding: 5,
                    }}
                    span={24}
                  >
                    {dataSearch.map((item: any, index: any) => {
                      return (
                        <StyledDiv
                          onClick={() => handleNavigateFromSearch(item)}
                          key={item.id}
                        >
                          <div
                            style={{
                              flexDirection: "column",
                              display: "flex",
                              padding: 5,
                            }}
                          >
                            {item.title && (
                              <span
                                style={{
                                  display: "-webkit-box",
                                  WebkitLineClamp: 1,
                                  WebkitBoxOrient: "vertical",
                                  overflow: "hidden",
                                  textOverflow: "ellipsis",
                                  height: 20,
                                }}
                                className="textfs14-fw600-black"
                                dangerouslySetInnerHTML={{ __html: item.title }}
                              />
                            )}
                            {item.subject && (
                              <span
                                style={{
                                  display: "-webkit-box",
                                  WebkitLineClamp: 1,
                                  WebkitBoxOrient: "vertical",
                                  overflow: "hidden",
                                  textOverflow: "ellipsis",
                                  height: 20,
                                }}
                                className="textfs14-fw600-black"
                                dangerouslySetInnerHTML={{
                                  __html: item.subject,
                                }}
                              />
                            )}
                            {item.name && (
                              <span
                                style={{
                                  display: "-webkit-box",
                                  WebkitLineClamp: 1,
                                  WebkitBoxOrient: "vertical",
                                  overflow: "hidden",
                                  textOverflow: "ellipsis",
                                  height: 20,
                                }}
                                className="textfs14-fw600-black"
                                dangerouslySetInnerHTML={{ __html: item.name }}
                              />
                            )}
                            {item.description && (
                              <span
                                style={{
                                  display: "-webkit-box",
                                  WebkitLineClamp: 1,
                                  WebkitBoxOrient: "vertical",
                                  overflow: "hidden",
                                  textOverflow: "ellipsis",
                                  height: 20,
                                }}
                                className="textfs14-fw400-gray truncate"
                                dangerouslySetInnerHTML={{
                                  __html: item.description,
                                }}
                              />
                            )}
                            {item.body && (
                              <span
                                style={{
                                  display: "-webkit-box",
                                  WebkitLineClamp: 1,
                                  WebkitBoxOrient: "vertical",
                                  overflow: "hidden",
                                  textOverflow: "ellipsis",
                                  height: 20,
                                }}
                                className="textfs14-fw400-gray truncate"
                                dangerouslySetInnerHTML={{
                                  __html: item.body,
                                }}
                              />
                            )}
                            {item.url && (
                              <span
                                style={{
                                  display: "-webkit-box",
                                  WebkitLineClamp: 1,
                                  WebkitBoxOrient: "vertical",
                                  overflow: "hidden",
                                  textOverflow: "ellipsis",
                                  height: 20,
                                }}
                                className="textfs14-fw400-gray truncate"
                                dangerouslySetInnerHTML={{
                                  __html: item.url,
                                }}
                              />
                            )}
                          </div>
                        </StyledDiv>
                      );
                    })}
                  </Col>
                  <Col
                    span={24}
                    style={{
                      backgroundColor: "white",
                      height: 30,
                      display: "block",
                    }}
                  >
                    <PaginationComponent
                      currentPage={currentPage}
                      setCurrentPage={setCurrentPage}
                      total={dataResultSearch?.records}
                      limit={dataResultSearch?.limit}
                    />
                  </Col>
                </Row>
              )}
            </div>
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
}

export default ResponsiveSearch;
