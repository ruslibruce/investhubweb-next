import React from "react";
import "swiper/css";
// Swiper
import SwiperCore from "swiper";
import { Pagination } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";

import useGetLearningJourney from "@/features/homescreen/hooks/useGetLearningJourney";
import { DASHBOARD_DETAIL_MY_LEARNING } from "@/shared/constants/path";
import { SketchOutlined } from "@ant-design/icons";
import { Box, CircularProgress, CircularProgressLabel } from "@chakra-ui/react";
import { Progress } from "antd";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import LoaderSpinGif from "./LoaderSpinGif";
import NoData from "./NoData";

SwiperCore.use([Pagination]);

const LearningJourneyCarousel: React.FC = () => {
  const navigate = useRouter();
  const { t: translate } = useTranslation();
  const { fetchGetLearnJourney } = useGetLearningJourney({});
  const { data: dataResultLearnJourney, isLoading: isLoadingJourney } =
    fetchGetLearnJourney;
  const { data: dataLearnJourney } = dataResultLearnJourney;

  const handleMyLearningJourney = (item: any) => {
    navigate.push(`${DASHBOARD_DETAIL_MY_LEARNING}/${item.id}/${item.name}`);
  };

  return (
    <Swiper
      slidesPerView={4}
      freeMode={true}
      navigation
      pagination={{ dynamicBullets: true }}
      scrollbar={{ draggable: true }}
      breakpoints={{
        460: {
          slidesPerView: 4,
        },
        0: {
          slidesPerView: 3,
        },
      }}
    >
      {!dataLearnJourney && (
        <SwiperSlide>
          <div className="learning-journey-card-carousel">
            <LoaderSpinGif size="large" />
          </div>
        </SwiperSlide>
      )}
      {dataLearnJourney?.length === 0 && <NoData />}
      {dataLearnJourney?.length > 0 &&
        dataLearnJourney?.map((item: any, index: any) => {
          const completedCount =
            item.requirements.length > 0
              ? item.requirements.filter(
                  (val: any) => val.progress && val.progress.is_completed
                ).length
              : 0;

          const valueCircular =
            completedCount === 0
              ? 0
              : (completedCount / item.requirements.length) * 100;
          return (
            <SwiperSlide key={index}>
              <div
                style={{
                  // width: 800,
                  cursor: "pointer",
                }}
                onClick={() => handleMyLearningJourney(item)}
                className="learning-journey-card-carousel"
              >
                <CircularProgress
                  value={valueCircular}
                  color="green.400"
                  className="journey-progress-carousel"
                >
                  <CircularProgressLabel>
                    <Box
                      display="flex"
                      alignItems="center"
                      justifyContent="center"
                    >
                      <SketchOutlined style={{ fontSize: 20, color: "#9F0E0F" }}/>
                    </Box>
                  </CircularProgressLabel>
                </CircularProgress>
                <h3
                  style={{
                    display: "-webkit-box",
                    WebkitLineClamp: 2,
                    WebkitBoxOrient: "vertical",
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    height: 40,
                    textAlign: "center",
                  }}
                  className="textfs12-fw400-black"
                >
                  {item.name}
                </h3>
                <Progress
                  status="success"
                  percent={
                    item.requirements.length > 0
                      ? (completedCount / item.requirements.length) * 100
                      : 0
                  }
                  showInfo={false}
                />
                <span className="textfs10-fw400-black">
                  {item.achieved_at
                    ? translate("Completed")
                    : `${completedCount} of ${
                        item.requirements.length
                      } ${translate("Completed")}`}
                </span>
              </div>
            </SwiperSlide>
          );
        })}
    </Swiper>
  );
};

export default LearningJourneyCarousel;
