import { ArrowRightOutlined } from "@ant-design/icons";
import { Button } from "antd";
import React from "react";

type PropsForm = {
  classButton?: string;
  title: string;
  type: "submit" | "reset" | "button" | undefined;
  input?: [key: string];
  onClick?: (e: any) => void;
  hiddenIcon?: boolean;
  disabled?: boolean;
  isLoading?: boolean;
  styleButton?: React.CSSProperties;
  icon?: React.ReactNode;
  position?: "start" | "end";
};

function FormButton({
  classButton,
  title,
  type,
  input,
  hiddenIcon,
  onClick,
  disabled,
  isLoading,
  styleButton,
  icon,
  position = "end",
}: PropsForm) {
  return (
    <Button
      onClick={onClick}
      disabled={disabled}
      type="primary"
      htmlType={type}
      {...input}
      className={`${classButton} form-button__button`}
      loading={isLoading}
      style={styleButton}
      iconPosition={position}
      icon={
        hiddenIcon ? null : icon ? (
          icon
        ) : (
          <ArrowRightOutlined />
        )
      }
    >
      {title}
    </Button>
  );
}

export default FormButton;
