import image4 from "@/shared/images/forum/image4.jpg";
import Image from "next/image";
import React from "react";
import SwiperCore from "swiper";
import "swiper/css";
import { Pagination } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";

SwiperCore.use([Pagination]);

const SwiperComponent: React.FC = () => {
  return (
    <Swiper
      spaceBetween={1}
      slidesPerView={4}
      freeMode={true}
      pagination={{ dynamicBullets: true }}
      scrollbar={{ draggable: true }}
      navigation
      breakpoints={{
        1200: {
          slidesPerView: 4,
        },
        850: {
          slidesPerView: 3,
        },
        0: {
          slidesPerView: 1,
        },
      }}
    >
      {[1, 2, 3, 4, 5].map((item) => (
        <SwiperSlide>
          <div className="card-promo-image">
            <Image
              width={100}
              height={100}
              src={image4.src}
              alt="Image 1"
              style={{ objectFit: "cover", borderRadius: "15px" }}
            />
          </div>
        </SwiperSlide>
      ))}
    </Swiper>
  );
};

export default SwiperComponent;
