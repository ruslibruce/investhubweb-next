import "swiper/css";
import React from "react";

// Swiper
import { Pagination } from "swiper/modules";
import SwiperCore from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

//ANTD
import { AntDesignOutlined, UserOutlined } from "@ant-design/icons";
import { Avatar, Col, Divider, Row, Spin, Tooltip } from "antd";
import { Flex, Progress } from "antd";
import Image from "next/image";
import iconCalendarCard from "@/shared/images/svg/ic_red_calendar_month.svg";
import image4 from "@/shared/images/forum/image4.jpg";
import { Button, ButtonGroup } from "@chakra-ui/react";
import iconCommentCard from "@/shared/images/icon/icon_comment_card.webp";
import iconCupCard from "@/shared/images/icon/icon_cup_card.webp";
import iconLikeCard from "@/shared/images/icon/icon_like_card.webp";
import iconMenuCard from "@/shared/images/icon/icon_menu_card.webp";
import iconShareCard from "@/shared/images/icon/icon_share_card.webp";
import iconStudentCard from "@/shared/images/icon/icon_student_card.webp";
import NoData from "./NoData";
import { DASHBOARD_COURSE_DETAIL } from "@/shared/constants/path";
import { useRouter } from "next/router";
import useGetCourseRecomendation from "@/features/homescreen/hooks/useGetCourseRecomendation";
import PaginationComponent from "@/shared/components/PaginationComponent";
import { paramsToString } from "@/shared/utils/string";
import { useTranslation } from "next-i18next";
import IconReportCard from "@/shared/images/icon/icon_report_card.webp";
import CardCourse from "./CardCourse";
import LoaderSpinGif from "./LoaderSpinGif";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { USER } from "@/shared/constants/storageStatis";

SwiperCore.use([Pagination]);

const ButtonColors = [
  {
    id: 1,
    color: "#9F0E0F",
  },
  {
    id: 2,
    color: "#0097C7",
  },
  {
    id: 3,
    color: "#D0C802",
  },
];

const SwiperComponent: React.FC = () => {
  const { t: translate } = useTranslation();
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const [currentPage, setCurrentPage] = React.useState(1);
  const { fetchGetCourseRecomendations } = useGetCourseRecomendation(
    {},
    paramsToString({ page: currentPage })
  );
  const { isLoading, data: dataResultCourse } = fetchGetCourseRecomendations;
  const { data: dataCourse } = dataResultCourse;
  console.log("dataCourse :", JSON.stringify(dataCourse, null, 2));

  const handleCourse = (event: React.MouseEvent<HTMLElement>, value: any) => {
    navigate.push(`${DASHBOARD_COURSE_DETAIL}/${value.id}`);
  };

  if (dataCourse?.length === 0) {
    return null;
  }

  return (
    <>
      <div className="learning-progress">
        <div className="learning-progress-title">
          <h2>{translate("Course_For_You")}</h2>
          {/* <Link href={DASHBOARD_COURSE}>
          <h3>{translate("ViewAll")}</h3>
        </Link> */}
        </div>
        {!dataCourse && dataUser.isLogin ? (
          <div className="learning-progress-carousel">
            <LoaderSpinGif size="large" />
          </div>
        ) : (
          <div className="learning-progress-carousel">
            {dataCourse?.length > 0 && <div className="background-content" />}
            <div className="learning-progress-carousel-content">
              <Swiper
                slidesPerView={3}
                navigation
                breakpoints={{
                  1200: {
                    slidesPerView: 4,
                  },
                  1000: {
                    slidesPerView: 3,
                  },
                  700: {
                    slidesPerView: 2,
                  },
                  500: {
                    slidesPerView: 1,
                  },
                  0: {
                    pagination: { dynamicBullets: true },
                    scrollbar: { draggable: true },
                    slidesPerView: 1,
                  },
                }}
              >
                {dataCourse?.length > 0 &&
                  dataCourse?.map((item: any) => (
                    <SwiperSlide key={item.id}>
                      <CardCourse isCarousel={true} item={item} />
                    </SwiperSlide>
                  ))}
              </Swiper>
            </div>
          </div>
        )}
        <PaginationComponent
          stylePagination={{ marginBottom: 0 }}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          total={dataResultCourse?.records}
          limit={dataResultCourse?.limit}
        />
      </div>
    </>
  );
};

export default SwiperComponent;
