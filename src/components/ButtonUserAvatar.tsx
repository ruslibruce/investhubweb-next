import { DownOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Dropdown } from "antd";
import { useTranslation } from "next-i18next";
import Image from "next/image";

type PropsButtonAvatar = {
  className: string;
  user?: string;
  setMyProfile: (event: any) => void;
  myProfile: boolean;
  menuProps: {};
};

function ButtonUserAvatar({
  className,
  user,
  setMyProfile,
  myProfile,
  menuProps,
}: PropsButtonAvatar) {
  const { t: translation } = useTranslation();
  return (
    <Dropdown
      menu={menuProps}
      placement="bottom"
      arrow
      className={`${className} nav-button-avatar`}
    >
      <Button>
        {user ? (
          <Image
            src={user}
            alt="flag-icon"
            className="flag-icon"
            width={100}
            height={100}
            style={{
              borderRadius: 25,
              width: 25,
              height: 25,
              borderWidth: 1,
              borderColor: "#9F0E0F",
              borderStyle: "solid",
              padding: 1,
            }}
          />
        ) : (
          <UserOutlined style={{ fontSize: 20, color: "#9F0E0F" }} />
        )}
        <span>{translation("My_Account")}</span>
        <DownOutlined className="ic-flag" />
      </Button>
    </Dropdown>
  );
}

export default ButtonUserAvatar;
