import React from "react";
import { useMediaQuery } from "react-responsive";
import styled from "styled-components";

type PropsText = {
  children: React.ReactNode;
  desktop: number;
  bigScreen?: number;
  mobile: number;
  retina?: number;
  color: string;
  fontWeight: string | number;
  isSpan?: boolean;
  numberOfLines?: number;
};

const StyledSpan = styled.span<{
  $fontSize: number;
  $color: string;
  $fontWeight: string | number;
  $numberOfLines?: number;
}>`
  fontsize: ${(props) => props.$fontSize};
  color: ${(props) => props.$color};
  fontWeight: ${(props) => props.$fontWeight};
  text-align: justify;

  &.isNumberOfLine {
    display: -webkit-box;
    display: box; /* Add this for compatibility */
    -webkit-line-clamp: ${(props) => props.$numberOfLines};
    line-clamp: 2; /* Add this for compatibility */
    -webkit-box-orient: vertical;
    box-orient: vertical; /* Add this for compatibility */
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

const StyledText = styled.p<{
  $fontSize: number;
  $color: string;
  $fontWeight: string | number;
  $numberOfLines?: number;
}>`
  fontsize: ${(props) => props.$fontSize};
  color: ${(props) => props.$color};
  fontWeight: ${(props) => props.$fontWeight};
  text-align: justify;

  &.isNumberOfLine {
    display: -webkit-box;
    display: box; /* Add this for compatibility */
    -webkit-line-clamp: ${(props) => props.$numberOfLines};
    line-clamp: 2; /* Add this for compatibility */
    -webkit-box-orient: vertical;
    box-orient: vertical; /* Add this for compatibility */
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;
export default function TextResponsive({
  children,
  desktop,
  bigScreen,
  mobile,
  retina,
  color,
  fontWeight,
  isSpan,
  numberOfLines,
}: PropsText) {
  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 1224px)",
  });
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1224px)" });
  const isBigScreen = useMediaQuery({ query: "(min-width: 1824px)" });
  const isPortrait = useMediaQuery({ query: "(orientation: portrait)" });
  const isRetina = useMediaQuery({ query: "(min-resolution: 2dppx)" });
  //   console.log('isRetina', isRetina);
  //   console.log('isPortrait', isPortrait);
  //   console.log('isTabletOrMobile', isTabletOrMobile);
  //   console.log('isDesktopOrLaptop', isDesktopOrLaptop);
  //   console.log('isBigScreen', isBigScreen);

  return (
    <div>
      {isDesktopOrLaptop && (
        <>
          {isSpan ? (
            <StyledSpan
              $fontSize={desktop}
              $color={color}
              $fontWeight={fontWeight}
              $numberOfLines={numberOfLines || 2}
              className={numberOfLines ? "isNumberOfLine" : ""}
            >
              {children}
            </StyledSpan>
          ) : (
            <StyledText
              $fontSize={desktop}
              $color={color}
              $fontWeight={fontWeight}
              $numberOfLines={numberOfLines || 2}
              className={numberOfLines ? "isNumberOfLine" : ""}
            >
              {children}
            </StyledText>
          )}
        </>
      )}
      {isTabletOrMobile && (
        <>
          {isSpan ? (
            <StyledSpan
              $fontSize={mobile}
              $color={color}
              $fontWeight={fontWeight}
              $numberOfLines={numberOfLines || 2}
              className={numberOfLines ? "isNumberOfLine" : ""}
              style={{
                fontSize: mobile,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </StyledSpan>
          ) : (
            <StyledText
              $fontSize={mobile}
              $color={color}
              $fontWeight={fontWeight}
              $numberOfLines={numberOfLines || 2}
              className={numberOfLines ? "isNumberOfLine" : ""}
              style={{
                fontSize: mobile,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </StyledText>
          )}
        </>
      )}
      {/* {isBigScreen && (
        <>
          {isSpan ? (
            <span
              style={{
                fontSize: bigScreen,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </span>
          ) : (
            <p
              style={{
                fontSize: bigScreen,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </p>
          )}
        </>
      )}
      {isRetina && (
        <>
          {isSpan ? (
            <span
              style={{
                fontSize: retina,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </span>
          ) : (
            <p
              style={{
                fontSize: retina,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </p>
          )}
        </>
      )} */}
    </div>
  );
}
