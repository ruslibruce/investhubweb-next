"use client";
import { USER } from "@/shared/constants/storageStatis";
import { COLORS } from "@/shared/styles/color";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  DownOutlined,
  FileTextFilled,
  MutedOutlined,
  PauseCircleOutlined,
  PlayCircleOutlined,
  SettingFilled,
  SoundOutlined,
} from "@ant-design/icons";
import {
  Button,
  Col,
  Menu,
  Popover,
  Row,
  Select,
  Slider,
  Space,
  Spin,
} from "antd";
import { useRouter } from "next/router";
import React, { lazy, Suspense, useCallback, useRef, useState } from "react";
import { BaseReactPlayerProps } from "react-player/base";
import TextResponsive from "./TextResponsive";
import { useTranslation } from "next-i18next";
import usePostProgress from "@/features/coursedetail/hooks/usePostProgress";
import { MenuProps } from "antd/lib";
import LoaderSpinGif from "./LoaderSpinGif";
import Image from "next/image";

type VideoProps = {
  url: string;
  className?: string;
  width?: number | string;
  height?: number | string;
  content?: any;
  idProgress?: string;
};

type SubtitleProps = {
  key: string;
  label: string;
  theme: string;
}[];

const Player = ({
  className,
  width,
  height,
  url,
  content,
  idProgress,
}: VideoProps) => {
  const { t: translate } = useTranslation();
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const videoRef = useRef<BaseReactPlayerProps>(null);
  const [Component, setComponent] =
    useState<React.ComponentType<BaseReactPlayerProps> | null>(null);
  const [currentTime, setCurrentTime] = useState(0);
  const [duration, setDuration] = useState(0);
  const [showControls, setShowControls] = useState(false);
  const [isPlaying, setIsPlaying] = useState(true);
  const [isMuted, setIsMuted] = useState(false);
  const [progressWidth, setProgressWidth] = useState("100%");
  const { handlePostProgress } = usePostProgress();
  const [isVideoError, setIsVideoError] = useState(false);
  const [isReady, setIsReady] = useState(false);
  const [playbackText, setPlaybackText] = useState("1x");
  const [quality, setQuality] = useState([]);
  const [subtitleItems, setSubtitleItems] = useState<SubtitleProps>([]);
  console.log("subtitleItems", subtitleItems);
  const [urlVideo, setUrlVideo] = useState("");
  const [selectedKeysQuality, setSelectedKeysQuality] = useState("");
  const [selectedKeysSubtitle, setSelectedKeysSubtitle] = useState("off");
  const [selectedKeysPlayback, setSelectedKeysPlayback] = useState("1");
  const [subtitles, setSubtitles] = useState([]);
  const tracks = videoRef.current?.getInternalPlayer()?.textTracks;
  // console.log("tracks", tracks);
  console.log("content", content);

  React.useEffect(() => {
    if (tracks && selectedKeysSubtitle) {
      handleGetTracks(tracks, selectedKeysSubtitle);
    }
  }, [tracks, selectedKeysSubtitle]);

  const handleGetTracks = (tracks: any, selectedKeysSubtitle: string) => {
    Object.keys(tracks).forEach((key) => {
      if (tracks[key].language === selectedKeysSubtitle) {
        tracks[key].mode = "showing";
      } else {
        tracks[key].mode = "disabled";
      }
    });
  };

  React.useEffect(() => {
    setUrlVideo(url);
    if (content) {
      let subtitleItems = [
        {
          key: "off",
          label: "OFF",
          theme: "dark",
        },
      ];
      if (content.content_files) {
        let resultParse =
          content.content_files && JSON.parse(content.content_files);
        let resultMap = resultParse.map((item: any) => ({
          ...item,
          label: item.resolution,
          key: item.filename,
          theme: "dark",
        }));
        setSelectedKeysQuality(resultMap[0].key);
        setQuality(resultMap);
      }
      if (content.subtitles && content.subtitles.length > 0) {
        let resultSubs = content.subtitles.map((item: any) => ({
          key: item.language,
          label: item.language,
          theme: "dark",
        }));
        subtitleItems.push(resultSubs[0]);
        let resultMap = content.subtitles.map((item: any) => ({
          ...item,
          kind: "subtitles",
          src: item.filename,
          srcLang: item.language,
          default: false,
          key: item.language,
          label: item.language,
          theme: "dark",
        }));
        setSubtitles(resultMap);
        setSubtitleItems(subtitleItems);
      }
    }
  }, [content]);

  React.useEffect(() => {
    setComponent(() => lazy(() => import("react-player/lazy")));
  }, []);

  React.useEffect(() => {
    // Function to handle the event
    const handleBack = (state: any) => {
      // Perform your logic here, for example:
      handleAccesssProgress(currentTime);
      // Return true to allow the pop state action to continue, false to prevent it
      return true;
    };

    // Add the event listener
    navigate.beforePopState(handleBack);

    // Cleanup function to remove the event listener when the component unmounts
    return () => {
      navigate.beforePopState(() => true);
    };
  }, [navigate, currentTime]);

  React.useEffect(() => {
    const handleBlur = () => {
      // Add your logic here for when the component loses focus
      handleAccesssProgress(currentTime);
    };

    window.addEventListener("blur", handleBlur);

    return () => {
      window.removeEventListener("blur", handleBlur);
    };
  }, [currentTime]);

  React.useEffect(() => {
    const intervalId = setInterval(() => {
      handleAccesssProgress(currentTime);
    }, 60000); // 60000 milliseconds = 1 minute

    // Cleanup interval on component unmount
    return () => clearInterval(intervalId);
  }, [currentTime]); // Empty dependency array ensures this runs once when the component mounts

  const handleAccesssProgress = useCallback(
    (currentTime: number) => {
      if (!dataUser.isLogin) {
        return;
      }

      if (!idProgress && !content) {
        return;
      }

      if(!content?.progress) {
        return;
      }

      handlePostProgress({
        participant_id: idProgress,
        content_id: content?.id,
        position: Math.ceil(currentTime),
      });
    },
    [idProgress, content, dataUser, handlePostProgress]
  );

  React.useEffect(() => {
    const handleResize = () => {
      // Example logic for responsiveness
      const screenWidth = window.innerWidth;
      if (screenWidth > 768) {
        setProgressWidth("100%");
      } else {
        setProgressWidth("50%"); // Adjust based on your requirements
      }
    };

    // Set initial width
    handleResize();

    // Add event listener
    window.addEventListener("resize", handleResize);

    // Cleanup
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const onProgress = (state: any) => {
    setCurrentTime(state.playedSeconds);
  };

  const onDuration = (duration: number) => {
    setDuration(duration);
    if (content && content?.progress?.position > 0) {
      videoRef.current?.seekTo(content?.progress?.position);
      setCurrentTime(content?.progress?.position);
      return;
    }
    setCurrentTime(0);
  };

  const getMinutesFromSeconds = (time: number) => {
    const minutes = time >= 60 ? Math.ceil(time / 60) : 0;
    const seconds = Math.ceil(time - minutes * 60);

    return `${minutes >= 10 ? minutes : "0" + minutes} : ${
      seconds >= 10 ? seconds : "0" + seconds
    }`;
  };

  const position = getMinutesFromSeconds(currentTime);
  const fullDuration = getMinutesFromSeconds(duration);

  const getPercentage = (currentTime: number, duration: number): number => {
    if (duration > 0) {
      return (currentTime / duration) * 100;
    }
    return 0; // Return 0% if duration is not positive
  };

  const onSeek = (value: number) => {
    const duration = videoRef.current?.getDuration();
    if (duration) {
      const targetTime = (value / 100) * duration;
      if (content && content?.progress?.is_completed) {
        videoRef.current?.seekTo(targetTime, "seconds");
        setCurrentTime(targetTime);
        return;
      }
      if (currentTime > targetTime) {
        videoRef.current?.seekTo(targetTime, "seconds");
        setCurrentTime(targetTime);
      }
    }
  };

  const onEnded = (currentTime: number) => {
    handleAccesssProgress(currentTime);
  };

  const onClick: MenuProps["onClick"] = (e) => {
    console.log(e);
    if (e.keyPath[1] === "quality") {
      setUrlVideo(`${content.base_url}${e.key}`);
      setSelectedKeysQuality(e.key);
    }

    if (e.keyPath[1] === "playback") {
      setPlaybackText(`${e.key}x`);
      setSelectedKeysPlayback(e.key);
    }

    if (e.keyPath[1] === "subtitle") {
      setSelectedKeysSubtitle(e.key);
    }
  };

  const onClickSubtile: MenuProps["onClick"] = (e) => {
    console.log(e);
    setSelectedKeysSubtitle(e.key);
  };

  const onClickPlayback: MenuProps["onClick"] = (e) => {
    console.log(e);
    setPlaybackText(`${e.key}x`);
    setSelectedKeysPlayback(e.key);
  };

  const onClickQuality: MenuProps["onClick"] = (e) => {
    console.log(e);
    setUrlVideo(`${content.base_url}${e.key}`);
    setSelectedKeysQuality(e.key);
  };

  type MenuItem = Required<MenuProps>["items"][number];

  const itemsPlayback: MenuItem[] = [
    {
      key: 1,
      label: "1x",
      theme: "dark",
    },
    {
      key: 1.5,
      label: "1.5x",
      theme: "dark",
    },
    {
      key: 2,
      label: "2x",
      theme: "dark",
    },
  ];

  const itemsQuality: MenuItem[] = quality;

  const itemsSubtitle: MenuItem[] = subtitleItems;

  const itemsSettings: MenuItem[] = [
    {
      key: "playback",
      label: translate("PlaybackRate"),
      theme: "dark",
      children: [
        { key: 1, label: "1x" },
        { key: 1.5, label: "1.5x" },
        { key: 2, label: "2x" },
      ],
    },
    {
      key: "quality",
      label: "Quality",
      theme: "dark",
      children: quality,
    },
    {
      key: "subtitle",
      label: "Subtitle",
      theme: "dark",
      children: [
        { key: "en", label: "English" },
        { key: "id", label: "Indonesia" },
        { key: "off", label: "OFF" },
      ],
    },
  ];

  const contentSubtitle = (
    <Menu
      onClick={onClickSubtile}
      style={{ width: 256 }}
      // openKeys={["sub1"]}
      selectedKeys={[selectedKeysSubtitle]}
      mode="vertical"
      theme="dark"
      items={itemsSubtitle}
      getPopupContainer={(node) => node.parentNode as HTMLElement}
    />
  );

  const contentQuality = (
    <Menu
      onClick={onClickQuality}
      style={{ width: 256 }}
      // openKeys={["sub1"]}
      selectedKeys={[selectedKeysQuality]}
      mode="vertical"
      theme="dark"
      items={itemsQuality}
      getPopupContainer={(node) => node.parentNode as HTMLElement}
    />
  );

  const contentPlayback = (
    <Menu
      onClick={onClickPlayback}
      style={{ width: 256 }}
      selectedKeys={[selectedKeysPlayback]}
      mode="vertical"
      theme="dark"
      items={itemsPlayback}
      getPopupContainer={(node) => node.parentNode as HTMLElement}
    />
  );

  const contentSetting = (
    <Menu
      onClick={onClick}
      style={{ width: 256 }}
      mode="vertical"
      theme="dark"
      items={itemsSettings}
      getPopupContainer={(node) => node.parentNode as HTMLElement}
    />
  );

  if (!Component) {
    return <LoaderSpinGif size="large" />;
  }

  return (
    <Col
      span={24}
      style={{ width: width, height: height }}
      onMouseEnter={() => setShowControls(true)}
      onMouseLeave={() => setShowControls(false)}
      className={`player-wrapper`}
    >
      {/* <Video src={url} /> */}
      <Suspense>
        <Component
          className="react-player"
          ref={videoRef}
          playing={isPlaying && isReady}
          muted={isMuted}
          onProgress={onProgress}
          onDuration={onDuration}
          onEnded={() => onEnded(currentTime)}
          controls={false}
          onReady={() => setIsReady(true)}
          // light is usefull incase of dark mode
          light={
            <Image
              src={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/logo_investhub.webp`}
              alt="image-video"
              width={500}
              height={500}
            />
          }
          // picture in picture
          pip={true}
          url={urlVideo}
          playsinline
          width="100%"
          height="100%"
          onError={() => setIsVideoError(true)}
          playbackRate={Number(selectedKeysPlayback)}
          config={{
            file: {
              tracks: subtitles,
              attributes: {
                crossOrigin: "true",
                contenttype: "text/plain",
              },
            },
          }}
        />
      </Suspense>
      {showControls && !isVideoError && (
        <>
          {isReady && (
            <Row
              style={{
                position: "absolute",
                left: 0,
                top: 0,
                bottom: 0,
                width: "100%",
                background: "rgba(0, 0, 0, 0.5)",
                color: "white",
                padding: "5px 10px",
                alignItems: "end",
              }}
              justify="start"
            >
              <Col
                span={24}
                style={{
                  display: "flex",
                  marginTop: "10%",
                }}
              >
                {!isPlaying ? (
                  <PlayCircleOutlined
                    onClick={() => setIsPlaying(true)}
                    style={{ fontSize: 50, margin: "0 auto" }}
                  />
                ) : (
                  <PauseCircleOutlined
                    onClick={() => setIsPlaying(false)}
                    style={{ fontSize: 50, margin: "0 auto" }}
                  />
                )}
              </Col>
              <Col span={24}>
                <Slider
                  style={{ width: progressWidth }}
                  min={0}
                  max={100}
                  value={getPercentage(currentTime, duration)}
                  onChange={onSeek}
                />
                <Row gutter={20}>
                  <span className="textfs14-fw600-white">{`${position} / ${fullDuration}`}</span>
                  {isMuted ? (
                    <MutedOutlined
                      style={{ marginLeft: 10 }}
                      onClick={() => setIsMuted(false)}
                    />
                  ) : (
                    <SoundOutlined
                      style={{ marginLeft: 10 }}
                      onClick={() => setIsMuted(true)}
                    />
                  )}
                  <Popover
                    align={{ offset: [0, 10] }}
                    content={contentPlayback}
                    color="#001529"
                    style={{ color: COLORS.WHITE }}
                  >
                    <Space
                      style={{
                        cursor: "pointer",
                        background: "rgba(0, 0, 0, 0.5)",
                        padding: 10,
                        borderRadius: 10,
                        color: COLORS.WHITE,
                        marginLeft: 10,
                      }}
                    >
                      {playbackText}
                    </Space>
                  </Popover>
                  {quality.length > 0 && (
                    <Popover
                      align={{ offset: [0, 10] }}
                      content={contentQuality}
                      color="#001529"
                    >
                      <SettingFilled
                        style={{
                          cursor: "pointer",
                          marginLeft: 10,
                        }}
                      />
                    </Popover>
                  )}
                  {subtitles.length > 0 && (
                    <Popover
                      align={{ offset: [0, 10] }}
                      content={contentSubtitle}
                      color="#001529"
                    >
                      <FileTextFilled
                        style={{
                          cursor: "pointer",
                          marginLeft: 10,
                        }}
                      />
                    </Popover>
                  )}
                  {/* <Popover
                    align={{ offset: [0, 10] }}
                    content={contentSetting}
                    color="#001529"
                  >
                    <SettingFilled
                      style={{
                        cursor: "pointer",
                        background: "rgba(0, 0, 0, 0.5)",
                        marginLeft: 10,
                      }}
                    />
                  </Popover> */}
                </Row>
              </Col>
            </Row>
          )}
        </>
      )}
      {isVideoError && (
        <Row
          style={{
            position: "absolute",
            bottom: 0,
            top: 0,
            width: "100%",
            background: "rgba(0, 0, 0, 0.5)",
            color: "white",
            padding: "5px 10px",
            alignItems: "center",
          }}
        >
          <Space
            direction="vertical"
            size="middle"
            style={{ margin: "0 auto" }}
          >
            <h1 className="textfs16-fw600-white">{translate("ErrVideo")}</h1>
            <Button
              type="primary"
              onClick={() => {
                setIsVideoError(false);
                videoRef.current?.seekTo(0);
                setIsPlaying(false);
              }}
            >
              {translate("Retry")}
            </Button>
          </Space>
        </Row>
      )}
    </Col>
  );
};

export default Player;
