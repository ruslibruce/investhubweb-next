import React from "react";
import { Menu, Dropdown } from "antd";
import { DownOutlined } from "@ant-design/icons";
import Link from "next/link";

const LearningMenu = () => {
  // Isi menu untuk dropdown
  const menuItems = Array.from({ length: 9 }, (_, i) => (
    <Menu.Item key={`item-${i + 1}`}>
      <Link href={`#menu-${i + 1}`}>{`Menu ${i + 1}`}</Link>
    </Menu.Item>
  ));

  // Menentukan jumlah kolom
  const columns = 3;

  // Membuat grid dari menuItems
  const menuGrid = Array.from(
    { length: Math.ceil(menuItems.length / columns) },
    (_, rowIndex) => (
      <Menu.ItemGroup
        key={`group-${rowIndex + 1}`}
        title={`Row ${rowIndex + 1}`}
      >
        {menuItems.slice(rowIndex * columns, (rowIndex + 1) * columns)}
      </Menu.ItemGroup>
    )
  );

  // Menyusun grid ke dalam menu
  const dropdownMenu = <Menu>{menuGrid}</Menu>;

  return (
    <Dropdown overlay={dropdownMenu} placement="bottomLeft">
      <Link
        href={""}
        className="ant-dropdown-link"
        onClick={(e) => e.preventDefault()}
      >
        Learning <DownOutlined />
      </Link>
    </Dropdown>
  );
};

export default LearningMenu;
